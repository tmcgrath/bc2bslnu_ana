# $B_c^+\to B_s^0 \mu^+\nu_\mu$ Search Analysis Code

To do list of unfinished business in the analysis located in this [CodiMD page](https://codimd.web.cern.ch/s/jk_jtzlVs#).

## Python Environment

All the python scripts used in this analysis can be re-run with the conda environment saved in the `.yml` file: [`MyPythonEnv.yml`](./MyPythonEnv.yml)
The environment can be recreated by:
```bash
conda env create -f MyPythonEnv.yml
```

The only exception is the `RooFit` fitting code in [`Fit`](./Fit) whose environment `yml` file is [`NewROOTEnv.yml`](./Fit/NewROOTEnv.yml). The main difference is the `ROOT` version: `v6.30.4`.

## File Processing

The data and MC stored on the grid for the data, signal and background MC, is processed in a series of steps that starts with ntupling with DaVinci with ganga and ends with the BDT application before being used for the fit. The files for each step are stored in `eos` in the general location `/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana` (check location I have left LHCb). All directories mentioned as file input/ouput locations of code run on lxplus are with respect to this directory on `eos`. These may all have to be changed if someone else is running them.  

### Pipeline

This section is an ordered list of the data/MC processing pipeline with code locations and input/output file locations mentioned for each step. A description of how to run the code for each step is outlined in the linked directory.

**Data:**
1. [Ntupling with DaVinci](./DaVinciOptions/README.md#Ntupling-with-DaVinci) 
    * Code location: [`DaVinciOptions`](./DaVinciOptions)
5. [Offline cuts after stripping selection](./Preselection/README.md#Offline-Selection)
    * Code location: [`Preselection`](./Preselection)
    * Input location: Files on the grid
    * Output location: `DataReduced`
3. [BDT application](./BDT/README.md#BDT-Application)
    * Code location: [`BDT`](./BDT)
    * Input location: `DataReduced`
    * Output location: `DataBDTApplied`

**MC:**
1. [Ntupling with DaVinci](./DaVinciOptions/README.md#Ntupling-with-DaVinci) 
    * Code location: [`DaVinciOptions`](./DaVinciOptions)
2. [PIDGen Application](./PID/README.md#PIDGen-Application)
    * Code location: [`PIDGen`](./PID)
    * Input/Output location: Within `MC`
    * Each $B_c^+$ decay channel has its own directory and subdirectories for each year
3. [Merging DaVinci and PIDGen files](./Preselection/README.md#Merging-DaVinci-and-PIDGen-Files)
    * Code location: [`Preselection/merge_files.py`](./Preselection/merge_files.py)
    * Input/Output location: Within `MC`
    * Each $B_c^+$ decay channel has its own directory and subdirectories for each year
5. [Offline cuts after stripping selection](./Preselection/README.md#Offline-Selection)
    * Code location: [`Preselection`](./Preselection)
    * Input location: `MC`
    * Output location: `MCReduced` 
6. [BDT application](./BDT/README.md#BDT-Application)
    * Code location: [`BDT`](./BDT)
    * Input location: `MCReduced`
    * Output location: `MCBDTApplied`

## Stripping Filter 

Stripping filters for each $B_s^0$ mode and data-taking year are located in [`Stripping/Filter`](./Stripping/Filter/). These were used to filter the ReDecay background MC that was requested centrally via the SLWG. Some shell scripts containing commands to test the filter scrpts on some background channel MC file are located in [`TestBkg`](./Stripping/Filter/TestBkg/).

## Fitting

Maximum likelihood fits can be done on files that have had the BDT applied. The fitting code is located in [`Fit`](./Fit).

## Efficiencies

The script in [`Efficiencies`](./Efficiencies/) calculates all the efficiencies except the acceptance efficiency and summarises them in text files in [`Effs`](./Efficiencies/Effs/).
