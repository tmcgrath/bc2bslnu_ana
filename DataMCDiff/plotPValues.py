#
# plotPValues.py
#
# Code for plotting the p-values comparing data and MC
#

import math, sys
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
small_size = 12
medium_size = 14
rc('font',**{'family':'serif','serif':['Roman'], 'size': medium_size})
rc('lines',**{'markeredgewidth': 1})

#####################################
# Functions
#####################################

def getInfoFromFile(filename):

    particles, variables, p_values = [], [], []

    with open(filename, 'r') as f:
        lines = f.readlines()

        for line in lines:
            line.strip("\n")
            lineSplit = line.split(" ")

            if lineSplit[0] == "For":
                particle = str( (lineSplit[1]).split("_")[0] )
                var = str( (lineSplit[1]).split("_")[1] )
                p_value = float(lineSplit[-1])

                particles.append(particle)
                variables.append(var)
                p_values.append(p_value)

    #print(particles)
    #print(variables)
    #print(p_values)

    return particles, variables, p_values    

#####################################
# Main
#####################################

chans = ("DsPi", "JpsiPhi")
file = "./Tests/chisq_test_{}.txt"

plot_items ={ # (label, colour)
    "PT": {"label": r"$p_T$", "color": "lightsteelblue"},
    "P": {"label": r"$p$", "color": "slateblue"},
    "ETA": {"label": r"$\eta$", "color": "seagreen"},
    "Bach": {"label": r"$\pi^{+}$", "color": ""},
    "Bs": {"label": r"$B_{s}^{0}$", "color": ""},
    "Bc": {"label": r"$B_{c}^{+}$", "color": ""}
}

for chan in chans:
    particles, variables, p_values = getInfoFromFile(file.format(chan))

    # Gather info
    solo_particles = list( dict.fromkeys(particles) )
    solo_variables = list( dict.fromkeys(variables) )
    particle_ticks = (plot_items[part]["label"] for part in solo_particles) # particle labels to draw
    to_plot = {}
    for n, var in enumerate(solo_variables):
        values = []
        for i in range(0, 3):
            the_value = p_values[i*3 + n]
            values.append(the_value)
        to_plot[var] = values
    print(to_plot)

    # PLOT!!
    # Plot info
    fig, ax = plt.subplots(layout="constrained")
    x_for_ticks = np.arange(len(solo_particles))
    width = 0.25  # the width of the bars
    multiplier = 0

    for v, p in to_plot.items():
        offset = width * multiplier
        p_plots = ax.bar(x_for_ticks + offset, p, width, label=plot_items[v]["label"], color=plot_items[v]["color"])
        multiplier += 1

    # Windowdressing
    line = ax.axhline(y = 0.05, linestyle='dashed', color = 'k')
    ax.set_ylabel("p-values")
    ax.set_ylim([0.0, 1.2])
    ax.set_xticks(x_for_ticks + width, particle_ticks)
    ax.legend(loc='upper right')
    plt.savefig(f"./Figs/all_pvalues_{chan}.png")
    plt.savefig(f"./Figs/all_pvalues_{chan}.pdf")

    # For testing
    #plt.show()
    #sys.exit()
