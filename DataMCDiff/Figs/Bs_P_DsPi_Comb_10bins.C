void Bs_P_DsPi_Comb_10bins()
{
//=========Macro generated from canvas: c1_n10/c1_n10
//=========  (Thu Nov  2 17:31:56 2023) by ROOT version 6.24/06
   TCanvas *c1_n10 = new TCanvas("c1_n10", "c1_n10",0,0,700,500);
   gStyle->SetOptStat(0);
   c1_n10->SetHighLightColor(2);
   c1_n10->Range(-82894.74,-0.1316456,509210.5,0.6911392);
   c1_n10->SetFillColor(0);
   c1_n10->SetBorderMode(0);
   c1_n10->SetBorderSize(2);
   c1_n10->SetTickx(1);
   c1_n10->SetTicky(1);
   c1_n10->SetLeftMargin(0.14);
   c1_n10->SetTopMargin(0.05);
   c1_n10->SetBottomMargin(0.16);
   c1_n10->SetFrameLineWidth(2);
   c1_n10->SetFrameBorderMode(0);
   c1_n10->SetFrameLineWidth(2);
   c1_n10->SetFrameBorderMode(0);
   
   TH1D *Bs_P_MC_signal__30 = new TH1D("Bs_P_MC_signal__30","",10,0,450000);
   Bs_P_MC_signal__30->SetBinContent(1,0.0228385);
   Bs_P_MC_signal__30->SetBinContent(2,0.3800979);
   Bs_P_MC_signal__30->SetBinContent(3,0.3442088);
   Bs_P_MC_signal__30->SetBinContent(4,0.182708);
   Bs_P_MC_signal__30->SetBinContent(5,0.045677);
   Bs_P_MC_signal__30->SetBinContent(6,0.009787928);
   Bs_P_MC_signal__30->SetBinContent(7,0.009787928);
   Bs_P_MC_signal__30->SetBinContent(9,0.004893964);
   Bs_P_MC_signal__30->SetBinError(1,0.006103846);
   Bs_P_MC_signal__30->SetBinError(2,0.02490104);
   Bs_P_MC_signal__30->SetBinError(3,0.02369631);
   Bs_P_MC_signal__30->SetBinError(4,0.01726428);
   Bs_P_MC_signal__30->SetBinError(5,0.008632141);
   Bs_P_MC_signal__30->SetBinError(6,0.003995905);
   Bs_P_MC_signal__30->SetBinError(7,0.003995905);
   Bs_P_MC_signal__30->SetBinError(9,0.002825531);
   Bs_P_MC_signal__30->SetMinimum(0);
   Bs_P_MC_signal__30->SetMaximum(0.65);
   Bs_P_MC_signal__30->SetEntries(613);
   Bs_P_MC_signal__30->SetDirectory(0);
   Bs_P_MC_signal__30->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = 1209;
   color = new TColor(ci, 0.6, 0.8, 0.2, " ", 0.4);
   Bs_P_MC_signal__30->SetFillColor(ci);

   ci = 1208;
   color = new TColor(ci, 0.6, 0.8, 0.2, " ", 0.4);
   Bs_P_MC_signal__30->SetLineColor(ci);
   Bs_P_MC_signal__30->SetLineWidth(2);
   Bs_P_MC_signal__30->SetMarkerStyle(20);
   Bs_P_MC_signal__30->GetXaxis()->SetTitle("B_{s}^{0} p [MeV/c]");
   Bs_P_MC_signal__30->GetXaxis()->SetNdivisions(505);
   Bs_P_MC_signal__30->GetXaxis()->SetLabelFont(132);
   Bs_P_MC_signal__30->GetXaxis()->SetLabelOffset(0.01);
   Bs_P_MC_signal__30->GetXaxis()->SetLabelSize(0.06);
   Bs_P_MC_signal__30->GetXaxis()->SetTitleSize(0.072);
   Bs_P_MC_signal__30->GetXaxis()->SetTitleOffset(0.95);
   Bs_P_MC_signal__30->GetXaxis()->SetTitleFont(132);
   Bs_P_MC_signal__30->GetYaxis()->SetTitle("AU");
   Bs_P_MC_signal__30->GetYaxis()->SetLabelFont(132);
   Bs_P_MC_signal__30->GetYaxis()->SetLabelOffset(0.01);
   Bs_P_MC_signal__30->GetYaxis()->SetLabelSize(0.06);
   Bs_P_MC_signal__30->GetYaxis()->SetTitleSize(0.072);
   Bs_P_MC_signal__30->GetYaxis()->SetTitleOffset(0.95);
   Bs_P_MC_signal__30->GetYaxis()->SetTitleFont(132);
   Bs_P_MC_signal__30->GetZaxis()->SetLabelFont(132);
   Bs_P_MC_signal__30->GetZaxis()->SetLabelSize(0.06);
   Bs_P_MC_signal__30->GetZaxis()->SetTitleSize(0.072);
   Bs_P_MC_signal__30->GetZaxis()->SetTitleOffset(1.2);
   Bs_P_MC_signal__30->GetZaxis()->SetTitleFont(132);
   Bs_P_MC_signal__30->Draw("HIST");
   
   TH1D *Bs_P_data__31 = new TH1D("Bs_P_data__31","",10,0,450000);
   Bs_P_data__31->SetBinContent(1,0.0255102);
   Bs_P_data__31->SetBinContent(2,0.3061224);
   Bs_P_data__31->SetBinContent(3,0.3010204);
   Bs_P_data__31->SetBinContent(4,0.2040816);
   Bs_P_data__31->SetBinContent(5,0.08163265);
   Bs_P_data__31->SetBinContent(6,0.04591837);
   Bs_P_data__31->SetBinContent(7,0.0255102);
   Bs_P_data__31->SetBinContent(8,0.01020408);
   Bs_P_data__31->SetBinError(1,0.01140851);
   Bs_P_data__31->SetBinError(2,0.03952024);
   Bs_P_data__31->SetBinError(3,0.03918952);
   Bs_P_data__31->SetBinError(4,0.03226814);
   Bs_P_data__31->SetBinError(5,0.02040816);
   Bs_P_data__31->SetBinError(6,0.01530612);
   Bs_P_data__31->SetBinError(7,0.01140851);
   Bs_P_data__31->SetBinError(8,0.007215375);
   Bs_P_data__31->SetEntries(196);
   Bs_P_data__31->SetDirectory(0);
   Bs_P_data__31->SetStats(0);

   ci = TColor::GetColor("#666666");
   Bs_P_data__31->SetLineColor(ci);
   Bs_P_data__31->SetLineWidth(2);
   Bs_P_data__31->SetMarkerStyle(20);
   Bs_P_data__31->GetXaxis()->SetNdivisions(505);
   Bs_P_data__31->GetXaxis()->SetLabelFont(132);
   Bs_P_data__31->GetXaxis()->SetLabelOffset(0.01);
   Bs_P_data__31->GetXaxis()->SetLabelSize(0.06);
   Bs_P_data__31->GetXaxis()->SetTitleSize(0.072);
   Bs_P_data__31->GetXaxis()->SetTitleOffset(0.95);
   Bs_P_data__31->GetXaxis()->SetTitleFont(132);
   Bs_P_data__31->GetYaxis()->SetLabelFont(132);
   Bs_P_data__31->GetYaxis()->SetLabelOffset(0.01);
   Bs_P_data__31->GetYaxis()->SetLabelSize(0.06);
   Bs_P_data__31->GetYaxis()->SetTitleSize(0.072);
   Bs_P_data__31->GetYaxis()->SetTitleOffset(0.95);
   Bs_P_data__31->GetYaxis()->SetTitleFont(132);
   Bs_P_data__31->GetZaxis()->SetLabelFont(132);
   Bs_P_data__31->GetZaxis()->SetLabelSize(0.06);
   Bs_P_data__31->GetZaxis()->SetTitleSize(0.072);
   Bs_P_data__31->GetZaxis()->SetTitleOffset(1.2);
   Bs_P_data__31->GetZaxis()->SetTitleFont(132);
   Bs_P_data__31->Draw("HIST SAME");
   c1_n10->Modified();
   c1_n10->cd();
   c1_n10->SetSelected(c1_n10);
}
