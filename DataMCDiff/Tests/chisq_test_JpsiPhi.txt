****************************************
Performing Chi2 Test for JpsiPhi Mode
****************************************

Chi2ndof for Bs and Bach separately:
On histogram with Bs_PT_MC
On histogram with Bs_P_MC
On histogram with Bs_ETA_MC
Chi^2 / NDoF = 4.2070962779730054 / 15 = 0.28047308519820036, p-value = 0.9969763362431937
On histogram with Bach_PT_MC
On histogram with Bach_P_MC
On histogram with Bach_ETA_MC
Chi^2 / NDoF = 15.562534970060003 / 15 = 1.0375023313373335, p-value = 0.41170581902776804
Total chi2ndof:
Chi^2 / NDoF = 19.769631248033008 / 30 = 0.658987708267767, p-value = 0.9224021040520815