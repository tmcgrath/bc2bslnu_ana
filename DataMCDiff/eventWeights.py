
# plotBKinematics.py
#
# Code for plotting the Bc, Bs, and bach kinematics for Bc->Bspi data/MC and Bc->Bsmunu MC in one canvas
#

import sys
sys.path.append("/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit")
import argparse
import ROOT as R
import uproot as ur
import pandas as pd, numpy as np
from BDT_cuts import BDT_cuts_sig, BDT_cuts_norm
from plotBKinematics import applyCuts
from plotBKinematics import channels as Bs_channels
# Get ranges for pt and eta
from plotBKinematics import eta_range, Bs_pt_range, Bach_pt_range, nBins

# xtra setup
RDF = R.RDataFrame
R.gROOT.ProcessLine(".L /afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/lhcbStyle.C")
R.TH1.SetDefaultSumw2()

##################################################
# Functions
##################################################

def getFilename(Bc_channel, Bs_channel, MC_or_data):
    # Some conditional string args
    if Bc_channel == "Comb":
        # If combinatoric and MC -> use prompt Bs MC 
        # If combinatoric and data -> use Bc2BsPi sideband
        Bc_channel_str = "" if MC_or_data == "MC" else "Bc2BsPi"
    else:
        Bc_channel_str = Bc_channel + "_"
    if MC_or_data == "MC":
        itsMC = "MC_"
    else:
        itsMC = ""

    infolder = f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/{MC_or_data}BDTApplied"
    infilename = f"{Bc_channel_str}Bs2{Bs_channel}_All_{itsMC}BDT.root"
    filename = f"{infolder}/{infilename}"

    return filename

# returns tree for 
def getDF(Bc_channel, Bs_channel, MC_or_data, tree="DecayTree"):

    file_to_load = getFilename(Bc_channel, Bs_channel, MC_or_data)
    print(f"Loading {MC_or_data}: {file_to_load}")

    with ur.open(file_to_load) as root_file:
        #print(root_file.classnames())
        the_tree = root_file[tree]
        the_df = the_tree.arrays(library="pd")

    return the_df, file_to_load


# Get weights from plots made in plotBKinematics and returns the weights plot and lookup table
def getWeightsFromRoot(particle, weight_type, Bs_channel, bins = nBins):
    '''
    weights = data / MC 
    weight_type = "comb", "sig", "norm"
    '''

    # Get weights histogram from file
    weights_filename = f"./Weights/weights_histograms_{Bs_channel}_{bins}bins.root"
    weights_file = R.TFile.Open(weights_filename, "READ")
    print(f"Getting weights from {weights_filename}.....")
    weights_plot_name = f"{particle}_weights_for_{weight_type}"
    weights_plot = weights_file.Get(weights_plot_name)

    # Make look_up table
    print("Making look-up table.....")
    look_up = pd.DataFrame(0, index=np.arange(1, (bins*bins) + 1), columns=["weight", "pt_min", "pt_max", "eta_min", "eta_max"])
    for bin_y in range(1, weights_plot.GetNbinsY()+1):
        eta_min = weights_plot.GetYaxis().GetBinLowEdge(bin_y)
        eta_max = weights_plot.GetYaxis().GetBinUpEdge(bin_y)

        for bin_x in range(1, weights_plot.GetNbinsX()+1):
            the_bin = weights_plot.GetBin(bin_x, bin_y)

            pt_min = weights_plot.GetXaxis().GetBinLowEdge(bin_x)
            pt_max = weights_plot.GetXaxis().GetBinUpEdge(bin_x)

            content = weights_plot.GetBinContent(bin_x, bin_y)
            print(the_bin)
            print(f"{bin_x} {bin_y}")
            print(f"pt_min = {pt_min}, pt_max = {pt_max}")
            print(f"eta_min = {eta_min}, eta_max = {eta_max}")
            print(f"content: {content}")
            print()

            # TODO fill lookup table row
            look_up.loc[the_bin] = [content, pt_min, pt_max, eta_min, eta_max]
    
    return look_up


# returns the weights plot and the lookup table df
def getWeightsPlot(MC_fn, data_fn, particle, tree="DecayTree"):
    '''
    weights = data / MC
    '''
    if particle == "Bach":
        x_range = Bach_pt_range
    else:
        x_range = Bs_pt_range

    bins = x_range[0]

    print(f"\nMaking weights plot using {MC_fn} and {data_fn}.....")
    print(x_range)
    MC_plot = RDF(tree, MC_fn).Histo2D(("MC", "", x_range[0], x_range[1], x_range[2], eta_range[0], eta_range[1], eta_range[2]), f"{particle}_PT", f"{particle}_ETA")
    data_plot = RDF(tree, data_fn).Histo2D(("Data", "", x_range[0], x_range[1], x_range[2], eta_range[0], eta_range[1], eta_range[2]), f"{particle}_PT", f"{particle}_ETA")
    # Normalise
    MC_plot = MC_plot.GetValue()
    MC_plot.Scale(1 / MC_plot.Integral())
    data_plot = data_plot.GetValue()
    data_plot.Scale(1 / data_plot.Integral())
    # Get ratio
    weights_plot = data_plot.Clone()
    weights_plot.Divide(MC_plot)
    weights_plot.SetTitle("")

    print(f"MC xaxis {MC_plot.GetNbinsX()} yaxis {MC_plot.GetNbinsY()}")
    print(f"Data xaxis {data_plot.GetNbinsX()} yaxis {data_plot.GetNbinsY()}")

    # make look_up table
    print("\nMaking look-up table.....")
    look_up = pd.DataFrame(0, index=np.arange(1, (weights_plot.GetNbinsY()*weights_plot.GetNbinsX()) + 1), columns=["weight", "pt_min", "pt_max", "eta_min", "eta_max"])
    for bin_y in range(1, weights_plot.GetNbinsY()+1):
        eta_min = weights_plot.GetYaxis().GetBinLowEdge(bin_y)
        eta_max = weights_plot.GetYaxis().GetBinUpEdge(bin_y)

        for bin_x in range(1, weights_plot.GetNbinsX()+1):
            the_bin = weights_plot.GetBin(bin_x, bin_y)

            pt_min = weights_plot.GetXaxis().GetBinLowEdge(bin_x)
            pt_max = weights_plot.GetXaxis().GetBinUpEdge(bin_x)

            content = weights_plot.GetBinContent(bin_x, bin_y)
            print(the_bin)
            print(f"{bin_x} {bin_y}")
            print(f"pt_min = {pt_min}, pt_max = {pt_max}")
            print(f"eta_min = {eta_min}, eta_max = {eta_max}")
            print(f"content: {content}")
            print()

            # TODO fill lookup table row
            look_up.loc[the_bin] = [content, pt_min, pt_max, eta_min, eta_max]

    # save weights histogram
    both_chans = (MC_fn.split("/")[-1]).split("_All")[0]
    weight_plot_name = f"{both_chans}_{particle}_weights_plot"
    print(f"Saving weights histogram as {weight_plot_name}......\n")
    c = R.TCanvas()
    xaxis_title, yaxis_title = f"{particle} p_{{T}} [MeV/c]", f"{particle} #eta"
    weights_plot.GetXaxis().SetTitle(xaxis_title)
    weights_plot.GetYaxis().SetTitle(yaxis_title)
    weights_plot.Draw("colz")
    c.SaveAs(f"./Figs/{weight_plot_name}.png")
    c.SaveAs(f"./Figs/{weight_plot_name}.pdf")
    c.SaveAs(f"./Figs/{weight_plot_name}.C")
    MC_plot.GetXaxis().SetTitle(xaxis_title)
    MC_plot.GetYaxis().SetTitle(yaxis_title)
    data_plot.GetXaxis().SetTitle(xaxis_title)
    data_plot.GetYaxis().SetTitle(yaxis_title)
    
    # Save in a root file - probs no need
    #outfile = f"./Figs/{both_chans}_{particle}_plots.root"
    #with ur.recreate(outfile) as outf:
    #    outf[f"{particle}_PT_vs_ETA_MC"] = MC_plot
    #    outf[f"{particle}_PT_vs_ETA_data"] = data_plot
    #    outf[f"{particle}_PT_vs_ETA_weights"] = weights_plot

    return look_up

def newDF(df, infilename):

    outdir = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCWeighted"
    outfilename = outdir + (infilename.split("/")[-1]).split("_BDT.roo")[0] + "_weighted.root"
    outfile = f"{outdir}/{outfilename}"
    print(f"Saving file as {outfile}...")

    with ur.recreate(outfile) as outf:
        outf["DecayTree"] = df

#def saveDF(df, infilename):

#    print(f"Updating file as {infilename}...")
#    with ur.recreate(infilename) as outf:
#       outf["FriendTree"] = df[["Bs_weight", "Bach_weight"]]

def lookUpWeight(row, look_up, particle):
    print(row[f"{particle}_PT"])
    a_match = ( (look_up["pt_min"] <= row[f"{particle}_PT"]) & (row[f"{particle}_PT"] < look_up["pt_max"]) ) & ( (look_up["eta_min"] <= row[f"{particle}_ETA"]) & (row[f"{particle}_ETA"] < look_up["eta_max"]))
    weight = look_up["weight"][a_match]
    print(weight)
    print(weight.values[0])
    return weight.values[0]

def applyWeights(MC, look_up_dict):
    print(look_up_dict["Bs"])
    print(look_up_dict["Bach"])

    # Make new columns of the Bs and Bach weights using the lookup table
    new_MC_df = MC.copy()
    for particle_name, particle_look_up in look_up_dict.items():
        #new_MC_df.loc[:, f"{particle}_weight"] = np.zeros(MC.shape[0])
        new_MC_df[f"{particle_name}_weight"] = new_MC_df.apply(lambda row: lookUpWeight(row, particle_look_up, particle_name), axis=1) #.values[0])

    return new_MC_df

# Function to do everything
def weightApplication(Bc_channel, Bs_channel):

    print("Application of weights to MC from ratio of pt-eta histograms between data and MC!")

    # Get the dataframes
    MC_df, MC_filename = getDF(Bc_channel, Bs_channel, "MC")
    data_df, data_filename = getDF(Bc_channel, Bs_channel, "Data")

    # Get Bc2BsPi sideband if combinatorial bkg channels
    if Bc_channel == "Comb":
        data_df = getSideband(data_df) 

    # Get the 2D histograms of the weights for Bs and bachelor particle
    particles = ("Bs", "Bach")
    look_up_dictionary = {}
    for particle in particles:
        look_up_dictionary[particle] = getWeightsPlot(MC_filename, data_filename, particle)

    # Apply weights to MC
    MC_df_with_weights = applyWeights(MC_df, look_up_dictionary)

    # Save the df in a root file
    newDF(MC_df_with_weights, MC_filename)

    

##################################################
# Main
##################################################

# So that I can use some of the functions in other scripts
if __name__ == '__main__':

    # Set batch mode
    R.gROOT.SetBatch( True )

    # Bc decay options
    all_options = ["Bc2BsMu", "Bc2BsPi", "Bc2BsK", "Bc2BsKst", "Bc2BsstMuNu", "Comb"]
    partial_options = ["Bc2BsMu", "Bc2BsPi", "Comb", "Bc2BsstMuNu"]

    parser=argparse.ArgumentParser()
    parser.add_argument('--Bc_channel','-c', type=str, choices=partial_options, default="Bc2BsPi", help=f'Bc decay channel to reweight\nOptions: {partial_options}')
    args = parser.parse_args()
    Bc_chan = args.Bc_channel

    weightApplication(Bc_chan, Bs_channels[0])

    # later do this
    # for Bs_chan in Bs_channels: 


