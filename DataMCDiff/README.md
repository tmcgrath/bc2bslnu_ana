# Data / MC comparison

Code for comparing $B_c^+ \to B_s^0 \pi^+$ data and MC for $p_T$, $p$, and $\eta$ of $\pi^+$, $B_s^0$, and $B_c^+$ in [`plotBKinematics.py`](./plotBKinematics.py). [`plotPValues.py`](./plotPValues.py) plots all the p-values comparing data and MC for each variable on one bar chart.
