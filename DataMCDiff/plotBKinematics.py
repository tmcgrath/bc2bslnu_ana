#
# plotBKinematics.py
#
# Code for plotting the Bc, Bs, and bach kinematics for Bc->Bspi data/MC and Bc->Bsmunu MC in one canvas
#

import sys
import ROOT as R
import math
import argparse
from scipy import stats
RDF = R.RDataFrame
R.gROOT.ProcessLine(".L /afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/lhcbStyle.C")
sys.path.append("/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit")
from BDT_cuts import BDT_cuts_sig, BDT_cuts_norm

## Functions

# Bs decay channels to loop over
channels = ("DsPi", "JpsiPhi")

# ranges to plot over
nBins = 5
eta_range = (nBins, 2, 5)
Bs_pt_range = (nBins, 0, 35000); Bs_p_range = (nBins, 0, 450000)
Bach_pt_range = (nBins, 0, 6500); Bach_p_range = (nBins, 0, 80000)
Bc_pt_range = (nBins, 0, 40000); Bc_p_range = (nBins, 0, 600000)

# Bc mass range from normalisation mode fits
Bc_mass_ranges = {
    "DsPi"  : (6274.73 - 3 * 4.75, 6274.73 + 3 * 4.75),
    "JpsiPhi" : (6272.7 - 3 * 7.7, 6272.7 + 3 * 7.7)
}
# tuple = (mass + 5 sigma, exp. constant, N_comb)
Bc_sideband = {
    "DsPi"  : (6274.7 + 5 * 4.75, -0.00602, 286),
    "JpsiPhi" : (6272.7 + 5 * 7.7, -0.00269, 867)
}

def applyCuts(df, Bcchan, chan, sideband = False, year="All"):
    year = str(year)

    if Bcchan == "sig":
        BDT_cuts = BDT_cuts_sig
        extra_cut = ""
    else:
        BDT_cuts = BDT_cuts_norm

        # Apply cuts for normalisation mode
        if sideband == False:
            # Bc mass cut
            Bc_mass_range = Bc_mass_ranges[chan]
            extra_cut = f"{Bc_mass_range[0]} <= Bc_MDTF_M && Bc_MDTF_M <= {Bc_mass_range[1]}"
        else:
            # Bc sideband cut - make region as big as possible for higher stats
            extra_cut = f"{Bc_sideband[chan][0]} <= Bc_MDTF_M && Bc_MDTF_M <= 8000"

    print(f"Applying cut: {extra_cut}")
    filtered_df = df.Filter(BDT_cuts[chan][year], BDT_cuts[chan][year])\
    .Filter("Bs_MM > 5300 & Bs_MM < 5420")
    if Bcchan == "norm": filtered_df = filtered_df.Filter(extra_cut)

    #print(filtered_df.Count().GetValue())
    return filtered_df

def applyCuts_MC(df, Bcchan, chan, year="All"):

    if Bcchan == "sig":
        BDT_cuts = BDT_cuts_sig
    else:
        BDT_cuts = BDT_cuts_norm
    
    #print("Filtering")
    #print(df.Count().GetValue())
    filtered_df = df.Filter(BDT_cuts[chan][year], BDT_cuts[chan][year])\
    .Filter("Bs_MM > 5300 & Bs_MM < 5420")
    #print(filtered_df.Count().GetValue())

    return filtered_df

# Get the higher number of the sideband range
def calcMaxRange(x0, x1, x2, chan):
    '''
    x3 = ln( exp(cx1) - exp(cx0) + exp(cx2) )
    where
    x0 = M - 3sigma, x1 = M + 3sigma, x2 = M + 5sigma
    '''
    c = Bc_sideband[chan][1] / 6
    cx3 = math.log( math.exp(c * x1) - math.exp(c * x0) + math.exp(c * x2) )
    x3 = cx3 / c
    return x3

# Integral calculation for checks
def calcIntegral(x0, x1, chan):
    '''
    Not real integral, simplified version for checks
    Int = exp(cx1) - exp(cx0)
    '''
    c = Bc_sideband[chan][1] / 6
    N = Bc_sideband[chan][2]
    #print(f"Calculating integral-ish for range [{x0},{x1}]")
    area = (math.exp(c * x1) - math.exp(c * x0)) * (N / c)

    return area

# Calculate chi^2 for one data point / bin with original data and bkg histograms
def calcChiSq(MC_bin, MC_integral, data_bin, data_integral, bkg_bin, both_bin):
    '''
    MC_bin is normalised
    returns Chi^2_i = (norm_data_bin_i - norm_MC_bin_i)^2 / error_added_in_quadrature_i^2
    '''
    error_MC = math.sqrt(MC_bin)
    error_data = math.sqrt( bkg_bin + both_bin )
    if MC_bin == 0:
        error_MC = 1.0
    if data_bin == 0:
        error_data = 1.0

    error_MC_norm = error_MC / MC_integral
    error_data_norm = error_data / data_integral

    error_combined = math.sqrt( math.pow(error_MC_norm, 2) + math.pow(error_data_norm, 2) )

    MC_bin_norm = MC_bin / MC_integral; data_bin_norm = data_bin / data_integral
    chisq = math.pow((data_bin_norm - MC_bin_norm), 2) / math.pow(error_combined, 2)

    print(f"MC bin content: {MC_bin}, MC integral: {MC_integral}, MC bin norm: {MC_bin_norm}")
    print(f"Data bin content: {data_bin}, Data integral: {data_integral}, Data bin norm: {data_bin_norm}")
    print(f"Bkg bin content: {bkg_bin}")
    print(f"Data + bkg bin content: {both_bin}")
    print(f"Error in quadrature: {error_combined}")

    return chisq


# Returns df for background subtraction
def getDFForBkgSub(df_data, chan):
    '''
    Get sideband range where:
    area( under mass peak in norm mode ) = area( equivalent in sideband )
    '''
    print(f"\nGetting DF for bkg subtraction")
    max_Bc_mass = calcMaxRange(Bc_mass_ranges[chan][0], Bc_mass_ranges[chan][1], Bc_sideband[chan][0], chan)
    bkg_range = (Bc_sideband[chan][0], max_Bc_mass)

    # Filter data to isolate the sideband region for the background subtraction
    bkg_range_cut = f"{bkg_range[0]} <= Bc_MDTF_M && Bc_MDTF_M <= {bkg_range[1]}"
    print(f"Got range: {bkg_range_cut}")
    df_bkg_region = df_data.Filter(bkg_range_cut)

    # For checks - can comment out
    a_mass_peak = calcIntegral(Bc_mass_ranges[chan][0], Bc_mass_ranges[chan][1], chan); a_sideband = calcIntegral( bkg_range[0], bkg_range[1], chan)
    print(f"Range {Bc_mass_ranges[chan]} under mass peak: {a_mass_peak}")
    print(f"Range {bkg_range} sideband: {a_sideband}")

    return df_bkg_region


# plot weights histogram and save as Fig and in root file
def plotWeights(df_MC, df_data, particle, weight_type, chan, open_file):
    '''
    weights = data / MC
    if weight = NaN, set to 0 if no data in bin, set to 1 if no MC in bin
    weight_type = "comb", "sig", "norm"
    '''

    if particle == "Bach":
        x_range = Bach_pt_range
    else:
        x_range = Bs_pt_range

    # Get MC and data 2D plots
    print(f"\nMaking weights plot using of {particle} for {weight_type}.....")
    MC_plot = df_MC.Histo2D((f"MC_for_{particle}", "", x_range[0], x_range[1], x_range[2], eta_range[0], eta_range[1], eta_range[2]), f"{particle}_PT", f"{particle}_ETA")
    data_plot = df_data.Histo2D((f"Data_for_{particle}", "", x_range[0], x_range[1], x_range[2], eta_range[0], eta_range[1], eta_range[2]), f"{particle}_PT", f"{particle}_ETA")

    # Normalise
    MC_plot = MC_plot.GetValue(); MC_plot.Sumw2()
    MC_plot.Scale(1 / MC_plot.Integral())
    data_plot = data_plot.GetValue(); data_plot.Sumw2()
    data_plot.Scale(1 / data_plot.Integral())
    # Get ratio
    weights_plot = data_plot.Clone()
    weights_plot.Divide(MC_plot)
    
    # Set contents
    for nY in range(1, nBins+1):
        for nX in range(1, nBins+1):
            # Bin contents
            #weight = weights_plot.GetBinContent(nX, nY)
            MC_content = MC_plot.GetBinContent(nX, nY)
            data_content = data_plot.GetBinContent(nX, nY)

            if data_content == 0.0:
                weights_plot.SetBinContent(nX, nY, 0.0)
            elif MC_content == 0.0 and data_content != 0.0:
                weights_plot.SetBinContent(nX, nY, 1.0)
            
            #new_weight = weights_plot.GetBinContent(nX, nY)
            #print(f"Weight: {weight}")
            #print(f"Data: {data_content}, MC: {MC_content}")
            #print(f"New weight: {new_weight}")
    
    # Cosmetics
    weights_plot.SetTitle("")
    weights_plot.SetName(f"{particle}_weights_for_{weight_type}")
    xaxis_title, yaxis_title = f"{particle} p_{{T}} [MeV/c]", f"{particle} #eta"
    weights_plot.GetXaxis().SetTitle(xaxis_title)
    weights_plot.GetYaxis().SetTitle(yaxis_title)

    # Save as a plot
    c3 = R.TCanvas()
    weights_plot.Draw("COLZ,TEXT")
    #c3.SaveAs(f"./Figs/{particle}_weights_for_{weight_type}_{chan}_{nBins}bins.png")
    #c3.SaveAs(f"./Figs/{particle}_weights_for_{weight_type}_{chan}_{nBins}bins.pdf")
    #c3.SaveAs(f"./Figs/{particle}_weights_for_{weight_type}_{chan}_{nBins}bins.C")

    # Save in root file
    open_file.cd()
    weights_plot.Write()


def plot_MC_Data(df_MC_sig, df_MC_norm, df_data, df_data_bkg, var, therange, chan, open_file, subtract_bkg=True, returnHists=False):
    # Signal MC
    MC_sig = df_MC_sig.Histo1D((f"{var}_MC_signal", "", therange[0], therange[1], therange[2]), var)
    # Norm MC and Data
    MC = df_MC_norm.Histo1D((f"{var}_MC", "", therange[0], therange[1], therange[2]), var)
    data_before = df_data.Histo1D((f"{var}_data", "", therange[0], therange[1], therange[2]), var)
    bkg = df_data_bkg.Histo1D((f"{var}_bkg_data", "", therange[0], therange[1], therange[2]), var)
    MC_sig = MC_sig.GetValue(); MC = MC.GetValue()
    data_before = data_before.GetValue(); bkg = bkg.GetValue()
    MC_sig.Sumw2(); MC.Sumw2(); data_before.Sumw2(); bkg.Sumw2()

    print(f"\nPlotting for {chan} {var}...")
    # Get background subtracted histogram
    print(f"before: {data_before.Integral()}")
    data = data_before.Clone()
    if subtract_bkg == True:
        print("Performing background subtraction")
        bkg_count = bkg.Integral()
        print(f"Entries in background {bkg_count}")
        data.Add(bkg, -1)
    print(f"after: {data.Integral()}")

    # Clone un-normalised histograms for chisq test
    MC_sig_og = MC_sig.Clone(); MC_og = MC.Clone(); data_og = data.Clone()

    MC_sig.Scale(1 / MC_sig.Integral())
    MC.Scale(1 / MC.Integral())
    data.Scale(1 / data.Integral())

    if "_PT" in var:
        variable = "p_{T} [MeV/c]"
        y_max = 0.75
    elif "_P" in var and var[-1] == "P":
        variable = "p [MeV/c]"
        y_max = 0.7
    elif "_ETA" in var:
        variable = "#eta"
        y_max = 0.5
    
    if "Bach" in var:
        particle = "Bachelor particle"
    elif "Bs" in var:
        particle = "B_{s}^{0}"
    elif "Bc" in var:
        particle = "B_{c}^{+}"

    x_label = f"{particle} {variable}"
    
    ## First, a chisq test
    #chi2 = data.Chi2Test(MC, option = "UW, CHI2/NDF")
    #chi2_alt = data.Chi2Test(MC, option = "UU, CHI2/NDF")
    #chi2_alt2 = data.Chi2Test(MC, option = "WW, CHI2/NDF")
    #print(f"Chi2 (UW): {chi2}, Chi2 (UU): {chi2_alt}, Chi2 (WW): {chi2_alt2}")

    ## Plotting config
    
    MC_sig.SetLineColorAlpha(433, 0.35)
    MC_sig.SetFillColorAlpha(433, 0.35)
    
    MC.SetLineColorAlpha(R.kOrange+5, 0.6)

    data.SetLineColor(R.kBlack)
    data.SetMarkerColor(R.kBlack)

    # Find y range
    data.GetYaxis().SetRangeUser(0, y_max)

    # Draw all plots
    c = R.TCanvas()
    c.cd()

    data.GetXaxis().SetTitle(x_label)
    data.GetYaxis().SetTitle("AU")
    data.Draw("E")
    MC.Draw("HIST SAME")
    MC_sig.Draw("HIST SAME")

    leg = R.TLegend(0.6,0.7,0.85,0.9)
    leg.SetLineStyle(0)
    leg.AddEntry(data,"B_{c}^{+} #rightarrow B_{s}^{0} #pi^{+} Data","l")
    leg.AddEntry(MC,"B_{c}^{+} #rightarrow B_{s}^{0} #pi^{+} MC", "l")
    leg.AddEntry(MC_sig, "B_{c}^{+} #rightarrow B_{s}^{0} #mu^{+} #nu_{#mu} MC", "f")
    if "_PT" in var:
        leg.Draw()
    
    # Add text
    #extra_text = R.TPaveText(0.6, 0.63, 0.85, 0.7, "NDC")
    #extra_text.AddText(f"#chi^{{2}}/nDOF = {chi2:.03f}")
    #extra_text.SetLineColor(0)
    #extra_text.SetFillStyle(0)
    #extra_text.Draw()


    c.Update()
    #c.SaveAs(f"./Figs/{var}_{chan}_{nBins}bins.png")
    #c.SaveAs(f"./Figs/{var}_{chan}_{nBins}bins.pdf")
    #c.SaveAs(f"./Figs/{var}_{chan}_{nBins}bins.C")

    # Save plots in root file
    MC_sig.SetName(f"signalMC_{var}")
    MC.SetName(f"normMC_{var}")
    data.SetName(f"normData_{var}")
    open_file.cd()
    MC_sig.Write(); MC.Write(); data.Write()

    if returnHists == True:
        return MC_og, data_og, bkg, data_before


def plot_comb_Data(df_MC_comb, df_data_sideband, var, therange, chan, open_file):
    # Comb MC
    MC_comb = df_MC_comb.Histo1D((f"{var}_MC_signal", "", therange[0], therange[1], therange[2]), var)
    # Data sideband
    data = df_data_sideband.Histo1D((f"{var}_data", "", therange[0], therange[1], therange[2]), var)
    MC_comb = MC_comb.GetValue(); data = data.GetValue()
    MC_comb.Sumw2(); data.Sumw2()
    MC_comb.Scale(1 / MC_comb.Integral())
    data.Scale(1 / data.Integral())

    if "_PT" in var:
        variable = "p_{T} [MeV/c]"
        if "Bach" in var: y_max = 0.975
        else: y_max = 0.7
    elif "_P" in var and var[-1] == "P":
        variable = "p [MeV/c]"
        if "Bach" in var: y_max = 0.975
        else: y_max = 0.6
    elif "_ETA" in var:
        variable = "#eta"
        y_max = 0.5
    
    if "Bach" in var:
        particle = "Bachelor particle"
    elif "Bs" in var:
        particle = "B_{s}^{0}"
    elif "Bc" in var:
        particle = "B_{c}^{+}"

    x_label = f"{particle} {variable}"

    # Set y range
    MC_comb.GetYaxis().SetRangeUser(0, y_max)

    ## Plotting config

    c = R.TCanvas()
    MC_comb.SetLineColorAlpha(R.kSpring + 5, 0.4)
    MC_comb.SetFillColorAlpha(R.kSpring + 5, 0.4)
    MC_comb.GetXaxis().SetTitle(x_label)
    MC_comb.GetYaxis().SetTitle("AU")

    data.SetLineColor(R.kGray + 2)
    data.SetMarkerColor(R.kGray + 2)

    # Draw
    MC_comb.Draw("HIST")
    data.Draw("E SAME")
    
    leg = R.TLegend(0.6,0.7,0.85,0.9)
    leg.SetLineStyle(0)
    leg.AddEntry(data,"B_{c}^{+} #rightarrow B_{s}^{0} #pi^{+} Sideband","l")
    leg.AddEntry(MC_comb, "Prompt B_{s}^{0} (Combinatorial) MC", "f")
    if "_PT" in var:
        leg.Draw()
    
    c.Update()
    #c.SaveAs(f"./Figs/{var}_{chan}_Comb_{nBins}bins.png")
    #c.SaveAs(f"./Figs/{var}_{chan}_Comb_{nBins}bins.pdf")
    #c.SaveAs(f"./Figs/{var}_{chan}_Comb_{nBins}bins.C")

    # Save plots in root file
    MC_comb.SetName(f"combMC_{var}")
    data.SetName(f"normSidebandData_{var}")
    open_file.cd()
    MC_comb.Write(); data.Write()


# Chi2 test function
def chi2Test(MC_hist_list: list, data_hist_list: list, bkg_hist_list: list, both_hist_list: list, text_file):
    '''
    Returns total chi2 / ndof of MC and data histograms
    '''
    tot_chisq = 0; ndof = 0
    for MC_hist, data_hist, bkg_hist, both_hist in zip(MC_hist_list, data_hist_list, bkg_hist_list, both_hist_list):
        print(f"On histogram with {MC_hist.GetName()}")
        text_file.write(f"On histogram with {MC_hist.GetName()}\n")
        nBins_here = MC_hist.GetNbinsX()
        ndof += nBins_here

        MC_int = MC_hist.Integral(); data_int = data_hist.Integral()

        for i in range(1, nBins_here + 1):
            MC_bin_i = MC_hist.GetBinContent(i)
            data_bin_i = data_hist.GetBinContent(i)
            bkg_bin_i = bkg_hist.GetBinContent(i)
            both_bin_i = both_hist.GetBinContent(i)

            chisq_i = calcChiSq(MC_bin_i, MC_int, data_bin_i, data_int, bkg_bin_i, both_bin_i)
            print(f"+ {chisq_i}")

            tot_chisq += chisq_i
    
    chisq_ndof = tot_chisq / ndof
    prob = 1 - stats.chi2.cdf(tot_chisq, ndof)
    prob_root = R.TMath.Prob(tot_chisq, ndof)

    print(f"Chi^2 / NDoF = {tot_chisq} / {ndof} = {chisq_ndof}, p-value = {prob}")
    text_file.write(f"Chi^2 / NDoF = {tot_chisq} / {ndof} = {chisq_ndof}, p-value = {prob}\n")
    print(f"p-value (ROOT) = {prob_root}")
    return tot_chisq, ndof



# So that I can use some of the functions in other scripts
if __name__ == '__main__':

    # Set batch mode
    R.gROOT.SetBatch( True )

    print("#"*40)
    print("Plotting Bs and Bach weights!")
    print("#"*40)
    print("\n")

    for Bschan in channels:

        print(f"Running for Bs2{Bschan} mode!\n")

        # Filenames
        MCDir = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied"
        DataDir = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/DataBDTApplied"
        sig_MC_file = f"{MCDir}/Bc2BsMu_Bs2{Bschan}_All_MC_BDT.root" 
        norm_MC_file = f"{MCDir}/Bc2BsPi_Bs2{Bschan}_All_MC_BDT.root"
        norm_data_file = f"{DataDir}/Bc2BsPi_Bs2{Bschan}_All_BDT.root"
        comb_MC_file = f"{MCDir}/Bs2{Bschan}_All_MC_BDT.root"

        # Dataframes
        tree = "DecayTree"
        print("Loading dataframes......")
        sig_MC = RDF(tree, sig_MC_file).Filter(f"Bc_is_from_Bc_BsMuNu == 1 && Bs_is_from_Bs_{Bschan} == 1")
        norm_MC = RDF(tree, norm_MC_file).Filter(f"Bc_is_from_Bc_BsPi == 1 && Bs_is_from_Bs_{Bschan} == 1")
        norm_data = RDF(tree, norm_data_file)
        sideband_data = RDF(tree, norm_data_file)
        alt_sideband_data = RDF(tree, norm_data_file)
        comb_MC = RDF(tree, comb_MC_file).Filter(f"Bs_is_from_Bs_{Bschan} == 1")

        # Cuts
        print("Applying cuts to dataframes......")
        sig_MC = applyCuts_MC(sig_MC, "sig", Bschan)
        norm_MC = applyCuts_MC(norm_MC, "norm", Bschan)
        norm_data = applyCuts(norm_data, "norm", Bschan)

        comb_MC = applyCuts_MC(comb_MC, "sig", Bschan)
        sideband_data = applyCuts(sideband_data, "norm", Bschan, sideband=True)
        alt_sideband_data = applyCuts(alt_sideband_data, "norm", Bschan, sideband=True)

        # Get DF for bkg subtraction
        bkgsub_sideband_data = getDFForBkgSub(alt_sideband_data, Bschan)
        print()
        
        ## PLOT
        print("\nPlotting pT and eta plots......")

        # Save file
        #saveFilename = f"./Weights/weights_histograms_{Bschan}_{nBins}bins.root"
        #saveFile = R.TFile.Open(saveFilename, "RECREATE")
        saveFilename = f"./Weights/blah.root"
        saveFile = R.TFile.Open(saveFilename, "RECREATE")

        ## Bc->BsPi Bc mass region plots for signal MC

        # Bs pt, p, eta
        Bs_PT_MC, Bs_PT_data, Bs_PT_bkg, Bs_PT_both     = plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bs_PT", Bs_pt_range, Bschan, saveFile, returnHists=True)
        Bs_P_MC, Bs_P_data, Bs_P_bkg, Bs_P_both         = plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bs_P", Bs_p_range, Bschan, saveFile, returnHists=True)
        Bs_ETA_MC, Bs_ETA_data, Bs_ETA_bkg, Bs_ETA_both = plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bs_ETA", eta_range, Bschan, saveFile, returnHists=True)

        # Bach pt, p, eta
        Bach_PT_MC, Bach_PT_data, Bach_PT_bkg, Bach_PT_both     = plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bach_PT", Bach_pt_range, Bschan, saveFile, returnHists=True)
        Bach_P_MC, Bach_P_data, Bach_P_bkg, Bach_P_both         = plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bach_P", Bach_p_range, Bschan, saveFile, returnHists=True)
        Bach_ETA_MC, Bach_ETA_data, Bach_ETA_bkg, Bach_ETA_both = plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bach_ETA", eta_range, Bschan, saveFile, returnHists=True)

        # Bc pt, p, eta
        plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bc_PT", Bc_pt_range, Bschan, saveFile)
        plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bc_P", Bc_p_range, Bschan, saveFile)
        plot_MC_Data(sig_MC, norm_MC, norm_data, bkgsub_sideband_data, "Bc_ETA", eta_range, Bschan, saveFile)

        # Save the weights for norm_data / norm_MC 
        plotWeights(norm_MC, norm_data, "Bs", "norm", Bschan, saveFile)
        plotWeights(norm_MC, norm_data, "Bach", "norm", Bschan, saveFile)

        ## Bc->BsPi sideband plots for combinatorial MC
        
        # Bs pt, p, eta
        plot_comb_Data(comb_MC, sideband_data, "Bs_PT", Bs_pt_range, Bschan, saveFile)
        plot_comb_Data(comb_MC, sideband_data, "Bs_P", Bs_p_range, Bschan, saveFile)
        plot_comb_Data(comb_MC, sideband_data, "Bs_ETA", eta_range, Bschan, saveFile)

        # Bach pt, p, eta
        plot_comb_Data(comb_MC, sideband_data, "Bach_PT", Bach_pt_range, Bschan, saveFile)
        plot_comb_Data(comb_MC, sideband_data, "Bach_P", Bach_p_range, Bschan, saveFile)
        plot_comb_Data(comb_MC, sideband_data, "Bach_ETA", eta_range, Bschan, saveFile)
        
        # Save the weights for sideband_data / comb_MC
        plotWeights(comb_MC, sideband_data, "Bs", "comb", Bschan, saveFile)
        plotWeights(comb_MC, sideband_data, "Bach", "comb", Bschan, saveFile)


        ## Calculate chi^2
        print("\n"+"*"*40)
        print(f"Performing Chi2 Test for {Bschan} Mode")
        print("*"*40)
        Bs_MC_list = [Bs_PT_MC, Bs_P_MC, Bs_ETA_MC]; Bs_data_list = [Bs_PT_data, Bs_P_data, Bs_ETA_data]
        Bs_bkg_list = [Bs_PT_bkg, Bs_P_bkg, Bs_ETA_bkg]; Bs_both_list = [Bs_PT_both, Bs_P_both, Bs_ETA_both]
        Bach_MC_list = [Bach_PT_MC, Bach_P_MC, Bach_ETA_MC]; Bach_data_list = [Bach_PT_data, Bach_P_data, Bach_ETA_data]
        Bach_bkg_list = [Bach_PT_bkg, Bach_P_bkg, Bach_ETA_bkg]; Bach_both_list = [Bach_PT_both, Bach_P_both, Bach_ETA_both]
        
        with open(f"./Tests/chisq_test_{Bschan}.txt", 'w') as f:
            f.write("*"*40)
            f.write(f"\nPerforming Chi2 Test for {Bschan} Mode\n")
            f.write("*"*40)
            print("\nChi2ndof for Bs and Bach separately:")
            f.write("\n\nChi2ndof for Bs and Bach separately:\n")
            chi2_Bs, ndof_Bs = chi2Test(Bs_MC_list, Bs_data_list, Bs_bkg_list, Bs_both_list, f)
            chi2_Bach, ndof_Bach = chi2Test(Bach_MC_list, Bach_data_list, Bach_bkg_list, Bach_both_list, f)
            print("\nTotal chi2ndof:")
            f.write("Total chi2ndof:\n")
            chi2_total = chi2_Bs+chi2_Bach; ndof_total = ndof_Bach + ndof_Bs
            chi2_per_ndof = chi2_total / ndof_total
            tot_prob = prob = 1 - stats.chi2.cdf(chi2_total, ndof_total)
            print(f"Chi^2 / NDoF = {chi2_total} / {ndof_total} = {chi2_per_ndof}, p-value = {tot_prob}")
            f.write(f"Chi^2 / NDoF = {chi2_total} / {ndof_total} = {chi2_per_ndof}, p-value = {tot_prob}")

        saveFile.Close()
        print(f"Saved file {saveFilename}")

