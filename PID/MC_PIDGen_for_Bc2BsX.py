#
# MC_PIDGen_for_Bs2DsPi.py
#
# Script for applying PID resmpling for all MC files for signal and background
# For both Bs decay channels
#
# 27.10.22
#

import os
import sys

##########################################
# Arguments
##########################################

# List arguments
import argparse
parser=argparse.ArgumentParser()
parser.add_argument('--file','-f',dest='file',required=True,type=str,help='File to process')
parser.add_argument('--magnet_polarity', '-m', required = True, type=str, choices=['MagUp','MagDown'])
parser.add_argument('--bach','-b',dest='bach',required=True,type=str,choices=['mu','pi','K'],
                    help='Whatever is reconstructed as mu in Bc->Bsmunu (e.g. pi in Bc->Bspi)')
parser.add_argument('--tree','-t',dest='tree',required=True,type=str,
                    help='DecayTree to get from file')
args = parser.parse_args()

# Get information from args
infile = args.file
outfile = infile.split("/")[-1].replace(".root","_pidgen_applied.root")
magpol = args.magnet_polarity
theActualBach = args.bach
intree = args.tree
outtree = intree.split("/")[-1]
BsChan = (infile.split("/")[-1]).split("_")[1] 
year = (infile.split("/")[-1]).split("_")[2] 
if "Bc" not in (infile.split("/")[-1]).split("_")[0]:
    BsChan = (infile.split("/")[-1]).split("_")[0]
    year_index = 1
    if "loose" in infile:
        year_index+=1
    year = (infile.split("/")[-1]).split("_")[year_index]
if "Bc2BsMu" in intree:
    theRecoBach = "mu"
elif "Bc2BsPi" in intree:
    theRecoBach = "pi"
else:
    print("ERROR cannot work out reconstructed bachelor particle ")
print(f"Processing {infile} with tree {intree}")
print(f"Actual bachelor particle in MC is {theActualBach}")

# put output in same directory for convenience
outfolder = os.path.dirname( infile )

##########################################
# PIDGen Config
##########################################

simversion = "run2"

# Postfixes of the Pt, Eta and Ntracks variables (ntuple variable name w/o branch name)
# e.g. if the ntuple contains "pion_PT", it should be just "PT"
ptvar = "PT"
etavar = None
pvar = "P"
## Could use P variable instead of eta
# etavar = None
# pvar   = "p"

ntrvar = "nTracks"  # This should correspond to the number of "Best tracks", not "Long tracks"!


##########################################
# Variable dictionary
##########################################

# The first key needs to be the variable name in the MC file
# The second key (in first value) needs to be the PID variable
# The second value needs to be the name of the template that is required for the ACTUAL particle
# (not what it is reconstructed as)

# if Bs->DsPi
if BsChan == "Bs2DsPi":
    tracks = {
        "Bach" : {
            "PIDmu" : theActualBach+"_CombDLLmu_Brunel",
            #"PIDpi" : theActualBach+"_CombDLLpi_Brunel",
            "PIDK"  : theActualBach+"_CombDLLK_Brunel"
        },
        "PionBs" : {
            "PIDK"  : "pi_CombDLLK_Brunel"
        },
        "H1Ds" : {
            "PIDK"  : "K_CombDLLK_Brunel"
        },
        "H2Ds" : {
            "PIDK"  : "K_CombDLLK_Brunel"
        },
        "H3Ds" : {
            "PIDK"  : "pi_CombDLLK_Brunel"
        }
    }
else: # if Bs->JpsiPhi
    tracks = {
        "Bach" : {
            "PIDmu" : theActualBach+"_CombDLLmu_Brunel",
            #"PIDpi" : theActualBach+"_CombDLLpi_Brunel",
            "PIDK"  : theActualBach+"_CombDLLK_Brunel"
        },
        "MuP"  : {
            "PIDmu" : "mu_CombDLLmu_Brunel"
        },
        "MuM"  : {
            "PIDmu" : "mu_CombDLLmu_Brunel"
        },
        "KaonP" : {
            "PIDK"  : "K_CombDLLK_Brunel"
        },
        "KaonM" : {
            "PIDK"  : "K_CombDLLK_Brunel"
        }
    }

##########################################
# Running PIDGen!
##########################################

files = []
files.append( (infile, outfile, magpol+"_"+year) )
good_files = []

for input_file, output_file, dataset in files:
    #rand_string = ''.join(
    #random.choice(string.ascii_uppercase + string.digits)
    #for _ in range(10))  # get 10 random chars for temp_file prefix
    #temp_file_prefix = temp_folder + '/' + rand_string  # prefix temp files with folder and unique ID
    #if outputfile exits, continue

    if os.path.isfile(os.getcwd()+'/'+output_file):
        print('file',output_file,' alredy exists! continuing!')
        continue

    print("*"*30)
    print('now processing',input_file)
    print("*"*30)

    tmpinfile = input_file
    for track, subst in tracks.items():
        for var, config in subst.items():
            this_outfile_name = output_file.replace(".root",f"_{track}_{var}.root")
            print(f'will write {track} {var} to {this_outfile_name}')
            #sys.exit(1)
            tmpconfig = config
            #command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDCorr.py"
            command = 'python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDGen.py'
            command += " -m %s_%s" % (track, ptvar)
            if etavar:
                command += " -e %s_%s" % (track, etavar)
            elif pvar:
                command += " -q %s_%s" % (track, pvar)
            else:
                print('Specify either ETA or P branch name per track')
                sys.exit(1)
            command += " -n %s" % ntrvar
            command += " -t %s" % intree
            command += " -p %s_%s_corr" % (track, var)
            command += " -c %s" % tmpconfig
            command += " -d %s" % dataset
            command += " -i %s" % tmpinfile
            command += " -o %s" % this_outfile_name
            command += " --noclone"
                
            print(command)
            os.system(command)
            if os.path.isfile(os.getcwd()+'/'+this_outfile_name):
                good_files.append(this_outfile_name)

#combine all trees
print('combining',good_files)
if len(good_files)==0:
    print('no files! giving up!')
    sys.exit(1)

from ROOT import RDataFrame as rdf
import ROOT as R
f1 = R.TFile.Open(good_files[0])
t1 = f1.Get(outtree)
for f in good_files[1:]:
    t1.AddFriend("DecayTree",f) # it might always be this
df = rdf(t1)
print("saving "+outfolder+"/PIDGen/"+output_file)
df.Snapshot("DecayTree",outfolder+"/PIDGen/"+output_file)
