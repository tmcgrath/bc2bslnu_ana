#
# submit_Bc2BsX.py
#
# submitting PIDGen for Bkg channels
#

import glob, sys
import argparse

# some global vars
bs = "Bs2DsPi"
bach = "mu"
tree = "Bc2BsMu_Tuple/DecayTree"


# 2017
j_17MU = Job()
j_17MU.name = 'PIDGen_Bs2DsPi_17MU'
j_17MU.application.exe  = File('../run_PIDGen.sh')
j_17MU.inputfiles = [ LocalFile('../MC_PIDGen_for_Bc2BsX.py') ]
j_17MU.outputfiles = [ DiracFile("*.root") ]
myargs_17MU = ["MagUp", bach, tree]
args_for_splitting_17MU = []
files_17MU = glob.glob("/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bs2DsPi/2017/Bs2DsPi_2017_MU_MC_job*.root")
for file in files_17MU:
    args_for_splitting_17MU.append([file] + myargs_17MU)
print(args_for_splitting_17MU)
j_17MU.splitter = ArgSplitter(args= args_for_splitting_17MU)
j_17MU.backend = Condor()
j_17MU.backend.getenv = "True"
j_17MU.backend.cdf_options['+JobFlavour']='"longlunch"'

j_17MD = Job()
j_17MD.name = 'PIDGen_Bs2DsPi_17MD'
j_17MD.application.exe  = File('../run_PIDGen.sh')
j_17MD.inputfiles = [ LocalFile('../MC_PIDGen_for_Bc2BsX.py') ]
j_17MD.outputfiles = [ DiracFile("*.root") ]
myargs_17MD = ["MagDown", bach, tree]
args_for_splitting_17MD = []
files_17MD = glob.glob("/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bs2DsPi/2017/Bs2DsPi_2017_MD_MC_job*.root")
for file in files_17MD:
    args_for_splitting_17MD.append([file] + myargs_17MD)
print(args_for_splitting_17MD)
j_17MD.splitter = ArgSplitter(args= args_for_splitting_17MD)
j_17MD.backend = Condor()
j_17MD.backend.getenv = "True"
j_17MD.backend.cdf_options['+JobFlavour']='"longlunch"'

sys.exit()

# 2018
j_18MU = Job()
j_18MU.name = 'PIDGen_Bs2DsPi_18MU'
j_18MU.application.exe  = File('../run_PIDGen.sh')
j_18MU.inputfiles = [ LocalFile('../MC_PIDGen_for_Bc2BsX.py') ]
j_18MU.outputfiles = [ DiracFile("*.root") ]
myargs_18MU = ["MagUp", bach, tree]
args_for_splitting_18MU = []
files_18MU = glob.glob("/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bs2DsPi/2018/Bs2DsPi_2018_MU_MC_job*.root")
for file in files_18MU:
    args_for_splitting_18MU.append([file] + myargs_18MU)
print(args_for_splitting_18MU)
j_18MU.splitter = ArgSplitter(args= args_for_splitting_18MU)
j_18MU.backend = Condor()
j_18MU.backend.getenv = "True"
j_18MU.backend.cdf_options['+JobFlavour']='"longlunch"'

j_18MD = Job()
j_18MD.name = 'PIDGen_Bs2DsPi_18MD'
j_18MD.application.exe  = File('../run_PIDGen.sh')
j_18MD.inputfiles = [ LocalFile('../MC_PIDGen_for_Bc2BsX.py') ]
j_18MD.outputfiles = [ DiracFile("*.root") ]
myargs_18MD = ["MagDown", bach, tree]
args_for_splitting_18MD = []
files_18MD = glob.glob("/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bs2DsPi/2018/Bs2DsPi_2018_MD_MC_job*.root")
for file in files_18MD:
    args_for_splitting_18MD.append([file] + myargs_18MD)
print(args_for_splitting_18MD)
j_18MD.splitter = ArgSplitter(args= args_for_splitting_18MD)
j_18MD.backend = Condor()
j_18MD.backend.getenv = "True"
j_18MD.backend.cdf_options['+JobFlavour']='"longlunch"'
