#!/bin/bash

file=$1
magpol=$2
bach=$3
tree=$4

lb-run -c best Urania/v10r1 python MC_PIDGen_for_Bc2BsX.py -f $file -m $magpol -b $bach -t $tree
