# PIDGen Application

[PIDGen](https://twiki.cern.ch/twiki/bin/view/LHCb/MeerkatPIDResampling) application is for replacing the PID response in MC with a randomly generated distribution from calibration PDFs.

There are python scripts for PIDGen application in ganga in the [`ganga`](./ganga) folder for each $B_c^+$ decay that is relevant to the analysis that submits this for all year. For submission in ganga:
```
%ganga submit_<Bc_decay>_<Bs_decay>.py
```
The only exception is the [`submit_Bc2BsX.py`](./ganga/submit_Bc2BsX.py) and the corresponding ReDecay version where you have to specify the $B_c^+$ and $B_s^0$ decay as well as the actual particle that is reconstructed as the bachelor muon (options: "mu", "pi", "K"). For submission:
```
%ganga submit_Bc2BsX.py -c <Bc_decay> -bs <Bs_decay> -r <actual_bachelor_particle>
```

This should automatically output the application scripts of the form `<MC_file_name>_pidgen_applied.root` in the folder `/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/<Bc_decay>/<year>/PIDGen`, unless changed.

FYI the scripts [`MC_PIDGen_for_Bs2BsX.py`](./MC_PIDGen_for_Bs2BsX.py) and [`run_PIDGen.sh`](./run_PIDGen.sh) are the actual python scripts for the PID resampling. 