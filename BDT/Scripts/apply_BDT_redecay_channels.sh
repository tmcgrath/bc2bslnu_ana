#!/bin/bash

python3 apply_BDT_DsPi.py -c Bc2BsRho -r
python3 apply_BDT_DsPi.py -c Bc2BsstMuNu -r
python3 apply_BDT_JpsiPhi.py -c Bc2BsRho -r
python3 apply_BDT_JpsiPhi.py -c Bc2BsstMuNu -r
