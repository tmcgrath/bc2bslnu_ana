#
# BDT_Bc2BsX_DsPi.py
#
# Python script for BDT training and application for Bc->BsMuNu or Bc->BsPi for Bs->DsPi
#
# 02.11.22
#


# General Python packages
import uproot as ur
from root_pandas import read_root
import vector
import pandas as pd
import numpy as np
#import mplhep
#mplhep.style.use(mplhep.style.LHCb2)
import argparse
import sys, os

# Plotting
#import matplotlib
#matplotlib.font_manager._rebuild()
from matplotlib import pyplot as plt
import matplotlib.font_manager 
#print(matplotlib.font_manager.fontManager.ttflist)
from matplotlib import rc
small_size = 12
medium_size = 14
rc('font',**{'family':'serif','serif':['Roman'], 'size': medium_size})
rc('text', usetex=True)

# ML related packages
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from scipy.stats import pearsonr
from mpl_toolkits.axes_grid1 import make_axes_locatable

# For saving BDT
import pickle

##################################################
# Functions
##################################################

## general functions ##

# Apply all cuts before Bc selection 
# NOTE: should be applied AFTER redefenition
def makeBcCuts(dataframe):
    df_cuts = dataframe.query("1950 < Ds_MM & Ds_MM < 1990")
    return df_cuts

def PIDcuts(dataframe):
    df_cuts = dataframe.query()
    return df_cuts

def makeBsPlot(df):
    plt.figure(figsize=(5,5))
    plt.hist(df['Bs_MM'], bins=100, histtype='step', range=(5000, 5800), color='darkblue')
    plt.xlabel(r"$M(D_s^- \pi^+)$", fontsize='large')
    plt.ylabel(r"Candidates / ($80 MeV/c^2$)",fontsize='large')
    #plt.title(r'$B_s^0\rightarrow J/\psi\phi$', fontsize='large')
    plt.tight_layout()

def makeBcPlot(df, bachName, xmin, xmax):
    plt.figure(figsize=(5,5))
    nBins = 50
    if bach == "Pi":
        xLabel = r"$M(B_s^0 \pi^+)\ (MeV/c^2)$"
    elif bach == "Mu":
        xLabel = r"$M(B_s^0 \mu^+)\ (MeV/c^2)$"
    plt.hist(df['Bc_MM'], bins=nBins, histtype='step', range=(xmin, xmax), color='darkblue')
    plt.xlabel(xLabel, fontsize='large')
    plt.ylabel(r"Candidates / ($18 MeV/c^2$)",fontsize='large')
    #plt.title(r'$B_c\rightarrow B_s^0 \pi^+$', fontsize='large')
    plt.tight_layout()

# Getting pandas dataframe
def get_df(filename, columns):
    theDF = read_root(filename, columns=columns)
    return theDF

## Remove 2 dimensional arrays from uprooted file so that they aren't problematic
def remove_2d(branches, types):
    to_be_removed = []
    for key, value in types.items():
        if value != "bool" and value != "double" and value != "int32_t" and value != "int64_t" and value != "uint32_t" and value != "uint64_t" and value != "int16_t":
            to_be_removed.append(key)

    #print("Remove these branches:")
    #print(to_be_removed)
    for b in to_be_removed:
        if b in branches:
            branches.remove(b)

    return branches

# Function for getting a pandas dataframe of a file using uproot
def get_df2(filename, treename):
    theFile = ur.open(filename)
    if treename+";1" not in theFile.keys():
        print(f"\nERROR In file {filename}, could not find {treename}, please check file!")
        sys.exit()
    theTree = theFile[treename]
    
    # remove problematic branches
    branches = theTree.keys()
    types = theTree.typenames()
    newBranches = remove_2d(branches, types)

    theDF = theTree.arrays(newBranches, library='pd')
    return theDF

# Save figure in both png and pdf (include directory in plot name)
def saveFig(name):
    plt.savefig(name+".png")
    plt.savefig(name+".pdf")

## Defining variables ##

def defVariables(df, dtf):
    df = df.query("Bc_IPCHI2_OWNPV >= 0") # why?
    df.reset_index(inplace=True, drop=True)
    df_copy = df.copy()

    df_copy.loc[:,'sqrt_Bc_IPCHI2_OWNPV']         = np.sqrt(df.Bc_IPCHI2_OWNPV) # one negative value
    df_copy.loc[:,'sqrt_Ds_IPCHI2_OWNPV']         = np.sqrt(df.Ds_IPCHI2_OWNPV)
    df_copy.loc[:,'sqrt_Pi_from_Bs_IPCHI2_OWNPV'] = np.sqrt(df.PionBs_IPCHI2_OWNPV)
    df_copy.loc[:,'sqrt_Bs_IPCHI2_OWNPV']         = np.sqrt(df.Bs_IPCHI2_OWNPV)
    df_copy.loc[:,'sqrt_Bach_IPCHI2_OWNPV']       = np.sqrt(df.Bach_IPCHI2_OWNPV)
    df_copy.loc[:,'sqrt_K1_IPCHI2_OWNPV']         = np.sqrt(df.H1Ds_IPCHI2_OWNPV)
    df_copy.loc[:,'sqrt_K2_IPCHI2_OWNPV']         = np.sqrt(df.H2Ds_IPCHI2_OWNPV)
    df_copy.loc[:,'sqrt_Pi_IPCHI2_OWNPV']         = np.sqrt(df.H3Ds_IPCHI2_OWNPV)

    df_copy.loc[:,'Log_Bs_FD_OWNPV']   = np.log(df.Bs_FD_OWNPV)
    df_copy.loc[:,'Log_Bach_PT']       = np.log(df.Bach_PT)
    df_copy.loc[:,'Log_Pi_PT']         = np.log(df.H3Ds_PT)
    df_copy.loc[:,'Log_K1_PT']         = np.log(df.H1Ds_PT)
    df_copy.loc[:,'Log_K2_PT']         = np.log(df.H2Ds_PT)
    df_copy.loc[:,'Log_Ds_PT']         = np.log(df.Ds_PT)
    df_copy.loc[:,'Log_Pi_from_Bs_PT'] = np.log(df.PionBs_PT)

    df_copy.loc[:,'Bc_ENDVERTEX_CHI2NDOF'] = np.divide(df.Bc_ENDVERTEX_CHI2, df.Bc_ENDVERTEX_NDOF)

    Bc_mom = vector.array({"px": df.Bc_PX,
                           "py": df.Bc_PY, 
                           "pz": df.Bc_PZ,
                           "E" : df.Bc_PE})

    Bach_mom = vector.array({"px": df.Bach_PX,
                           "py": df.Bach_PY, 
                           "pz": df.Bach_PZ,
                           "E" : df.Bach_PE})

    Bach_mom_boost = Bach_mom.boostCM_of(Bc_mom)

    Bc_ENDVERTEX = vector.array({"x": df.Bc_ENDVERTEX_X,
                                 "y": df.Bc_ENDVERTEX_Y,
                                 "z": df.Bc_ENDVERTEX_Z})

    Bc_OWNPV = vector.array({"x": df.Bc_OWNPV_X,
                             "y": df.Bc_OWNPV_Y,
                             "z": df.Bc_OWNPV_Z})

    Bc_FD = Bc_ENDVERTEX - Bc_OWNPV

    Bach_mom_3vec = vector.array({"x": Bach_mom_boost.px,
                                  "y": Bach_mom_boost.py,
                                  "z": Bach_mom_boost.pz})

    Bach_CosTheta = np.divide(Bach_mom_3vec.dot(Bc_FD), Bach_mom_3vec.mag*Bc_FD.mag)

    df_copy.loc[:,'Bach_CosTheta'] = Bach_CosTheta
        
    # Decay tree fitter variables
    Bc_MDTF_Bs_ctau, Bc_MDTF_Bs_ctauS = [], []
    Bc_MDTF_M, Bc_MDTF_chi2nDOF = [], []
    for i in range(df.shape[0]):
        Bc_MDTF_chi2nDOF.append(df.Bc_MassFit_chi2[i][0]/df.Bc_MassFit_nDOF[i][0])
        Bc_MDTF_Bs_ctau.append(df.Bc_MassFit_B_s0_ctau[i][0])
        Bc_MDTF_Bs_ctauS.append(df.Bc_MassFit_B_s0_ctau[i][0] / df.Bc_MassFit_B_s0_ctauErr[i][0])
        Bc_MDTF_M.append(df.Bc_MassFit_M[i][0])
        
    df_copy.loc[:,'Bc_MDTF_chi2nDOF'] = np.array(Bc_MDTF_chi2nDOF)
    df_copy.loc[:,'Bc_MDTF_Bs_ctauS'] = np.array(Bc_MDTF_Bs_ctauS)
    df_copy.loc[:,'Bc_MDTF_Bs_ctau'] = np.array(Bc_MDTF_Bs_ctau)
    df_copy.loc[:,'Bc_MDTF_M'] = np.array(Bc_MDTF_M)

    if dtf == "DTF":
        Bc_DTF_Bs_ctau, Bc_DTF_Bs_ctauS, Bc_DTF_ctauS, Bc_DTF_ctau = [], [], [], []
        Bc_DTF_M, Bc_DTF_chi2nDOF = [], []

        for i in range(df.shape[0]):
            Bc_DTF_chi2nDOF.append(df.Bc_PVFit_chi2[i][0]/df.Bc_PVFit_nDOF[i][0])
            Bc_DTF_Bs_ctau.append(df.Bc_PVFit_B_s0_ctau[i][0])
            Bc_DTF_Bs_ctauS.append(df.Bc_PVFit_B_s0_ctau[i][0] / df.Bc_PVFit_B_s0_ctauErr[i][0])
            Bc_DTF_ctauS.append(df.Bc_PVFit_ctau[i][0]/df.Bc_PVFit_ctauErr[i][0])
            Bc_DTF_ctau.append(df.Bc_PVFit_ctau[i][0])
            Bc_DTF_M.append(df.Bc_PVFit_M[i][0])
        
        df_copy.loc[:,'Bc_DTF_Bs_ctauS'] = np.array(Bc_DTF_Bs_ctauS)
        df_copy.loc[:,'Bc_DTF_Bs_ctau'] = np.array(Bc_DTF_Bs_ctau)
        df_copy.loc[:,'Bc_DTF_ctauS'] = np.array(Bc_DTF_ctauS)
        df_copy.loc[:,'Bc_DTF_ctau'] = np.array(Bc_DTF_ctau)
        df_copy.loc[:,'Bc_DTF_chi2nDOF'] = np.array(Bc_DTF_chi2nDOF)
        df_copy.loc[:,'Bc_DTF_M'] = np.array(Bc_DTF_M)

    
    return df_copy

## BDT related functions ##

# Mimic train_test_split return df with 
def splitEvenOdd(df):
    df_even = df.copy()
    df_even = df_even.query("eventNumber % 2 == 0")
    df_odd = df.copy()
    df_odd = df_odd.query("eventNumber % 2 != 0")
    print(f"Split dataframe into lists of {len(df_even.index)} and {len(df_odd.index)}")
    return df_even, df_odd

# Reweights so that sum of signal weight = sum of background weight
def calcWeights(train_sig, train_bkg):
    weight_b = len(train_sig)/len(train_bkg) # weight of background = Ns / Nb if weight of signal is 1

    weights_s = np.ones(len(train_sig))
    weights_b = np.full((len(train_bkg)), weight_b)
    print(weights_s)
    print(weights_b)
    
    allweights = np.concatenate([weights_s, weights_b])
    return allweights

def plotVars(df_MC, df_BG, variables, nX = 4):
    plt.figure(figsize=(13,10))

    nY = int(len(variables) / nX) if len(variables) % nX == 0 else int(len(variables) / nX) + 1

    for i, variable in enumerate(variables, 1):
        xlim = np.percentile(np.hstack([df_MC[variable]]), [0.01, 99.99])
        plt.subplot(nY,nX,i)
        _, bins, _ = plt.hist(df_MC[variable], bins=50, alpha=0.7, density=True, label="Signal MC")
        plt.hist(df_BG[variable], bins=bins, alpha=0.7, density=True, label="Background sideband")
        plt.xlabel(variable)
        plt.ylabel("Candidates (AU)")
    plt.legend()
    plt.tight_layout()

def getCorrelation(df, train_vars):
    corr_matrix = np.zeros((len(train_vars), len(train_vars)))
    for i in range(len(train_vars)):
        for j in range(len(train_vars)):
            corr = pearsonr(df[train_vars[i]], df[train_vars[j]])[0]
            corr_matrix[i][j] = corr
            print(f'Correlation between {train_vars[i]} and {train_vars[j]}: {round(corr, 3)}')


    fig, ax = plt.subplots(figsize=(8,6))
    #ax = plt.gca()
    im, cbar = heatmap(corr_matrix, train_vars, train_vars, ax=ax, cmap='seismic', vmin=-1, vmax=1)
    for i in range(corr_matrix.shape[0]):
        for j in range(corr_matrix.shape[1]):
            ax.text(i, j, str(round(corr_matrix[i][j], 2)), ha="center", va="center", color="k", 
                    fontsize='small')

    fig.tight_layout()


def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    divider = make_axes_locatable(ax)
    cax1 = divider.append_axes("right", size="5%", pad=0.20)

    cbar = ax.figure.colorbar(im, cax=cax1, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom", fontsize='small')
    cbar.ax.tick_params(labelsize='small') 


    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False, labelsize='small')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    #ax.spines.right.set_visible(False)
    #ax.spines.bottom.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    #ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False, labelsize='small')

    return im, cbar

def plotResults(test_output, train_output, Y_test, Y_train):    
    plot_colors = "br"
    class_names = ["Background", "Signal"] # for background and signal
    #twoclass_output = bdt.decision_function(X_train)
    plot_range = (train_output.min(), train_output.max())
    #test_output = bdt.decision_function(X_test)

    # Plot BDT scores and ROC curve

    plt.figure()
    for i, n, c in zip(range(2), class_names, plot_colors):
        plt.hist(train_output[Y_train == i],
                 bins=30,
                 range=plot_range,
                 facecolor=c,
                 label='%s (train)' % n,
                 alpha=.5,
                 edgecolor='k',
                 density=True,
                 histtype='stepfilled')
        hist, bins = np.histogram(test_output[Y_test == i],
                                  bins=30,
                                  range=plot_range,
                                  density=True)
        scale = len(test_output)/sum(hist)
        err = np.sqrt(hist*scale)/scale
        width = (bins[1] - bins[0])
        centre = (bins[:-1] + bins[1:])/2
        plt.errorbar(centre, hist, yerr=err, fmt='o', c=c, label='%s (test)' % n)

    x1, x2, y1, y2 = plt.axis()
    plt.axis((x1, x2, y1, y2 * 1.2))
    plt.legend()
    plt.xlim([plot_range[0],plot_range[1]])
    plt.ylabel('Samples', fontsize='large')
    plt.xlabel('BDT Decision Score', fontsize='large')
    #plt.title('Decision Scores')

def makeROCcurve(Y_test, test_output):

    fpr, tpr, thresholds = roc_curve(Y_test, test_output)
    roc_auc = roc_auc_score(Y_test, test_output)

    plt.figure()

    plt.plot(fpr, tpr, lw=1, label='ROC (area = %0.2f)'%(roc_auc))

    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate', fontsize='large')
    plt.ylabel('True Positive Rate', fontsize='large')
    #plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.grid()

    plt.gca().set_aspect('equal', 'box')

    return roc_auc

def BDTcut(Y_test, test_output, df_sig, df_BG, Punzi=False):
    print("calculating Punzi/significance")
    fpr, tpr, thresholds = roc_curve(Y_test, test_output)

    sig = df_sig.shape[0]
    nsig = tpr[1:]*sig
    bkg = df_BG.shape[0]
    nbkg = fpr[1:]*bkg
    significance = nsig/np.sqrt(nsig+nbkg)
    sig_eff = nsig/sig
    bkg_eff = nbkg/bkg

    index = np.argmax(significance)
    cut = thresholds[index]
    
    punzi = sig_eff/(5/2 + np.sqrt(nbkg))
    #print(punzi)
    index = np.argmax(punzi)
    punzicut = thresholds[index]
    
    _, ax = plt.subplots()
    ax2 = ax.twinx()
    line1 = ax.plot(thresholds[1:], significance, label='Significance')
    #ax.set_xlabel('BDT variable (cut '+str(round(cut,2))+')')
    ax.set_xlabel("BDT Decision Score")
    ax.set_ylabel('Significance')
    ax2.set_ylabel('Efficiency')
    line2 = ax2.plot(thresholds[1:], sig_eff, color='darkorange', label='Signal Efficiency')
    line3 = ax2.plot(thresholds[1:], bkg_eff, color='forestgreen', label='Background Efficiency')
    ax2.plot(np.ones(100)*cut, np.linspace(-0.05,1.05,100), color='grey', linestyle='dashed')
    ax2.set_ylim(0,1)
    lines = line1+line2+line3
    labels = [l.get_label() for l in lines]
    ax.legend(lines, labels, loc=0, fontsize='small')
    ax.set_xlim(-4,4)
    
    if Punzi == True:
        plt.figure()
        plt.plot(thresholds[1:], punzi, label='Punzi', color='darkred')
        ax2.plot(np.ones(100)*punzicut, np.linspace(-0.05,1.05,100), color='grey', linestyle='dashed')
        plt.xlabel("BDT Decision Score")
        plt.ylabel(r'$\epsilon_s\ /\ (5/2+\sqrt{b}$)')
        cut = punzicut
    
    return cut

# Applied both even and odd BDT and then takes the one of the opposite index
def applyBDT(df, vars, bdt_even, bdt_odd, bdt_type="Bc"):
    if bdt_type != "Bc" and bdt_type != "Bs":
        print("Check BDT type name in BDT application! (Should be Bc or Bs)")

    new_df = df.copy()

    bdt_even_result = bdt_even.decision_function(new_df[vars])
    bdt_odd_result  = bdt_odd.decision_function(new_df[vars])

    new_df.loc[:,f'{bdt_type}BDT_even_score'] = bdt_even_result
    new_df.loc[:,f'{bdt_type}BDT_odd_score'] = bdt_odd_result

    # if even index keep the odd and if odd index keep the even
    new_df.loc[:,f'{bdt_type}BDT_score'] =  np.where(new_df.loc[:,"eventNumber"] % 2 == 0, new_df.loc[:,f'{bdt_type}BDT_odd_score'], new_df.loc[:,f'{bdt_type}BDT_even_score'])

    print(new_df[["eventNumber", f'{bdt_type}BDT_even_score', f'{bdt_type}BDT_odd_score', f'{bdt_type}BDT_score']])

    return new_df


##################################################
# Arguments
##################################################

columns = ["Bs_MM", "Bc_ENDVERTEX_CHI2", "Bc_ENDVERTEX_NDOF", "Bc_PT", "H3Ds_PT", "H3Ds_ProbNNpi",
            "Bc_IPCHI2_OWNPV", "Bs_IPCHI2_OWNPV", "Bach_IPCHI2_OWNPV", "PionBs_IPCHI2_OWNPV", "Ds_IPCHI2_OWNPV", "H1Ds_IPCHI2_OWNPV",
            "H2Ds_IPCHI2_OWNPV", "H3Ds_IPCHI2_OWNPV",
            "Bs_PT", "Ds_PT", "PionBs_PT", "H1Ds_PT", "H2Ds_PT", "H3Ds_PT",
            "Ds_ENDVERTEX_CHI2", "Bs_ENDVERTEX_CHI2", "Bs_ENDVERTEX_NDOF", "Bs_FD_OWNPV", "Bs_TAU",
            "Bc_TAU", "Bc_TAUERR", "Bc_TAUCHI2", "Ds_MM",
            "Bc_DIRA_OWNPV", "Ds_ID", "PionBs_ID",
            "Bc_MM", "H3Ds_PT", "H3Ds_IPCHI2_OWNPV", 
            "H1Ds_ProbNNk", "H2Ds_ProbNNk", "PionBs_ProbNNpi", "H3Ds_ProbNNghost",
            "H3Ds_ProbNNghost", "H1Ds_PIDK", "H2Ds_PIDK", "H3Ds_PIDK", "PionBs_PIDK",
            "H3Ds_PX", "H3Ds_PY", "H3Ds_PZ", "H3Ds_P", "H3Ds_PE", "H3Ds_M",
            "Bach_PX", "Bach_PY", "Bach_PZ", "Bach_P", "Bach_PE", "Bach_PT", "Bach_P", "Bach_ETA",
            "Bs_PX", "Bs_PY", "Bs_PZ", "Bs_P", "Bs_PE", "Bs_LV02", "Bs_ETA",
            "Bs_OWNPV_X", "Bs_OWNPV_Y", "Bs_OWNPV_Z", "Bs_ENDVERTEX_X", "Bs_ENDVERTEX_Y", "Bs_ENDVERTEX_Z",
            "Bc_MassFit_chi2", "Bc_LV02", "Bc_PVFit_nPV",
            "Bc_OWNPV_X", "Bc_OWNPV_Y", "Bc_OWNPV_Z", "Bc_ENDVERTEX_X", "Bc_ENDVERTEX_Y", "Bc_ENDVERTEX_Z",
            "Bc_PX", "Bc_PY", "Bc_PZ", "Bc_PE", "Bc_P", "Bc_ETA", "Bc_CORR_M", 
            "Bc_PVFit_chi2", "Bc_PVFit_nDOF", "Bc_PVFit_ctau", "Bc_PVFit_ctauErr", "Bc_PVFit_B_s0_ctau", "Bc_PVFit_B_s0_ctauErr", "Bc_PVFit_M", 
            "Bc_MassFit_chi2", "Bc_MassFit_nDOF", "Bc_MassFit_B_s0_ctau", "Bc_MassFit_B_s0_ctauErr", "Bc_MassFit_M", 
            "runNumber", "eventNumber"
            ]

if __name__ == "__main__":
    
    # Check directory -> changes output location
    currentDir = os.path.dirname(os.path.abspath(__file__))
    # if on manchester clusters
    if "tamaki" in currentDir:
        outDir = "/hepgpu5-data3/tamaki"
        cDir = "."
    # lxplus or condor
    else:
        outDir = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana"
        cDir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/BDT"

    parser=argparse.ArgumentParser()
    parser.add_argument('--year','-y',type=int,default = 0,choices=[0, 2016, 2017, 2018], help='Enter year of data and MC you would like to run the BDT or 0 for all years combined (2016-2018)')
    parser.add_argument('--bachelor','-b',required=True, type=str, choices=["Mu", "Pi"], help='Bachelor particle of decay to train BDT for (Mu or Pi)')
    parser.add_argument('--pv_fit', '-p', action='store_true', help='Flag whether to use PV fit DTF variables (for Bc2BsPi only)')
    args = parser.parse_args()

    year = args.year
    bach = args.bachelor
    PV_fit = args.pv_fit
    channel = "Bs2DsPi"

    # For testing condor
    #print("Condor test")
    #print(f"{year}, {bach}, {PV_fit}, DsPi")
    #print(f"{outDir}")
    #print(f"{cDir}")
    #sys.exit()

    if bach == "Mu": 
        mcbach = "MuNu"
        fitType = "sig"
        sim = "h"
        TMString = "Bc_is_from_Bc_BsMuNu"

        # Check the PV fit flag isnt on for this
        if PV_fit == True:
            print("PV fit DTF use set to True for Bc2BsMuNu, this is bad, setting to False instead")
            PV_fit = False
    else: 
        fitType = "norm"
        mcbach = "Pi"
        sim = "k"
        TMString = "Bc_is_from_Bc_BsPi"

    # Get data file for both MU and MD for specified year and bachelor particle
    if year == 0:
        # Get all data 
        print("Loading data and MC for all years")
        data_MU_file_16 = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_2016_MU_reduced.root"
        data_MD_file_16 = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_2016_MD_reduced.root"
        data_MU_file_17 = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_2017_MU_reduced.root"
        data_MD_file_17 = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_2017_MD_reduced.root"
        data_MU_file_18 = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_2018_MU_reduced.root"
        data_MD_file_18 = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_2018_MD_reduced.root"

        mc_MU_file_16 = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_2016_MU_reduced.root"
        mc_MD_file_16 = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_2016_MD_reduced.root"
        mc_MU_file_17 = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_2017_MU_reduced.root"
        mc_MD_file_17 = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_2017_MD_reduced.root"
        mc_MU_file_18 = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_2018_MU_reduced.root"
        mc_MD_file_18 = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_2018_MD_reduced.root"

    else:
        data_MU_file = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_{year}_MU_reduced.root"
        data_MD_file = f"{outDir}/DataReduced/Bc2Bs{bach}_DsPi_{year}_MD_reduced.root"
        print(data_MU_file)

        mc_MU_file = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_{year}_MU_reduced.root"
        mc_MD_file = f"{outDir}/MCReduced/Bc2Bs{mcbach}_DsPi_MC_sim09{sim}_{year}_MD_reduced.root"
        print(mc_MU_file)

    # Output folder for plots and root files
    saved_year = "All" if year == 0 else str(year)
    plotSuffix = f"{channel}_{saved_year}"
    plotFolder = f"{cDir}/Figs/Bc2Bs{bach}/{saved_year}_kFold/"
    rootFolder_data = f"{outDir}/DataBDTApplied/"
    rootFolder_MC   = f"{outDir}/MCBDTApplied/"
    BDTFolder = f"{cDir}/BDTModel/"
    logFilename = f"{BDTFolder}{fitType}_BDT_log_{plotSuffix}.txt"

    # Checks
    print(f"Running BDT for Bc->Bs{bach} ({channel}) using")
    if year != 0:
        print(f"Data files: {data_MU_file} & {data_MD_file}")
        print(f"MC files: {mc_MU_file} & {mc_MD_file}")
    if PV_fit == False:
        dtf = "MDTF"
        DTFVars = ["Bc_MDTF_Bs_ctau", "Bc_MDTF_Bs_ctauS", "Bc_MDTF_chi2nDOF"]
        varsX, varsY = 4, 3
        print("DTF: Mass constraint")
    else:
        dtf = "DTF"
        DTFVars = ["Bc_DTF_Bs_ctau", "Bc_DTF_Bs_ctauS", "Bc_DTF_ctau", "Bc_DTF_ctauS", "Bc_DTF_chi2nDOF"]
        varsX, varsY = 4, 4
        print("DTF: PV and mass constraint (To be used on Bc->Bspi Only)")


    # Function to plot the BDT score, roc curve and significance curve all in one go and saves
    def makeAllPlots(test_output, train_output, Y_test, Y_train, df_sig, df_bkg, Punzi=True, bdt_part = "Bc", bdt_version=""):

        # for saving plots
        if bdt_version != "" and bdt_version[-1] != "_":
            bdt_version = bdt_version+"_"

        # Plot BDT scores
        plotResults(test_output, train_output, Y_test, Y_train)
        #plt.show()
        saveFig(f"{plotFolder}{bdt_part}_BDTscores_{bdt_version}{plotSuffix}")

        # plot roc curves
        roc_auc_value = makeROCcurve(Y_test, test_output)
        #plt.show()
        saveFig(f"{plotFolder}{bdt_part}_ROCcurve_{bdt_version}{plotSuffix}")

        # significance estimate
        cut = BDTcut(Y_test, test_output, df_sig, df_bkg, Punzi=Punzi )
        saveFig(f'{plotFolder}{bdt_part}_significance_curve_{bdt_version}{plotSuffix}')

        return cut, roc_auc_value

##################################################
# Main execution
##################################################

    print("Starting script....")

    ## Data ##

    # Both MU+MD
    print("Loading data.......................")
    if year == 0:
        df_MU_16 = get_df(data_MU_file_16, columns)
        df_MD_16 = get_df(data_MD_file_16, columns)
        df_MU_17 = get_df(data_MU_file_17, columns)
        df_MD_17 = get_df(data_MD_file_17, columns)
        df_MU_18 = get_df(data_MU_file_18, columns)
        df_MD_18 = get_df(data_MD_file_18, columns)
        df_MU_16.loc[:, 'year'] = 2016
        df_MD_16.loc[:, 'year'] = 2016
        df_MU_17.loc[:, 'year'] = 2017
        df_MD_17.loc[:, 'year'] = 2017
        df_MU_18.loc[:, 'year'] = 2018
        df_MD_18.loc[:, 'year'] = 2018
        data_list = [df_MU_16, df_MD_16, df_MU_17, df_MD_17, df_MU_18, df_MD_18]
        data = pd.concat(data_list, ignore_index=True)

    else:
        df_MU = get_df(data_MU_file, columns)
        df_MD = get_df(data_MD_file, columns)
        data = pd.concat([df_MD, df_MU], ignore_index=True)

    # Redefine variables
    df_data = defVariables(data, dtf)
    df_data.reset_index(inplace=True, drop=True)
    df_data

    ## MC ##

    print("Loading MC.......................")
    MCcolumns = columns+[TMString, "Bs_is_from_Bs_DsPi"]
    if bach == "Mu":
        MCcolumns = MCcolumns + ["Bc_MC_MOTHER_ID", "Bc_TRUEID", "Bc_MC_MOTHER_KEY",
                                "Bach_MC_MOTHER_ID", "Bach_MC_GD_MOTHER_ID", "Bach_MC_MOTHER_KEY", "Bach_MC_GD_MOTHER_KEY", "Bach_TRUEID",
                                "Bs_MC_MOTHER_ID", "Bs_MC_GD_MOTHER_ID", "Bs_MC_MOTHER_KEY", "Bs_MC_GD_MOTHER_KEY", "Bs_TRUEID"]
    if year == 0:
        df_MC_MU_16 = get_df(mc_MU_file_16, MCcolumns)
        df_MC_MD_16 = get_df(mc_MD_file_16, MCcolumns)
        df_MC_MU_17 = get_df(mc_MU_file_17, MCcolumns)
        df_MC_MD_17 = get_df(mc_MD_file_17, MCcolumns)
        df_MC_MU_18 = get_df(mc_MU_file_18, MCcolumns)
        df_MC_MD_18 = get_df(mc_MD_file_18, MCcolumns)
        df_MC_MU_16.loc[:, 'year'] = 2016
        df_MC_MD_16.loc[:, 'year'] = 2016
        df_MC_MU_17.loc[:, 'year'] = 2017
        df_MC_MD_17.loc[:, 'year'] = 2017
        df_MC_MU_18.loc[:, 'year'] = 2018
        df_MC_MD_18.loc[:, 'year'] = 2018
        MC_list = [df_MC_MU_16, df_MC_MD_16, df_MC_MU_17, df_MC_MD_17, df_MC_MU_18, df_MC_MD_18]
        MC = pd.concat(MC_list, ignore_index=True)
        
    else:
        df_MC_MU = get_df(mc_MU_file, MCcolumns)
        df_MC_MD = get_df(mc_MD_file, MCcolumns)
        MC = pd.concat([df_MC_MD, df_MC_MU], ignore_index=True)

    # Make dataframe for BDT and redefine variables
    df_MC = defVariables(MC, dtf)
    print(df_MC)

    # Save plot for beforehand
    makeBsPlot(df_MC)
    saveFig(f"{plotFolder}Bs_MM_MC_{plotSuffix}")

    ## Apply Bc cuts to sample ##
    print("Redefining variables....")
    df_data_cut = makeBcCuts(df_data)
    df_data_cut.reset_index(inplace=True, drop = True) # need index after the cuts
    df_MC_cut = makeBcCuts(df_MC)
    df_MC_cut.reset_index(inplace=True, drop = True)

    # Set df for saving later
    df_data_toSave = df_data_cut.copy()
    df_MC_toSave = df_MC_cut.copy()


    #### Bs BDT ####

    print("\nBs BDT Training\n")
    # Make Bs before BDT plot
    def makeBsPlot(df):
        plt.figure(figsize=(5,5))
        plt.hist(df['Bs_MM'], bins=100, histtype='step', range=(5200, 5900), color='darkblue')
        plt.xlabel(r"$M(D_s^- \pi^+)\ (MeV/c^2)$", fontsize='large')
        plt.ylabel(r"Candidates / ($70 MeV/c^2$)",fontsize='large')
        #plt.title(r'$B_s\rightarrow D_s\pi$', fontsize='large')
        # Bs_MM > 5300 & Bs_MM < 5420
        top = plt.gca().get_ylim()[1]
        plt.plot([5300,5300], [0,top], linestyle='--', linewidth=1, color='darkred')
        plt.plot([5420,5420], [0,top], linestyle='--', linewidth=1, color='darkred')
        plt.ylim(top=top)
        plt.tight_layout()
    makeBsPlot(df_data_cut)
    saveFig(f"{plotFolder}Bs_MM_before_BDT")


    # Make background dataframe using both Bs upper sideband
    df_Bs_BG = df_data_cut.query("Bs_MM>5600 & Bs_MM<5950")
    df_Bs_BG.reset_index(inplace=True)
    df_Bs_BG.dropna(inplace=True)

    # Make signal dataframe
    df_Bs_SG = df_MC_cut.query(f"{TMString} == 1 & Bs_is_from_Bs_DsPi == 1")
    df_Bs_SG.reset_index(inplace=True)
    df_Bs_SG.dropna(inplace=True)
    makeBsPlot(df_Bs_SG)
    saveFig(f"{plotFolder}Bs_MM_TMMC")

    # Training Variables
    train_vars_Bs = ["sqrt_Pi_from_Bs_IPCHI2_OWNPV", "sqrt_Ds_IPCHI2_OWNPV",
                    "sqrt_Pi_IPCHI2_OWNPV", "sqrt_K1_IPCHI2_OWNPV", "sqrt_K2_IPCHI2_OWNPV",
                    "Log_Pi_from_Bs_PT", "Log_Ds_PT", "Log_Pi_PT", "Log_K1_PT", "Log_K2_PT",
                    "Ds_ENDVERTEX_CHI2", "Bs_ENDVERTEX_CHI2", "Log_Bs_FD_OWNPV", "Bs_LV02"]
    print("Training with training variables: ")
    print(train_vars_Bs)

    plotVars(df_Bs_SG, df_Bs_BG, train_vars_Bs)
    saveFig(f"{plotFolder}Bs_BDTinputvars_{year}")

    # Get correlation maps
    getCorrelation(df_Bs_BG, train_vars_Bs)
    saveFig(f'{plotFolder}correlation_heatmap_Bs_BG_{plotSuffix}')
    getCorrelation(df_Bs_SG, train_vars_Bs)
    saveFig(f'{plotFolder}correlation_heatmap_Bs_SG_{plotSuffix}')

    # split df_SG and df_BG
    Bs_BG_even, Bs_BG_odd = splitEvenOdd(df_Bs_BG)
    Bs_SG_even, Bs_SG_odd = splitEvenOdd(df_Bs_SG)

    # Check
    print("BG even:")
    print(Bs_BG_even[["eventNumber"]])
    print("BG odd:")
    print(Bs_BG_odd[["eventNumber"]])

    print("Signal even:")
    print(Bs_SG_even[["eventNumber"]])
    print("Signal odd:")
    print(Bs_SG_odd[["eventNumber"]])

    # 2-fold cross val
    Bs_bdt_trainings = { "even" : { "train": [Bs_BG_even, Bs_SG_even], "test": [Bs_BG_odd, Bs_SG_odd]},
                      "odd":   { "train": [Bs_BG_odd, Bs_SG_odd], "test": [Bs_BG_even, Bs_SG_even]}
                    }

    # make logfile
    with open(logFilename, "w") as f:
        f.write(f"BDT training for Bc2Bs{bach}, Bs2{channel}, for year '{saved_year}' \n\n")

    # Train even event BDT and odd event BDT
    forBsBDT = {}
    for name, bdt_instance in Bs_bdt_trainings.items():

        print(f"On {name} Bs BDT, so test sample is the opposite....\n")

        # Configure BDT
        df_train_Bs, df_test_Bs = bdt_instance["train"][0], bdt_instance["test"][0]
        MC_train_Bs, MC_test_Bs = bdt_instance["train"][1], bdt_instance["test"][1]

        X_train_Bs = np.concatenate([MC_train_Bs[train_vars_Bs], df_train_Bs[train_vars_Bs]])
        X_test_Bs  = np.concatenate([MC_test_Bs[train_vars_Bs], df_test_Bs[train_vars_Bs]])

        Y_train_Bs = np.concatenate([np.ones(len(MC_train_Bs)),np.zeros(len(df_train_Bs))])
        Y_test_Bs  = np.concatenate([np.ones(len(MC_test_Bs)),np.zeros(len(df_test_Bs))])

        # Reweight so that the sum of weights of signal and background is equal
        weights = calcWeights( MC_train_Bs, df_train_Bs ) 

        print("\nTraining BDT........................")

        bdt_Bs = GradientBoostingClassifier(loss='deviance', n_estimators=100, learning_rate=0.1, max_depth=3, random_state=0)
        bdt_Bs.fit(X_train_Bs, Y_train_Bs, sample_weight=weights)

        index = np.argsort(-bdt_Bs.feature_importances_)
        with open(logFilename, "a") as f:
            f.write(f"Variable importance Bs BDT {name}:\n")
            for i in index:
                item = f"{train_vars_Bs[i]} : {bdt_Bs.feature_importances_[i]}"
                print(item)
                f.write(item + "\n")
            f.write("\n")

        # Save Bs BDT
        print(f"Saving {name} Bs BDT model to: {BDTFolder}{fitType}_Bs_BDT_{name}_{plotSuffix}.sav")
        pickle.dump(bdt_Bs, open(f"{BDTFolder}{fitType}_Bs_BDT_{name}_{plotSuffix}.sav", 'wb'))

        #some results for plotting
        train_result = bdt_Bs.decision_function(X_train_Bs) # first round applying even BDT on even train sample
        test_result = bdt_Bs.decision_function(X_test_Bs)   # first round applying even BDT on odd test sample

        # container for results, hopefully no memory leak
        container = {}
        container["BDT"]  = bdt_Bs
        container["X_test"], container["X_train"] = X_test_Bs, X_train_Bs
        container["Y_test"], container["Y_train"] = Y_test_Bs, Y_train_Bs
        container["test_result"], container["train_result"] = test_result, train_result
        forBsBDT[name] = container

    print("Saving Bs BDT results........................")

    # combine decision outputs
    tot_X_test_Bs = np.concatenate( [forBsBDT["even"]["X_test"], forBsBDT["odd"]["X_test"]] ) # test is odd for first and even for second
    tot_X_train_Bs = np.concatenate( [forBsBDT["even"]["X_train"], forBsBDT["odd"]["X_train"]] )
    tot_Y_test_Bs = np.concatenate( [forBsBDT["even"]["Y_test"], forBsBDT["odd"]["Y_test"]] )
    tot_Y_train_Bs = np.concatenate( [forBsBDT["even"]["Y_train"], forBsBDT["odd"]["Y_train"]] )
    tot_test_result_Bs = np.concatenate( [forBsBDT["even"]["test_result"], forBsBDT["odd"]["test_result"]] )
    tot_train_result_Bs = np.concatenate( [forBsBDT["even"]["train_result"], forBsBDT["odd"]["train_result"]] )

    len_even = len( forBsBDT["even"]["test_result"] ); len_odd = len( forBsBDT["odd"]["test_result"] ); length = len( tot_test_result_Bs )

    # plot result for total BDT
    total_cut_Bs, total_roc_auc_value = makeAllPlots(tot_test_result_Bs, tot_train_result_Bs, tot_Y_test_Bs, tot_Y_train_Bs, df_Bs_SG, df_Bs_BG, Punzi=False, bdt_part="Bs")
    # even bdt
    even_cut_Bs, even_roc_auc_value = makeAllPlots(forBsBDT["even"]["test_result"], forBsBDT["even"]["train_result"], forBsBDT["even"]["Y_test"], forBsBDT["even"]["Y_train"], Bs_SG_even, Bs_BG_even, Punzi=False, bdt_part="Bs", bdt_version = "even")
    # odd bdt
    odd_cut_Bs, odd_roc_auc_value = makeAllPlots(forBsBDT["odd"]["test_result"], forBsBDT["odd"]["train_result"], forBsBDT["odd"]["Y_test"], forBsBDT["odd"]["Y_train"], Bs_SG_odd, Bs_BG_odd, Punzi=False, bdt_part="Bs", bdt_version = "odd") 

    # outputs
    print(f"BDT cut for Bs sel = {total_cut_Bs}")
    print(f"For even BDT = {even_cut_Bs}, for odd BDT = {odd_cut_Bs}")
    with open(logFilename, "a") as f:
        f.write(f"Bs BDT even ROC AUC = {even_roc_auc_value}")
        f.write(f"BDT even cut for Bs sel = {even_cut_Bs}\n")
        f.write(f"Bs BDT odd ROC AUC = {odd_roc_auc_value}")
        f.write(f"BDT odd cut for Bs sel = {odd_cut_Bs}\n")
        f.write(f"Bs BDT ROC AUC = {total_roc_auc_value}")
        f.write(f"BDT cut for Bs sel = {total_cut_Bs}\n")

    # Apply Bs BDT to root files
    col_to_drop = []
    for col in df_data_toSave.columns:
        if "PVFit" in col or "MassFit" in col:
            print(f"dropping {col}")
            col_to_drop.append(col)
    df_data_toSave = df_data_toSave.drop(columns=col_to_drop)
    df_MC_toSave = df_MC_toSave.drop(columns=col_to_drop)
    df_data_toSave = applyBDT(df_data_toSave, train_vars_Bs, forBsBDT["even"]["BDT"], forBsBDT["odd"]["BDT"], bdt_type="Bs")
    df_MC_toSave = applyBDT(df_MC_toSave, train_vars_Bs, forBsBDT["even"]["BDT"], forBsBDT["odd"]["BDT"], bdt_type="Bs")

    # Apply BDT to samples for Bc BDT training
    df_Bs_BDT = df_data_toSave.query(f'BsBDT_score > {total_cut_Bs}')
    df_Bs_BDT_MC = df_MC_toSave.query(f"{TMString} == 1 & Bs_is_from_Bs_DsPi == 1").query(f'BsBDT_score > {total_cut_Bs}') # has truth matching already applied

    # Save Bs plot after BDT
    makeBsPlot(df_Bs_BDT)
    saveFig(f"{plotFolder}Bs_MM_after_BDT")



    #### Bc BDT ####

    print("\nBc BDT Training\n")

    # Make background dataframe using upper Bc sideband
    df_Bc_BG = df_Bs_BDT.query("Bc_MM>6600 & Bc_MM<7500 & Bs_MM > 5300 & Bs_MM < 5420")
    df_Bc_BG.reset_index(inplace=True)

    # Plot background distribution ONLY
    makeBcPlot(df_Bc_BG, bach, 6600, 7500)
    saveFig(f"{plotFolder}Bc_MM_BG_sample_{plotSuffix}")

    # Make signal TM df
    df_Bc_SG = df_Bs_BDT_MC.query("Bs_MM > 5300 & Bs_MM < 5420")
    df_Bc_SG.reset_index(inplace=True)

    # Training Variables
    train_vars_Bc = ["sqrt_Bc_IPCHI2_OWNPV", "sqrt_Bs_IPCHI2_OWNPV", "sqrt_Bach_IPCHI2_OWNPV", "Bs_PT", "Bach_PT",
                    "Bc_TAU", "Bc_DIRA_OWNPV"] + DTFVars

    print("Training with training variables: ")
    print(train_vars_Bc)

    plotVars(df_Bc_SG, df_Bc_BG, train_vars_Bc)
    saveFig(f"{plotFolder}Bc_BDTinputvars_{plotSuffix}")

    # Get correlation maps
    getCorrelation(df_Bc_BG, train_vars_Bc)
    saveFig(f'{plotFolder}correlation_heatmap_Bc_BG_{plotSuffix}')
    getCorrelation(df_Bc_SG, train_vars_Bc)
    saveFig(f'{plotFolder}correlation_heatmap_Bc_SG_{plotSuffix}')

    # split df_SG and df_BG
    Bc_BG_even, Bc_BG_odd = splitEvenOdd(df_Bc_BG)
    Bc_SG_even, Bc_SG_odd = splitEvenOdd(df_Bc_SG)

    # Check
    print("BG even:")
    print(Bc_BG_even[["eventNumber"]])
    print("BG odd:")
    print(Bc_BG_odd[["eventNumber"]])

    print("Signal even:")
    print(Bc_SG_even[["eventNumber"]])
    print("Signal odd:")
    print(Bc_SG_odd[["eventNumber"]])

    # 2-fold cross val
    Bc_bdt_trainings = { "even" : { "train": [Bc_BG_even, Bc_SG_even], "test": [Bc_BG_odd, Bc_SG_odd]},
                         "odd":   { "train": [Bc_BG_odd, Bc_SG_odd], "test": [Bc_BG_even, Bc_SG_even]}
                        }

    # Train even event BDT and odd event BDT
    forBcBDT = {}
    for name, bdt_instance in Bc_bdt_trainings.items():

        print(f"On {name} Bc BDT, so test sample is the opposite....\n")

        # Configure BDT
        df_train_Bc, df_test_Bc = bdt_instance["train"][0], bdt_instance["test"][0]
        MC_train_Bc, MC_test_Bc = bdt_instance["train"][1], bdt_instance["test"][1]

        X_train_Bc = np.concatenate([MC_train_Bc[train_vars_Bc], df_train_Bc[train_vars_Bc]])
        X_test_Bc  = np.concatenate([MC_test_Bc[train_vars_Bc], df_test_Bc[train_vars_Bc]])

        Y_train_Bc = np.concatenate([np.ones(len(MC_train_Bc)),np.zeros(len(df_train_Bc))])
        Y_test_Bc  = np.concatenate([np.ones(len(MC_test_Bc)),np.zeros(len(df_test_Bc))])

        # Reweight so that the sum of weights of signal and background is equal
        weights_2 = calcWeights( MC_train_Bc, df_train_Bc ) 

        print("\nTraining BDT........................")

        bdt_Bc = GradientBoostingClassifier(loss='deviance', n_estimators=100, learning_rate=0.1, max_depth=3, random_state=0)
        bdt_Bc.fit(X_train_Bc, Y_train_Bc, sample_weight=weights_2)

        index = np.argsort(-bdt_Bc.feature_importances_)
        with open(logFilename, "a") as f:
            f.write(f"Variable importance Bc BDT {name}:\n")
            for i in index:
                item = f"{train_vars_Bc[i]} : {bdt_Bc.feature_importances_[i]}"
                print(item)
                f.write(item + "\n")
            f.write("\n")

        # Save Bc BDT
        print(f"Saving {name} Bc BDT model to: {BDTFolder}{fitType}_Bc_BDT_{name}_{plotSuffix}.sav")
        pickle.dump(bdt_Bc, open(f"{BDTFolder}{fitType}_Bc_BDT_{name}_{plotSuffix}.sav", 'wb'))

        #some results for plotting
        train_result_Bc = bdt_Bc.decision_function(X_train_Bc) # first round applying even BDT on even train sample
        test_result_Bc = bdt_Bc.decision_function(X_test_Bc)   # first round applying even BDT on odd test sample

        # container for results, hopefully no memory leak
        container = {}
        container["BDT"]  = bdt_Bc
        container["X_test"], container["X_train"] = X_test_Bc, X_train_Bc
        container["Y_test"], container["Y_train"] = Y_test_Bc, Y_train_Bc
        container["test_result"], container["train_result"] = test_result_Bc, train_result_Bc
        forBcBDT[name] = container


    print("Saving results........................")

    # combine decision outputs
    tot_X_test = np.concatenate( [forBcBDT["even"]["X_test"], forBcBDT["odd"]["X_test"]] ) # test is odd for first and even for second
    tot_X_train = np.concatenate( [forBcBDT["even"]["X_train"], forBcBDT["odd"]["X_train"]] )
    tot_Y_test = np.concatenate( [forBcBDT["even"]["Y_test"], forBcBDT["odd"]["Y_test"]] )
    tot_Y_train = np.concatenate( [forBcBDT["even"]["Y_train"], forBcBDT["odd"]["Y_train"]] )
    tot_test_result = np.concatenate( [forBcBDT["even"]["test_result"], forBcBDT["odd"]["test_result"]] )
    tot_train_result = np.concatenate( [forBcBDT["even"]["train_result"], forBcBDT["odd"]["train_result"]] )

    # plot result for total BDT
    total_cut, total_roc_auc_value = makeAllPlots(tot_test_result, tot_train_result, tot_Y_test, tot_Y_train, df_Bc_SG, df_Bc_BG)
    # even bdt
    even_cut, even_roc_auc_value = makeAllPlots(forBcBDT["even"]["test_result"], forBcBDT["even"]["train_result"], forBcBDT["even"]["Y_test"], forBcBDT["even"]["Y_train"], Bc_SG_even, Bc_BG_even, bdt_version = "even")
    # odd bdt
    odd_cut, odd_roc_auc_value = makeAllPlots(forBcBDT["odd"]["test_result"], forBcBDT["odd"]["train_result"], forBcBDT["odd"]["Y_test"], forBcBDT["odd"]["Y_train"], Bc_SG_odd, Bc_BG_odd, bdt_version = "odd") 

    # outputs
    print(f"BDT cut for Bc sel = {total_cut}")
    print(f"For even BDT = {even_cut}, for odd BDT = {odd_cut}")
    with open(logFilename, "a") as f:
        f.write(f"Bc BDT even ROC AUC = {even_roc_auc_value}")
        f.write(f"BDT even cut for Bc sel = {even_cut}\n")
        f.write(f"Bc BDT odd ROC AUC = {odd_roc_auc_value}")
        f.write(f"BDT odd cut for Bc sel = {odd_cut}\n")
        f.write(f"Bc BDT ROC AUC = {total_roc_auc_value}")
        f.write(f"BDT cut for Bc sel = {total_cut}\n")

    # Apply BDT to root files
    df_MC_toSave = applyBDT(df_MC_toSave, train_vars_Bc, forBcBDT["even"]["BDT"], forBcBDT["odd"]["BDT"])
    df_data_toSave = applyBDT(df_data_toSave, train_vars_Bc, forBcBDT["even"]["BDT"], forBcBDT["odd"]["BDT"])

    # Save root fill
    print("Saving BDT application to:")
    print(f"{rootFolder_data}Bc2Bs{bach}_{plotSuffix}_BDT.root")
    print(f"{rootFolder_MC}Bc2Bs{bach}_{plotSuffix}_MC_BDT.root")
    outdata = ur.recreate(f"{rootFolder_data}Bc2Bs{bach}_{plotSuffix}_BDT.root")
    outMC   = ur.recreate(f"{rootFolder_MC}Bc2Bs{bach}_{plotSuffix}_MC_BDT.root")
    outdata["DecayTree"] = df_data_toSave.dropna()
    outMC["DecayTree"]   = df_MC_toSave.dropna()

    # Only plot if Bc->BsPi
    df_Bc_BDT = df_data_toSave.query(f'BsBDT_score > {total_cut_Bs} & Bs_MM > 5300 & Bs_MM < 5420 & BcBDT_score > {total_cut}')
    if bach == "Pi":
        plt.figure()
        plt.hist(df_Bc_BDT['Bc_MDTF_M'], bins=100, histtype='step', range=(6200, 6500))
        plt.xlabel(r"$M(B_s^0 \pi^+)$")
        plt.ylabel(r"Candidates / (30 MeV/$c^2)$")
        #plt.title(r'$B_c\rightarrow B_s\pi$')
        #plt.show()
        saveFig(f'{plotFolder}PostBDT_Bc_MDTF_M_{plotSuffix}')

