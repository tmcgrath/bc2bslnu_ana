# BDT Application

## BDT training!

The BDT application scripts are a bit different depending on running on the $B_c^+\to B_s^0 \mu^+\nu_\mu$ + $B_c^+\to B_s^0 \pi^+$ or the background MC channels so proceed differently based on situation.

### For signal and normalisation channel data and MC

The scripts [`BDT_Bc2BsX_DsPi.py`](./BDT_Bc2BsX_DsPi.py) and [`BDT_Bc2BsX_JpsiPhi.py`](./BDT_Bc2BsX_JpsiPhi.py) does the following things:
- Trains the $B_c^+$ (and $B_s^0$ for $B_s^0 \to D_s^- \pi^+$) selection BDTs
- Saves the BDT output into data and MC root files as a new leaf
- Saves the BDT model itself in [`BDTModel`](./BDTModel/) in `.sav` files

This is done separately for both $B_c^+\to B_s^0 \mu^+\nu_\mu$ and $B_c^+\to B_s^0 \pi^+$ depending on input arguments.

There are shell scripts that run the python scripts in this directory. The condor submission scripts for running this are in [`condor`](./condor/). Again, recommended on institution machines but they can be run on lxplus as well.

### For application of BDT on background channel MC

The scripts [`apply_BDT_DsPi.py`](./apply_BDT_DsPi.py) and [`apply_BDT_JpsiPhi.py`](./apply_BDT_JpsiPhi.py) just apply the $B_c^+\to B_s^0 \mu^+\nu_\mu$ channel BDT model on the background MC files (outputs a root file with an extra BDT output leaf). The shell script to run all of this on the available ReDecay MC files are in [`Scripts`](./Scripts/).

## $B_c^+$ BDT cut optimisation

[`optimise_BDT_cut.py`](./optimise_BDT_cut.py) calculates the optimum BDT cut with the signal significance for $B_c^+\to B_s^0 \pi^+$ and the [Punzi figure of merit](https://arxiv.org/pdf/physics/0308063) for $B_c^+\to B_s^0 \mu^+\nu_\mu$. 

**IMPORTANT** The optimised BDT cut is printed in the terminal as well as the associated plots and then I manually added them to [`BDT_cuts.py`](../Fit/BDT_cuts.py) for applying these cuts in the fitting scripts. I never got round to automating this :(
