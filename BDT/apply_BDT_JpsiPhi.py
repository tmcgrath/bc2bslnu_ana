#
# apply_BDT_JpsiPhi.py
#
# Python script for applying BDT to bkg channels for Bs->JpsiPhi
#
# 30.01.23
#

# General Python packages
import uproot as ur
from root_pandas import read_root
import pandas as pd
import numpy as np
#import mplhep
#mplhep.style.use(mplhep.style.LHCb2)
import vector
import argparse
import sys, os

# ML related packages
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from scipy.stats import pearsonr
from mpl_toolkits.axes_grid1 import make_axes_locatable

# For using BDT
import pickle

# Import other functions from original BDT scripts
from BDT_Bc2BsX_JpsiPhi import get_df, defVariables, makeBcCuts, applyBDT
from BDT_Bc2BsX_JpsiPhi import columns

##################################################
# Arguments
##################################################

# Check directory -> changes output location
currentDir = os.path.dirname(os.path.abspath(__file__))
if "tamaki" in currentDir:
    outDir = "/hepgpu5-data3/tamaki"
    cDir = "."
# lxplus or condor
else:
    outDir = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana"
    cDir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/BDT"
BDTFolder = f"{cDir}/BDTModel/"

parser=argparse.ArgumentParser()
parser.add_argument('--year','-y',type=int,default = 0,choices=[0, 2016, 2017, 2018], help='Enter year of data and MC you would like to run the BDT or 0 for all years combined (2016-2018)')
parser.add_argument('--year_bdt', '-yb', type=int,default=0,choices=[0, 2016, 2017, 2018],help='Enter year of BDT or 0 for all years combined (2016-2018)')
parser.add_argument('--bachelor','-b',type=str, default = "Mu", choices=["Mu", "Pi"], help='Bachelor particle of decay to train BDT for (Mu or Pi)')
parser.add_argument('--channel', '-c', required=True, type=str, choices=["Bc2BsMuNu", "Bc2BsPi", "Bc2BsPi_misID", "Bc2BsK", "Bc2BsKst", "Bc2BsRho", "Bc2BsstMuNu", "Bc2BsstPi", "Bs2JpsiPhi", "Bs2JpsiPhi_loose"], help='Background channel to apply BDT to')
parser.add_argument('--data','-d', action='store_true', help='Flag whether to apply to data (default is MC)')
parser.add_argument('--redecay','-r', action='store_true', help='Flag whether to apply to ReDecay')
parser.add_argument('--pv_fit', '-p', action='store_true', help='Flag whether to use PV fit DTF variables (for Bc2BsPi only)')
args = parser.parse_args()

year = args.year
yb = args.year_bdt
saved_year = "All" if year == 0 else str(year)
year_bdt  = "All" if yb == 0 else str(yb)
extra_suffix = "" if yb == 0 else f"_{str(yb)}"

bach = args.bachelor
sig_or_norm = "sig" if bach == "Mu" else "norm"

plotSuffix = f"Bs2JpsiPhi_{saved_year}"
bkg = args.channel

if args.data == True:
    data_or_mc = "Data" 
    add_flag = ""
    if bkg == "Bc2BsK" or bkg == "Bc2BsKst" or bkg == "Bc2BsRho" or bkg == "Bc2BsstMuNu" or bkg == "Bc2BsstPi" or bkg == "Bs2JpsiPhi" or bkg == "Bs2JpsiPhi_loose":
        print(f"Error! No data available for {bkg}, setting to MC")
        data_or_mc = "MC"
        add_flag = "MC_"
else:
    data_or_mc = "MC" 
    add_flag = "MC_"
rootFolder   = f"{outDir}/{data_or_mc}BDTApplied/"

redecay = args.redecay
if redecay == True:
    if bkg == "Bc2BsK" or bkg == "Bc2BsKst" or bkg == "Bc2BsRho" or bkg == "Bc2BsstMuNu":
        add_flag = "ReDecay_"
    else:
        print(f"Added -r flag but {bkg} has no redecay files - ignoring!")

# Get TM string
chan_name = f"{bkg}_JpsiPhi"
if bkg == "Bc2BsMuNu":
    sim = "sim09h_"
    TMString = ["Bc_is_from_Bc_BsMuNu", "Bs_is_from_Bs_JpsiPhi"]
elif "Bc2BsPi" in bkg:
    sim = "sim09k_"
    TMString = ["Bc_is_from_Bc_BsPi", "Bs_is_from_Bs_JpsiPhi"]
elif bkg == "Bc2BsK":
    sim = "sim09l_" if redecay else "sim09k_"
    TMString = ["Bc_is_from_Bc_BsK", "Bs_is_from_Bs_JpsiPhi"]
elif bkg == "Bc2BsKst":
    sim = "sim09l_" if redecay else "sim09k_"
    TMString = ["Bc_is_from_Bc_BsKstar_Ks2pippim", "Bc_is_from_Bc_BsKstar_Ks2pizpiz", "Bs_is_from_Bs_JpsiPhi"]
elif bkg == "Bc2BsRho":
    sim = "sim09l_" if redecay else "sim09k_"
    TMString = ["Bc_is_from_Bc_BsRho", "Bs_is_from_Bs_JpsiPhi"]
elif bkg == "Bc2BsstMuNu":
    sim = "sim09l_" if redecay else "sim09k_"
    TMString = ["Bc_is_from_Bc_BsstMuNu", "Bs_is_from_Bs_JpsiPhi"]
elif bkg == "Bc2BsstPi":
    sim = "sim09j_"
    TMString = ["Bc_is_from_Bc_BsstPi", "Bs_is_from_Bs_JpsiPhi"]
elif "Bs2JpsiPhi" in bkg:
    sim = "sim09h_"
    TMString = ["Bs_is_from_Bs_JpsiPhi"]
    chan_name = bkg
    plotSuffix = saved_year

if args.data == True: sim = ""

PV_fit = args.pv_fit
if PV_fit == False:
    dtf = "MDTF"
    #DTFVars = ["Bc_MDTF_Bs_DecayLength", "Bc_MDTF_Bs_DecayLengthErr",
    #           "Bc_MDTF_Bs_ctau", "Bc_MDTF_Bs_ctauErr",
    #           "Bc_MDTF_chi2"]
    DTFVars = ["Bc_MDTF_Bs_ctau", "Bc_MDTF_Bs_ctauS", "Bc_MDTF_chi2nDOF"]
    print("DTF: Mass constraint")
else:
    dtf = "DTF"
    #DTFVars = ["Bc_DTF_Bs_DecayLength", "Bc_DTF_Bs_DecayLengthErr", "Bc_DTF_DecayLength", "Bc_DTF_DecayLengthErr",
    #           "Bc_DTF_Bs_ctau", "Bc_DTF_ctau", "Bc_DTF_Bs_ctauErr", "Bc_DTF_ctauErr",
    #           "Bc_DTF_chi2"]
    DTFVars = ["Bc_DTF_Bs_ctau", "Bc_DTF_Bs_ctauS", "Bc_DTF_ctau", "Bc_DTF_ctauS", "Bc_DTF_chi2nDOF"]
    print("DTF: PV and mass constraint (To be used on Bc->Bspi Only)")

print(PV_fit)
print(dtf)

##################################################
# Main execution
##################################################

if data_or_mc == "MC":
    MCcolumns = columns + TMString 
    if bkg != "Bc2BsstPi":
        MCcolumns = MCcolumns + ["Bc_MC_MOTHER_ID", "Bc_TRUEID", "Bc_MC_MOTHER_KEY",
                                 "Bach_MC_MOTHER_ID", "Bach_MC_GD_MOTHER_ID", "Bach_MC_MOTHER_KEY", "Bach_MC_GD_MOTHER_KEY", "Bach_TRUEID",
                                 "Bs_MC_MOTHER_ID", "Bs_MC_GD_MOTHER_ID", "Bs_MC_MOTHER_KEY", "Bs_MC_GD_MOTHER_KEY", "Bs_TRUEID"]
else:
    MCcolumns = columns

# Get data file for both MU and MD for specified year and bachelor particle
if year == 0:
    # Get all data 
    print(f"Loading {data_or_mc} for all years")

    if redecay == False and bkg != "Bc2BsstPi":
        mc_MU_file_16 = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}2016_MU_reduced.root"
        mc_MD_file_16 = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}2016_MD_reduced.root"
        df_MC_MU_16 = get_df(mc_MU_file_16, MCcolumns)
        df_MC_MD_16 = get_df(mc_MD_file_16, MCcolumns)
        df_MC_MU_16.loc[:, 'year'] = 2016
        df_MC_MD_16.loc[:, 'year'] = 2016
    if bkg != "Bc2BsstPi":
        mc_MU_file_18 = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}2018_MU_reduced.root"
        mc_MD_file_18 = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}2018_MD_reduced.root"
        df_MC_MU_18 = get_df(mc_MU_file_18, MCcolumns)
        df_MC_MD_18 = get_df(mc_MD_file_18, MCcolumns)
        df_MC_MU_18.loc[:, 'year'] = 2018
        df_MC_MD_18.loc[:, 'year'] = 2018

    mc_MU_file_17 = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}2017_MU_reduced.root"
    mc_MD_file_17 = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}2017_MD_reduced.root"
    print(mc_MU_file_17)

    df_MC_MU_17 = get_df(mc_MU_file_17, MCcolumns)
    df_MC_MD_17 = get_df(mc_MD_file_17, MCcolumns)
    df_MC_MU_17.loc[:, 'year'] = 2017
    df_MC_MD_17.loc[:, 'year'] = 2017
    MC_list = [df_MC_MU_17, df_MC_MD_17]
    if redecay == False and bkg != "Bc2BsstPi": MC_list += [df_MC_MU_16, df_MC_MD_16]
    if bkg != "Bc2BsstPi": MC_list += [df_MC_MU_18, df_MC_MD_18]
    MC = pd.concat(MC_list, ignore_index=True)

else:
    print(f"Loading {data_or_mc} for all {year}")
    mc_MU_file = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}{year}_MU_reduced.root"
    mc_MD_file = f"{outDir}/{data_or_mc}Reduced/{chan_name}_{add_flag}{sim}{year}_MD_reduced.root"
    print(mc_MU_file)

    df_MC_MU = get_df(mc_MU_file, MCcolumns)
    df_MC_MD = get_df(mc_MD_file, MCcolumns)
    MC = pd.concat([df_MC_MD, df_MC_MU], ignore_index=True)

df_MC_toSave = makeBcCuts(defVariables(MC, dtf), dtf)
df_MC_toSave.reset_index(inplace=True, drop=True) # just use index after all the Bc cuts and then don't change
# get rid of infinite or NaN numbers
col_to_drop = []
for col in df_MC_toSave.columns:
    if "PVFit" in col or "MassFit" in col:
        print(f"dropping {col}")
        col_to_drop.append(col)
df_MC_toSave = df_MC_toSave.drop(columns=col_to_drop)
df_MC_toSave.replace([np.inf, -np.inf], np.nan, inplace=True) 
df_MC_toSave.dropna(inplace=True)

# Load BDT models and apply

#train_vars_Bc = ["Bach_PT", "Bach_P", "Bc_PT",
#                 "Bc_IPCHI2_OWNPV", "Bc_ENDVERTEX_CHI2", "Bc_DIRA_OWNPV"] + DTFVars
train_vars_Bc = ["sqrt_Bc_IPCHI2_OWNPV", "sqrt_Bs_IPCHI2_OWNPV", "sqrt_Bach_IPCHI2_OWNPV", "Bs_PT", "Bach_PT",
                 "Bc_TAU", "Bc_DIRA_OWNPV"] + DTFVars
Bc_BDT_even_file = f"{BDTFolder}{sig_or_norm}_Bc_BDT_even_Bs2JpsiPhi_{year_bdt}.sav"
Bc_BDT_odd_file = f"{BDTFolder}{sig_or_norm}_Bc_BDT_odd_Bs2JpsiPhi_{year_bdt}.sav"
print(f"Applying Bc BDT from {Bc_BDT_even_file} and {Bc_BDT_odd_file}")
Bc_BDT_model_even = pickle.load(open(Bc_BDT_even_file, 'rb'))
Bc_BDT_model_odd = pickle.load(open(Bc_BDT_odd_file, 'rb'))
df_MC_toSave = applyBDT(df_MC_toSave, train_vars_Bc, Bc_BDT_model_even, Bc_BDT_model_odd)

# Save root file
print("Saving BDT application to:")
print(f"{rootFolder}{bkg}_{plotSuffix}_MC_BDT{extra_suffix}.root")
outMC   = ur.recreate(f"{rootFolder}{bkg}_{plotSuffix}_{add_flag}BDT{extra_suffix}.root")
outMC["DecayTree"]   = df_MC_toSave.dropna()
