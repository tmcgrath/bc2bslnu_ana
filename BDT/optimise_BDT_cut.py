#
# optimise_BDT_cut.py
#
# Python script for obtaining the best BDT cut using Punzi FoM
#

# General Python packages
import uproot as ur
import pandas as pd
import numpy as np
import math
import argparse
import sys, os

# Plotting
#import matplotlib
#import matplotlib.font_manager 
#matplotlib.font_manager._rebuild()
#print(matplotlib.font_manager.fontManager.ttflist)
import matplotlib.pyplot as plt
from matplotlib import rc#Params
small_size = 12
medium_size = 14
rc('font',**{'family':'serif','serif':['Roman'], 'size': medium_size})
#plt.rcParams['font.family'] = "DeJavu Serif"
#plt.rcParams['font.serif'] = ["Times New Roman"]
#rcParams['font.size'] = medium_size

sys.path.append("/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit")
from BDT_Bc2BsX_JpsiPhi import saveFig #get_df

# Background ranges
#bkg_ranges = {
#    "DsPi": [6600, 7500],
#    "JpsiPhi": [6600, 7500]
#    #"JpsiPhi": [6450,8000]
#}
bkg_ranges_norm = [6400, 6550]
bkg_ranges_sig = [6400, 7350]
# multiply this by signal eff to get the expected signal
S_scale = {
    "DsPi": 135 / 0.65, 
    "JpsiPhi": 134 / 0.62 
}

# BDT cuts to test (this bit is manual unfortunately)
BDT_range_for_looping_norm = {
    "DsPi": (0, 4.3, 0.1),
    "JpsiPhi": (0, 4.3, 0.1)
}
BDT_range_for_looping_sig = {
    "DsPi": (0, 4.3, 0.1),
    "JpsiPhi": (0, 4.3, 0.1)
}


## Calculation settings
#norm_ranges = {
#    "DsPi": np.arange(),
#    "JpsiPhi": np.arange()
#}

##################################################
# Functions
##################################################

# Get pandas dataframe
def get_df(filename, tree="DecayTree"):
    filenameplus = f"{filename}:{tree}"
    tree = ur.open(filenameplus)
    branches = tree.keys()
    theDF = tree.arrays(branches, library="pd")

    return theDF

# Cuts to just dspi channel
def applyDsPiCuts(df, Bs_BDT_cut):
    # safety first
    if "BcBDT_score" in Bs_BDT_cut:
        Bs_BDT_cut = Bs_BDT_cut.split(" & ")[0]

    total_cuts = f"{Bs_BDT_cut} & (Bs_MM > 5300 & Bs_MM < 5420)"
    print(f"Bs2DsPi channel so applying {total_cuts} cuts")
    df = df.query(total_cuts)

    return df

# Get signal dataframe
def getSignal(df, bs_chan, TM):
    '''
    Dataframe must be MC
    '''
    df_SG = df.query(f"{TM} == 1 & Bs_is_from_Bs_{bs_chan} == 1")
    df_SG = df_SG.dropna()

    return df_SG

# Get bkg dataframe
def getBackground(df, bs_chan):
    '''
    Dataframe must be data
    '''
    df_BG = df.query(f"Bc_MM>{bkg_ranges[0]} & Bc_MM<{bkg_ranges[1]}")
    df_BG = df_BG.dropna()
    return df_BG

# Calculate scale for background assuming linear background
def calcBkgScale(df, bachelor):
    '''
    Dataframe must be data
    Gets two adjacent sections of sideband and divides the first by the second to get a scale
    Crude background estimate = background number in region x scale
    '''
    if bachelor == "Pi":
        ranges_1 = (6350, 6450); ranges_2 = (6450, 6650)
    else:
        ranges_1 = (6400, 6800); ranges_2 = (6800, 7200)
    
    df_1 = df.copy()
    df_2 = df.copy()
    cut_1 = df_1.query(f"{ranges_1[0]} < Bc_MDTF_M & Bc_MDTF_M < {ranges_1[1]}")
    cut_2 = df_2.query(f"{ranges_2[0]} < Bc_MDTF_M & Bc_MDTF_M < {ranges_2[1]}")
    nom = float(cut_1.shape[0]); denom = float(cut_2.shape[0])
    print(f"{nom} and {denom}")
    scale = nom / denom

    return scale

# Function for punzi fom and error calculation
def calcPunzi(S_after_cut, S, B, alpha=5):
    '''
    Punzi FoM = eff_s / ( alpha/2 + \sqrt(B) )
    Binomial errors for efficiency
    '''
    eff_s = S_after_cut / S
    punzi = eff_s / (alpha/2 + math.sqrt(B))
    
    # Error calculation
    #eff_s_error = eff_s * math.sqrt( (S / math.pow(S, 2)) + (S_after_cut / math.pow(S_after_cut, 2)) )
    #sqrt_B_error = math.sqrt(B) * (math.sqrt(B) / 2*B)
    #punzi_error = punzi * math.sqrt( math.sqrt(eff_s_error / eff_s, 2) + math.sqrt(B_error / B, 2) )
    eff_s_error = math.sqrt( eff_s * (1-eff_s) / S )
    punzi_error = eff_s_error / (alpha/2 + math.sqrt(B))

    return punzi, punzi_error

# Function for just plotting Punzi FoM
def plotPunzi(x_list, y_list, sig_list, savename, y_err=None):
    '''
    Function for just plotting x=BDT cuts versus y=Punzi FoM and looks all nice
    '''
    # Get BDT cut with highest punzi
    max_BDT = x_list[max(range(len(y_list)), key=y_list.__getitem__)]
    max_BDT_sig = x_list[max(range(len(sig_list)), key=sig_list.__getitem__)]
    print(f"Best BDT cut from punzi: {max_BDT}")
    if "Norm" in savename: print(f"Best BDT cut from s/sqrt(s+b): {max_BDT_sig}")

    # Plot Punzi FoM
    _, ax = plt.subplots()
    punzi_plot = ax.plot(x_list, y_list, label='Punzi FoM', color='darkred')
    ax.axvline(x = max_BDT, color = 'lightcoral', label = 'Optimised BDT cut', linestyle='dashed')
    ax.set_xlabel("BDT Decision Score")
    ax.set_ylabel(r'$\epsilon_s\ /\ (5/2+\sqrt{b}$)')
    plt.tight_layout()
    saveFig(savename+"_punzi")

    if "Norm" in savename:
        # Plot Punzi and Significance
        ax2 = ax.twinx()
        sig_plot = ax2.plot(x_list, sig_list, label='Significance')
        ax2.axvline(x = max_BDT_sig, color = 'cornflowerblue', label = 'Optimised BDT cut', linestyle='dashed')
        ax2.set_ylabel(r'$s\ /\ (\sqrt{s+b})$')
        lines = punzi_plot + sig_plot
        labels = [l.get_label() for l in lines]
        ax.legend(lines, labels, loc="best", fontsize='medium')
        plt.tight_layout()
        saveFig(savename)

        # Plot significance
        _, ax3 = plt.subplots()
        ax3.plot(x_list, sig_list, label='Significance')
        ax3.axvline(x = max_BDT_sig, color = 'cornflowerblue', label = 'Optimised BDT cut', linestyle='dashed')
        ax3.set_xlabel("BDT Decision Score")
        ax3.set_ylabel(r'$s\ /\ (\sqrt{s+b})$')
        plt.tight_layout()
        saveFig(savename+"_sig")


# Mega function that plots the Punzi FoM for each BDT cut and also plots it
def getBcCutAndPlot(df_signal, df_background, cuts_list, bs_chan, bachelor):
    '''
    Calculates Punzi FoM after each BDT cut and plots the values to get the optmimal value
    For Bc2BsPi: calculate Punzi FoM in the Bc mass region, scale S in terms of signal_yield/MC ratio to get S estimate
    S ratio for DsPi (at BDT cut 3.0):   For JpsiPhi (as BDT cut 2.5):
    '''
    sig = df_signal.shape[0]
    punzi_list, significance_list = [], []
    for cut in cuts_list:
        #print(f"On BcBDT_score > {cut}")
        df_signal_to_cut = df_signal.copy(); df_background_to_cut = df_background.copy()

        # For Bc2BsPi cut around Bc mass (but liberally)
        if bachelor == "Pi":
            df_signal_to_cut = df_signal_to_cut.query("6250 < Bc_MDTF_M & Bc_MDTF_M < 6300")
            sig_range = 50
        elif bachelor == "Mu":
            df_signal_to_cut = df_signal_to_cut.query("5400 < Bc_MDTF_M & Bc_MDTF_M < 6350")
            sig_range = 950
        else:
            print("Um how did you get a different bachelor?? ABORTING")
            sys.exit()

        df_signal_after_cut = df_signal_to_cut.query(f"BcBDT_score > {cut}")
        df_background_after_cut = df_background_to_cut.query(f"BcBDT_score > {cut}")

        sig_after_cut = df_signal_after_cut.shape[0]
        bkg_after_cut = df_background_after_cut.shape[0]
        sig_eff = sig_after_cut / sig
        #print(f"Signal after cut: {sig_after_cut}, signal eff after cut: {sig_eff}")

        # ESTIMATING BKG CONTRIBUTION UNDER SIGNAL
        bkg_range = bkg_ranges[1] - bkg_ranges[0]
        bkg_frac = float(bkg_range) / (float(sig_range))
        #bkg_scale = calcBkgScale(df_background_after_cut, bachelor)
        bkg_scale = 1
        #print(f"frac: {float(bkg_range)} / {(float(sig_range))}")
        #print(f"Scale: {bkg_scale}, frac: {bkg_frac}")
        bkg_after_cut_adjusted = bkg_after_cut * bkg_scale / bkg_frac
        #print(f"Bkg after cut: {bkg_after_cut}, after scaling: {bkg_after_cut_adjusted}")

        punzi, punzi_error = calcPunzi(sig_after_cut, sig, bkg_after_cut_adjusted)
        #punzi_error = 
        punzi_list.append(punzi)

        # Estimate significance as well
        expected_S = S_scale[bs_chan] * sig_eff
        significance = float(expected_S) / math.sqrt( float(expected_S) + float(bkg_after_cut_adjusted) )
        significance_list.append(significance)

    # Plot results!
    plotPunzi(cuts_list, punzi_list, significance_list, f"{plotFolder}Bc_BDT_optimised_{plotSuffix}")


##################################################
# Main execution
##################################################

if __name__ == "__main__":

    # Check directory -> changes output location
    currentDir = os.path.dirname(os.path.abspath(__file__))
    # if on manchester clusters
    if "tamaki" in currentDir:
        outDir = "/hepgpu5-data3/tamaki"
        cDir = "."
    # lxplus or condor
    else:
        outDir = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana"
        cDir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/BDT"

    parser=argparse.ArgumentParser()
    parser.add_argument('--year','-y',type=int,default = 0,choices=[0, 2016, 2017, 2018], help='Enter year of data and MC you would like to run the BDT or 0 for all years combined (2016-2018)')
    parser.add_argument('--channel','-c',required=True, type=str, choices=["DsPi", "JpsiPhi"], help='Bs decay mode (DsPi or JpsiPhi)')
    parser.add_argument('--bachelor','-b',type=str, default = "Pi", choices=["Mu", "Pi"], help='Bachelor particle to specify signal or normalisation mode (Mu or Pi)')
    args = parser.parse_args()

    year = args.year
    bach = args.bachelor
    chan = args.channel
    Bschan = f"Bs2{args.channel}"

    # Import Bs cut to use
    if bach == "Mu":
        from BDT_cuts import BDT_cuts_sig as BDT_cuts
        BDT_range_for_looping = BDT_range_for_looping_sig
        bkg_ranges = bkg_ranges_sig
        mcbach = f"{bach}Nu"
        TMString = "Bc_is_from_Bc_BsMuNu"
        Bc_mode = "Sig"
    else:
        from BDT_cuts import BDT_cuts_norm as BDT_cuts
        BDT_range_for_looping = BDT_range_for_looping_norm
        bkg_ranges = bkg_ranges_norm
        mcbach = f"{bach}"
        TMString = "Bc_is_from_Bc_BsPi"
        Bc_mode = "Norm"

    # Import columns to use
    if args.channel == "DsPi":
        from BDT_Bc2BsX_DsPi import columns
    else:
        from BDT_Bc2BsX_JpsiPhi import columns

    print("Loading data.......................")
    if year == 0:
        print("Loading data and MC for all years")
        data_file = f"{outDir}/DataBDTApplied/Bc2Bs{bach}_{Bschan}_All_BDT.root"
        MC_file = f"{outDir}/MCBDTApplied/Bc2Bs{bach}_{Bschan}_All_MC_BDT.root"
        year_str = "All"
    else:
        print(f"Loading data and MC for {year}")
        data_file = f"{outDir}/DataBDTApplied/Bc2Bs{bach}_{Bschan}_{year}_BDT.root"
        MC_file = f"{outDir}/MCBDTApplied/Bc2Bs{bach}_{Bschan}_{year}_MC_BDT.root"
        year_str = f"{year}"

    plotSuffix = f"{Bc_mode}_{chan}"
    plotFolder = f"/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/BDT/Figs/"

    df_data = get_df(data_file)
    df_MC = get_df(MC_file)

    # Need to apply some cuts to DsPi channel
    correct_BDT_cut = BDT_cuts[chan][year_str]
    if chan == "DsPi":
        df_data = applyDsPiCuts(df_data, correct_BDT_cut)
        df_MC = applyDsPiCuts(df_MC, correct_BDT_cut)

    # Get signal and bkg df
    df_sig = getSignal(df_MC, chan, TMString)
    df_bkg = getBackground(df_data, chan)

    ## Optimisation bit!!
    # Loop through BDT cut values and caluclate Punzi FoM and plot!
    BDT_range = BDT_range_for_looping[chan]
    cuts = np.arange(BDT_range[0], BDT_range[1], BDT_range[2], dtype=float)
    cuts = cuts.tolist()
    getBcCutAndPlot(df_sig, df_bkg, cuts, chan, bach)

    print("Done")
