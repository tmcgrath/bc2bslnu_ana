Variable importance Bc BDT:
Bc_DIRA_OWNPV : 0.43790327252561223
Bach_PT : 0.14367729692155617
Bc_MDTF_Bs_ctauErr : 0.12927960184065693
Bach_P : 0.12281806651737666
Bc_PT : 0.07416020870517559
Bc_ENDVERTEX_CHI2 : 0.04456021603038743
Bc_MDTF_Bs_DecayLengthErr : 0.021001541149680468
Bc_MDTF_chi2 : 0.018402501225776878
Bc_IPCHI2_OWNPV : 0.008102623325590261
Bc_MDTF_Bs_DecayLength : 9.467175818744877e-05
Bc_MDTF_Bs_ctau : 0.0

Bc BDT ROC AUC = 0.9779246237047675
BDT cut for Bc sel = 2.413575538784