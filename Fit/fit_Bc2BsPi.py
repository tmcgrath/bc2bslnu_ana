#
# fit_Bc2BsPi.py
#
# 1D maximum likelihood fit on Bc2BsPi mass
#
# 11.11.22
#

import argparse
import ROOT as R
import os, sys

from BDT_cuts import BDT_cuts_norm as BDT_cuts
from common_things import getPullHist

#################################
# Functions                     #
#################################

#def sigModel():

def pause():
    programPause = input("Press the <ENTER> key to continue...")

# gets chi2 from frame
def getChi2( frame, nParams ):
    chi2 = frame.chiSquare(nParams)
    return chi2


def plotFit(frame, pullFrame, xlabel, xrange=[6200, 6550], paramOn = False):
    print(type(frame))
    frame.getAttText().SetTextSize(0.0275)
    #frame.getAttText().SetTextFont(132)
    frame.GetXaxis().SetLabelOffset(999) # Remove X axis labels
    frame.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    #if chan == "JpsiPhi":

    frame.GetYaxis().SetLabelFont(132)
    frame.GetYaxis().SetTitleFont(132)
    frame.getAttText().SetLineWidth(0)
    frame.getAttText().SetFillStyle(0)
    frame.GetYaxis().SetLabelSize(0.04)
    frame.GetYaxis().SetTitle("Candidates / (6 MeV/c^{2})")
    frame.SetTitleSize(0.04, 'XY')
    frame.SetTitle("")

    # add legend if not param on
    if paramOn == False:
        leg = R.TLegend(0.65,0.65,0.9, 0.8);
        leg.SetFillColor(0)
        leg.SetLineColor(0)
        leg.SetHeader("LHCb 5.4fb^{-1}")
        leg.AddEntry("data","Data", "P")
        leg.AddEntry("norm","B_{c}^{+} #rightarrow B_{s}^{0} #pi^{+}","L")
        leg.AddEntry("comb","Comb. Bkg.", "L")
        leg.AddEntry("bc2bsstpi","B_{c}^{+} #rightarrow B_{s}^{0} #pi^{+}", "L")


    # Plot pulls 
    pullFrame.SetTitle("")
    pullFrame.GetXaxis().SetTitleOffset(1.)
    pullFrame.SetMinimum(-5)
    pullFrame.SetMaximum(5)
    pullFrame.GetXaxis().SetTitle('#font[12]{%s} (MeV/c^{2})'%xlabel)
    pullFrame.GetXaxis().SetTitleOffset(1.)
    pullFrame.GetXaxis().SetLabelFont(132)
    pullFrame.GetXaxis().SetTitleFont(132)
    pullFrame.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    pullFrame.SetLabelSize(0.08, 'XY')
    pullFrame.GetYaxis().SetTitle('(data - model)/#font[12]{#sigma}_{data}')
    pullFrame.GetYaxis().SetTitleOffset(0.5)
    pullFrame.GetYaxis().SetNdivisions(209)
    pullFrame.GetYaxis().SetLabelFont(132)
    pullFrame.GetYaxis().SetTitleFont(132)
    pullFrame.SetTitleSize(0.08, 'XY')

    c = R.TCanvas("c", "c", 600, 700)
    upperPad = R.TPad('upperPad', 'upperPad', .005, .330, .995, .995)
    lowerPad = R.TPad('lowerPad', 'lowerPad', .005, .005, .995, .330)
    upperPad.SetBottomMargin(0.03)
    lowerPad.SetTopMargin(0.02)
    lowerPad.SetBottomMargin(0.3)
    upperPad.SetLeftMargin(0.15)
    lowerPad.SetLeftMargin(0.15)
    upperPad.SetRightMargin(0.05)
    lowerPad.SetRightMargin(0.05)
    upperPad.Draw()
    lowerPad.Draw()

    # Draw pads
    upperPad.cd()
    frame.Draw()
    if paramOn ==False:
        leg.Draw()
    upperPad.Update()

    # Draw pads
    lowerPad.cd()
    pullFrame.Draw()
    c.Draw()
    
    return c

#################################
# Fitting                       #
#################################

def fitMC(MCvar, ds, saveLocation):

    # Set up fit model #
    # Fit parameters
    mu = R.RooRealVar("#mu", "Mean of DCB", 6275, 6250, 6300)
    sigma = R.RooRealVar("#sigma", "Width of DCB", 0.01, 200)
    aL = R.RooRealVar("#alpha_{L}", "Tail threshold left of DCB", 1, 0.2, 3)
    aR = R.RooRealVar("#alpha_{R}", "Tail threshold right of DCB", 1, 0.2, 3)
    nL = R.RooRealVar("n_{L}", "n left of DCB", 0.01, 10)
    nR = R.RooRealVar("n_{R}", "n left of DCB", 0.01, 10)
    if year is None:
        dcb_yield = R.RooRealVar("yield", "yield", 0, 100000)
    else:
        dcb_yield = R.RooRealVar("yield", "yield", 0, 10000)

    # Model
    dcb = R.RooCrystalBall("dcb_pdf", "Double Crystal Ball PDF", MCvar, mu, sigma, aL, nL, aR, nR)

    # Signal model for MC
    mc_model = R.RooAddPdf("dcb_model", "Double Crystal Ball model", R.RooArgList(dcb), R.RooArgList(dcb_yield))

    # Fit!
    mc_fit = mc_model.fitTo(ds, R.RooFit.Range(6200, 6400), R.RooFit.Save(True))
    params = mc_fit.floatParsFinal().getSize() # number of parameters for chi2 test

    # Plot!
    theFrame_MC = MCvar.frame()
    ds.plotOn(theFrame_MC, R.RooLinkedList())
    mc_model.plotOn(theFrame_MC)
    # Calculate chi2
    chi2_MC = theFrame_MC.chiSquare(params) # number of fit parameters
    
    # Pulls
    pull_MC = MCvar.frame()
    pullHist = getPullHist(theFrame_MC)
    pull_MC.addPlotable(pullHist, 'B') 
    # Plot everything else
    #mc_model.paramOn(theFrame_MC, R.RooFit.ShowConstants(True), R.RooFit.Layout(0.56, 0.95, 0.9), R.RooFit.Label('#chi^{{2}}/ndof = {} for {} dof'.format( round(chi2_MC, 2), theFrame_MC.GetNbinsX() - params)))
    mc_model.paramOn(theFrame_MC, R.RooFit.ShowConstants(True), R.RooFit.Layout(0.65, 0.95, 0.9))
    canv_MC = plotFit(theFrame_MC, pull_MC, "M(B_{s}^{0} #pi^{+})", xrange=[6200, 6400], paramOn = True)
    canv_MC.Draw()
    print('Chi2 / ndf = {} for {} dof'.format( round(chi2_MC, 2), theFrame_MC.GetNbinsX() - params))

    # save canvas
    canv_MC.SaveAs(saveLocation+".C")
    canv_MC.SaveAs(saveLocation+".png")
    canv_MC.SaveAs(saveLocation+".pdf")
    #pause()

    # Return nL and nR for data fit to help with fit
    return aL.getValV(), aR.getValV(), nL.getValV(), nR.getValV()

def fit_MC_Bc2BsstPi(var, ds, saveLocation):
    print("To be done...")

def fitData(var, dataset, aL_input, aR_input, nL_input, nR_input, saveLocation, sigModel = "gauss", bkgModel = "exp"):

    # Signal fit - Double crystal ball or Gaussian #
    # Signal fit parameters
    if sigModel == "gauss":
        print("Signal gaussian model")
        mean = R.RooRealVar("Sig. #mu", "Mean of Gaussian", 6270, 6250, 6300)
        sigma = R.RooRealVar("Sig. #sigma", "Width of Gaussian", 5, 0.01, 25)

        # Signal model
        sig = R.RooGaussian("sig_pdf", "Gaussian PDF", var, mean, sigma)

    elif sigModel == "dcb":
        print("Fixing nL and nR for signal DCB model")
        mu = R.RooRealVar("Sig. #mu", "Mean of DCB", 6270, 6250, 6300)
        sigma = R.RooRealVar("Sig. #sigma", "Width of DCB", 0.01, 50)
        aL = R.RooRealVar("Sig. #alpha_{L}", "Tail threshold left of DCB", aL_input)
        aR = R.RooRealVar("Sig. #alpha_{R}", "Tail threshold right of DCB", aR_input)
        nL = R.RooRealVar("Sig. n_{L}", "n left of DCB", nL_input) 
        nR = R.RooRealVar("Sig. n_{R}", "n right of DCB", nR_input)

        # Signal Model
        sig = R.RooCrystalBall("sig_pdf", "Double Crystal Ball PDF", var, mu, sigma, aL, nL, aR, nR)

    elif sigModel == "john":
        print("Trying Johnson S_U")
        mu = R.RooRealVar("mean", "Mean of DCB", 6270, 6250, 6300)
        jlambda = R.RooRealVar("lambda", "Width of Johnsons", 0.01, 50)
        gamma = R.RooRealVar("gamma", "Gamma of Johnsons", 0, -5, 5) # gamma negative for tail on right
        delta = R.RooRealVar("delta", "Delta of Johnsons", 0, 5) # some kind of height factor

        # Signal Model
        sig = R.RooJohnson("sig_pdf", "Johnson S_U PDF", var, mu, jlambda, gamma, delta)

    else:
        print("ERROR! Unknown signal model (not dcb or gauss) - please check")
        sys.exit()

    # signal yield
    sig_yield = R.RooRealVar("N_{sig}", "Signal yield", 0, 1000)

    # Background fit #
    # Combinatoric: exponential or chebyshev 
    if bkgModel == "exp":
        c = R.RooRealVar("Comb. c", "Exponent of exponential", -5, 5)

        # Bkg model 
        comb = R.RooExponential("comb_pdf", "Exponential PDF", var, c)

    elif bkgModel == "cheb":
        a0 = R.RooRealVar("Comb. a_{0}", "Chebyshev a0", 0.5, -5.0, 5.0)
        a1 = R.RooRealVar("Comb. a_{1}", "Chebyshev a1", 0.2, -5.0, 5.0)

        # Bkg model
        comb = R.RooChebychev("comb_pdf", "Chebychev PDF", var, R.RooArgList(a0, a1))
    
    else:
        print("ERROR! Unknown background model (not exponential or chebyshev) - please check")
        sys.exit()
    comb_yield = R.RooRealVar("N_{comb}", "Combinatoric yield", 0, 10000)

    # Bc2BsstPi: gaussian
    bc2bsstpi_string = "B_{c}^{+} #rightarrow B_{s}^{*0} #pi^{+}"
    bkg_mean = R.RooRealVar(f"{bc2bsstpi_string} #mu", "Mean of Bc2BsstPi", 6225, 6200, 6250)
    bkg_sigma = R.RooRealVar(f"{bc2bsstpi_string} #sigma", "Width of Bc2BsstPi", 5, 0.01, 20)
    bc2bsstpi = R.RooGaussian("bc2bsstpi_pdf", "Gaussian PDF for Bc2BsstPi", var, bkg_mean, bkg_sigma)
    frac = R.RooRealVar(f"{bc2bsstpi_string} Ratio", "Ratio of Bc2BsstPi and Bc2BsPi Yields", 0.1, 0.01, 1.0)
    bc2bsstpi_yield = R.RooFormulaVar("Bc2BsstPi_Yield", "@0 * @1", R.RooArgList(frac, sig_yield))

    # Some silly memeory thing 
    yields = []; yields.append(sig_yield); yields.append(comb_yield); yields.append(bc2bsstpi_yield)

    # Extended Model
    model = R.RooAddPdf("ext_model", "Sig+Bkg model", R.RooArgList(sig, comb, bc2bsstpi), R.RooArgList(sig_yield, comb_yield, bc2bsstpi_yield) )

    # Fit!
    data_fit = model.fitTo(dataset, R.RooFit.Range(6200, 6550), R.RooFit.Save(), R.RooFit.Extended())
    params = data_fit.floatParsFinal().getSize() # number of parameters for chi2 test

    # Plot!
    theFrame_data = var.frame()
    dataset.plotOn(theFrame_data, R.RooFit.Name("data")) # , R.RooLinkedList()
    model.plotOn(theFrame_data)
    # Calculate chi2
    chi2 = theFrame_data.chiSquare(params) # number of fit parameters
    print(f"Chi2 / ndf = {chi2}")
    # Pulls
    pull_data = var.frame()
    pullHist = getPullHist(theFrame_data)
    pull_data.addPlotable(pullHist, 'B') # P for marker HIST for without or B for bar 
    # Plot rest of the pdfs
    model.plotOn(theFrame_data, R.RooFit.Name("norm"), R.RooFit.Components("sig_pdf"), R.RooFit.DrawOption("F"), R.RooFit.FillStyle(1001), R.RooFit.FillColor(R.kCyan-8))
    model.plotOn(theFrame_data, R.RooFit.Name("comb"), R.RooFit.Components("comb_pdf"), R.RooFit.LineStyle(2), R.RooFit.LineColor(R.kSpring + 5))
    model.plotOn(theFrame_data, R.RooFit.Name("bc2bsstpi"), R.RooFit.Components("bc2bsstpi_pdf"), R.RooFit.DrawOption("F"), R.RooFit.FillStyle(1001), R.RooFit.FillColor(R.kAzure-6))
    #print(type(theFrame_data))
    #canv_data = plotFit(theFrame_data, pull_data, "M(B_{s}^{0} #pi^{+})", paramOn = False)
    #canv_data.Draw()

    # save canvas
    #canv_data.SaveAs(saveLocation+".C")
    #canv_data.SaveAs(saveLocation+".png")
    #canv_data.SaveAs(saveLocation+".pdf")

    # Plot with paramteres on 
    #model.paramOn(theFrame_data, R.RooFit.ShowConstants(True), R.RooFit.Layout(0.57, 0.95, 0.9), R.RooFit.Label('#chi^{{2}}/ndof = {} for {} dof'.format( round(chi2, 2), theFrame_data.GetNbinsX() - params)))
    model.paramOn(theFrame_data, R.RooFit.ShowConstants(False), R.RooFit.Layout(0.57, 0.95, 0.9), R.RooFit.Label("LHCb 5.4fb^{-1}"))
    canv_data2 = plotFit(theFrame_data, pull_data, "M(B_{s}^{0} #pi^{+})", paramOn = True)
    canv_data2.Draw()

    # print chi2
    print('Chi2 / ndf = {} for {} dof'.format( round(chi2, 2), theFrame_data.GetNbinsX() - params))

    # save canvas
    canv_data2.SaveAs(saveLocation+"_paramOn.C")
    canv_data2.SaveAs(saveLocation+"_paramOn.png")
    canv_data2.SaveAs(saveLocation+"_paramOn.pdf")   


    pause() 



#################################
# SetUp                         #
#################################

print("*"*30)
print("Wooooo Normalisation Fit")
print("*"*30)


parser=argparse.ArgumentParser()
parser.add_argument('--channel','-c',required=True,type=str, choices=["DsPi", "JpsiPhi"], help="Enter Bs decay channel to fit to (DsPi or JpsiPhi)")
parser.add_argument('--year','-y',required=False,type=int,choices=[2016, 2017, 2018], help='Enter year of data and MC you would like to run the BDT (2016-2018)')
parser.add_argument('--year_bdt', '-yb', type=int,default=0,choices=[0, 2016, 2017, 2018],help='Enter year of BDT or 0 for all years combined (2016-2018)')
parser.add_argument('--sig_model', '-sig', required=False, default = "gauss", type=str, choices=["gauss", "dcb", "john"], help='Enter the background model (gauss or dcb)')
parser.add_argument('--bkg_model', '-bkg', required=False, default = "exp", type=str, choices=["exp", "cheb"], help='Enter the background model (exp or cheb)')
parser.add_argument('--sep_year', '-sy', action='store_true', help="Flag whether to use BDT cut trained separate for each year")
args = parser.parse_args()

chan = args.channel
year = args.year
year_bdt  = "All" if args.year_bdt == 0 else str(args.year_bdt)
extra_suffix = "" if args.year_bdt == 0 else f"_{str(args.year_bdt)}"
sig_model = args.sig_model
bkg_model = args.bkg_model
if args.sep_year:
    year_str = str(year)
    print("Using BDT cut trained on separate years")
else: # nominal - use same BDT cut for all years
    year_str = "All"
    print("Using BDT cut trained on all years")

# Check directory -> changes output location
currentDir = os.path.dirname(os.path.abspath(__file__))
if "tamaki" in currentDir:
    MCPath = "/hepgpu5-data3/tamaki/MCBDTApplied/"
    filePath = "/hepgpu5-data3/tamaki/DataBDTApplied/"
else:
    MCPath = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/"
    filePath = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/DataBDTApplied/"

MCString = f"Bc2BsPi_Bs2{chan}_{{0}}_MC_BDT{extra_suffix}.root"
fileString = f"Bc2BsPi_Bs2{chan}_{{0}}_BDT{extra_suffix}.root"

# Define cuts
if chan == "DsPi":
    TM = "Bc_is_from_Bc_BsPi == 1 && Bs_is_from_Bs_DsPi == 1"
else: # JpsiPhi
    TM = "Bc_is_from_Bc_BsPi == 1 && Bs_is_from_Bs_JpsiPhi == 1"

if year is not None:
    theCuts = BDT_cuts[chan][year_str]

    fullCuts = theCuts + " && " + TM

# Get data and MC and apply cuts
theChain = R.TChain("DecayTree")
theMCChain = R.TChain("DecayTree")
if year == None:
    allYears = True
    dataID = "all" # used for dataset titles etc

    print("\nFitting for all years:")

    if args.sep_year:
        theChain2016, theChain2017, theChain2018 = R.TChain("DecayTree"), R.TChain("DecayTree"), R.TChain("DecayTree")
        theMCChain2016, theMCChain2017, theMCChain2018 = R.TChain("DecayTree"), R.TChain("DecayTree"), R.TChain("DecayTree")
        theChain2016.Add(filePath + fileString.format(2016))
        theChain2017.Add(filePath + fileString.format(2017))
        theChain2018.Add(filePath + fileString.format(2018))
        theMCChain2016.Add(MCPath + MCString.format(2016))
        theMCChain2017.Add(MCPath + MCString.format(2017))
        theMCChain2018.Add(MCPath + MCString.format(2018))

        print(filePath + fileString.format(2016))
        print(filePath + fileString.format(2017))
        print(filePath + fileString.format(2018))
    else:
        theChain.Add(filePath + fileString.format("All"))
        theMCChain.Add(MCPath + MCString.format("All"))

        print(filePath + fileString.format("All"))
        print(MCPath + MCString.format("All"))

        print("Getting data from right place")

else:
    allYears = False
    dataID = f"{year}" # used for dataset titles etc
    print(f"\nFitting for {year}")
    theChain.Add(filePath + fileString.format(year))
    theMCChain.Add(MCPath + MCString.format(year))

    print(filePath + fileString.format(year))
    print(MCPath + MCString.format(year))

outFolder = "./FitResults/Norm"
logFolder = "./Logs/Norm"

#################################
# Main                          #
#################################

# Direct output to log file
#stdoutOrigin=sys.stdout 
#sys.stdout = open(f"{logFolder}/BcFitResults_{chan}_{dataID}_{bkg_model}.txt", "w")

## Import data ##
print("\nImporting samples.....")
# observable is the Bc mass (dtf)
MCmass = R.RooRealVar("Bc_MDTF_M", "MC M(B_{s}^{0}#pi^{+})", 6100, 6700)
mass = R.RooRealVar("Bc_MDTF_M", "Data M(B_{s}^{0}#pi^{+})", 6100, 6700)
TM1    = R.RooRealVar("Bc_is_from_Bc_BsPi", "Bc_is_from_Bc_BsPi", 0, 1)
TM2    = R.RooRealVar(f"Bs_is_from_Bs_{chan}", f"Bs_is_from_Bs_{chan}", 0, 1)
BcBDT = R.RooRealVar("BcBDT_score", "BcBDT_score", -10, 10)
if chan =="DsPi":
    Bs_Mass = R.RooRealVar("Bs_MM", "Bs_MM", 5000, 6000)
    BsBDT = R.RooRealVar("BsBDT_score", "BsBDT_score", -10, 10)
    MCFullSet = R.RooArgList(MCmass, BcBDT, BsBDT, Bs_Mass, TM1, TM2)
    DataFullSet = R.RooArgList(mass, BcBDT, BsBDT, Bs_Mass)
    BsCut = " && 5300 < Bs_MM && Bs_MM < 5420"
else:
    MCFullSet = R.RooArgList(MCmass, BcBDT, TM1, TM2)
    DataFullSet = R.RooArgList(mass, BcBDT)
    BsCut = ""

# Datasets
print(f"Applying BDT cuts")
if year == None:

    if args.sep_year:
        data_2016 = R.RooDataSet("DataSamples_2016", f"Data Bc->Bs Bs->{chan} 2016 MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain2016 ),  R.RooFit.Cut( BDT_cuts[chan][2016] + BsCut ))
        data_2017 = R.RooDataSet("DataSamples_2017", f"Data Bc->Bs Bs->{chan} 2017 MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain2017 ),  R.RooFit.Cut( BDT_cuts[chan][2017] + BsCut ))
        data_2018 = R.RooDataSet("DataSamples_2018", f"Data Bc->Bs Bs->{chan} 2018 MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain2018 ),  R.RooFit.Cut( BDT_cuts[chan][2018] + BsCut ))

        mc_2016   = R.RooDataSet("MCSamples_2016", f"MC Bc->Bs Bs->{chan} 2016 MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain2016 ), R.RooFit.Cut( BDT_cuts[chan][2016] + " && " +  TM + BsCut))
        mc_2017   = R.RooDataSet("MCSamples_2017", f"MC Bc->Bs Bs->{chan} 2017 MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain2017 ), R.RooFit.Cut( BDT_cuts[chan][2017] + " && " +  TM + BsCut))
        mc_2018   = R.RooDataSet("MCSamples_2018", f"MC Bc->Bs Bs->{chan} 2018 MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain2018 ), R.RooFit.Cut( BDT_cuts[chan][2018] + " && " +  TM + BsCut))

        data = R.RooDataSet("DataSamples", f"Data Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( DataFullSet ))
        data.append(data_2016); data.append(data_2017); data.append(data_2018)
        mc = R.RooDataSet("MCSamples", f"MC Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( MCFullSet ))
        mc.append(mc_2016); mc.append(mc_2017); mc.append(mc_2018)
    else:
        data = R.RooDataSet("DataSamples_All", f"Data Bc->Bs Bs->{chan} All MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain ),  R.RooFit.Cut( BDT_cuts[chan][year_bdt] + BsCut ))
        mc   = R.RooDataSet("MCSamples_All", f"MC Bc->Bs Bs->{chan} All MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain ), R.RooFit.Cut( BDT_cuts[chan][year_bdt] + " && " +  TM + BsCut))
        print("All years BDT on all years combined")

else:
    data = R.RooDataSet("DataSamples", f"Data Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain ),  R.RooFit.Cut( theCuts + BsCut ))
    mc   = R.RooDataSet("MCSamples", f"MC Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain ), R.RooFit.Cut( fullCuts + BsCut ))

## Fit on MC ##
# signal model - Double sided crystal ball
# fix n values with MC (alpha and n are correlated)
# for bs->Jpsiphi fix both alpha and n to help with everything
MCoutFile = f"{outFolder}/MC_BcFit_{chan}_{dataID}_{sig_model}"
if sig_model == "dcb":
    aL_fixed, aR_fixed, nL_fixed, nR_fixed = fitMC( MCmass, mc, MCoutFile)
else:
    aL_fixed, aR_fixed, nL_fixed, nR_fixed = None, None, None, None

## Data fit ##
dataoutFile = f"{outFolder}/Data_BcFit_{chan}_{dataID}_{sig_model}_{bkg_model}"
fitData( mass, data, aL_fixed, aR_fixed, nL_fixed, nR_fixed, dataoutFile, sigModel = sig_model, bkgModel=bkg_model)

#sys.stdout.close()
#sys.stdout=stdoutOrigin

print("Done")
