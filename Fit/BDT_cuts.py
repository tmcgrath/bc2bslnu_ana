#
# BDT_cuts.py
#
# Dictionary summarising BDT cuts for each Bs channel and each year
#

BDT_cuts_norm = {
    "DsPi" : {
        "2016": "BsBDT_score > 2.81 & BcBDT_score > 4.0",
        "2017": "BsBDT_score > 2.09 & BcBDT_score > 4.0",
        "2018": "BsBDT_score > 2.15 & BcBDT_score > 3.8",
        "All" : "BsBDT_score > 1.90 & BcBDT_score > 3.2" # No Bach_CosTheta & 2Fold
        #"All" : "BsBDT_score > 2.47 & BcBDT_score > 3.7" # No angular vars
        #"All" : "BsBDT_score > 1.90 & BcBDT_score > 3.0" # For getting signal scale for significance 
    },
    "JpsiPhi": {
        "2016": "BcBDT_score > 2.8",
        "2017": "BcBDT_score > 2.8",
        "2018": "BcBDT_score > 2.8",
        "All" : "BcBDT_score > 2.9"  # No Bach_CosTheta
        #"All" : "BcBDT_score > 2.0"  # No angular vars
        #"All" : "BcBDT_score > 2.5" # For getting signal scale for significance 
    }
}

BDT_cuts_sig = {
    "DsPi" : {
        "2016": "",
        "2017": "",
        "2018": "",
        "All" : "BsBDT_score > -0.56 & BcBDT_score > 3.1"  # No Bach_CosTheta & kFold
        #"All" : "BsBDT_score > -0.44 & BcBDT_score > 2.8" # No angular vars
    },
    "JpsiPhi": {
        "2016": "",
        "2017": "",
        "2018": "",
        "All" : "BcBDT_score > 3.2"  # No Bach_CosTheta
        #"All" : "BcBDT_score > 2.2" # No angular vars
    }
}