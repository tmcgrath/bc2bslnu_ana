#
# common_things.py
#
# Some common dicts, lists used in >= 2 files in this folder 
#
# 27.02.2023
#

import ROOT as R
import numpy as np
from array import array

## Components to keep in fit

dict_of_pdf_names_mcorr = {
    #"Bc2BsK" : "BsKPDF",
    #"Bc2BsKst" : "BsKstPDF",
    #"Bc2BsPi_misID": "BsPiMisIDPDF",
    #"Bc2BsRho" : "BsRhoPDF",
    "combinatoric": "combPDF",
    #"signal": "signalPDF",
    #"Bc2BsstMuNu" : "BsstMuNuPDF",
    "signal_combined" : "sigCombinedPDF"
}
dict_of_pdf_names_mvis = {}
for key, value in dict_of_pdf_names_mcorr.items():
    dict_of_pdf_names_mvis[key] = f"{value}_Bcmvis"
dict_of_pdf_names_2D = {}
for key, value in dict_of_pdf_names_mcorr.items():
    dict_of_pdf_names_2D[key] = f"{value}_2D"

## Variables ##

mcorr = "Bc_CORR_M"
mcorr_binning_info = (38, 5400, 13000)
step = int( (mcorr_binning_info[2] - mcorr_binning_info[1]) / mcorr_binning_info[0] )
mcorr_bins_np = np.arange(mcorr_binning_info[1], mcorr_binning_info[2] + step, step)
mcorr_bins_nom = array('d', tuple(mcorr_bins_np))
#mcorr_bins = array('d', (5000, 5200, 5400, 5600, 5800, 
mcorr_bins = array('d', (5400, 5800,
                         6000, 6200, 6400, 6600, 6800, 
                         7000, 7200, 7400, 
                         7800, 8200, 8600, 9000, 9400,
                         9800, 10200, 10600, 11000))

mcorr_bins_for_2D = array('d', (5400, 6000, 6400, 6800, 
                                7200, 8500, 11000))

mvis = "Bc_MDTF_M"
mvis_binning_info = (16, 5450, 6650)
step_bcm = int( (mvis_binning_info[2] - mvis_binning_info[1]) / mvis_binning_info[0] )
mvis_bins_np = np.arange(mvis_binning_info[1], mvis_binning_info[2] + step_bcm, step_bcm)
mvis_bins = array('d', tuple(mvis_bins_np))
mvis_binning_info_for_2D = (6, 5450, 6650)
step_bcm_for_2D = int( (mvis_binning_info_for_2D[2] - mvis_binning_info_for_2D[1]) / mvis_binning_info_for_2D[0] )
mvis_bins_np_for_2D = np.arange(mvis_binning_info_for_2D[1], mvis_binning_info_for_2D[2] + step_bcm_for_2D, step_bcm_for_2D)
mvis_bins_for_2D = array('d', (mvis_bins_np_for_2D))

# Fixed Bc2Bsmunu and Bc2Bs*munu ratio
ratio_dspi = (0.24, 0.08) # restricted version
ratio_jpsiphi = (0.18, 0.08)

## Dicts ##

# Fit ranges and central values of the yields + colours used for the stacked plot for the fit result
dict_of_fit_ranges_for_data_dspi = {
    "signal": (10, -150, 100, 433),        # kCyan + 1  
    "Bc2BsstMuNu" : (24, -100, 100, 863),  # kAzure + 3
    "signal_combined": (34, -150, 200, 433),        # kCyan + 1
    "Bc2BsK" : (5, 0, 500, 633),         # kRed + 1
    "Bc2BsKst" : (5, 0, 500, 801),       # kOrange + 1
    "Bc2BsRho" : (5, -500, 500, 875),       # kViolet -5, kMagenta - 1 # used to be kSpring + 5
    "combinatoric": (600, 0, 2000, 825), # kSpring + 5
    #"combinatoric_Mcorr": (200, 0, 2000, 825), # kSpring + 5
    #"combinatoric_Mvis": (180, 0, 2000, 825), # kSpring + 5
    "Bc2BsPi_misID": (5, -500, 500, 797)       # kOrange - 3
}
# same dict for jpsiphi
#dict_of_fit_ranges_for_data_jpsiphi = {}
#for key, value in dict_of_fit_ranges_for_data_dspi.items():
#    dict_of_fit_ranges_for_data_jpsiphi[key] = value
dict_of_fit_ranges_for_data_jpsiphi = {
    "signal": (10, -150, 100, 433),        # kCyan + 1 
    "Bc2BsstMuNu" : (24, -100, 100, 863),  # kAzure + 3
    "signal_combined": (34, -150, 150, 433),        # kCyan + 1 
    "Bc2BsK" : (5, 0, 500, 633),         # kRed + 1
    "Bc2BsKst" : (5, 0, 500, 801),       # kOrange + 1
    "Bc2BsRho" : (5, -500, 500, 875),       # kViolet -5, kMagenta - 1 # used to be kSpring + 5
    "combinatoric": (240, 0, 2000, 825), # kSpring + 5
    #"combinatoric_Mcorr": (200, 0, 2000, 825), # kSpring + 5
    #"combinatoric_Mvis": (180, 0, 2000, 825), # kSpring + 5
    "Bc2BsPi_misID": (5, -500, 500, 797)       # kOrange - 3
}


dict_of_fit_ranges_for_toys = {
    "signal": (150, 0, 1000, 433),
    "Bc2BsstMuNu" : (120, 0, 1000, 863),
    "Bc2BsK" : (20, 0, 500, 633),
    "Bc2BsKst" : (20, 0, 500, 801),
    "Bc2BsRho" : (20, 0, 500, 615),
    "combinatoric": (3000, 0, 10000, 825)
}
dict_of_leg_entries = {
    "signal": "B_{c}^{+} #rightarrow B_{s}^{0} #mu^{+} #nu_{#mu}",
    "combinatoric": "Combinatorial bkg",
    "Bc2BsstMuNu" : "B_{c}^{+} #rightarrow B_{s}^{*0} #mu^{+} #nu_{#mu}",
    "signal_combined" : "B_{c}^{+} #rightarrow B_{s}^{(*)0} #mu^{+} #nu_{#mu}",
    "Bc2BsK" : "B_{c}^{+} #rightarrow B_{s}^{0} K^{+}",
    "Bc2BsKst" : "B_{c}^{+} #rightarrow B_{s}^{0} K^{*+}",
    "Bc2BsRho" : "B_{c}^{+} #rightarrow B_{s}^{0} #rho^{+}",
    "Bc2BsPi_misID": "B_{c}^{+} #rightarrow B_{s}^{0} #pi^{+}"
}


#################################
# General Functions             #
#################################

def pause():
    programPause = input("Press the <ENTER> key to continue...")

def printBlindWarning():
    print("Blinding set to FALSE, last warning before unblinding")
    programPause = input("Press the <ENTER> key to continue or ctrl-C to exit...")

# Feldman-Cousins method for getting a confidence interval
# Based on https://root.cern.ch/doc/master/rs401c__FeldmanCousins_8py.html
def runFeldmanCousins(x, sig_yield, bkg_yield, model, toy, CL):
    
    # Time it for fun
    t = R.TStopwatch()
    t.Start()

    # Set min and max of POI of sig yield
    val = sig_yield.getValV(); err = sig_yield.getError()
    parameters = R.RooArgSet(sig_yield)
    min = val - 10*err; max = val + 10*err
    print(f"min = {min}, max = {max}")
    #if min < 0:
    #    min = 0 # not sure what negative would do
    sig_yield.setMin(min); sig_yield.setMax(max); # set to 10 sigma away from fit

    # Set background to constant
    #combVal = bkg_yield.getValV()
    bkg_yield.setConstant(True)

    # Set up ModelConfig for RooStats object
    w = R.RooWorkspace()
    modelConfig = R.RooStats.ModelConfig("myModel", w)
    modelConfig.SetPdf(model)
    modelConfig.SetParametersOfInterest(parameters) # parameter of interest is signal yield
    modelConfig.SetObservables(R.RooArgSet(x))
    w.Print()

    print(toy)
    print(f"Number of entries in toy dataset: {toy.sumEntries()}")
    print(model)
    print(sig_yield)
    print(bkg_yield)

    # Set up Feldman-Cousins
    fc = R.RooStats.FeldmanCousins(toy, modelConfig)
    fc.SetConfidenceLevel(CL)
    fc.UseAdaptiveSampling(True) # not sure what this does
    fc.FluctuateNumDataEntries(False)  # number counting analysis: dataset always has 1 entry with N events observed
    fc.SetNBins(100) # number of points to test per parameter

    # Get interval
    interval = fc.GetInterval()
    result = [interval.LowerLimit(sig_yield), interval.UpperLimit(sig_yield)]

    t.Stop()
    t.Print()

    return result
