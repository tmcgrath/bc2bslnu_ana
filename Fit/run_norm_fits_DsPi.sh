#!/bin/bash

python3 fit_Bc2BsPi.py -c DsPi -bkg exp
python3 fit_Bc2BsPi.py -c DsPi -sig dcb -bkg exp

python3 fit_Bc2BsPi.py -c DsPi -bkg cheb
python3 fit_Bc2BsPi.py -c DsPi -sig dcb -bkg cheb
