# Fits!

Before running fits, make sure the [`BDT_cuts.py`](./BDT_cuts.py) is up to date as the BDT selection is applied in the fit code.

## Normalisation mode: $B_c^+\to B_s^0 \pi^+$

Fit code is [`fit_Bc2BsPi.py`](./fit_Bc2BsPi.py). Can do 
- Gaussian or double crystal ball for signal model 
- Exponential or chebychev background model 

based on input arguments. Nominal model is gaussian signal, exponential background. The radiative mode $B_c^+ \to B_s^{*0} \pi^+$ is gaussian. 

## Signal mode: $B_c^+\to B_s^0 \mu^+\nu_\mu$

Before running the fits, the templates are made with [`makeTemplates.py`](./makeTemplates.py), which outputs 1D templates and 2D templates into one root file per $B_s^0$ channel. The shell script with the commands and arguments I used is [`run_template_making.py`](./run_template_making.sh).

Fit code is [`fit_Bc2BsPi.py`](./fit_Bc2BsMuNu.py), which currently contains functions to fit 1D, 1D simultaneous, and 2D models with sampled templates. The toy studies in the code is quite robust but the fits to data may need some more work. 
It imports functions from:
- [`common_things.py`](./common_things.py): contains binning for the 1D, 2D fits, fit ranges and labels for each channel in the model 
- [`drawOptions.py`](./drawOptions.py): functions for drawing plots

The [Beeston-Barlow method](https://www.sciencedirect.com/science/article/abs/pii/001046559390005W) application with HistFactory is included in the 2D model as an optional thing. This isn't fully debugged, but the current syntax is based on someone else's 2D fit application.

An example of applying the [Feldman-Cousins method](https://arxiv.org/abs/physics/9711021) for CL calculations is written in the code using RooStats. This also needs some repurposing.
