#
# eventMixing.py
#
# apply event mixing to BDT selected samples
#
# 07.02.23
#

# General Python packages
import uproot as ur
from root_pandas import read_root
import pandas as pd
import numpy as np
import vector
import argparse
import sys, os
import math

sys.path.append("/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit")
from BDT_cuts import BDT_cuts_sig as BDT_cuts

##################################################
# Functions
##################################################

def get_df(filename):
    theDF = read_root(filename)
    return theDF

def applyBDTCut_DsPi(df, year):
    df_tm = df.query("Bc_is_from_Bc_BsMuNu == 1 & Bs_is_from_Bs_DsPi == 1")
    bdt_cut = BDT_cuts["DsPi"][year]
    df_cuts = df_tm.query(f"Bs_MM > 5300 & Bs_MM < 5420 & {bdt_cut}")
    return df_cuts

def applyBDTCut_JpsiPhi(df, year):
    df_tm = df.query("Bc_is_from_Bc_BsMuNu == 1 & Bs_is_from_Bs_JpsiPhi == 1")
    bdt_cut = BDT_cuts["JpsiPhi"][year]
    df_cuts = df_tm.query(bdt_cut)
    return df_cuts


## Functions for calculating the corrected mass
#def calcPtPrime():


#def calcInvM():

# Function for just parallelised mcorr calculation
def calcMcorrMvis(df_Bs, df_mu):

    # calculation
    # Bs and mu 4-momenta
    Bs_P = vector.array({
        "px": df_Bs.Bs_PX,
        "py": df_Bs.Bs_PY, 
        "pz": df_Bs.Bs_PZ,
        "E" : df_Bs.Bs_PE
    })
    Bach_P = vector.array({
        "px": df_mu.Bach_PX,
        "py": df_mu.Bach_PY, 
        "pz": df_mu.Bach_PZ,
        "E" : df_mu.Bach_PE
    })
    Bs_mom = vector.array({
        "px": df_Bs.Bs_PX,
        "py": df_Bs.Bs_PY, 
        "pz": df_Bs.Bs_PZ
    })
    Bach_mom = vector.array({
        "px": df_mu.Bach_PX,
        "py": df_mu.Bach_PY, 
        "pz": df_mu.Bach_PZ
    })
    # Bc flight direction
    Bc_PV = vector.array({
        "x": df_Bs.Bc_OWNPV_X,
        "y": df_Bs.Bc_OWNPV_Y,
        "z": df_Bs.Bc_OWNPV_Z
    })
    Bc_SV = vector.array({
        "x": df_Bs.Bc_ENDVERTEX_X,
        "y": df_Bs.Bc_ENDVERTEX_Y,
        "z": df_Bs.Bc_ENDVERTEX_Z
    })

    tot3vec = Bs_mom + Bach_mom
    tot4vec = Bs_P + Bach_P
    fd = Bc_SV - Bc_PV
    mag = np.multiply( fd.mag, tot3vec.mag )
    dira = np.divide( fd.dot(tot3vec), mag )
    dir = np.arccos(dira)
    sin = np.sin(dir)

    mvis2 = tot4vec.M2
    mvis_array = tot4vec.M
    ptp = np.multiply(sin, tot4vec.p)
    ptp2 = np.multiply( ptp, ptp )
    
    step = np.sqrt( np.add(mvis2, ptp2) )
    mcorr_array = step + np.abs(ptp)

    return mcorr_array, mvis_array # returns np array 

# Function that applies
def eventMix(df):
    # empty df to fill new Mcorr
    newdf = pd.DataFrame()
    df_copy = df.copy()

    # new df to get number
    nRows = df_copy.shape[0]

    mcorr, mvis = calcMcorrMvis(df_copy, df_copy)

    df_copy.loc[:,'Bc_CORR_M_calc'] = mcorr
    df_copy.loc[:,'Bc_MM_calc'] = mvis

    print(df_copy[["Bc_CORR_M", "Bc_CORR_M_calc", "Bc_MM", "Bc_MM_calc"]])

    # now do this with the df where the columns are rotated an iterable number of times for the Bach columns
    df_dict = {}
    for next in range(1,11):
        # make a copy of the df
        rotated_df = pd.DataFrame()
        rotated_df.loc[:, 'Bach_PX'] = np.roll( df_copy.Bach_PX, next )
        rotated_df.loc[:, 'Bach_PY'] = np.roll( df_copy.Bach_PY, next )
        rotated_df.loc[:, 'Bach_PZ'] = np.roll( df_copy.Bach_PZ, next )
        rotated_df.loc[:, 'Bach_PE'] = np.roll( df_copy.Bach_PE, next )
        #rotated_df.loc[:, 'eventNumber'] = np.roll( df_copy.Bach_PY, next )
        rotated_df.loc[:, 'index'] = np.roll( df_copy.index, next )

        new_mcorrs, new_mvis = calcMcorrMvis(df_copy, rotated_df)
        rotated_df.loc[:, 'Bc_CORR_M_calc'] = new_mcorrs
        rotated_df.loc[:, 'Bc_MM_calc'] = new_mvis

        print(df_copy[["Bach_PZ"]])
        print(rotated_df[["Bach_PZ"]])

        df_dict[next] = rotated_df.copy()

    #print(df_dict) 

    df_list = list(df_dict.values())
    print(len(df_list))
    newdf = pd.concat(df_list, ignore_index=True)

    return newdf, df_copy

parser=argparse.ArgumentParser()
parser.add_argument('--year','-y',type=int,default = 0,choices=[0, 2016, 2017, 2018], help='Enter year of data and MC you would like to run the BDT or 0 for all years combined (2016-2018)')
parser.add_argument('--bachelor','-b', type=str, default = "Mu", choices=["Mu", "Pi"], help='Bachelor particle of decay to train BDT for (Mu or Pi)')
parser.add_argument('--chan', '-c', required=True, type=str, choices = ["DsPi", "JpsiPhi"], help='Bs decay channel (DsPi or JpsiPhi)' )
args = parser.parse_args()

year = args.year
year_str = str(year)
if year == 0:
    year_str = "All"

bach = args.bachelor
chan = args.chan

infolder = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied"
outFolder = infolder
infilename = f"Bc2Bs{bach}_Bs2{chan}_{year_str}_MC_BDT.root"
file_to_mix   = f"{infolder}/{infilename}"
print(f"Getting file to mix: {file_to_mix}")

df_from_file = get_df(file_to_mix)
if chan == "DsPi":
    df_to_mix = applyBDTCut_DsPi(df_from_file, year_str)
else:
    print("Applying cut")
    df_to_mix = applyBDTCut_JpsiPhi(df_from_file, year_str)
df_to_mix.reset_index(inplace=True) # can drop if I don't want to add index as a column

# Apply event mixing and also sanity check mcorr calculation
mixed, df_check = eventMix(df_to_mix)

outfilename = f"EventMixed_Bc2Bs{bach}_Bs2{chan}_{year_str}.root"
checkfilename = f"Bc2Bs{bach}_Bs2{chan}_{year_str}_MC_BDT_calculatedMcorr.root"
print(f"Saving output {outfilename}")

# Saving check
outMC   = ur.recreate(f"{outFolder}/{checkfilename}")
outMC["DecayTree"]   = df_check.dropna()

mixedFile = ur.recreate(f"{outFolder}/{outfilename}")
mixedFile["DecayTree"]   = mixed.dropna()

