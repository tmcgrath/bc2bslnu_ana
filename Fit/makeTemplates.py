#
# makeTemplates.py
#
# Script to make the templates for the fit on the Bc->Bsmunu corrected mass
# Saves both the histograms in a root file and HistFactory samples in a RooWorkspace
#
# 11.11.22
#

import sys, glob
import ROOT as R
from ROOT import TFile
import argparse
RDF = R.RDataFrame

sys.path.append("/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit")
from BDT_cuts import BDT_cuts_sig as BDT_cuts

#################
# Cuts required #
#################

def applyCuts_DsPi(df, TM, bdt=True, comb=False, year="All"):
    year = str(year)
    filtered_df = df.Filter(TM, TM)
    if bdt==True:
        filtered_df = filtered_df.Filter("Bs_MM > 5300 & Bs_MM < 5420", "Bs_MM > 5300 & Bs_MM < 5420").Filter(BDT_cuts["DsPi"][year], BDT_cuts["DsPi"][year])
        print("Applying BDT cuts")
    else:
        print("Not applying BDT cuts")
    if comb == True:
        filtered_df = filtered_df.Filter("abs(Bach_TRUEID) == 13 & Bs_MC_MOTHER_KEY != Bach_MC_MOTHER_KEY", "abs(Bach_TRUEID) == 13 & Bs_MC_MOTHER_KEY != Bach_MC_MOTHER_KEY")
    return filtered_df

def applyCuts_JpsiPhi(df, TM, bdt=True, comb=False, year="All"):
    year = str(year)
    filtered_df = df.Filter(TM, TM)
    if bdt==True:
        filtered_df = filtered_df.Filter(BDT_cuts["JpsiPhi"][year], BDT_cuts["JpsiPhi"][year])
        print("Applying BDT cuts")
    else:
        print("Not applying BDT cuts")
    if comb == True:
        filtered_df = filtered_df.Filter("abs(Bach_TRUEID) == 13 & Bs_MC_MOTHER_KEY != Bach_MC_MOTHER_KEY", "abs(Bach_TRUEID) == 13 & Bs_MC_MOTHER_KEY != Bach_MC_MOTHER_KEY")
    return filtered_df

def getDF(channel, file_string, tree="DecayTree"):
    df = RDF(tree, file_string.format(channel))

    return df

# temporary df making of the background channels while we have small amount of samples
def temp_getDF(channel, file_string, tree="DecayTree"):
    files = glob.glob(file_string.format(channel))
    filesVec = R.std.vector('string')()
    for f in files:
        filesVec.push_back(f)
    df = RDF(tree, filesVec)

    return df

#######################
# Global variables    #
#######################

# Dict of fit ranges has colours for each one in third index of tuple
from common_things import (mcorr, mcorr_bins_nom, mcorr_bins, mvis, mvis_bins, mcorr_bins_for_2D, mvis_bins_for_2D, 
                           dict_of_fit_ranges_for_data_dspi, dict_of_fit_ranges_for_data_jpsiphi, 
                           dict_of_leg_entries)

dict_of_pdf_names_TM_DsPi ={
    "signal": ["signalPDF", "Bc_is_from_Bc_BsMuNu == 1"],
    "combinatoric": ["combPDF", ""],
    "Bc2BsstMuNu" : ["BsstMuNuPDF", "Bc_is_from_Bc_BsstarMuNu == 1"],
    "Bc2BsK" : ["BsKPDF", "Bc_is_from_Bc_BsK"],
    "Bc2BsKst" : ["BsKstPDF", "(Bc_is_from_Bc_BsKstar_Ks2pippim == 1 || Bc_is_from_Bc_BsKstar_Ks2pizpiz == 1)"],
    "Bc2BsRho" : ["BsRhoPDF", "Bc_is_from_Bc_BsRho == 1"],
    "Bc2BsPi_misID": ["BsPiMisIDPDF", "Bc_is_from_Bc_BsPi == 1"]
}
dict_of_pdf_names_TM_JpsiPhi ={
    "signal": ["signalPDF", "Bc_is_from_Bc_BsMuNu == 1"],
    "combinatoric": ["combPDF", ""],
    "Bc2BsstMuNu" : ["BsstMuNuPDF", "Bc_is_from_Bc_BsstMuNu == 1"],
    "Bc2BsK" : ["BsKPDF", "Bc_is_from_Bc_BsK"],
    "Bc2BsKst" : ["BsKstPDF", "(Bc_is_from_Bc_BsKstar_Ks2pippim == 1 || Bc_is_from_Bc_BsKstar_Ks2pizpiz == 1)"],
    "Bc2BsRho" : ["BsRhoPDF", "Bc_is_from_Bc_BsRho == 1"],
    "Bc2BsPi_misID": ["BsPiMisIDPDF", "Bc_is_from_Bc_BsPi == 1"]
}

dict_of_files_fullMC = {
    "signal": "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsMu_Bs2{}_All_MC_BDT.root",
    "combinatoric": "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bs2{}_All_MC_BDT.root",
    "Bc2BsstMuNu" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsstMuNu_Bs2{}_All_MC_BDT.root",
    "Bc2BsK" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsK_Bs2{}_All_MC_BDT.root",
    "Bc2BsKst" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsKst_Bs2{}_All_MC_BDT.root",
    "Bc2BsRho" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsRho_Bs2{}_All_MC_BDT.root",
    "Bc2BsPi_misID" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsPi_misID_Bs2DsPi_All_MC_BDT.root" # only DsPi available for both channels
}

dict_of_files_redecay = {
    "signal": "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsMu_Bs2{}_All_MC_BDT.root",
    "combinatoric": "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bs2{}_All_MC_BDT.root",
    "Bc2BsstMuNu" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsstMuNu_Bs2{}_All_ReDecay_BDT.root",
    "Bc2BsRho" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsRho_Bs2{}_All_ReDecay_BDT.root",
    "Bc2BsK" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bc2BsK/2018/Bc2BsK_Bs2{}_2018_*_MC_job*_merged.root",
    "Bc2BsKst" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bc2BsKst/2018/Bc2BsKst_Bs2{}_2018_*_MC_job*_merged.root",
    "Bc2BsPi_misID" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsPi_misID_Bs2DsPi_All_MC_BDT.root" # only DsPi available for both channels
}

dict_of_files_temp = {
    "signal": "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsMu_Bs2{}_All_MC_BDT.root",
    "combinatoric": "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bs2{}_All_MC_BDT.root",
    "Bc2BsstMuNu" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bc2BsstMuNu/2018/Bc2BsstMuNu_Bs2{}_2018_*_MC_job*_merged.root",
    "Bc2BsK" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bc2BsK/2018/Bc2BsK_Bs2{}_2018_*_MC_job*_merged.root",
    "Bc2BsKst" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bc2BsKst/2018/Bc2BsKst_Bs2{}_2018_*_MC_job*_merged.root",
    "Bc2BsRho" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MC/Bc2BsRho/2018/Bc2BsRho_Bs2{}_2018_*_MC_job*_merged.root",
    "Bc2BsPi_misID" : "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/Bc2BsPi_misID_Bs2DsPi_All_MC_BDT.root" # only DsPi available for both channels
}


#################
# Main          #
#################

if __name__ == "__main__":
    
    # set 1D histogram binning for various plotting purposes - comment below out if using nominal for 1D
    #mcorr_bins = mcorr_bins_for_2D
    #mvis_bins = mvis_bins_for_2D

    # Set batch mode
    R.gROOT.SetBatch( True )
    R.gROOT.ProcessLine(".L /afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/lhcbStyle.C")

    # Arguments
    parser=argparse.ArgumentParser()
    parser.add_argument('--year','-y',required=False, type=int,default = 0,choices=[0, 2016, 2017, 2018], help='Enter year of data and MC you would like to run the BDT or 0 for all years combined (2016-2018)')
    parser.add_argument('--channel','-c',required=True, type=str, choices=["DsPi", "JpsiPhi"], help='Specify Bs decay channel (DsPi or JpsiPhi)')
    parser.add_argument('--redecay', '-r', action="store_true", help="Flag whether to use the redecay files or not")
    parser.add_argument('--save_figs', '-s', action="store_true", help='Flag whether to save the figures as pdfs (in addition to a root file)')
    args = parser.parse_args()

    # Get arguments
    year = "All" if args.year == 0 else str(args.year)
    chan = args.channel
    redecay = args.redecay
    if chan == "DsPi":
        applyCuts = applyCuts_DsPi
        dict_of_fit_ranges_for_data = dict_of_fit_ranges_for_data_dspi
        dict_of_pdf_names_TM = dict_of_pdf_names_TM_DsPi
        TM_Bs = "Bs_is_from_Bs_DsPi == 1"
    else:
        applyCuts = applyCuts_JpsiPhi
        dict_of_fit_ranges_for_data = dict_of_fit_ranges_for_data_jpsiphi
        dict_of_pdf_names_TM = dict_of_pdf_names_TM_JpsiPhi
        TM_Bs = "Bs_is_from_Bs_JpsiPhi == 1"
    save_figs = args.save_figs

    if redecay:
        print("Using ReDecay files for bkg")
        dict_of_files = dict_of_files_redecay
    else:
        print("Using full MC for bkg")
        dict_of_files = dict_of_files_temp

    # HistFactory stuff
    

    # Get histograms from file
    # temporary handling
    dict_of_hists = {}; dict_of_hists_mvis = {}; dict_of_hists_2D = {}
    for key, fileStr in dict_of_files.items():
        if key == "Bc2BsK" or key == "Bc2BsKst": continue
        TMStr = TM_Bs + " && " + dict_of_pdf_names_TM[key][1]

        print(f"On {key}, using truth-matching {TMStr}")
        if key == "signal":
            DF = getDF(chan, fileStr)
            filtered = applyCuts(DF, TMStr)
            mcorr_hist = filtered.Histo1D((dict_of_pdf_names_TM[key][0], key, len(mcorr_bins)-1, mcorr_bins), mcorr)
            mcorr_hist = mcorr_hist.GetPtr()
            mvis_hist = filtered.Histo1D((f"{dict_of_pdf_names_TM[key][0]}_Bcmvis", key, len(mvis_bins)-1, mvis_bins), mvis)
            mvis_hist = mvis_hist.GetPtr()
            both_hist = filtered.Histo2D((f"{dict_of_pdf_names_TM[key][0]}_2D", key, len(mcorr_bins_for_2D)-1, mcorr_bins_for_2D, len(mvis_bins_for_2D)-1, mvis_bins_for_2D), mcorr, mvis)
            both_hist = both_hist.GetPtr()

        elif key == "combinatoric":
            # combine comb templates for both channels for more stats
            TMStr1 = "Bs_is_from_Bs_DsPi == 1" 
            TMStr2 = "Bs_is_from_Bs_JpsiPhi == 1" 
            DF1 = getDF("DsPi", fileStr)
            DF2 = getDF("JpsiPhi", fileStr)
            filtered1 = applyCuts_DsPi(DF1, TMStr1, comb=True)
            filtered2 = applyCuts_JpsiPhi(DF2, TMStr2, comb=True)
            mcorr_hist_1 = filtered1.Histo1D((dict_of_pdf_names_TM[key][0], key+" Mcorr Bs2DsPi", len(mcorr_bins)-1, mcorr_bins), mcorr)
            mcorr_hist_2 = filtered2.Histo1D((dict_of_pdf_names_TM[key][0], key+" Mcorr Bs2JpsiPhi", len(mcorr_bins)-1, mcorr_bins), mcorr)
            mcorr_hist_1 = mcorr_hist_1.GetPtr(); mcorr_hist_2 = mcorr_hist_2.GetPtr()
            mcorr_hist = mcorr_hist_1.Clone()
            mcorr_hist.Add(mcorr_hist_2)

            mvis_hist_1 = filtered1.Histo1D((f"{dict_of_pdf_names_TM[key][0]}_Bcmvis", key+" Bc mass Bs2DsPi", len(mvis_bins)-1, mvis_bins), mvis)
            mvis_hist_2 = filtered2.Histo1D((f"{dict_of_pdf_names_TM[key][0]}_Bcmvis", key+" Bc mass Bs2JpsiPhi", len(mvis_bins)-1, mvis_bins), mvis)
            mvis_hist_1 = mvis_hist_1.GetPtr(); mvis_hist_2 = mvis_hist_2.GetPtr()
            mvis_hist = mvis_hist_1.Clone()
            mvis_hist.Add(mvis_hist_2)

            both_hist_1 = filtered1.Histo2D((f"{dict_of_pdf_names_TM[key][0]}_2D", key, len(mcorr_bins_for_2D)-1, mcorr_bins_for_2D, len(mvis_bins_for_2D)-1, mvis_bins_for_2D), mcorr, mvis)
            both_hist_2 = filtered2.Histo2D((f"{dict_of_pdf_names_TM[key][0]}_2D", key, len(mcorr_bins_for_2D)-1, mcorr_bins_for_2D, len(mvis_bins_for_2D)-1, mvis_bins_for_2D), mcorr, mvis)
            both_hist_1 = both_hist_1.GetPtr(); both_hist_2 = both_hist_2.GetPtr()
            both_hist = both_hist_1.Clone()
            both_hist.Add(both_hist_2)

        elif key == "Bc2BsPi_misID": # only DsPi channel available for Bc2BsPi
            TMStr = "Bs_is_from_Bs_DsPi == 1 && " + dict_of_pdf_names_TM[key][1]
            DF = getDF("DsPi", fileStr)
            filtered = applyCuts_DsPi(DF, TMStr)
            mcorr_hist = filtered.Histo1D((dict_of_pdf_names_TM[key][0], key, len(mcorr_bins)-1, mcorr_bins), mcorr)
            mcorr_hist = mcorr_hist.GetPtr()
            mvis_hist = filtered.Histo1D((f"{dict_of_pdf_names_TM[key][0]}_Bcmvis", key, len(mvis_bins)-1, mvis_bins), mvis)
            mvis_hist = mvis_hist.GetPtr()
            both_hist = filtered.Histo2D((f"{dict_of_pdf_names_TM[key][0]}_2D", key, len(mcorr_bins_for_2D)-1, mcorr_bins_for_2D, len(mvis_bins_for_2D)-1, mvis_bins_for_2D), mcorr, mvis)
            both_hist = both_hist.GetPtr()
        else:
            if redecay == True and (key == "Bc2BsstMuNu" or key == "Bc2BsRho"):
                print(f"Using ReDecay files with BDT cuts for {key}")
                DF = getDF(chan, fileStr)
                filtered = applyCuts(DF, TMStr)
            else: 
                DF = temp_getDF(chan, fileStr)
                filtered = applyCuts(DF, TMStr, bdt=False)
            mcorr_hist = filtered.Histo1D((dict_of_pdf_names_TM[key][0], key, len(mcorr_bins)-1, mcorr_bins), mcorr)
            mcorr_hist = mcorr_hist.GetPtr()
            mvis_hist = filtered.Histo1D((f"{dict_of_pdf_names_TM[key][0]}_Bcmvis", key, len(mvis_bins)-1, mvis_bins), mvis)
            mvis_hist = mvis_hist.GetPtr()
            both_hist = filtered.Histo2D((f"{dict_of_pdf_names_TM[key][0]}_2D", key, len(mcorr_bins_for_2D)-1, mcorr_bins_for_2D, len(mvis_bins_for_2D)-1, mvis_bins_for_2D), mcorr, mvis)
            both_hist = both_hist.GetPtr()

        # Do stuff to the histograms
        mcorr_hist.Scale(1/mcorr_hist.Integral())
        mcorr_hist.Sumw2()
        dict_of_hists[key] = mcorr_hist

        mvis_hist.Scale(1/mvis_hist.Integral())
        mvis_hist.Sumw2()
        dict_of_hists_mvis[key] = mvis_hist

        ## Set bins with 0 in model to something very small for the fit
        Nx = both_hist.GetNbinsX(); Ny = both_hist.GetNbinsY()
        for j in range(1, Ny+1):
            for i in range(1, Nx+1):
                bin_content = both_hist.GetBinContent(i, j)
                #if bin_content == 0.0:
                    #both_hist.SetBinContent(i, j, 1e-6)

        both_hist.Scale(1/both_hist.Integral())
        both_hist.Sumw2()
        dict_of_hists_2D[key] = both_hist

    # Add a histogram for combination of signal and Bc2Bsstmunu
    # Just use Bc2Bsstmunu cos it uses more MC
    #sig_hist = dict_of_hists["signal"];
    #comb_hist = averageHist(sig_hist, rad_hist)
    comb_hist_name = "signal_combined"
    comb_hist_pdf = f"sigCombinedPDF"
    rad_hist = dict_of_hists["Bc2BsstMuNu"].Clone()
    rad_hist.SetName(comb_hist_pdf)
    rad_hist.SetTitle(comb_hist_name)
    dict_of_hists[comb_hist_name] = rad_hist
    
    rad_hist_mvis = dict_of_hists_mvis["Bc2BsstMuNu"].Clone()
    rad_hist_mvis.SetTitle(comb_hist_name)
    rad_hist_mvis.SetName(f"{comb_hist_pdf}_Bcmvis")
    dict_of_hists_mvis[comb_hist_name] = rad_hist_mvis

    rad_hist_2D = dict_of_hists_2D["Bc2BsstMuNu"].Clone()
    rad_hist_2D.SetTitle(comb_hist_name)
    rad_hist_2D.SetName(f"{comb_hist_pdf}_2D")
    dict_of_hists_2D[comb_hist_name] = rad_hist_2D


    # Open file and save the hists in file
    outdir = "./Templates"
    outfile = f"{outdir}/Mcorr_templates_{chan}.root"
    print(f"Saving histograms in {outfile}..........")
    outf = R.TFile.Open(outfile, "RECREATE"); outf.cd()

    # make canvas for a all pdfs on same plot
    if save_figs: 
        canv_mcorr = R.TCanvas()
        legend = R.TLegend(0.55,0.60,0.85,0.9)
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetFillStyle(4000)

        canv_mvis = R.TCanvas()
        legend2 = R.TLegend(0.2,0.60,0.55,0.9)
        legend2.SetBorderSize(0)
        legend2.SetFillColor(0)
        legend2.SetFillStyle(4000)

    for key, theHist in dict_of_hists.items():
        print("\n"+key)
        theHist.GetXaxis().SetTitle("M_{corr}(B_{c}^{+}) [MeV/c^{2}]")
        theHist.GetYaxis().SetTitle("AU") #/ [200 MeV/c^{2}]
        theHist.Write() # set histogram name based on dict key
    
        # Saving figs if flag set
        if save_figs:
            print(f"Saving histogram figures in {outdir}..........")
            canv = R.TCanvas()
            canv.cd()
            theHist.SetTitle("")
            theHist.SetLineColor(dict_of_fit_ranges_for_data[key][3])
            theHist.Draw("hist")
            canv.Update()
            canv.SaveAs(f"{outdir}/Figs/{key}_{chan}.png")
            canv.SaveAs(f"{outdir}/Figs/{key}_{chan}.pdf")
            canv.SaveAs(f"{outdir}/Figs/{key}_{chan}.C")

            if key == comb_hist_name: break
            canv_mcorr.cd()
            option = "hist" if key == "signal" else "hist,same"
            if key=="signal":
                theHist.GetYaxis().SetRangeUser(0, 0.55)
            theHist.Draw(option)
            canv_mcorr.Update()
            legend.AddEntry(theHist, dict_of_leg_entries[key], "l")

    for key, theHist in dict_of_hists_mvis.items():
        print("\n"+key)
        theHist.GetXaxis().SetTitle("M(B_{s}^{0} #mu^{+}) [MeV/c^{2}]")
        theHist.GetYaxis().SetTitle("AU") #/ [200 MeV/c^{2}]
        theHist.Write() # set histogram name based on dict key
    

        # Saving figs if flag set
        if save_figs:
            print(f"Saving histogram figures in {outdir}..........")
            canv = R.TCanvas()
            canv.cd()
            theHist.SetTitle("")
            theHist.SetLineColor(dict_of_fit_ranges_for_data[key][3])
            theHist.Draw("hist")
            canv.Update()
            canv.SaveAs(f"{outdir}/Figs/{key}_Bcmvis_{chan}.png")
            canv.SaveAs(f"{outdir}/Figs/{key}_Bcmvis_{chan}.pdf")
            canv.SaveAs(f"{outdir}/Figs/{key}_Bcmvis_{chan}.C")

            if key==comb_hist_name: break
            canv_mvis.cd()
            option = "hist" if key == "signal" else "hist,same"
            if key=="signal":
                theHist.GetYaxis().SetRangeUser(0, 0.9)
            theHist.Draw(option)
            canv_mvis.Update()
            legend2.AddEntry(theHist, dict_of_leg_entries[key], "l")

    R.gStyle.SetPalette(R.kBlueGreenYellow)
    for key, theHist in dict_of_hists_2D.items():
        print("\n"+key)
        theHist.GetXaxis().SetTitle("M_{corr}(B_{c}^{+}) [MeV/c^{2}]")
        theHist.GetYaxis().SetTitle("M(B_{s}^{0} #mu^{+}) [MeV/c^{2}]") 
        theHist.Write() # set histogram name based on dict key
    
        # Saving figs if flag set
        if save_figs:
            print(f"Saving histogram figures in {outdir}..........")
            canv = R.TCanvas()
            canv.cd()
            canv.SetRightMargin(0.14) # for z axis numbers to fit in frame
            theHist.SetTitle("")
            theHist.Draw("colz")
            canv.Update()
            canv.SaveAs(f"{outdir}/Figs/{key}_2D_{chan}.png")
            canv.SaveAs(f"{outdir}/Figs/{key}_2D_{chan}.pdf")
            canv.SaveAs(f"{outdir}/Figs/{key}_2D_{chan}.C")


    if save_figs:
        canv_mcorr.cd()
        legend.Draw()
        canv_mcorr.SaveAs(f"{outdir}/Figs/Most_PDFs_{chan}.png")
        canv_mcorr.SaveAs(f"{outdir}/Figs/Most_PDFs_{chan}.pdf")
        canv_mcorr.SaveAs(f"{outdir}/Figs/Most_PDFs_{chan}.C")

        canv_mvis.cd()
        legend2.Draw()
        canv_mvis.SaveAs(f"{outdir}/Figs/Most_PDFs_Bcmvis_{chan}.png")
        canv_mvis.SaveAs(f"{outdir}/Figs/Most_PDFs_Bcmvis_{chan}.pdf")
        canv_mvis.SaveAs(f"{outdir}/Figs/Most_PDFs_Bcmvis_{chan}.C")

    outf.Close()

