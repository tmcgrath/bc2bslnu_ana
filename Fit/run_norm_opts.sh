#!/bin/bash

if [ $1 == '-d' ]; then
    echo Saving individual fits
    python3 -b fit_Bc2BsPi_opt_BDT.py -c DsPi -d
    python3 -b fit_Bc2BsPi_opt_BDT.py -c JpsiPhi -d
else
    python3 -b fit_Bc2BsPi_opt_BDT.py -c DsPi
    python3 -b fit_Bc2BsPi_opt_BDT.py -c JpsiPhi
fi
