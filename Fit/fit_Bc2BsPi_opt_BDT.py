#
# fit_Bc2BsPi_opt_BDT.py
#
# Optimise Bc BDT cuts by 
#
# 11.11.22
#

import argparse
import ROOT as R
import os, sys
import math

from BDT_cuts import BDT_cuts_norm as BDT_cuts
R.gROOT.SetBatch(True)

import matplotlib.font_manager; 
print(matplotlib.font_manager.fontManager.ttflist)
#################################
# Functions                     #
#################################

#def sigModel():

def pause():
    programPause = input("Press the <ENTER> key to continue...")

# gets chi2 from frame
def getChi2( frame, nParams ):
    chi2 = frame.chiSquare(nParams)
    return chi2

def plotFit(frame, pullFrame, chi2ndof, xlabel, xrange=[6200, 6550]):
    frame.getAttText().SetTextSize(0.0275)
    #frame.getAttText().SetTextFont(132)
    frame.GetXaxis().SetLabelOffset(999) # Remove X axis labels
    frame.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    #if chan == "JpsiPhi":

    frame.GetYaxis().SetLabelFont(132)
    frame.GetYaxis().SetTitleFont(132)
    frame.getAttText().SetLineWidth(0)
    frame.getAttText().SetFillStyle(0)
    frame.GetYaxis().SetLabelSize(0.04)
    frame.SetTitleSize(0.04, 'XY')

    # add chi2 to plot
    label = R.TPaveLabel(0.55,0.35,0.95, 0.4, "#chi^{2}/ndof = %f"%chi2ndof)
    frame.addObject(label)

    # Plot pulls 
    pullFrame.SetTitle("")
    pullFrame.GetXaxis().SetTitleOffset(1.)
    pullFrame.SetMinimum(-5)
    pullFrame.SetMaximum(5)
    pullFrame.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    pullFrame.GetXaxis().SetTitleOffset(1.)
    pullFrame.GetXaxis().SetLabelFont(132)
    pullFrame.GetXaxis().SetTitleFont(132)
    pullFrame.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    pullFrame.SetLabelSize(0.08, 'XY')
    pullFrame.GetYaxis().SetTitle('(data - model)/#font[12]{#sigma}_{data}')
    pullFrame.GetYaxis().SetTitleOffset(0.5)
    pullFrame.GetYaxis().SetNdivisions(209)
    pullFrame.GetYaxis().SetLabelFont(132)
    pullFrame.GetYaxis().SetTitleFont(132)
    pullFrame.SetTitleSize(0.08, 'XY')
    pullFrame.SetFillColor(R.kSpring-6)

    c = R.TCanvas("c", "c", 600, 700)
    upperPad = R.TPad('upperPad', 'upperPad', .005, .330, .995, .995)
    lowerPad = R.TPad('lowerPad', 'lowerPad', .005, .005, .995, .330)
    upperPad.SetBottomMargin(0.03)
    lowerPad.SetTopMargin(0.02)
    lowerPad.SetBottomMargin(0.3)
    upperPad.SetLeftMargin(0.15)
    lowerPad.SetLeftMargin(0.15)
    upperPad.SetRightMargin(0.05)
    lowerPad.SetRightMargin(0.05)
    upperPad.Draw()
    lowerPad.Draw()

    upperPad.cd()
    frame.Draw()
    label.Draw()
    upperPad.Update()
    lowerPad.cd()
    line = R.TLine(5100,0,14100,0)
    line.SetLineColor(1)
    pullFrame.Draw()
    line.Draw()
    c.Draw()
    
    return c

#################################
# Fitting                       #
#################################

def fitMC(MCvar, ds, saveLocation):

    # Set up fit model #
    # Fit parameters
    mu = R.RooRealVar("mean", "Mean of DCB", 6270, 6250, 6300)
    sigma = R.RooRealVar("sigma", "Width of DCB", 0.01, 200)
    aL = R.RooRealVar("alphaL", "Tail threshold left of DCB", 1, 0.2, 3)
    aR = R.RooRealVar("alphaR", "Tail threshold right of DCB", 1, 0.2, 3)
    nL = R.RooRealVar("nL", "n left of DCB", 0.01, 10)
    nR = R.RooRealVar("nR", "n left of DCB", 0.01, 10)
    if year is None:
        dcb_yield = R.RooRealVar("yield", "yield", 0, 100000)
    else:
        dcb_yield = R.RooRealVar("yield", "yield", 0, 10000)

    # Model
    dcb = R.RooCrystalBall("dcb_pdf", "Double Crystal Ball PDF", MCvar, mu, sigma, aL, nL, aR, nR)

    # Signal model for MC
    mc_model = R.RooAddPdf("dcb_model", "Double Crystal Ball model", R.RooArgList(dcb), R.RooArgList(dcb_yield))

    # Fit!
    mc_fit = mc_model.fitTo(ds, R.RooFit.Range(6200, 6550), R.RooFit.Save(True))
    params = mc_fit.floatParsFinal().getSize() # number of parameters for chi2 test

    # Plot!
    theFrame_MC = MCvar.frame()
    ds.plotOn(theFrame_MC, R.RooLinkedList())
    mc_model.plotOn(theFrame_MC)
    # Calculate chi2
    chi2_MC = theFrame_MC.chiSquare(params) # number of fit parameters
    print(f"Chi2 / ndf = {chi2_MC}")
    # Pulls
    pull_MC = MCvar.frame()
    pull_MC.addPlotable(theFrame_MC.pullHist(), 'P') 
    # Plot everything else
    mc_model.paramOn(theFrame_MC, R.RooFit.ShowConstants(True), R.RooFit.Layout(0.56, 0.95, 0.9), R.RooFit.Label('#chi^{{2}}/ndof = {} for {} dof'.format( round(chi2_MC, 2), theFrame_MC.GetNbinsX() - params)))
    canv_MC = plotFit(theFrame_MC, pull_MC, chi2_MC, "M(B_{s}^{0} #pi^{+})")
    canv_MC.Draw()

    # save canvas
    #canv_MC.SaveAs(saveLocation+".C")
    canv_MC.SaveAs(saveLocation+".png")
    #canv_MC.SaveAs(saveLocation+".pdf")
    #pause()

    # Return nL and nR for data fit to help with fit
    return aL.getValV(), aR.getValV(), nL.getValV(), nR.getValV()

def fit_MC_Bc2BsstPi(var, ds, saveLocation):
    print("To be done...")

def fitData(var, dataset, saveLocation, bkgModel = "exp", draw = False):

    # Signal fit - Double crystal ball or Gaussian #
    # Signal fit parameters
    print("Signal gaussian model")
    mean = R.RooRealVar("sig_mean", "Mean of Gaussian", 6270, 6250, 6300)
    sigma = R.RooRealVar("sig_sigma", "Width of Gaussian", 5, 0.01, 25)

    # Signal model
    sig = R.RooGaussian("sig_pdf", "Gaussian PDF", var, mean, sigma)

    # signal yield
    sig_yield = R.RooRealVar("sig_yield", "Signal yield", 0, 1000)

    # Background fit #
    # Combinatoric: exponential or chebyshev 
    if bkgModel == "exp":
        c = R.RooRealVar("c", "Exponent of exponential", -5, 5)

        # Bkg model 
        comb = R.RooExponential("comb_pdf", "Exponential PDF", var, c)

    elif bkgModel == "cheb":
        a0 = R.RooRealVar("a0", "Chebyshev a0", 0.5, -5.0, 5.0)
        a1 = R.RooRealVar("a1", "Chebyshev a1", 0.2, -5.0, 5.0)

        # Bkg model
        comb = R.RooChebychev("comb_pdf", "Chebychev PDF", var, R.RooArgList(a0, a1))
    
    else:
        print("ERROR! Unknown background model (not exponential or chebyshev) - please check")
        sys.exit()
    comb_yield = R.RooRealVar("comb_yield", "Combinatoric yield", 0, 10000)

    # Bc2BsstPi: gaussian
    bkg_mean = R.RooRealVar("Bc2BsstPi_mean", "Mean of Gaussian Background", 6225, 6200, 6250)
    bkg_sigma = R.RooRealVar("Bc2BsstPi_sigma", "Width of Gaussian Background", 5, 0.01, 50)
    bc2bsstpi = R.RooGaussian("bc2bsstpi_pdf", "Gaussian PDF for Bc2BsstPi", var, bkg_mean, bkg_sigma)
    frac = R.RooRealVar("Bc2BsstPi_yield_frac", "Fraction of Bc2BsstPi in Background", 0.1, 0.01, 1.0)
    bc2bsstpi_yield = R.RooFormulaVar("Bc2BsstPi_Yield", "@0 * @1", R.RooArgList(frac, sig_yield))

    # Some silly memeory thing 
    yields = []; yields.append(sig_yield); yields.append(comb_yield); yields.append(bc2bsstpi_yield)

    # Extended Model
    model = R.RooAddPdf("ext_model", "Sig+Bkg model", R.RooArgList(sig, comb, bc2bsstpi), R.RooArgList(sig_yield, comb_yield, bc2bsstpi_yield) )

    # Fit!
    data_fit = model.fitTo(dataset, R.RooFit.Range(6200, 6550), R.RooFit.Save(), R.RooFit.Extended())
    params = data_fit.floatParsFinal().getSize() # number of parameters for chi2 test

    # Plot!
    theFrame_data = var.frame()
    dataset.plotOn(theFrame_data, R.RooLinkedList())
    model.plotOn(theFrame_data)
    # Calculate chi2
    chi2 = theFrame_data.chiSquare(params) # number of fit parameters
    print(f"Chi2 / ndf = {chi2}")

    if draw:

        # Pulls
        pull_data = var.frame()
        pull_data.addPlotable(theFrame_data.pullHist(), 'P') # P for marker HIST for without or B for bar 
        # Plot rest of the pdfs
        model.plotOn(theFrame_data, R.RooFit.Components("sig_pdf"), R.RooFit.DrawOption("F"), R.RooFit.FillStyle(1001), R.RooFit.FillColor(R.kCyan-8))
        model.plotOn(theFrame_data, R.RooFit.Components("comb_pdf"), R.RooFit.LineStyle(2), R.RooFit.LineColor(R.kSpring + 5))
        model.plotOn(theFrame_data, R.RooFit.Components("bc2bsstpi_pdf"), R.RooFit.DrawOption("F"), R.RooFit.FillStyle(1001), R.RooFit.FillColor(R.kAzure-6))
        model.paramOn(theFrame_data, R.RooFit.ShowConstants(True), R.RooFit.Layout(0.57, 0.95, 0.9), R.RooFit.Label('#chi^{{2}}/ndof = {} for {} dof'.format( round(chi2, 2), theFrame_data.GetNbinsX() - params)))
    
        canv_data = plotFit(theFrame_data, pull_data, chi2, "M(B_{s}^{0} #pi^{+})")
        canv_data.Draw()

        # save canvas
        #canv_data.SaveAs(saveLocation+".C")
        canv_data.SaveAs(saveLocation+".png")
        #canv_data.SaveAs(saveLocation+".pdf")
    
        #pause()
    
    # Outputs
    sigY = sig_yield.getValV()
    sigY_error = sig_yield.getError()
    combY = comb_yield.getValV()
    combY_error = comb_yield.getError()
    sig = sigY / math.sqrt( sigY + combY )
    sig_error = sig * math.sqrt((sigY_error / sigY)**2 + (0.5 * math.sqrt(sigY_error**2 + combY_error**2) / (sigY + combY))**2)

    return sigY, sigY_error, sig, sig_error, chi2


#################################
# SetUp                         #
#################################

print("*"*30)
print("Wooooo Normalisation Fit")
print("*"*30)

parser=argparse.ArgumentParser()
parser.add_argument('--channel','-c',required=True,type=str, choices=["DsPi", "JpsiPhi"], help="Enter Bs decay channel to fit to (DsPi or JpsiPhi)")
parser.add_argument('--year','-y',required=False,type=int,choices=[2016, 2017, 2018], help='Enter year of data and MC you would like to run the BDT (2016-2018)')
parser.add_argument('--year_bdt', '-yb', type=int,default=0,choices=[0, 2016, 2017, 2018],help='Enter year of BDT or 0 for all years combined (2016-2018)')
parser.add_argument('--sig_model', '-s', required=False, default = "gauss", type=str, choices=["gauss", "dcb", "john"], help='Enter the background model (gauss or dcb)')
parser.add_argument('--bkg_model', '-b', required=False, default = "exp", type=str, choices=["exp", "cheb"], help='Enter the background model (exp or cheb)')
parser.add_argument('--sep_year', '-sy', action='store_true', help="Flag whether to use BDT cut trained separate for each year")
parser.add_argument('--draw', '-d', action='store_true', help="Flag whether to save each plot")
args = parser.parse_args()

## Fill args ##

chan = args.channel
year = args.year
year_bdt  = "All" if args.year_bdt == 0 else str(args.year_bdt)
extra_suffix = "" if args.year_bdt == 0 else f"_{str(args.year_bdt)}"
sig_model = args.sig_model
bkg_model = args.bkg_model
if args.sep_year:
    year_str = str(year)
    print("Using BDT cut trained on separate years")
else: # nominal - use same BDT cut for all years
    year_str = "All"
    print("Using BDT cut trained on all years")
draw = args.draw


# Check directory -> changes output location
currentDir = os.path.dirname(os.path.abspath(__file__))
if "tamaki" in currentDir:
    MCPath = "/hepgpu5-data3/tamaki/MCBDTApplied/"
    filePath = "/hepgpu5-data3/tamaki/DataBDTApplied/"
else:
    MCPath = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/"
    filePath = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/DataBDTApplied/"

MCString = f"Bc2BsPi_Bs2{chan}_{{0}}_MC_BDT{extra_suffix}.root"
fileString = f"Bc2BsPi_Bs2{chan}_{{0}}_BDT{extra_suffix}.root"

# Define cuts
if chan == "DsPi":
    TM = "Bc_is_from_Bc_BsPi == 1 && Bs_is_from_Bs_DsPi == 1"
else: # JpsiPhi
    TM = "Bc_is_from_Bc_BsPi == 1 && Bs_is_from_Bs_JpsiPhi == 1"

if year is not None:
    theCuts = BDT_cuts[chan][year_str]

    fullCuts = theCuts + " && " + TM

# Get data and MC and apply cuts
theChain = R.TChain("DecayTree")
theMCChain = R.TChain("DecayTree")
if year == None:
    allYears = True
    dataID = "all" # used for dataset titles etc

    print("\nFitting for all years:")

    if args.sep_year:
        theChain2016, theChain2017, theChain2018 = R.TChain("DecayTree"), R.TChain("DecayTree"), R.TChain("DecayTree")
        theMCChain2016, theMCChain2017, theMCChain2018 = R.TChain("DecayTree"), R.TChain("DecayTree"), R.TChain("DecayTree")
        theChain2016.Add(filePath + fileString.format(2016))
        theChain2017.Add(filePath + fileString.format(2017))
        theChain2018.Add(filePath + fileString.format(2018))
        theMCChain2016.Add(MCPath + MCString.format(2016))
        theMCChain2017.Add(MCPath + MCString.format(2017))
        theMCChain2018.Add(MCPath + MCString.format(2018))

        print(filePath + fileString.format(2016))
        print(filePath + fileString.format(2017))
        print(filePath + fileString.format(2018))
    else:
        theChain.Add(filePath + fileString.format("All"))
        theMCChain.Add(MCPath + MCString.format("All"))

        print(filePath + fileString.format("All"))
        print(MCPath + MCString.format("All"))

else:
    allYears = False
    dataID = f"{year}" # used for dataset titles etc
    print(f"\nFitting for {year}")
    theChain.Add(filePath + fileString.format(year))
    theMCChain.Add(MCPath + MCString.format(year))

    print(filePath + fileString.format(year))
    print(MCPath + MCString.format(year))

outFolder = "./FitResults/Norm"

#################################
# Main                          #
#################################

## Import data ##
print("\nImporting samples.....")
# observable is the Bc mass (dtf)
MCmass = R.RooRealVar("Bc_MDTF_M", "MC M(B_{s}^{0}#pi^{+})", 6100, 6700)
mass = R.RooRealVar("Bc_MDTF_M", "Data M(B_{s}^{0}#pi^{+})", 6100, 6700)
TM1    = R.RooRealVar("Bc_is_from_Bc_BsPi", "Bc_is_from_Bc_BsPi", 0, 1)
TM2    = R.RooRealVar(f"Bs_is_from_Bs_{chan}", f"Bs_is_from_Bs_{chan}", 0, 1)
BcBDT = R.RooRealVar("BcBDT_score", "BcBDT_score", -10, 10)
if chan =="DsPi":
    Bs_Mass = R.RooRealVar("Bs_MM", "Bs_MM", 5000, 6000)
    BsBDT = R.RooRealVar("BsBDT_score", "BsBDT_score", -10, 10)
    MCFullSet = R.RooArgList(MCmass, BcBDT, BsBDT, Bs_Mass, TM1, TM2)
    DataFullSet = R.RooArgList(mass, BcBDT, BsBDT, Bs_Mass)
    BsCut = " && 5300 < Bs_MM && Bs_MM < 5420"
else:
    MCFullSet = R.RooArgList(MCmass, BcBDT, TM1, TM2)
    DataFullSet = R.RooArgList(mass, BcBDT)
    BsCut = ""

## Bc BDT cuts to try ##
# For this I'll have to check the plots
if chan == "DsPi":
    theRange = (2.7, 4.7)
    #theRange = (3.2, 3.6)
    Bc_BDT_cuts = [2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 
		   3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 
                   4.0, 4.1, 4.2]
else:
    theRange = (1.8, 3.8)
    #theRange = (2.4, 2.8)
    Bc_BDT_cuts = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 
		   2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 
                   3.0, 3.1, 3.2, 3.3]
#    Bc_BDT_cuts = [1.1, 1.2]
#Bc_BDT_cuts = [ theRange[0] + step * i for i in range(steps) ]
print("Fitting for Bc BDT cuts:")
print(Bc_BDT_cuts)

## Fit data for various BDT cuts ##

# Lists of fit results
sigYields, sigYErrors, sigs, sigErrors, chi2s = [], [], [], [], []

for BcCut in Bc_BDT_cuts:

    # Datasets
    print(f"Applying BDT cuts")
    if year == None:

        if args.sep_year:
            data_2016 = R.RooDataSet("DataSamples_2016", f"Data Bc->Bs Bs->{chan} 2016 MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain2016 ),  R.RooFit.Cut( BDT_cuts[chan][2016] + BsCut ))
            data_2017 = R.RooDataSet("DataSamples_2017", f"Data Bc->Bs Bs->{chan} 2017 MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain2017 ),  R.RooFit.Cut( BDT_cuts[chan][2017] + BsCut ))
            data_2018 = R.RooDataSet("DataSamples_2018", f"Data Bc->Bs Bs->{chan} 2018 MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain2018 ),  R.RooFit.Cut( BDT_cuts[chan][2018] + BsCut ))

            #mc_2016   = R.RooDataSet("MCSamples_2016", f"MC Bc->Bs Bs->{chan} 2016 MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain2016 ), R.RooFit.Cut( BDT_cuts[chan][2016] + " && " +  TM + BsCut))
            #mc_2017   = R.RooDataSet("MCSamples_2017", f"MC Bc->Bs Bs->{chan} 2017 MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain2017 ), R.RooFit.Cut( BDT_cuts[chan][2017] + " && " +  TM + BsCut))
            #mc_2018   = R.RooDataSet("MCSamples_2018", f"MC Bc->Bs Bs->{chan} 2018 MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain2018 ), R.RooFit.Cut( BDT_cuts[chan][2018] + " && " +  TM + BsCut))

            data = R.RooDataSet("DataSamples", f"Data Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( DataFullSet ))
            data.append(data_2016); data.append(data_2017); data.append(data_2018)
            #mc = R.RooDataSet("MCSamples", f"MC Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( MCFullSet ))
            #mc.append(mc_2016); mc.append(mc_2017); mc.append(mc_2018)
        else:
            if chan == "DsPi":
                # Get Bs BDT cut
                full_BDT_cut = (BDT_cuts[chan][year_bdt].split(" & "))[0] + " && BcBDT_score > " + str(BcCut)
            else:
                full_BDT_cut = "BcBDT_score > " + str(BcCut)

            full_cut = full_BDT_cut + BsCut

            data = R.RooDataSet("DataSamples_All", f"Data Bc->Bs Bs->{chan} All MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain ),  R.RooFit.Cut( full_cut ) )
            #mc_All   = R.RooDataSet("MCSamples_All", f"MC Bc->Bs Bs->{chan} All MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain ), R.RooFit.Cut( BDT_cuts[chan][year_bdt] + " && " +  TM + BsCut))

            #data = R.RooDataSet("DataSamples", f"Data Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( DataFullSet ))
            #data.append(data_All)

            #mc = R.RooDataSet("MCSamples", f"MC Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( MCFullSet ))
            #mc.append(mc_All)

    else:
        data = R.RooDataSet("DataSamples", f"Data Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( DataFullSet ), R.RooFit.Import( theChain ),  R.RooFit.Cut( theCuts + BsCut ))
        #mc   = R.RooDataSet("MCSamples", f"MC Bc->Bs Bs->{chan} {dataID} MU+MD", R.RooArgSet( MCFullSet ), R.RooFit.Import( theMCChain ), R.RooFit.Cut( fullCuts + BsCut ))


    ## Fit for each instance ##

    dataoutFile = f"{outFolder}/Data_BcFit_{chan}_{dataID}_{bkg_model}_BcCut_{BcCut}"
    sigYield, sigYield_error, sig, sig_error, chi2 = fitData( mass, data, dataoutFile, bkgModel=bkg_model, draw = draw)

    # Append results to list
    sigYields.append(sigYield)
    sigYErrors.append(sigYield_error)
    sigs.append(sig)
    sigErrors.append(sig_error)
    chi2s.append(chi2)

## Print Results ##
print(f"For Bc BDT cuts: {Bc_BDT_cuts}")
print(f"Signal Yields: {sigYields}")
print(f"s/sqrt(s+b): {sigs}")
print(f"Chi2: {chi2s}")


## Plot Results ##
#import matplotlib as mpl
#mpl.rcParams.update(mpl.rcParamsDefault)
from matplotlib import pyplot as plt
from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

#plt.figure(figsize=(2,6))
fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
fig.set_figheight(8)
fig.set_figwidth(4)

# Signal yield
ax1.errorbar(Bc_BDT_cuts, sigYields, yerr = sigYErrors, fmt='o', color="slateblue")
ax1.set_ylabel('Signal Yield')
# significance
ax2.errorbar(Bc_BDT_cuts, sigs, yerr = sigErrors, fmt='o', color="plum")
ax2.set_ylabel(r'$\frac{ s }{ \sqrt{s + b} }$')
# chi2
ax3.plot(Bc_BDT_cuts, chi2s, 'o-', color="darksalmon")
ax3.set_xlabel(r'$B_c^+$ BDT Score Cut')
ax3.set_ylabel(r'Fit $\chi^2$')

plt.tight_layout()
#plt.show()
plt.savefig(f"{outFolder}/Data_BDT_opt_{chan}_{dataID}_{bkg_model}.png")
plt.savefig(f"{outFolder}/Data_BDT_opt_{chan}_{dataID}_{bkg_model}.pdf")

print("Done")
