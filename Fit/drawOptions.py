#
# drawOptions.py
#
# Functions used for drawing fit results (just binned fit)
#

import ROOT as R
import math


# Global variables
# Some plotting variables

from common_things import (mcorr_bins, mvis_bins, mcorr_bins_for_2D, mvis_bins_for_2D, 
                           dict_of_pdf_names_mcorr, dict_of_pdf_names_mvis, dict_of_pdf_names_2D,
                           dict_of_leg_entries,
                           pause)

plotting_vars_mcorr = {
    "xrange" : [mcorr_bins[0], mcorr_bins[-1]],
    "xlabel" : "M_{corr}(B_{c}^{+})",
    "left"   : [4400, mcorr_bins[0]],
    "right"  : [mcorr_bins[-1], 14000]
}
plotting_vars_mvis = {
    "xrange" : [mvis_bins[0], mvis_bins[-1]],
    "xlabel" : "M(B_{s}^{0} #mu^{+})",
    "left"   : [4450, mvis_bins[0]],
    "right"  : [mvis_bins[-1], 7500]
}

#################################
# Plotting Functions            #
#################################

def getRooBinning(binning):
    '''
    Make RooBinning object from bin array fror plotting in RooPlot
    '''
    print(f"Binning: {binning}")
    roobins = R.RooBinning(binning[0], binning[-1])

    for index, boundary in enumerate(binning):
        # skip first and last boundary
        if index == 0 or index == len(binning)-1:
            continue

        roobins.addBoundary(boundary)
    
    return roobins

# gets pull hist and configures
def getPullHist(frame):
    hist = frame.pullHist()
    hist.SetFillColor(R.kGray + 2)
    hist.SetLineColor(0)
    hist.SetMarkerColor(0)

    return hist

# Calculate pull
def calcPull(data, fit, sigma_fit):
    '''
    Standard pull calculation for one bin
    Pull = data - fit / sigma
    '''
    if sigma_fit <= 1e-7 or data == 0:
        pull = 0.0
        #print(f"Pull is set to {pull} as {sigma_fit} <= 1e-7 or data = {data} (fit = {fit})")
    else: 
        pull = (data - fit) / sigma_fit

    return pull

def calcExtendedPull(data, fit, sigma_data, sigma_fit):
    '''
    Calculate pull with MC statistical uncertainty for one bin
    Pull = data - fit / \sqrt( sigma^2 + sigma_stat^2 )
    '''
    sigma = math.sqrt( pow(sigma_fit, 2) + pow(sigma_data, 2) )
    if sigma <= 1e-7 or data == 0:
        pull = 0.0
    else: pull = (data - fit) / sigma

    return pull


def drawStacked(x, dataset, model_after_fit, params, bins, dict_for_fit, plotName, blind = None, stacked = True):

    # Decide whether its Mcorr or Mvis to plot
    if x.GetName() == "Bc_CORR_M":
        plotting_vars = plotting_vars_mcorr
        dict_of_pdf_names = dict_of_pdf_names_mcorr
        line_range = (mcorr_bins[0], mcorr_bins[-1])
        var_binning = mcorr_bins
    elif x.GetName() == "Bc_MDTF_M":
        plotting_vars = plotting_vars_mvis
        dict_of_pdf_names = dict_of_pdf_names_mvis
        line_range = (mvis_bins[0], mvis_bins[-1])
        var_binning = mvis_bins
    xlabel = plotting_vars["xlabel"]
    xrange = plotting_vars["xrange"]
    left_sideband  = plotting_vars["left"]
    right_sideband = plotting_vars["right"]

    # Set up custom binning for plotting
    roobins = getRooBinning(var_binning)

    # Setting depending on stacked or not
    if stacked:
        # Plot formatting
        model_linewidth = 0 
        pdfs_drawoption = "F1"
        pdfs_fillstyle = 1001
        leg_style = "f"

        list_of_stacked = []
        list_of_values = list(dict_of_pdf_names.values())
        list_of_values.reverse()
        total_pdfname = ""
        for pdfname in list_of_values:
            # will need to add normrange
            if total_pdfname == "":
                total_pdfname = pdfname
            else:
                total_pdfname += f",{pdfname}"
            list_of_stacked.append(total_pdfname)
        print("list of stacked:")
        list_of_stacked.reverse()
        print(list_of_stacked)
    
    else:
        # Plot formatting
        model_linewidth = 3
        pdfs_drawoption = "F1"
        pdfs_fillstyle = 4001 #transparent
        leg_style = "l"

    if blind == None:
        print("No blind category, plotting")
        showPlot = True
    else:
        if blind.getIndex() == 1:
            print("BLINDING, not plotting")
            showPlot = False

            # Set up blinding range
            x.setRange("full", xrange[0], xrange[1])
            x.setRange("left", left_sideband[0], left_sideband[1])
            x.setRange("right", right_sideband[0], right_sideband[1])
        else:
            print("NO BLINDING, plotting")
            showPlot = True
        
    # Make frames
    #plotOpts = R.RooAbsData.PlotOpt()
    #plotOpts.bins = roobins
    #plotOpts.correctForBinWidth = True
    mcorrFrameForPlotting = x.frame()
    dataset.plotOn(mcorrFrameForPlotting, R.RooFit.MarkerStyle(8), R.RooFit.MarkerSize(1.5), R.RooFit.Binning( roobins ))
    #dataset.plotOn(mcorrFrameForPlotting, plotOpts)
    model_after_fit.plotOn(mcorrFrameForPlotting, R.RooFit.LineWidth( model_linewidth ), R.RooFit.Normalization(1.0,R.RooAbsReal.RelativeExpected)) # make line invisible cos of stack

    # Always get pull after plotting model
    #pullFrame = getPull(x, dataset, model_after_fit)
    pullFrame = x.frame()
    pullHist = getPullHist(mcorrFrameForPlotting)
    pullFrame.addPlotable(pullHist, 'B')

    # Calculate chi2
    chi2 = mcorrFrameForPlotting.chiSquare(params) # number of fit parameters
    
    # Canvas and legend
    c = R.TCanvas("c", "c", 600, 700)
    legend = R.TLegend(0.01,0.01,0.99,0.99) #0.65,0.55,0.9,0.9
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(4000)

    # Plot the components
    index = 0
    list_of_leg_entries = {}
    
    for key, pdfname in dict_of_pdf_names.items():
        theColour = dict_for_fit[key][3]
        if stacked:
            total_pdfname = list_of_stacked[index]
        else:
            total_pdfname = pdfname

        #if showPlot == True:
        print(f"Plotting {total_pdfname}")
        model_after_fit.plotOn(
            mcorrFrameForPlotting,
            R.RooFit.Name( "N_"+key ),
            R.RooFit.Components( total_pdfname ), # or the yield roorealvar name #Normalization(1.0,R.RooAbsReal.RelativeExpected),
            R.RooFit.Normalization(1.0,R.RooAbsReal.RelativeExpected),
            R.RooFit.Range(line_range[0], line_range[-1]),
            R.RooFit.LineColor( theColour ),
            R.RooFit.DrawOption( pdfs_drawoption ),
            R.RooFit.FillStyle( pdfs_fillstyle ),
            R.RooFit.LineStyle( R.kSolid ),
            R.RooFit.FillColor( theColour ),
            R.RooFit.LineWidth( 3 )
        )
    
        # Add entry to legend with all this extra stuff
        tmp = R.TH1D(f"{pdfname}_tmp",f"Temp {pdfname}", 1, 0, 1)
        tmp.Fill(0.5)
        tmp.SetLineWidth(5)
        tmp.SetLineColor( theColour )
        tmp.SetFillColor( theColour )
        #tmp.Draw()
        list_of_leg_entries[key] = tmp
        legend.AddEntry(list_of_leg_entries[key], dict_of_leg_entries[key], leg_style)

        index += 1

    # plot on top
    dataset.plotOn(mcorrFrameForPlotting, R.RooFit.MarkerStyle(8), R.RooFit.MarkerSize(1.5), R.RooFit.Binning( roobins ))

    ## THIS IS THE BLINDING BIT
    # For blinding plot fake frame with nothing on it
    if showPlot == False: # if blinding
        mcorrFrame = x.frame()
    else: # if not blinding
        mcorrFrame = mcorrFrameForPlotting

    # Plot
    #mcorrFrame.getAttText().SetTextSize(0.0275)
    #mcorrFrame.getAttText().SetTextFont(132)
    mcorrFrame.GetXaxis().SetLabelOffset(999) # Remove X axis labels
    mcorrFrame.GetXaxis().SetRangeUser(xrange[0], xrange[1])

    mcorrFrame.GetYaxis().SetLabelFont(132)
    mcorrFrame.GetYaxis().SetTitleFont(132)
    #mcorrFrame.getAttText().SetLineWidth(0)
    #mcorrFrame.getAttText().SetFillStyle(0)
    mcorrFrame.GetYaxis().SetLabelSize(0.04)
    mcorrFrame.SetTitle("")
    mcorrFrame.SetTitleSize(0.04, 'XY')

    # Plot pulls 
    pullFrame.SetTitle("")
    pullFrame.SetMinimum(-5)
    pullFrame.SetMaximum(5)
    pullFrame.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    pullFrame.GetXaxis().SetTitleOffset(1.)
    pullFrame.GetXaxis().SetLabelFont(132)
    pullFrame.GetXaxis().SetTitleFont(132)
    pullFrame.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    pullFrame.SetLabelSize(0.08, 'XY')
    pullFrame.GetYaxis().SetTitle('(data - model)/#font[12]{#sigma}_{data}')
    pullFrame.GetYaxis().SetTitleOffset(0.5)
    pullFrame.GetYaxis().SetNdivisions(209)
    pullFrame.GetYaxis().SetLabelFont(132)
    pullFrame.GetYaxis().SetTitleFont(132)
    pullFrame.SetTitleSize(0.08, 'XY')
    pullFrame.SetFillColor(R.kSpring-6)

    upperPad = R.TPad('upperPad', 'upperPad', .005, .330, .995, .995)
    lowerPad = R.TPad('lowerPad', 'lowerPad', .005, .005, .995, .330)
    if len(dict_of_pdf_names) == 3:
        lowerbound = 0.75
    elif len(dict_of_pdf_names) == 2:
        lowerbound = 0.8
    else: # if number of components >3
        lowerbound = 0.6
    topPad   = R.TPad('topPad', 'topPad', 0.65,lowerbound,0.9,0.9)
    upperPad.SetBottomMargin(0.03)
    lowerPad.SetTopMargin(0.02)
    lowerPad.SetBottomMargin(0.3)
    upperPad.SetLeftMargin(0.15)
    lowerPad.SetLeftMargin(0.15)
    upperPad.SetRightMargin(0.05)
    lowerPad.SetRightMargin(0.05)
    upperPad.SetFillStyle(4000)
    upperPad.SetFrameFillStyle(4000)
    lowerPad.SetFillStyle(4000)
    lowerPad.SetFrameFillStyle(4000)
    topPad.SetFillStyle(4000)
    topPad.SetFrameFillStyle(4000)
    upperPad.Draw()
    lowerPad.Draw()
    topPad.Draw()

    upperPad.cd()
    mcorrFrame.Draw()
    # add chi2 to plot
    label = R.TPaveLabel(0.65,0.5,0.9, 0.6, "#chi^{2}/ndof = %f"%chi2)
    label.SetLineColor(0)
    label.SetBorderSize(1) # removes shadow
    label.SetFillStyle(0)
    label.Draw()
    upperPad.Update()
    lowerPad.cd()
    line = R.TLine(line_range[0],0,line_range[-1],0)
    line.SetLineColor(14) # dark gray
    #pullFrame.Draw("B")
    pullFrame.Draw()
    line.Draw()
    topPad.cd()
    legend.Draw()
    topPad.Update()
    print(f"Chi2 / ndf = {chi2}")
    #pause()

    if stacked:
        c.SaveAs(f"{plotName}_stacked.png")
        c.SaveAs(f"{plotName}_stacked.pdf")
        c.SaveAs(f"{plotName}_stacked.C")
    else:
        c.SaveAs(f"{plotName}.png")
        c.SaveAs(f"{plotName}.pdf")
        c.SaveAs(f"{plotName}.C")

####
        
def drawStacked2D(x, y, dataset, model_after_fit, params, dict_for_fit, plotName, stacked = True):

    # Decide whether its Mcorr or Mvis to plot
    plotting_vars_x = plotting_vars_mcorr
    line_range_x = (mcorr_bins[0], mcorr_bins[-1])
    var_binning_x = mcorr_bins
    xlabel = plotting_vars_x["xlabel"]
    xrange = plotting_vars_x["xrange"]
    
    plotting_vars_y = plotting_vars_mvis
    line_range_y = (mvis_bins[0], mvis_bins[-1])
    var_binning_y = mvis_bins
    ylabel = plotting_vars_y["xlabel"]
    yrange = plotting_vars_y["xrange"]

    dict_of_pdf_names = dict_of_pdf_names_2D

    # Set up custom binning for plotting
    xbins = getRooBinning(mcorr_bins_for_2D)
    ybins = getRooBinning(mvis_bins_for_2D)

    # Setting depending on stacked or not
    if stacked:
        # Plot formatting
        model_linewidth = 0 
        pdfs_drawoption = "F1"
        pdfs_fillstyle = 1001
        leg_style = "f"

        list_of_stacked = []
        list_of_values = list(dict_of_pdf_names.values())
        list_of_values.reverse()
        total_pdfname = ""
        for pdfname in list_of_values:
            # will need to add normrange
            if total_pdfname == "":
                total_pdfname = pdfname
            else:
                total_pdfname += f",{pdfname}"
            list_of_stacked.append(total_pdfname)
        print("list of stacked:")
        list_of_stacked.reverse()
        print(list_of_stacked)
    
    else:
        # Plot formatting
        model_linewidth = 3
        pdfs_drawoption = "F1"
        pdfs_fillstyle = 4001 #transparent
        leg_style = "l"

    # Canvas and legend
    c = R.TCanvas("c", "c", 900, 500)
    legend_mcorr = R.TLegend(0.01,0.01,0.99,0.99) #0.65,0.55,0.9,0.9
    legend_mcorr.SetBorderSize(0)
    legend_mcorr.SetFillColor(0)
    legend_mcorr.SetFillStyle(4000)
    legend_mvis = R.TLegend(0.01,0.01,0.99,0.99) #0.65,0.55,0.9,0.9
    legend_mvis.SetBorderSize(0)
    legend_mvis.SetFillColor(0)
    legend_mvis.SetFillStyle(4000)

    #########
    # MCORR #
    #########

    # Make frames
    mcorrFrameForPlotting = x.frame()
    dataset.plotOn(mcorrFrameForPlotting, R.RooFit.MarkerStyle(8), R.RooFit.MarkerSize(1.5), R.RooFit.Binning( xbins ))
    model_after_fit.plotOn(mcorrFrameForPlotting, R.RooFit.LineWidth( model_linewidth ), R.RooFit.Normalization(1.0,R.RooAbsReal.RelativeExpected)) # make line invisible cos of stack

    # Always get pull after plotting model
    #pullMcorrFrame = x.frame()
    #pullMcorrHist = getPullHist(mcorrFrameForPlotting)
    #pullMcorrFrame.addPlotable(pullMcorrHist, 'B')

    # Calculate chi2
    #chi2_mcorr = mcorrFrameForPlotting.chiSquare(params) # number of fit parameters

    # Plot the components
    index = 0
    list_of_leg_entries_mcorr = {}

    for key, pdfname in dict_of_pdf_names.items():
        theColour = dict_for_fit[key][3]
        if stacked:
            total_pdfname = list_of_stacked[index]
        else:
            total_pdfname = pdfname

        #if showPlot == True:
        print(f"Plotting {total_pdfname}")
        model_after_fit.plotOn(
            mcorrFrameForPlotting,
            R.RooFit.Name( "N_"+key ),
            R.RooFit.Components( total_pdfname ), # or the yield roorealvar name #Normalization(1.0,R.RooAbsReal.RelativeExpected),
            R.RooFit.Normalization(1.0,R.RooAbsReal.RelativeExpected),
            R.RooFit.LineColor( theColour ),
            R.RooFit.DrawOption( pdfs_drawoption ),
            R.RooFit.FillStyle( pdfs_fillstyle ),
            R.RooFit.LineStyle( R.kSolid ),
            R.RooFit.FillColor( theColour ),
            R.RooFit.LineWidth( 3 )
        )
    
        # Add entry to legend with all this extra stuff
        tmp = R.TH1D(f"{pdfname}_tmp",f"Temp {pdfname}", 1, 0, 1)
        tmp.Fill(0.5)
        tmp.SetLineWidth(5)
        tmp.SetLineColor( theColour )
        tmp.SetFillColor( theColour )
        #tmp.Draw()
        list_of_leg_entries_mcorr[key] = tmp
        legend_mcorr.AddEntry(list_of_leg_entries_mcorr[key], dict_of_leg_entries[key], leg_style)

        index += 1

    # plot on top
    dataset.plotOn(mcorrFrameForPlotting, R.RooFit.MarkerStyle(8), R.RooFit.MarkerSize(1.5), R.RooFit.Binning( xbins ))
    mcorrFrame = mcorrFrameForPlotting


    #########
    # MVIS  #
    #########

    # Make frames
    mvisFrameForPlotting = y.frame()
    dataset.plotOn(mvisFrameForPlotting, R.RooFit.MarkerStyle(8), R.RooFit.MarkerSize(1.5), R.RooFit.Binning( ybins ))
    model_after_fit.plotOn(mvisFrameForPlotting, R.RooFit.LineWidth( model_linewidth ), R.RooFit.Normalization(1.0,R.RooAbsReal.RelativeExpected)) # make line invisible cos of stack

    # Always get pull after plotting model
    #pullMvisFrame = y.frame()
    #pullMvisHist = getPullHist(mvisFrameForPlotting)
    #pullMvisFrame.addPlotable(pullMvisHist, 'B')

    # Calculate chi2
    #chi2_mvis = mvisFrameForPlotting.chiSquare(params) # number of fit parameters

    # Plot the components
    index = 0
    list_of_leg_entries_mvis = {}

    for key, pdfname in dict_of_pdf_names.items():
        theColour = dict_for_fit[key][3]
        if stacked:
            total_pdfname = list_of_stacked[index]
        else:
            total_pdfname = pdfname

        #if showPlot == True:
        print(f"Plotting {total_pdfname}")
        model_after_fit.plotOn(
            mvisFrameForPlotting,
            R.RooFit.Name( "N_"+key ),
            R.RooFit.Components( total_pdfname ), # or the yield roorealvar name #Normalization(1.0,R.RooAbsReal.RelativeExpected),
            R.RooFit.Normalization(1.0,R.RooAbsReal.RelativeExpected),
            R.RooFit.LineColor( theColour ),
            R.RooFit.DrawOption( pdfs_drawoption ),
            R.RooFit.FillStyle( pdfs_fillstyle ),
            R.RooFit.LineStyle( R.kSolid ),
            R.RooFit.FillColor( theColour ),
            R.RooFit.LineWidth( 3 )
        )
    
        # Add entry to legend with all this extra stuff
        tmp = R.TH1D(f"{pdfname}_tmp",f"Temp {pdfname}", 1, 0, 1)
        tmp.Fill(0.5)
        tmp.SetLineWidth(5)
        tmp.SetLineColor( theColour )
        tmp.SetFillColor( theColour )
        #tmp.Draw()
        list_of_leg_entries_mvis[key] = tmp
        legend_mvis.AddEntry(list_of_leg_entries_mvis[key], dict_of_leg_entries[key], leg_style)

        index += 1

    # plot on top
    dataset.plotOn(mvisFrameForPlotting, R.RooFit.MarkerStyle(8), R.RooFit.MarkerSize(1.5), R.RooFit.Binning( ybins ))

    
    #############
    # Plotting! #
    #############

    # Make pads
    upperLeftPad = R.TPad('upperLeftPad', 'upperLeftPad', .005, .005, .495, .995)
    #lowerLeftPad = R.TPad('lowerLeftPad', 'lowerLeftPad', .005, .005, .495, .330)
    upperRightPad = R.TPad('upperRightPad', 'upperRightPad', .505, .005, .995, .995)
    #lowerRightPad = R.TPad('lowerRightPad', 'lowerRightPad', .505, .005, .995, .330)
    if len(dict_of_pdf_names) == 3:
        lowerbound = 0.55
    elif len(dict_of_pdf_names) == 2:
        lowerbound = 0.6
    else: # if number of components >3
        lowerbound = 0.4
    legendPad   = R.TPad('legendPad', 'legendPad', 0.3,lowerbound,0.445,0.9)
    legendRightPad = R.TPad('legendRightPad', 'legendRightPad', 0.805,lowerbound,0.950,0.9)

    # Pad config Mcorr
    #upperLeftPad.SetBottomMargin(0.03)
    upperLeftPad.SetLeftMargin(0.15)
    upperLeftPad.SetRightMargin(0.05)
    upperLeftPad.SetFillStyle(4000)
    upperLeftPad.SetFrameFillStyle(4000)
    legendPad.SetFillStyle(4000)
    legendPad.SetFrameFillStyle(4000)
    upperLeftPad.Draw()
    legendPad.Draw()

    # Pad config Mvis
    #upperRightPad.SetBottomMargin(0.03)
    upperRightPad.SetLeftMargin(0.15)
    upperRightPad.SetRightMargin(0.05)
    upperRightPad.SetFillStyle(4000)
    upperRightPad.SetFrameFillStyle(4000)
    legendRightPad.SetFillStyle(4000)
    legendRightPad.SetFrameFillStyle(4000)
    upperRightPad.Draw()
    legendRightPad.Draw()


    #mcorrFrame.GetXaxis().SetLabelOffset(999) # Remove X axis labels
    mcorrFrame.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    mcorrFrame.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    mcorrFrame.GetXaxis().SetLabelFont(132)
    mcorrFrame.GetXaxis().SetTitleFont(132)
    mcorrFrame.GetXaxis().SetTitleOffset(1.)
    mcorrFrame.GetYaxis().SetLabelFont(132)
    mcorrFrame.GetYaxis().SetTitleFont(132)
    mcorrFrame.SetTitle("")
    mcorrFrame.SetTitleSize(0.04, 'XY')
    mcorrFrame.SetLabelSize(0.04, 'XY')

    upperLeftPad.cd()
    mcorrFrame.Draw()
    upperLeftPad.Update()
    lineLeft = R.TLine(line_range_x[0],0,line_range_x[-1],0)
    lineLeft.SetLineColor(14) # dark gray
    #pullFrame.Draw("B")
    lineLeft.Draw()
    legendPad.cd()
    legend_mcorr.Draw()
    legendPad.Update()

    #mvisFrameForPlotting.GetXaxis().SetLabelOffset(999) # Remove X axis labels
    mvisFrameForPlotting.GetXaxis().SetRangeUser(yrange[0], yrange[1])
    mvisFrameForPlotting.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%ylabel)
    mvisFrameForPlotting.GetXaxis().SetLabelFont(132)
    mvisFrameForPlotting.GetXaxis().SetTitleFont(132)
    mvisFrameForPlotting.GetXaxis().SetTitleOffset(1.)
    mvisFrameForPlotting.GetYaxis().SetLabelFont(132)
    mvisFrameForPlotting.GetYaxis().SetTitleFont(132)
    mvisFrameForPlotting.SetTitle("")
    mvisFrameForPlotting.SetTitleSize(0.04, 'XY')
    mvisFrameForPlotting.SetLabelSize(0.04, 'XY')

    # Plot pulls 
    upperRightPad.cd()
    mvisFrameForPlotting.Draw()
    upperRightPad.Update()
    lineRight = R.TLine(line_range_y[0],0,line_range_y[-1],0)
    lineRight.SetLineColor(14) # dark gray
    lineRight.Draw()
    legendRightPad.cd()
    legend_mvis.Draw() # same as mvis
    legendRightPad.Update()

    ###############
    # Save Canvas #
    ###############

    #print(f"Chi2 / ndf = {chi2_mcorr}")
    #print(f"Chi2 / ndf = {chi2_mvis}")

    if stacked:
        c.SaveAs(f"{plotName}_stacked.png")
        c.SaveAs(f"{plotName}_stacked.pdf")
        c.SaveAs(f"{plotName}_stacked.C")
    else:
        c.SaveAs(f"{plotName}.png")
        c.SaveAs(f"{plotName}.pdf")
        c.SaveAs(f"{plotName}.C")

def drawPulls2D(x, y, dataset, model_after_fit, params, plotName, beeston_barlow = False):
    '''
    Draw the pull histogram for 2D fit
    '''
    R.gROOT.ProcessLine(".L /afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/lhcbStyle.C")
    R.gStyle.SetPadRightMargin(1.4) #colz plot prettiness
    R.gStyle.SetPalette(R.kLightTemperature)
    c = R.TCanvas()

    # Decide whether its Mcorr or Mvis to plot
    plotting_vars_x = plotting_vars_mcorr
    xlabel = plotting_vars_x["xlabel"]
    
    plotting_vars_y = plotting_vars_mvis
    ylabel = plotting_vars_y["xlabel"]

    # Set up custom binning for plotting
    xbins = getRooBinning(mcorr_bins_for_2D)
    ybins = getRooBinning(mvis_bins_for_2D)

    # Create the data and model histograms
    data_hist = dataset.createHistogram("h_data", x, R.RooFit.Binning(xbins), R.RooFit.YVar(y, R.RooFit.Binning(ybins)))
    if beeston_barlow==True:
        print("Plotting the model the beeston-barlow way is not available yet")
    else:
        model_hist = model_after_fit.createHistogram("h_model", x, R.RooFit.Binning(xbins), R.RooFit.YVar(y, R.RooFit.Binning(ybins))) # possibly add R.RooFit.Extended(True) 
    # Scale model based on expected number of events to match data
    #og_model_hist = model_hist.Clone()
    model_hist.Scale(model_after_fit.expectedEvents(R.RooArgSet(x, y)) / model_hist.Integral())
    print(f"Model scale: {model_after_fit.expectedEvents(R.RooArgSet(x, y)) / model_hist.Integral()}")
    print(f"Original integral: {model_hist.Integral()}")

    # Plot data and model plots first

    # Make 2D pull histogram
    pull_hist = data_hist.Clone()
    pull_hist.GetZaxis().SetRangeUser(-5, 5)
    Nx = pull_hist.GetNbinsX(); Ny = pull_hist.GetNbinsY()
    chi2 = 0.0; ndof = 0
    for j in range(1, Ny + 1):
        for i in range(1, Nx + 1):
            data_content  = data_hist.GetBinContent(i, j)
            data_error    = data_hist.GetBinError(i, j)
            model_content = model_hist.GetBinContent(i, j)
            model_error   = model_hist.GetBinError(i, j)
            if beeston_barlow == True:
                pull = calcExtendedPull(data_content, model_content, data_error, model_error)
            else:
                pull = calcPull(data_content, model_content, data_error)
            if "toy" in plotName: print(f"Bin ({i}, {j}): {pull} = {data_content} - {model_content} / {data_error}")
            #print(f"Model bin content before scaling: {og_model_hist.GetBinContent(i, j)} +- {og_model_hist.GetBinError(i, j)}")
        
            pull_hist.SetBinContent(i, j, pull)
            chi2 += pull*pull
            #print(f"chi^2 += {pull*pull}")
            ndof += 1
    
    ndof = ndof - params
    print(f"Chi2 = {chi2} for {ndof} DoF")

    # Jazz up plot
    pull_hist.GetXaxis().SetTitle(f"{xlabel} [MeV/c^{{2}}]")
    pull_hist.GetYaxis().SetTitle(f"{ylabel} [MeV/c^{{2}}]")
    pull_hist.GetYaxis().SetTitleOffset(0.97)
    pull_hist.SetTitle("")

    pull_hist.Draw("colz")
    c.Draw()
    #c.SaveAs(f"{plotName}_2D_pull.png")
    c.SaveAs(f"{plotName}_2D_pull.pdf")
    #c.SaveAs(f"{plotName}_2D_pull.C")

    c2 = R.TCanvas()
    pull_hist.Draw("COLZ TEXT")
    c2.Draw()
    #c2.SaveAs(f"{plotName}_2D_pull_with_text.png")
    c2.SaveAs(f"{plotName}_2D_pull_with_text.pdf")
    #c2.SaveAs(f"{plotName}_2D_pull_with_text.C")


####

def drawStackedSim(x, dataset, model_after_fit, categories, params, bins, dict_for_fit, plotName, blind = None, stacked = True):

    # Decide whether its Mcorr or Mvis to plot
    if x.GetName() == "Bc_CORR_M":
        plotting_vars = plotting_vars_mcorr
        dict_of_pdf_names = dict_of_pdf_names_mcorr
        sample_name = "Bc_Mcorr"
    elif x.GetName() == "Bc_MDTF_M":
        plotting_vars = plotting_vars_mvis
        dict_of_pdf_names = dict_of_pdf_names_mvis
        sample_name = "Bc_Mvis"
    xlabel = plotting_vars["xlabel"]
    xrange = plotting_vars["xrange"]
    left_sideband  = plotting_vars["left"]
    right_sideband = plotting_vars["right"]

    print(f"Plotting {sample_name}")

    # Setting depending on stacked or not
    if stacked:
        # Plot formatting
        model_linewidth = 0 
        pdfs_drawoption = "F1"
        pdfs_fillstyle = 1001
        leg_style = "f"

        list_of_stacked = []
        list_of_values = list(dict_of_pdf_names.values())
        list_of_values.reverse()
        total_pdfname = ""
        for pdfname in list_of_values:
            # will need to add normrange
            if total_pdfname == "":
                total_pdfname = pdfname
            else:
                total_pdfname += f",{pdfname}"
            list_of_stacked.append(total_pdfname)
        print("list of stacked:")
        list_of_stacked.reverse()
        print(list_of_stacked)
    
    else:
        # Plot formatting
        model_linewidth = 3
        pdfs_drawoption = "F1"
        pdfs_fillstyle = 4001 #transparent
        leg_style = "l"

    if blind == None:
        print("No blind category, plotting")
        showPlot = True
    else:
        if blind.getIndex() == 1:
            print("BLINDING, not plotting")
            showPlot = False

            # Set up blinding range
            x.setRange("full", xrange[0], xrange[1])
            x.setRange("left", left_sideband[0], left_sideband[1])
            x.setRange("right", right_sideband[0], right_sideband[1])
        else:
            print("NO BLINDING, plotting")
            showPlot = True

    # Make frames
    mcorrFrameForPlotting = x.frame()
    dataset.plotOn(mcorrFrameForPlotting, 
                   R.RooFit.Cut(f"sample==sample::{sample_name}"),
                   R.RooFit.MarkerStyle(8), 
                   R.RooFit.MarkerSize(1.5), 
                   R.RooFit.Binning( bins[0], bins[1], bins[2] ))
    print("Categories:")
    categories.Print("v")
    print(f"Sample name: {sample_name}")
    theSlice = R.RooFit.Slice(categories, sample_name)
    theSet = R.RooArgSet(categories)
    theProj  = R.RooFit.ProjWData(theSet, dataset)
    model_after_fit.plotOn(mcorrFrameForPlotting, 
                           theSlice,
                           theProj) 
    # make line invisible cos of stack

    # Always get pull after plotting model
    #pullFrame = getPull(x, dataset, model_after_fit)
    pullFrame = x.frame()
    pullHist = getPullHist(mcorrFrameForPlotting)
    pullFrame.addPlotable(pullHist, 'B')

    # Calculate chi2
    chi2 = mcorrFrameForPlotting.chiSquare(params) # number of fit parameters
    
    # Canvas and legend
    c = R.TCanvas("c", "c", 600, 700)
    legend = R.TLegend(0.01,0.01,0.99,0.99) #0.65,0.55,0.9,0.9
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(4000)

    # Plot the components
    index = 0
    list_of_leg_entries = {}

    for key, pdfname in dict_of_pdf_names.items():
        theColour = dict_for_fit[key][3]
        if stacked:
            total_pdfname = list_of_stacked[index]
        else:
            total_pdfname = pdfname

        #if showPlot == True:
        print(f"Plotting {total_pdfname}")
        # or the yield roorealvar name #Normalization(1.0,R.RooAbsReal.RelativeExpected),
        model_after_fit.plotOn(
            mcorrFrameForPlotting,
            theSlice,
            R.RooFit.Components( total_pdfname ), 
            theProj,
            R.RooFit.Name( "N_"+key ),
            R.RooFit.Normalization(1.0,R.RooAbsReal.RelativeExpected),
            R.RooFit.LineColor( theColour ),
            R.RooFit.DrawOption( pdfs_drawoption ),
            R.RooFit.FillStyle( pdfs_fillstyle ),
            R.RooFit.FillColor( theColour ),
            R.RooFit.LineWidth( 3 )
        )
        #R.RooFit.LineStyle( R.kSolid ),
        #R.RooFit.LineWidth( 3 )

        # Add entry to legend with all this extra stuff
        tmp = R.TH1D(f"{pdfname}_tmp",f"Temp {pdfname}", 1, 0, 1)
        tmp.Fill(0.5)
        tmp.SetLineWidth(5)
        tmp.SetLineColor( theColour )
        tmp.SetFillColor( theColour )
        #tmp.Draw()
        list_of_leg_entries[key] = tmp
        legend.AddEntry(list_of_leg_entries[key], dict_of_leg_entries[key], leg_style)

        index += 1

    # plot on top
    dataset.plotOn(mcorrFrameForPlotting, R.RooFit.Cut(f"sample==sample::{sample_name}"), R.RooFit.MarkerStyle(8), R.RooFit.MarkerSize(1.5), R.RooFit.Binning( bins[0], bins[1], bins[2] ))

    ## THIS IS THE BLINDING BIT
    # For blinding plot fake frame with nothing on it
    if showPlot == False: # if blinding
        mcorrFrame = x.frame()
    else: # if not blinding
        mcorrFrame = mcorrFrameForPlotting

    # Plot
    #mcorrFrame.getAttText().SetTextSize(0.0275)
    #mcorrFrame.getAttText().SetTextFont(132)
    mcorrFrame.GetXaxis().SetLabelOffset(999) # Remove X axis labels
    mcorrFrame.GetXaxis().SetRangeUser(xrange[0], xrange[1])

    mcorrFrame.GetYaxis().SetLabelFont(132)
    mcorrFrame.GetYaxis().SetTitleFont(132)
    #mcorrFrame.getAttText().SetLineWidth(0)
    #mcorrFrame.getAttText().SetFillStyle(0)
    mcorrFrame.GetYaxis().SetLabelSize(0.04)
    mcorrFrame.SetTitle("")
    mcorrFrame.SetTitleSize(0.04, 'XY')

    # Plot pulls 
    pullFrame.SetTitle("")
    pullFrame.GetXaxis().SetTitleOffset(1.)
    pullFrame.SetMinimum(-5)
    pullFrame.SetMaximum(5)
    pullFrame.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    pullFrame.GetXaxis().SetTitleOffset(1.)
    pullFrame.GetXaxis().SetLabelFont(132)
    pullFrame.GetXaxis().SetTitleFont(132)
    pullFrame.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    pullFrame.SetLabelSize(0.08, 'XY')
    pullFrame.GetYaxis().SetTitle('(data - model)/#font[12]{#sigma}_{data}')
    pullFrame.GetYaxis().SetTitleOffset(0.5)
    pullFrame.GetYaxis().SetNdivisions(209)
    pullFrame.GetYaxis().SetLabelFont(132)
    pullFrame.GetYaxis().SetTitleFont(132)
    pullFrame.SetTitleSize(0.08, 'XY')
    pullFrame.SetFillColor(R.kSpring-6)

    upperPad = R.TPad('upperPad', 'upperPad', .005, .330, .995, .995)
    lowerPad = R.TPad('lowerPad', 'lowerPad', .005, .005, .995, .330)
    if len(dict_of_pdf_names) > 3:
        lowerbound = 0.6
    else:
        lowerbound = 0.75
    topPad   = R.TPad('topPad', 'topPad', 0.65,lowerbound,0.9,0.9)
    upperPad.SetBottomMargin(0.03)
    lowerPad.SetTopMargin(0.02)
    lowerPad.SetBottomMargin(0.3)
    upperPad.SetLeftMargin(0.15)
    lowerPad.SetLeftMargin(0.15)
    upperPad.SetRightMargin(0.05)
    lowerPad.SetRightMargin(0.05)
    upperPad.SetFillStyle(4000)
    upperPad.SetFrameFillStyle(4000)
    lowerPad.SetFillStyle(4000)
    lowerPad.SetFrameFillStyle(4000)
    topPad.SetFillStyle(4000)
    topPad.SetFrameFillStyle(4000)
    upperPad.Draw()
    lowerPad.Draw()
    topPad.Draw()

    upperPad.cd()
    mcorrFrame.Draw()
    # add chi2 to plot
    label = R.TPaveLabel(0.65,0.5,0.9, 0.6, "#chi^{2}/ndof = %f"%chi2)
    label.SetLineColor(0)
    label.SetBorderSize(1) # removes shadow
    label.SetFillStyle(0)
    label.Draw()
    upperPad.Update()
    lowerPad.cd()
    line = R.TLine(5000,0,13000,0)
    line.SetLineColor(14) # dark gray
    #pullFrame.Draw("B")
    pullFrame.Draw()
    line.Draw()
    topPad.cd()
    legend.Draw()
    topPad.Update()
    print(f"Chi2 / ndf = {chi2}")
    #pause()

    if stacked:
        c.SaveAs(f"{plotName}_stacked.png")
        c.SaveAs(f"{plotName}_stacked.pdf")
        c.SaveAs(f"{plotName}_stacked.C")
    else:
        c.SaveAs(f"{plotName}.png")
        c.SaveAs(f"{plotName}.pdf")
        c.SaveAs(f"{plotName}.C")

    
def drawModel(x, toydata, model_before_fit, bins):
    if x.GetName() == "Bc_CORR_M":
        xlabel = plotting_vars_mcorr["xlabel"]
    elif x.GetName() == "Bc_MDTF_M":
        xlabel = plotting_vars_mvis["xlabel"]

    mcorrFrame1 = x.frame()
    model_before_fit.plotOn(mcorrFrame1, R.RooFit.LineWidth( 3 ))
    mcorrFrame2 = x.frame()
    toydata.plotOn(mcorrFrame2, R.RooFit.MarkerStyle(8))

    c = R.TCanvas("c", "c", 1000, 600)
    c.Divide(2)
    c.cd(1)
    mcorrFrame1.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    mcorrFrame1.Draw()
    c.Update()
    c.cd(2)
    mcorrFrame2.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    mcorrFrame2.Draw()
    c.Update()
    c.Draw()

    c.SaveAs(f"FitResults/SigToys/check_{x.GetName()}_model_and_toy.png")

    pause()

def drawModel2D(x, y, toydata, model_before_fit):

    # Set batch mode
    R.gROOT.SetBatch( True )

    xlabel = plotting_vars_mcorr["xlabel"]
    ylabel = plotting_vars_mvis["xlabel"]
    xbins = getRooBinning(mcorr_bins_for_2D)
    ybins = getRooBinning(mvis_bins_for_2D)

    mcorrFrame1 = x.frame()
    model_before_fit.plotOn(mcorrFrame1, R.RooFit.LineWidth( 3 ))
    mcorrFrame2 = x.frame()
    toydata.plotOn(mcorrFrame2, R.RooFit.MarkerStyle(8), R.RooFit.Binning( xbins ))

    c = R.TCanvas("c", "c", 1000, 600)
    c.Divide(2)
    c.cd(1)
    mcorrFrame1.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    mcorrFrame1.Draw()
    c.Update()
    c.cd(2)
    mcorrFrame2.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%xlabel)
    mcorrFrame2.Draw()
    c.Update()
    c.Draw()
    c.SaveAs(f"FitResults/SigToys/check_{x.GetName()}_2D_model_and_toy.png")

    mvisFrame1 = y.frame()
    model_before_fit.plotOn(mvisFrame1, R.RooFit.LineWidth( 3 ))
    mvisFrame2 = y.frame()
    toydata.plotOn(mvisFrame2, R.RooFit.MarkerStyle(8), R.RooFit.Binning( ybins ))

    c2 = R.TCanvas("c2", "c2", 1000, 600)
    c2.Divide(2)
    c2.cd(1)
    mvisFrame1.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%ylabel)
    mvisFrame1.Draw()
    c2.Update()
    c2.cd(2)
    mvisFrame2.GetXaxis().SetTitle('#font[12]{%s} [MeV]'%ylabel)
    mvisFrame2.Draw()
    c2.Update()
    c2.Draw()
    c2.SaveAs(f"FitResults/SigToys/check_{y.GetName()}_2D_model_and_toy.png")

def drawHist2D(xvar, yvar, data_or_model, Bs_chan, BB):
    R.gROOT.ProcessLine(".L /afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/lhcbStyle.C")
    xbins = getRooBinning(mcorr_bins_for_2D); ybins = getRooBinning(mvis_bins_for_2D)

    # Make histogram
    hist = data_or_model.createHistogram("toy_data", xvar, R.RooFit.Binning(xbins), R.RooFit.YVar(yvar, R.RooFit.Binning(ybins)))
    Nx = hist.GetNbinsX(); Ny = hist.GetNbinsY()
    # Plot histogram and print
    c = R.TCanvas()
    hist.Draw("text colz")
    c.Draw()
    c.SaveAs(f"toy_model_check_{Bs_chan}_{BB}_BB.pdf")

    for j in range(1, Ny+1):
        for i in range(1, Nx+1):
            hist_content  = hist.GetBinContent(i, j)
            hist_error    = hist.GetBinError(i, j)
            if hist_content != 0:
                rel_stat_error = hist_error / hist_content 
            else:
                rel_stat_error = 0.0
            #print(f"Bin ({i}, {j}) (bin_{i-1}_{j-1}): {hist_content} +- {hist_error}, rel. stat. error: {rel_stat_error}")
