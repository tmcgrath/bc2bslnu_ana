#!/bin/bash

python3 fit_Bc2BsPi.py -c JpsiPhi -bkg exp
python3 fit_Bc2BsPi.py -c JpsiPhi -sig dcb -bkg exp

python3 fit_Bc2BsPi.py -c JpsiPhi -bkg cheb
python3 fit_Bc2BsPi.py -c JpsiPhi -sig dcb -bkg cheb
