#
# fit_Bc2BsMuNu.py
#
# Fits on data heavily blinded to begin with
# Fitting script for signal:
# - binned fit with pdfs from MC
#

import argparse
import ROOT as R
import os, sys
import copy, math
RDF = R.RDataFrame
RS = R.RooStats

#####################################
# Global variables and functions    #
#####################################

from common_things import (mcorr_bins, mvis_bins, mcorr_bins_for_2D, mvis_bins_for_2D,
                           dict_of_pdf_names_mcorr, dict_of_pdf_names_mvis, dict_of_pdf_names_2D,
                           dict_of_fit_ranges_for_data_dspi, dict_of_fit_ranges_for_data_jpsiphi, 
                           dict_of_leg_entries, ratio_dspi, ratio_jpsiphi,
                           printBlindWarning)

from drawOptions import (plotting_vars_mcorr, plotting_vars_mvis, 
                         getRooBinning, calcPull,
                         drawModel, drawModel2D, drawPulls2D, drawHist2D,
                         drawStacked, drawStacked2D, drawStackedSim)

from BDT_cuts import BDT_cuts_sig as BDT_cuts

#################################
# Functions for Fits            #
#################################
    
def samplePDFs(x, pdfFile):
    '''
    Gets histograms of PDFs and stores into a dictionary for either Bc mcorr or mvis
    Discriminating variable decided by x and pdf_names passed as an argument
    '''

    print("Sampling PDFs.........")
    dict_of_HistPdfs, dict_of_hists = {}, {}
    inFile = R.TFile.Open(pdfFile, "read")
    bins = []

    if x.GetName() == "Bc_CORR_M":
        pdf_names = dict_of_pdf_names_mcorr
    elif x.GetName() == "Bc_MDTF_M":
        pdf_names = dict_of_pdf_names_mvis
    else:
        print("Variable name does not match Bc_CORR_M or Bc_MDTF_M, aborting!")
        sys.exit()

    for key, value in pdf_names.items():
        # Get Mcorr histograms for the fit pdfs
        # assume these are scaled and sumw2'd
        theHist = inFile.Get(value)
        theHist.SetDirectory(0) # decouples histogram from file or something
        # get binning once
        if "signal" in key:
            bins.append(theHist.GetNbinsX())
            bins.append(theHist.GetXaxis().GetXmin())
            bins.append(theHist.GetXaxis().GetXmax())
        dict_of_hists[key] = theHist

        # Load datasets
        # the Import(, True) is apparently to get the bin density
        dataHist = R.RooDataHist(value+"_DataHist", value+" Data", x, R.RooFit.Import(theHist))

        # Get PDFs
        thePDF = R.RooHistPdf(value, value+" Actual PDF", x, dataHist)

        dict_of_HistPdfs[key] = copy.deepcopy(thePDF)

        print("Added "+dict_of_HistPdfs[key].GetName())

    inFile.Close()
    print(bins)

    return dict_of_HistPdfs, dict_of_hists, bins


def signalFitModel(pdfDict, dict_for_fit, ratio):
    '''
    Makes fit model for one discriminating variable
    '''

    # Fit set up here!
    # All pdfs combined here
    dict_yields = {}
    pdfs = R.RooArgList()
    yields = R.RooArgList()
    print(dict_for_fit)
    
    # Try combining the pdfs of the sl modes and get fractional yield
    # Add rest of background components
    for key, value in pdfDict.items():
        
        # Add the PDF
        pdfs.add(value)

        if len(dict_of_pdf_names_mcorr) == 2:
            ## For case where signal and Bc2Bsstmunu are combined
            theYield = R.RooRealVar("N_"+key, key+" Yield", dict_for_fit[key][0], dict_for_fit[key][1], dict_for_fit[key][2])
            dict_yields[key] = copy.deepcopy(theYield)
        else:
            ## For case where Bc2Bsstmunu is a fraction of signal
            if key != "Bc2BsstMuNu":
                theYield = R.RooRealVar("N_"+key, key+" Yield", dict_for_fit[key][0], dict_for_fit[key][1], dict_for_fit[key][2])
                dict_yields[key] = copy.deepcopy(theYield)
            else:
                # For BsstMuNu set a fraction compared to the Signal yield
                frac = R.RooRealVar("f_Bc2BsstMuNu", "Signal Fraction of Yield", ratio[0])
                Bsst_Yield = R.RooFormulaVar("Bc2BsstMuNu_Yield", "@0 * @1", R.RooArgList(frac, dict_yields["signal"]))
                dict_yields["frac"] = copy.deepcopy(frac)
                dict_yields[key] = copy.deepcopy(Bsst_Yield)

        # add
        yields.add(dict_yields[key])

    model_name = "fit_model"
        
    # Set up model
    model = R.RooAddPdf(model_name, "Mcorr fit model", pdfs, yields)
    print("List of PDFs:")
    pdfs.Print("v")
    print("List of Parameters:")
    yields.Print("v")

    return model, dict_yields

# For a simultaneous Bc corrected mass and visible mass fit
def simultaneousFitModel(pdfDictMcorr, pdfDictMvis, categories, dict_for_fit, ratio):
    '''
    Makes fit model for simultaneous fit of Bc mcorr and mvis
    '''

    # Fit set up here!
    # All pdfs combined here
    dict_yields = {}
    pdfs_mcorr, pdfs_mvis = R.RooArgList(), R.RooArgList()
    yields_mcorr, yields_mvis = R.RooArgList(), R.RooArgList()
    print(dict_for_fit)

    # Try combining the pdfs of the sl modes and get fractional yield
    # Keep same yield variable for everything except the combinatoric background
    for key, value in pdfDictMcorr.items():
        
        #if key != "combinatoric":
        #    continue

        # Add the PDFs to both ones
        pdfs_mcorr.add(value); pdfs_mvis.add(pdfDictMvis[key])
        print(f"{key} {value}")
        print(f"{key} {pdfDictMvis[key]}")

        if key != "combinatoric":

            if key != "Bc2BsstMuNu":
                theYield = R.RooRealVar("N_"+key, key+" Yield", dict_for_fit[key][0], dict_for_fit[key][1], dict_for_fit[key][2])
                dict_yields[key] = copy.deepcopy(theYield)
            else:
                # For BsstMuNu set a fraction compared to the Signal yield
                frac = R.RooRealVar("f_Bc2BsstMuNu", "Signal Fraction of Yield", ratio[0])
                Bsst_Yield = R.RooFormulaVar("Bc2BsstMuNu_Yield", "@0 * @1", R.RooArgList(frac, dict_yields["signal"]))
                dict_yields["frac"] = copy.deepcopy(frac)
                dict_yields[key] = copy.deepcopy(Bsst_Yield)

            # add
            yields_mcorr.add(dict_yields[key])
            yields_mvis.add(dict_yields[key])
        
        else:
            # Combinatorial yields separate for both models as the fit range for mvis doesn't cover it all
            combYield1 = R.RooRealVar("N_"+key+"_Mcorr", key+" Mcorr Yield", dict_for_fit[f"{key}_Mcorr"][0], dict_for_fit[f"{key}_Mcorr"][1], dict_for_fit[f"{key}_Mcorr"][2])
            dict_yields[f"{key}_Mcorr"] = copy.deepcopy(combYield1)
            yields_mcorr.add(dict_yields[f"{key}_Mcorr"])

            combYield2 = R.RooRealVar("N_"+key+"_Mvis", key+" Mvis Yield", dict_for_fit[f"{key}_Mvis"][0], dict_for_fit[f"{key}_Mvis"][1], dict_for_fit[f"{key}_Mvis"][2])
            dict_yields[f"{key}_Mvis"] = copy.deepcopy(combYield2)
            yields_mvis.add(dict_yields[f"{key}_Mvis"])

    print("Dict:")
    print(dict_yields)

    # Set up model
    model_mcorr = R.RooAddPdf(f"Mcorr_fit_model", "Mcorr fit model", pdfs_mcorr, yields_mcorr)
    model_mvis  = R.RooAddPdf(f"Mvis_fit_model", "Mvis fit model", pdfs_mvis, yields_mvis)
    
    sim_model = R.RooSimultaneous("sim_fit_model", "Simultaneous fit model", categories)
    sim_model.addPdf(model_mcorr, "Bc_Mcorr")
    sim_model.addPdf(model_mvis, "Bc_mvis")

    print("List of Mcorr PDFs:")
    pdfs_mcorr.Print("v")
    print("List of Mcorr coefficients:")
    yields_mcorr.Print("v")
    print("List of Mvis PDFs:")
    pdfs_mvis.Print("v")
    print("List of Mvis coefficients:")
    yields_mvis.Print("v")

    print("Simultaneous fit model:")
    sim_model.Print("v")

    return sim_model, model_mcorr, model_mvis, dict_yields


def sampleHistFactoryPDFs(x, pdfFile, ratio, dict_of_fit_ranges, variable="2D"):
    '''
    Make HistFactory::Sample for each fit component
    Calls ActivateStatError() to add parameters for the Beeston-Barlow implementation (incorporates MC stats errors to fit) 
    Depending on the last argument, can be adapted for 1D or 2D
    The argument x will have to reflect this, a RooRealVar of Mcorr or Mvis, or a RooArgList of both
    '''
    variables = ['mcorr', 'mvis', '2D']
    if variable not in variables:
        raise ValueError(f"Invalid variable type in PDF sampling function. Expected one of: {variables}")
    if variable == "2D":
        pdf_names = dict_of_pdf_names_2D
    elif variable == "mvis":
        pdf_names = dict_of_pdf_names_mvis
    else:
        pdf_names = dict_of_pdf_names_mcorr

    print("Sampling PDFs.........")
    inFile = R.TFile.Open(pdfFile, "read")

    dict_of_hists = {}; dict_HistPdfs = {}; dict_of_samples = {}; norm_factors = []
    for key, value in pdf_names.items():
        print(key)
        print(value)
        histName = value
        theHist = inFile.Get(histName)
        theHist.SetDirectory(0) # decouples histogram from file or something

        # save template as a HistFactory::Sample
        theSample = RS.HistFactory.Sample(histName, histName, pdfFile)
        #theSample.SetName(histName)
        #theSample.SetHisto(theHist)
        theSample.ActivateStatError() # make sure we do Beeston-Barlow lite!!
        theSample.SetNormalizeByTheory( False )
        # Set yield central values and ranges
        if key == "Bc2BsstMuNu": # N_Bc2BsstMuNu = ratio * N_Bc2BsMuNu
            theSample.AddNormFactor(f"signal_Yield", dict_of_fit_ranges["signal"][0], dict_of_fit_ranges["signal"][1], dict_of_fit_ranges["signal"][2])
            theSample.AddNormFactor(f"ratio_Bc2BsstMuNu", ratio[0], ratio[0]-ratio[1], ratio[0]+ratio[1]) # fix ratio
        else:
            theSample.AddNormFactor(f"{key}_Yield", dict_of_fit_ranges[key][0], dict_of_fit_ranges[key][1], dict_of_fit_ranges[key][2])

        # Set normalisation
        mcnorm_name = f"{key}_MCNorm"
        theSample.AddNormFactor(mcnorm_name, 1.0/theHist.Integral(),1.0/theHist.Integral(),1.0/theHist.Integral())
        norm_factors.append(mcnorm_name)

        #print(f"Mcorr Binning: {theHist.GetNbinsX()} bins, between [{theHist.GetXaxis().GetBinLowEdge(theHist.FindFirstBinAbove(0., 1))}, {theHist.GetXaxis().GetBinLowEdge(theHist.FindLastBinAbove(0., 1))}]")
        #print(f"Mvis Binning: {theHist.GetNbinsY()} bins, between [{theHist.GetYaxis().GetBinLowEdge(theHist.FindFirstBinAbove(0., 1))}, {theHist.GetYaxis().GetBinLowEdge(theHist.FindLastBinAbove(0., 1))}]")
        # Get RooHistPdfs for toy generation
        dataHist = R.RooDataHist(f"{histName}_DataHist", f"{histName} Data", x, R.RooFit.Import(theHist))
        thePDF = R.RooHistPdf(histName, f"{histName} Actual PDF", x, dataHist)

        # save hist in a dict for record keeping
        dict_of_samples[key] = copy.deepcopy(theSample)
        dict_HistPdfs[key] = copy.deepcopy(thePDF)
        dict_of_hists[key] = theHist

        # Add sample to channel
        #channel.AddSample(theSample) # no need to return cos we're persisting the additions

    inFile.Close()

    return dict_of_samples, dict_HistPdfs, dict_of_hists, norm_factors

def makeWorkspace(dict_of_samples, norm_factors): #, data, x, y, toy = True):
    '''
    Make 2D signal fit model with HistFactory -> returns Workspace
    Including Beeston-Barlow-Lite with the sample construction
    '''
    
    # Add a measurement
    meas = RS.HistFactory.Measurement("Get_Bc2BsMuNu")
    meas.SetExportOnly(True) # tell it not to fit straight away
    meas.SetPOI("signal_Yield") # the signal yield is our white whale
    meas.AddConstantParam("Lumi")
    meas.AddConstantParam("ratio_Bc2BsstMuNu")
    for factor in norm_factors:
        meas.AddConstantParam(factor)
    meas.SetLumi(1.0)
    meas.SetLumiRelErr(0.0) # still not sure what this does

    # Add a channel
    channel = RS.HistFactory.Channel("one_2D_fit")
    channel.SetStatErrorConfig(1e-5,"Poisson") 
    for sample in dict_of_samples.values():
        channel.AddSample(sample)

    # Make workspace
    meas.AddChannel(channel)
    meas.CollectHistograms() # don't need this if I'm adding the histograms to the sample directly
    ws = RS.HistFactory.MakeModelAndMeasurementFast(meas)
    #factory = RS.HistFactory.HistoToWorkspaceFactoryFast(meas)
    #ws = factory.MakeSingleChannelModel(meas, channel)
    #model_config = ws.obj("ModelConfig")
    #model_config.GetNuisanceParameters().find("Lumi").setConstant(R.kTRUE) # already set lumi to constant
    #model_config.GetNuisanceParameters().find("ratio_Bc2BsstMuNu").setConstant(R.kTRUE)

    return ws

# Feldman-Cousins method for getting a confidence interval
# Based on https://root.cern.ch/doc/master/rs401c__FeldmanCousins_8py.html
def runFeldmanCousins(x, sig_yield, bkg_yield, model, toy, CL):
    
    # Time it for fun
    t = R.TStopwatch()
    t.Start()

    # Set min and max of POI of sig yield
    val = sig_yield.getValV(); err = sig_yield.getError()
    parameters = R.RooArgSet(sig_yield)
    min = val - 10*err; max = val + 10*err
    print(f"min = {min}, max = {max}")
    sig_yield.setMin(min); sig_yield.setMax(max); # set to 5 sigma away from fit

    # Set background to constant
    #combVal = bkg_yield.getValV()
    bkg_yield.setConstant(True)

    # Set up ModelConfig for RooStats object
    w = R.RooWorkspace()
    modelConfig = R.RooStats.ModelConfig("myModel", w)
    modelConfig.SetPdf(model)
    modelConfig.SetParametersOfInterest(parameters) # parameter of interest is signal yield
    modelConfig.SetObservables(R.RooArgSet(x))
    w.Print()

    print(toy)
    print(f"Number of entries in toy dataset: {toy.sumEntries()}")
    print(model)
    print(sig_yield)
    print(bkg_yield)

    # Set up Feldman-Cousins
    fc = R.RooStats.FeldmanCousins(toy, modelConfig)
    fc.SetConfidenceLevel(CL)
    fc.UseAdaptiveSampling(True) # not sure what this does
    fc.FluctuateNumDataEntries(False)  # number counting analysis: dataset always has 1 entry with N events observed
    fc.SetNBins(100) # number of points to test per parameter

    # Get interval
    interval = fc.GetInterval()
    result = [interval.LowerLimit(sig_yield), interval.UpperLimit(sig_yield)]

    t.Stop()
    t.Print()

    return result

# Print and save blinded results
def printOutput(dict_yields, text_file = None):
    # For blinded results
    print("Printing BLINDED results:")
    
    comb = dict_yields["combinatoric"].getValV()
    comb_err = dict_yields["combinatoric"].getError()
    sig_err = dict_yields["signal"].getError()

    print(f"Comb yield = {comb} += {comb_err}")
    print(f"Signal yield = xxx += {sig_err}")

    if text_file is not None:
        with open(text_file, "w") as f:
            f.write("Printing BLINDED results:\n")
            f.write(f"Comb yield = {comb} += {comb_err}")
            f.write(f"Signal yield = xxx += {sig_err}")

# Print and save blinded results
def printHFOutput(fitResult, text_file = None):
    # For blinded results
    print("Printing BLINDED results:")
    
    comb_var = fitResult.floatParsFinal().find("combinatoric_Yield")
    sig_var  = fitResult.floatParsFinal().find("signal_Yield")

    comb = comb_var.getValV()
    comb_err = comb_var.getError()
    sig_err = sig_var.getError()

    print(f"Comb yield = {comb} += {comb_err}")
    print(f"Signal yield = xxx += {sig_err}")

    if text_file is not None:
        with open(text_file, "w") as f:
            f.write("Printing BLINDED results:\n")
            f.write(f"Comb yield = {comb} += {comb_err}")
            f.write(f"Signal yield = xxx += {sig_err}")

# Print output for simultaneous fits
def printSimOutput(dict_yields, text_file = None):
    # For blinded results
    print("Printing BLINDED results:")
    
    comb_mcorr = dict_yields[f"combinatoric_Mcorr"].getValV()
    comb_mcorr_err = dict_yields[f"combinatoric_Mcorr"].getError()
    comb_mvis = dict_yields[f"combinatoric_Mvis"].getValV()
    comb_mvis_err = dict_yields[f"combinatoric_Mvis"].getError()
    sig_err = dict_yields["signal"].getError()

    print(f"Comb yield for Mcorr = {comb_mcorr} += {comb_mcorr_err}")
    print(f"Comb yield for Mvis = {comb_mvis} += {comb_mvis_err}")
    print(f"Signal yield = xxx += {sig_err}")

    if text_file is not None:
        with open(text_file, "w") as f:
            f.write("Printing BLINDED results:\n")
            f.write(f"Comb yield for Mcorr = {comb_mcorr} += {comb_mcorr_err}\n")
            f.write(f"Comb yield for Mvis = {comb_mvis} += {comb_mvis_err}\n")
            f.write(f"Signal yield = xxx += {sig_err}")


######################
# Fitting
######################

def fitData(x, data, pdf_file, Bs_chan, dict_of_fit_ranges, ratio, blinding=True):

    print("Fitting data with Blind category!\n")
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/Sig"

    # Blinding
    blind, unblind = "blind", "unblind" 
    blindCat = R.RooCategory("blindCat", "Blinding state")
    blindCat.defineType(unblind, 0)
    blindCat.defineType(blind, 1)

    if blinding:
        print("Blinding signal yield!\n")
        blindCat.setLabel(blind)
        suffix = blind
    else:
        print("Unblinding signal yield!\n")
        blindCat.setLabel(unblind)
        suffix = unblind

    # Final check that blind category is not unblind!!
    if blindCat.getIndex() != 1 or blinding == False:
        print("Blind category set to 'unblind', ABORTING")
        sys.exit()

    ## Get all the components
    # Get dict of HistPdfs
    dict_HistPdfs, _, binning = samplePDFs(x, pdf_file)
    # Get fit model using those pdfs
    sigModel, dict_yield_vars = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    
    print("*"*30)

    #TODO: remove when test is done
    # Generate toy data with number of events expected (i.e. specified in RooRealVars)
    #sigModel_for_gen, _ = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    #data = sigModel_for_gen.generateBinned(x)

    if x.GetName() == "Bc_CORR_M": xrange = plotting_vars_mcorr["xrange"]
    elif x.GetName() == "Bc_MDTF_M": xrange = plotting_vars_mvis["xrange"]

    # Then fit
    R.RooMsgService.instance().setSilentMode(True)
    result = sigModel.fitTo(data, R.RooFit.Range(xrange[0], xrange[1]), R.RooFit.Extended( True ), R.RooFit.Save(True) )
    params = result.floatParsFinal().getSize()

    # save plot?
    outName = f"{outdir}/data_fit_{x.GetName()}_{Bs_chan}_{suffix}"
    drawStacked(x, data, sigModel, params, binning, dict_of_fit_ranges, outName, blind = blindCat, stacked=False)
    drawStacked(x, data, sigModel, params, binning, dict_of_fit_ranges, outName, blind = blindCat)

    # Blinded output
    text_output = f"{outdir}/fit_results_{x.GetName()}_{suffix}_{Bs_chan}.txt"
    printOutput(dict_yield_vars, text_output)

def fitDataSimultaneous(x1, x2, data, pdf_file, categories, Bs_chan, dict_of_fit_ranges, ratio, blinding=True):
    '''
    Function to fit a simultaneous fit on data
    The argument data must be a combined data here
    '''

    print("Fitting data with Blind category!\n")
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/Sig"

    # Blinding
    blind, unblind = "blind", "unblind" 
    blindCat = R.RooCategory("blindCat", "Blinding state")
    blindCat.defineType(unblind, 0)
    blindCat.defineType(blind, 1)

    if blinding:
        print("Blinding signal yield!\n")
        blindCat.setLabel(blind)
        suffix = blind
    else:
        print("Unblinding signal yield!\n")
        blindCat.setLabel(unblind)
        suffix = unblind

    # Final check that blind category is not unblind!!
    if blindCat.getIndex() != 1 or blinding == False:
        print("Blind category set to 'unblind', ABORTING")
        sys.exit()

    ## Get all the components
    # Get dict of HistPdfs
    dict_HistPdfs_1, _, binning_1 = samplePDFs(x1, pdf_file, dict_of_pdf_names_mcorr)
    dict_HistPdfs_2, _, binning_2 = samplePDFs(x2, pdf_file, dict_of_pdf_names_mvis)
    # Get fit model using those pdfs
    sigModel, _, _, dict_yield_vars = simultaneousFitModel(dict_HistPdfs_1, dict_HistPdfs_2, sample, dict_of_fit_ranges, ratio)
    
    print("*"*30)

    #TODO: remove when test is done
    # Generate toy data with number of events expected (i.e. specified in RooRealVars)
    #sigModel_for_gen, _ = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    #data = sigModel_for_gen.generateBinned(x)

    # Then fit
    R.RooMsgService.instance().setSilentMode(True)
    result = sigModel.fitTo(data, R.RooFit.Extended( True ), R.RooFit.Save(True) )
    params = result.floatParsFinal().getSize()

    # save plot?
    outName1 = f"{outdir}/data_sim_fit_{x1.GetName()}_{Bs_chan}_{suffix}"
    outName2 = f"{outdir}/data_sim_fit_{x1.GetName()}_{Bs_chan}_{suffix}"

    # Mcorr
    drawStackedSim(x1, data, sigModel, categories, params, binning_1, dict_of_fit_ranges, outName1, blind = blindCat, stacked=False)
    drawStackedSim(x1, data, sigModel, categories, params, binning_1, dict_of_fit_ranges, outName1, blind = blindCat)
    # Mvis
    drawStackedSim(x2, data, sigModel, categories, params, binning_2, dict_of_fit_ranges, outName2, blind = blindCat, stacked=False)
    drawStackedSim(x2, data, sigModel, categories, params, binning_2, dict_of_fit_ranges, outName2, blind = blindCat)

    # Blinded output
    text_output = f"{outdir}/sim_fit_results_{suffix}_{Bs_chan}.txt"
    printSimOutput(dict_yield_vars, text_output)

def fitData2D(x1, x2, data_item, pdf_file, Bs_chan, dict_of_fit_ranges, ratio, BB=False, blinding=True):
    '''
    Function to do a 2D Mcorr vs Mvis fit to data
    Option to do Beeston-Barlow-Lite
    '''
    with_or_without = "with" if BB else "without"
    print(f"Fitting data with a 2D model {with_or_without} the Beeston-Barlow lite method\n")
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/Sig"

    # Blind setting stuff
    print("*"*40)
    if blinding: print("* Fitting data blind - good to go!")
    else: print("* WARNING: blinding set to FALSE")
    print("*"*40)

    xy = R.RooArgSet(x1, x2)
    print(pdf_file)
    ## Get all the components
    # Get dict of HistPdfs
    dict_samples, dict_HistPdfs, dict_hists, mcnorms = sampleHistFactoryPDFs(xy, pdf_file, ratio, dict_of_fit_ranges, variable="2D")
    
    #####
    ## Model building without and with HistFactory
    #####
    if not BB:
        sigModel_for_toy, dict_yield_vars_for_toys = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
        sigModel, dict_yield_vars = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio) # Only for when testing without histfactory - otherwise comment

        # Get the fit variables 
        new_x = x1
        new_y = x2
        new_xy = R.RooArgSet(new_x, new_y)

    else:
        # Make measurement that configures fit with the fit pdfs
        ws = makeWorkspace(dict_samples, mcnorms)
        model_config = ws.obj("ModelConfig")
        sigModel = model_config.GetPdf() # hopefully this is a RooAddPdf or something similar
    
        # Get the fit variables
        idx = ws.cat("channelCat")
        new_x = ws.var("obs_x_one_2D_fit")
        new_x.SetTitle(plotting_vars_mcorr["xlabel"]); new_x.setUnit("MeV/c^{2}")
        new_y = ws.var("obs_y_one_2D_fit")
        new_y.SetTitle(plotting_vars_mvis["xlabel"]);  new_y.setUnit("MeV/c^{2}")
        new_xy = R.RooArgSet(new_x, new_y)

    ## Blind results
    if data_item != "toy" and blinding == False:
        printBlindWarning() # print warning if fitting to real data and the blinding is set to false
    R.RooMsgService.instance().setSilentMode(blinding)

    print("Done")
    sys.exit()

    # Get data depending on if toy or not
    # Make RooDataHist of data
    data = R.RooDataHist("2D_data", "2D data",
                            new_xy,
                            R.RooFit.Import(data_item.GetValue()))
    #print(f"Number of entries in generated dataset: {data.sumEntries()}")

    # Do the toy fit
    if BB:
        # Make nll model to data
        # - extended, and likelihood offsetting
        nll = sigModel.createNLL( data ) # , R.RooFit.Extended( True ), R.RooFit.Offset( True )

        # Create Beeston-Barlow likelihood
        bbnll = RS.HistFactory.RooBarlowBeestonLL("BBNLL", "BBNLL", nll)
        bbnll.setPdf( sigModel )
        bbnll.setDataset( data )
        bbnll.initializeBarlowCache() # not sure what this does - seems to only work for roosimultaneous

        # Then fit
        minuit = R.RooMinimizer( bbnll )
        minuit.setErrorLevel(0.5) # for correct error estimation
        minuit.setStrategy(2) # small step size
        minuit.optimizeConst( True ) # perform constant term optimization on function being minimized
        minuit.setOffsetting( True ) 
        minuit.migrad() # Minimization
        minuit.hesse() # Hessian error analysis
        result = minuit.save("result","result")

    else:
        result = sigModel.fitTo(data, R.RooFit.Extended( True ), R.RooFit.Save( True ))

    if blinding == False:
        result.Print("v")
    params = result.floatParsFinal().getSize()
    
    #sys.exit()
    # save plot?
    suffix = "blind" if blinding == True else "unblind"
    outName = f"{outdir}/fit_2D_results_{Bs_chan}_{suffix}_{with_or_without}_BB"
    # Plot results
    #if blinding == False or data_item == "toy":
        #drawStacked2D(new_x, new_y, data, sigModel, params, dict_of_fit_ranges, outName, stacked=False)
        #drawStacked2D(new_x, new_y, data, sigModel, params, dict_of_fit_ranges, outName)
        
    drawPulls2D(new_x, new_y, data, sigModel, params, outName, beeston_barlow=False)
    print("Done")

    # Blinded output
    if BB:
        printHFOutput(result) 
    else:
        printOutput(dict_yield_vars)

####

# Fit one toy and see what happens
def fitToyBlind(x, pdf_file, Bs_chan, dict_of_fit_ranges, ratio, blinding=True):

    print("Fitting one toy with Blind category!\n")
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/Sig"

    if x.GetName() == "Bc_CORR_M":
        xbins = mcorr_bins
        xlabel = plotting_vars_mcorr["xlabel"]
    elif x.GetName() == "Bc_MDTF_M":
        xbins = mvis_bins
        xlabel = plotting_vars_mvis["xlabel"]

    # Blinding
    blind, unblind = "blind", "unblind" 
    blindCat = R.RooCategory("blindCat", "Blinding state")
    blindCat.defineType(unblind, 0)
    blindCat.defineType(blind, 1)

    if blinding:
        print("Blinding signal yield!\n")
        blindCat.setLabel(blind)
        suffix = blind
    else:
        print("Unblinding signal yield!\n")
        blindCat.setLabel(unblind)
        suffix = unblind

    ## Get all the components
    # Get dict of HistPdfs
    dict_HistPdfs, dict_of_Hists, binning = samplePDFs(x, pdf_file)
    # Get fit model using those pdfs
    sigModel, dict_yield_vars = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    
    print("*"*30)

    # Generate toy data with number of events expected (i.e. specified in RooRealVars)
    sigModel_for_gen, _ = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    toy_data = sigModel_for_gen.generateBinned(x, R.RooFit.Extended()) 
    
    print("Checking generated dataset")
    print(f"Number of entries in generated dataset: {toy_data.sumEntries()}")
    #bin1 = toy_data.sumEntries("5400 < Bc_CORR_M & Bc_CORR_M < 5800")
    #bin4 = toy_data.sumEntries("6200 < Bc_CORR_M & Bc_CORR_M < 6400")
    #print(f"Number of entries in bin 1: {bin1}")
    #print(f"Number of entries in bin 4: {bin4}")
   
    # Plot for check before fit
    #drawModel(x, toy_data, sigModel, binning)

    # Then fit
    if blinding == True:
        R.RooMsgService.instance().setSilentMode(blinding)
    result = sigModel.fitTo( toy_data, R.RooFit.Range(5000, 13000), R.RooFit.Extended( True ), R.RooFit.Save(True) )
    params = result.floatParsFinal().getSize()
    
    # Check errors in model - DELETE WHEN TESTING FINISHED
    xbinning = getRooBinning(xbins)
    data_hist = toy_data.createHistogram("Data", x, R.RooFit.Binning(xbinning))
    sigModel_hist = sigModel.createHistogram("Model", x, R.RooFit.Binning(xbinning))
    sigModel_hist.Scale(sigModel.expectedEvents( R.RooArgSet(x) ) / sigModel_hist.Integral())
    Nbins = sigModel_hist.GetNbinsX()
    for i in range (1, Nbins + 1):
        data_content = data_hist.GetBinContent(i)
        data_error   = data_hist.GetBinError(i)
        mod_content = sigModel_hist.GetBinContent(i)
        mod_error   = sigModel_hist.GetBinError(i)
        pull = calcPull(data_content, mod_content, data_error)
        print(f"Bin {i}")
        print(f"Data bin content: {data_content} +- {data_error}")
        print(f"Model bin content: {mod_content} +- {mod_error}")
        print(f"Pull: {pull}")
    #canv = R.TCanvas("canv", "canv", 1000, 600)
    #canv.Divide(2)
    #canv.cd(1)
    #sigModel_hist.GetXaxis().SetTitle(xlabel)
    #sigModel_hist.Draw("HIST")
    #canv.cd(2)
    #data_hist.GetXaxis().SetTitle(xlabel)
    #data_hist.Draw()
    #canv.Update()
    #canv.Draw()
    #canv.SaveAs("./checking_data_model.pdf")

    # save plot?
    outName = f"{outdir}/toy_fit_{x.GetName()}_{Bs_chan}_{suffix}"
    #drawStacked(x, toy_data, sigModel, params, binning, dict_of_fit_ranges, outName, blind = blindCat, stacked=False)
    drawStacked(x, toy_data, sigModel, params, binning, dict_of_fit_ranges, outName, blind = blindCat)

    # Blinded output
    printOutput(dict_yield_vars)

# Function to do a RooMCStudy
def fitToys(x, pdf_file, Bs_chan, dict_of_fit_ranges, ratio):

    toys = 1000

    print(f"Fitting {x.GetName()} for {toys} toys!\n")
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/SigToysCombined"

    if x.GetName() == "Bc_CORR_M":
        xrange = plotting_vars_mcorr["xrange"]
    elif x.GetName() == "Bc_MDTF_M":
        xrange = plotting_vars_mvis["xrange"]

    ## Get all the components
    # Get dict of HistPdfs
    dict_HistPdfs, dict_of_Hists, binning = samplePDFs(x, pdf_file)
    # Get fit model using those pdfs
    sigModel, dict_yield_vars = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    
    print("*"*30)

    # Generate series of toy data with the pdfs and fit those pdfs with roomcstudy
    toystudy = R.RooMCStudy(
        sigModel,
        R.RooArgSet( x ),
        R.RooFit.Binned( True ),
        R.RooFit.Extended( True ),
        R.RooFit.FitOptions( R.RooFit.Save( True ), R.RooFit.PrintEvalErrors( 0 ) )
    )
    # inputs: fit model, observable roorealvar, binned -> bin the data between generation and fitting,
    #         silence -> kills all messages below the PROGRESS level, only a single message per sample executed
    #         extended -> extended ML term included in likelihood, poisson fluctuation introduced on number of generated events
    #         fit options -> 

    # Fit to toys
    print(f"Fits on {toys} toy data sets..............")
    toystudy.generateAndFit(toys, nEvtPerSample = 0, keepGenData = R.kTRUE)

    # Get results for signal yield to check?
    R.gStyle.SetOptStat(0)
    if "signal_combined" in list(dict_yield_vars.keys()): sig_var = "signal_combined"
    else: sig_var = "signal"
    signal_yield = dict_yield_vars[sig_var]
    toysSig = toystudy.plotParam(signal_yield, R.RooFit.Bins( 40 ))
    if Bs_chan == "JpsiPhi": 
        if x.GetName() == "Bc_CORR_M": toysSigError = toystudy.plotError(signal_yield, R.RooFit.Bins( 40 ), R.RooFit.Range(25, 40) )
        else: toysSigError = toystudy.plotError(signal_yield, R.RooFit.Bins( 40 ), R.RooFit.Range(15, 30))
    else: toysSigError = toystudy.plotError(signal_yield, R.RooFit.Bins( 40 ))
    toysPull = toystudy.plotPull(signal_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ), R.RooFit.Range(-4, 5))

    # Plot Signal results
    if "Bc2BsRho" in list(dict_yield_vars.keys()):
        nx = 3; ny = 4
    else:
        nx = 3; ny = 2
    figx = 300 * nx
    figy = 350 * ny 
    
    # Plot combinatorial results
    # Draw results
    #R.gStyle.SetPalette(1)
    c = R.TCanvas(f"toy_study_{toys}_times", f"toy_study_{toys}_times", figx, figy)
    c.Divide(nx, ny)
    c.cd(1)
    c.cd(1).SetLeftMargin(0.175)
    c.cd(1).SetRightMargin(0.05)
    c.cd(1).SetTopMargin(0.05)
    c.cd(1).SetBottomMargin(0.15)
    thepdf = dict_of_leg_entries[sig_var]
    toysSig.GetYaxis().SetTitleOffset(1.4)
    toysSig.SetTitle("")
    toysSig.GetXaxis().SetTitle(f"{thepdf} Yield")
    toysSig.SetTitleSize(0.06, 'XY')
    toysSig.SetLabelSize(0.05, 'XY')
    toysSig.Draw()
    c.cd(2)
    c.cd(2).SetLeftMargin(0.175)
    c.cd(2).SetRightMargin(0.05)
    c.cd(2).SetTopMargin(0.05)
    c.cd(2).SetBottomMargin(0.15)
    toysSigError.GetYaxis().SetTitleOffset(1.4)
    toysSigError.SetTitle("")
    toysSigError.GetXaxis().SetTitle(f"{thepdf} Yield Error")
    toysSigError.SetTitleSize(0.06, 'XY')
    toysSigError.SetLabelSize(0.05, 'XY')
    toysSigError.Draw()
    c.cd(3)
    c.cd(3).SetLeftMargin(0.175)
    c.cd(3).SetRightMargin(0.05)
    c.cd(3).SetTopMargin(0.05)
    c.cd(3).SetBottomMargin(0.15)
    toysPull.GetYaxis().SetTitleOffset(1.4)
    toysPull.GetXaxis().SetTitle(f"{thepdf} Yield Pull")
    toysPull.SetTitle("")
    toysPull.SetTitleSize(0.06, 'XY')
    toysPull.SetLabelSize(0.05, 'XY')
    toysPull.SetStats(0)
    toysPull.Draw()

    comb_yield = dict_yield_vars["combinatoric"]
    toysComb = toystudy.plotParam(comb_yield, R.RooFit.Bins( 40 ))
    toysCombError = toystudy.plotError(comb_yield, R.RooFit.Bins( 40 ))
    if Bs_chan == "DsPi": toysCombError = toystudy.plotError(comb_yield, R.RooFit.Bins( 40 ) ) 
    else: toysCombError = toystudy.plotError(comb_yield, R.RooFit.Bins( 40 ))
    toysCombPull = toystudy.plotPull(comb_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ), R.RooFit.Range(-4, 5))

    thepdf = dict_of_leg_entries["combinatoric"]
    c.cd(4)
    c.cd(4).SetLeftMargin(0.175)
    c.cd(4).SetRightMargin(0.05)
    c.cd(4).SetTopMargin(0.05)
    c.cd(4).SetBottomMargin(0.15)
    toysComb.GetYaxis().SetTitleOffset(1.4)
    toysComb.SetTitle("")
    toysComb.GetXaxis().SetTitle(f"{thepdf} Yield")
    toysComb.SetTitleSize(0.06, 'XY')
    toysComb.SetLabelSize(0.05, 'XY')
    toysComb.Draw()
    c.cd(5)
    c.cd(5).SetLeftMargin(0.175)
    c.cd(5).SetRightMargin(0.05)
    c.cd(5).SetTopMargin(0.05)
    c.cd(5).SetBottomMargin(0.15)
    toysCombError.GetYaxis().SetTitleOffset(1.4)
    toysCombError.SetTitle("")
    toysCombError.GetXaxis().SetTitle(f"{thepdf} Yield Error")
    toysCombError.SetTitleSize(0.06, 'XY')
    toysCombError.SetLabelSize(0.05, 'XY')
    toysCombError.Draw()
    c.cd(6)
    c.cd(6).SetLeftMargin(0.175)
    c.cd(6).SetRightMargin(0.05)
    c.cd(6).SetTopMargin(0.05)
    c.cd(6).SetBottomMargin(0.15)
    toysCombPull.GetYaxis().SetTitleOffset(1.4)
    toysCombPull.SetTitle("")
    toysCombPull.GetXaxis().SetTitle(f"{thepdf} Yield Pull")
    toysCombPull.SetTitleSize(0.06, 'XY')
    toysCombPull.SetLabelSize(0.05, 'XY')
    toysCombPull.SetStats(0)
    toysCombPull.Draw()

    if "Bc2BsRho" in list(dict_yield_vars.keys()):
        # BcBsPi
        pi_yield = dict_yield_vars["Bc2BsPi_misID"]
        toysPi = toystudy.plotParam(pi_yield, R.RooFit.Bins( 40 ))
        toysPiError = toystudy.plotError(pi_yield, R.RooFit.Bins( 40 ))
        toysPiPull = toystudy.plotPull(pi_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ))

        c.cd(7)
        R.gPad.SetLeftMargin(0.15)
        toysPi.GetYaxis().SetTitleOffset(1.4)
        toysPi.Draw()
        c.cd(8)
        R.gPad.SetLeftMargin(0.15)
        toysPiError.GetYaxis().SetTitleOffset(1.4)
        toysPiError.Draw()
        c.cd(9)
        R.gPad.SetLeftMargin(0.15)
        toysPiPull.GetYaxis().SetTitleOffset(1.4)
        toysPiPull.Draw()

        # BcBsPi
        rho_yield = dict_yield_vars["Bc2BsRho"]
        toysRho = toystudy.plotParam(rho_yield, R.RooFit.Bins( 40 ))
        toysRhoError = toystudy.plotError(rho_yield, R.RooFit.Bins( 40 ))
        toysRhoPull = toystudy.plotPull(rho_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ))

        c.cd(10)
        R.gPad.SetLeftMargin(0.15)
        toysRho.GetYaxis().SetTitleOffset(1.4)
        toysRho.Draw()
        c.cd(11)
        R.gPad.SetLeftMargin(0.15)
        toysRhoError.GetYaxis().SetTitleOffset(1.4)
        toysRhoError.Draw()
        c.cd(12)
        R.gPad.SetLeftMargin(0.15)
        toysRhoPull.GetYaxis().SetTitleOffset(1.4)
        toysRhoPull.Draw()

    c.Draw()
    c.SaveAs(f"{outdir}/toys_sig_summary_plots_{x.GetName()}_{toys}_toys_{Bs_chan}.png")
    c.SaveAs(f"{outdir}/toys_sig_summary_plots_{x.GetName()}_{toys}_toys_{Bs_chan}.pdf")
    c.SaveAs(f"{outdir}/toys_sig_summary_plots_{x.GetName()}_{toys}_toys_{Bs_chan}.C")
    
    ### fit one of the generated data and plot ###
    R.RooMsgService.instance().setSilentMode(False) # don't hide output for roomcstudy
    n = 999 # index of generated data
    toyfit_data = toystudy.genData( n )
    sigModel2, dict_yield_vars = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    result = sigModel2.fitTo( toyfit_data, R.RooFit.Range(xrange[0], xrange[1]), R.RooFit.Extended(True), R.RooFit.Save(True) )
    params = result.floatParsFinal().getSize()

    # save plot?
    #drawStacked(x, toyfit_data, sigModel2, params, binning, dict_of_fit_ranges, f"{outdir}/toy_fit_example_{x.GetName()}_toy{n}_{Bs_chan}", stacked = False)
    #drawStacked(x, toyfit_data, sigModel2, params, binning, dict_of_fit_ranges, f"{outdir}/toy_fit_example_{x.GetName()}_toy{n}_{Bs_chan}", stacked = True)



def fitToySimultaneous(x1, x2, pdf_file, categories, Bs_chan, dict_of_fit_ranges, ratio, blinding=True):
    '''
    Function to fit a simultaneous fit on data
    The argument data must be a combined data here
    '''

    print("Fitting one toy simultaneously for Bc Mcorr and Mvis with Blind category!\n")
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/SigToys"

    # Blinding
    blind, unblind = "blind", "unblind" 
    blindCat = R.RooCategory("blindCat", "Blinding state")
    blindCat.defineType(unblind, 0)
    blindCat.defineType(blind, 1)

    if blinding:
        print("Blinding signal yield!\n")
        blindCat.setLabel(blind)
        suffix = blind
    else:
        print("Unblinding signal yield!\n")
        blindCat.setLabel(unblind)
        suffix = unblind

    ## Get all the components
    # Get dict of HistPdfs
    dict_HistPdfs_1, _, binning_1 = samplePDFs(x1, pdf_file, dict_of_pdf_names_mcorr)
    dict_HistPdfs_2, _, binning_2 = samplePDFs(x2, pdf_file, dict_of_pdf_names_mvis)
    # Get fit model using those pdfs
    sigModel, McorrModel, MvisModel, dict_yield_vars = simultaneousFitModel(dict_HistPdfs_1, dict_HistPdfs_2, sample, dict_of_fit_ranges, ratio)
    
    print("*"*30)

    # Generate toy data with number of events expected (i.e. specified in RooRealVars)
    _, McorrModel_for_toy, MvisModel_for_toy,  _ = simultaneousFitModel(dict_HistPdfs_1, dict_HistPdfs_2, sample, dict_of_fit_ranges, ratio)
    toy_data_1 = McorrModel_for_toy.generateBinned(x1, R.RooFit.Extended()) 
    toy_data_2 = MvisModel_for_toy.generateBinned(x2, R.RooFit.Extended()) 
    # Draw model for check
    #drawModel(x1, toy_data_1, McorrModel, binning_1)
    #drawModel(x2, toy_data_2, MvisModel, binning_2)

    # Construct combined dataset
    toy_data = R.RooDataHist("combined_toy", "combined toy data", 
                            R.RooArgList(x1, x2),
                            R.RooFit.Index(categories),
                            R.RooFit.Import("Bc_Mcorr", toy_data_1),
                            R.RooFit.Import("Bc_Mvis", toy_data_2)
                            )
    print("Checking generated dataset")
    print(f"Number of entries in generated datasets: {toy_data_1.sumEntries()} {toy_data_2.sumEntries()} {toy_data.sumEntries()}")

    # Then fit
    if blinding == True:
        R.RooMsgService.instance().setSilentMode(True)
    result = sigModel.fitTo(toy_data, R.RooFit.Extended( True ), R.RooFit.Save( True ))
    params = result.floatParsFinal().getSize()

    # save plot?
    outName1 = f"{outdir}/sim_toy_fit_{x1.GetName()}_{Bs_chan}_{suffix}"
    outName2 = f"{outdir}/sim_toy_fit_{x2.GetName()}_{Bs_chan}_{suffix}"

    # Mcorr
    #drawStackedSim(x1, toy_data, sigModel, categories, params, binning_1, dict_of_fit_ranges, outName1, blind = blindCat, stacked=False)
    #drawStackedSim(x1, toy_data, sigModel, categories, params, binning_1, dict_of_fit_ranges, outName1, blind = blindCat)
    # Mvis

    drawStackedSim(x2, toy_data, sigModel, categories, params, binning_2, dict_of_fit_ranges, outName2, blind = blindCat, stacked=False)
    #drawStackedSim(x2, toy_data, sigModel, categories, params, binning_2, dict_of_fit_ranges, outName2, blind = blindCat)

    # Blinded output
    printSimOutput(dict_yield_vars)

def fitToys2D(x1, x2, pdf_file, Bs_chan, dict_of_fit_ranges, ratio, BB=False, blinding=True):
    '''
    Function to fit a simultaneous fit on data
    Option to do Beeston-Barlow-Lite
    '''
    toys = 1000

    with_or_without = "with" if BB else "without"
    print(f"Fitting a 2D fit model {with_or_without} the Beeston-Barlow lite method for {toys} toys!\n")
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/SigToysCombined"

    print(pdf_file)
    ## Get all the components
    # Get dict of HistPdfs
    dict_samples, dict_HistPdfs, dict_hists, mcnorms = sampleHistFactoryPDFs(R.RooArgSet(x1, x2), pdf_file, ratio, dict_of_fit_ranges, variable="2D")
    
    #####
    ## Model building without and with HistFactory
    #####
    if not BB:
        sigModel_for_toy, dict_yield_vars_for_toys = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
        sigModel, dict_yield_vars = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio) # Only for when testing without histfactory - otherwise comment

        # Get the fit variables 
        new_x = x1
        new_y = x2
        new_xy = R.RooArgSet(new_x, new_y)

        #data = sigModel_for_toy.generateBinned(new_xy, R.RooFit.Extended())
        data = sigModel_for_toy.generateBinned(new_xy)

    else:
        # Make measurement that configures fit with the fit pdfs
        ws = makeWorkspace(dict_samples, mcnorms)
        model_config = ws.obj("ModelConfig")
        sigModel = model_config.GetPdf() # hopefully this is a RooAddPdf or something similar
    
        # Get the fit variables
        idx = ws.cat("channelCat")
        new_x = ws.var("obs_x_one_2D_fit")
        new_x.SetTitle(plotting_vars_mcorr["xlabel"]); new_x.setUnit("MeV/c^{2}")
        new_y = ws.var("obs_y_one_2D_fit")
        new_y.SetTitle(plotting_vars_mvis["xlabel"]);  new_y.setUnit("MeV/c^{2}")
        new_xy = R.RooArgSet(new_x, new_y)

        # get toy data
        #toy_data = sigModel.generateBinned(new_xy, R.RooFit.Extended())
        toy_data = sigModel.generateBinned(new_xy)
        data = R.RooDataHist( "toyData", "toy 2D data", new_xy, R.RooFit.Index( idx ), R.RooFit.Import( "one_2D_fit", toy_data ))

    print("Got signal model:")
    sigModel.Print("v")

    print("Checking generated dataset")
    print(f"Number of entries in generated dataset: {data.sumEntries()}")
    #drawModel2D(new_x, new_y, data, sigModel)
    #drawHist2D(new_x, new_y, data, Bs_chan, with_or_without)

    ## Blind results
    #if blinding == False:
    #    printBlindWarning() # print warning if fitting to real data and the blinding is set to false
    R.RooMsgService.instance().setSilentMode(blinding)

    # Do the toy fit
    if BB:
        # Make nll model to data
        # - extended, and likelihood offsetting
        nll = sigModel.createNLL( data ) # , R.RooFit.Extended( True ), R.RooFit.Offset( True )

        # Create Beeston-Barlow likelihood
        bbnll = RS.HistFactory.RooBarlowBeestonLL("BBNLL", "BBNLL", nll)
        bbnll.setPdf( sigModel )
        bbnll.setDataset( data )
        bbnll.initializeBarlowCache() # not sure what this does - seems to only work for roosimultaneous

        # Then fit
        minuit = R.RooMinimizer( bbnll )
        minuit.setErrorLevel(0.5) # for correct error estimation
        minuit.setStrategy(2) # small step size
        minuit.optimizeConst( True ) # perform constant term optimization on function being minimized
        minuit.setOffsetting( True ) 
        minuit.migrad() # Minimization
        minuit.hesse() # Hessian error analysis
        result = minuit.save("result","result")

    else:
        result = sigModel.fitTo(data, R.RooFit.Extended( True ), R.RooFit.Save( True ))

    if blinding == False:
        result.Print("v")
    params = result.floatParsFinal().getSize()
    # Get variables from workspace
    #sys.exit()
    # save plot?
    outName = f"{outdir}/toy_fit_2D_{Bs_chan}_{with_or_without}_BB"
    
    # Draw toy
    drawStacked2D(new_x, new_y, data, sigModel, params, dict_of_fit_ranges, outName, stacked=False)
    drawStacked2D(new_x, new_y, data, sigModel, params, dict_of_fit_ranges, outName)
    drawPulls2D(new_x, new_y, data, sigModel, params, outName, beeston_barlow=False)
    # Blinded output
    #printHFOutput(result)

    #if BB: sys.exit() # for now
    #sys.exit() # for now

    #####
    ## Toy studies
    #####

    #if not BB: bcbsst_var = dict_yield_vars["Bc2BsstMuNu"]
    # Generate series of toy data with the pdfs and fit those pdfs with roomcstudy
    toystudy = R.RooMCStudy(
        sigModel,
        new_xy,
        R.RooFit.Binned( True ),
        R.RooFit.Extended( True ),
        R.RooFit.FitOptions( R.RooFit.Save( True ), R.RooFit.PrintEvalErrors( 0 ) )
    )
    # inputs: fit model, observable roorealvar, binned -> bin the data between generation and fitting,
    #         silence -> kills all messages below the PROGRESS level, only a single message per sample executed
    #         extended -> extended ML term included in likelihood, poisson fluctuation introduced on number of generated events
    #         fit options -> 

    # Fit to toys
    print(f"Fits on {toys} toy data sets..............")
    toystudy.generateAndFit(toys, nEvtPerSample = 0, keepGenData = R.kTRUE)

    # Get results for signal yield to check?
    R.gStyle.SetOptStat(0)
    if "signal_combined" in list(dict_yield_vars.keys()): sig_var = "signal_combined"
    else: sig_var = "signal"
    signal_yield = dict_yield_vars[sig_var]
    toysSig = toystudy.plotParam(signal_yield, R.RooFit.Bins( 40 ))
    if Bs_chan == "JpsiPhi": toysSigError = toystudy.plotError(signal_yield, R.RooFit.Bins( 40 ), R.RooFit.Range(15, 25) )
    else: toysSigError = toystudy.plotError(signal_yield, R.RooFit.Bins( 40 ))
    toysPull = toystudy.plotPull(signal_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ), R.RooFit.Range(-4, 5))

    # Plot Signal results
    if "Bc2BsRho" in list(dict_yield_vars.keys()):
        nx = 3; ny = 4
    else:
        nx = 3; ny = 2
    figx = 300 * nx
    figy = 350 * ny 
    
    # Plot combinatorial results
    # Draw results
    #R.gStyle.SetPalette(1)
    c = R.TCanvas(f"toy_study_{toys}_times", f"toy_study_{toys}_times", figx, figy)
    #R.gPad.SetLeftMargin(0.15)
    #R.gPad.SetRightMargin(0.05)
    #R.gPad.SetBottomMargin(0.15)
    #R.gPad.SetTopMargin(0.05)
    c.Divide(nx, ny)
    c.cd(1)
    c.cd(1).SetLeftMargin(0.175)
    c.cd(1).SetRightMargin(0.05)
    c.cd(1).SetTopMargin(0.05)
    c.cd(1).SetBottomMargin(0.15)
    thepdf = dict_of_leg_entries[sig_var]
    toysSig.GetYaxis().SetTitleOffset(1.4)
    toysSig.SetTitle("")
    toysSig.GetXaxis().SetTitle(f"{thepdf} Yield")
    toysSig.SetTitleSize(0.06, 'XY')
    toysSig.SetLabelSize(0.05, 'XY')
    toysSig.Draw()
    c.cd(2)
    c.cd(2).SetLeftMargin(0.175)
    c.cd(2).SetRightMargin(0.05)
    c.cd(2).SetTopMargin(0.05)
    c.cd(2).SetBottomMargin(0.15)
    toysSigError.GetYaxis().SetTitleOffset(1.4)
    toysSigError.SetTitle("")
    toysSigError.GetXaxis().SetTitle(f"{thepdf} Yield Error")
    toysSigError.SetTitleSize(0.06, 'XY')
    toysSigError.SetLabelSize(0.05, 'XY')
    toysSigError.Draw()
    c.cd(3)
    c.cd(3).SetLeftMargin(0.175)
    c.cd(3).SetRightMargin(0.05)
    c.cd(3).SetTopMargin(0.05)
    c.cd(3).SetBottomMargin(0.15)
    toysPull.GetYaxis().SetTitleOffset(1.4)
    toysPull.GetXaxis().SetTitle(f"{thepdf} Yield Pull")
    toysPull.SetTitle("")
    toysPull.SetTitleSize(0.06, 'XY')
    toysPull.SetLabelSize(0.05, 'XY')
    toysPull.SetStats(0)
    toysPull.Draw()

    comb_yield = dict_yield_vars["combinatoric"]
    toysComb = toystudy.plotParam(comb_yield, R.RooFit.Bins( 40 ))
    toysCombError = toystudy.plotError(comb_yield, R.RooFit.Bins( 40 ))
    toysCombPull = toystudy.plotPull(comb_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ), R.RooFit.Range(-4, 5))

    thepdf = dict_of_leg_entries["combinatoric"]
    c.cd(4)
    c.cd(4).SetLeftMargin(0.175)
    c.cd(4).SetRightMargin(0.05)
    c.cd(4).SetTopMargin(0.05)
    c.cd(4).SetBottomMargin(0.15)
    toysComb.GetYaxis().SetTitleOffset(1.4)
    toysComb.SetTitle("")
    toysComb.GetXaxis().SetTitle(f"{thepdf} Yield")
    toysComb.SetTitleSize(0.06, 'XY')
    toysComb.SetLabelSize(0.05, 'XY')
    toysComb.Draw()
    c.cd(5)
    c.cd(5).SetLeftMargin(0.175)
    c.cd(5).SetRightMargin(0.05)
    c.cd(5).SetTopMargin(0.05)
    c.cd(5).SetBottomMargin(0.15)
    toysCombError.GetYaxis().SetTitleOffset(1.4)
    toysCombError.SetTitle("")
    toysCombError.GetXaxis().SetTitle(f"{thepdf} Yield Error")
    toysCombError.SetTitleSize(0.06, 'XY')
    toysCombError.SetLabelSize(0.05, 'XY')
    toysCombError.Draw()
    c.cd(6)
    c.cd(6).SetLeftMargin(0.175)
    c.cd(6).SetRightMargin(0.05)
    c.cd(6).SetTopMargin(0.05)
    c.cd(6).SetBottomMargin(0.15)
    toysCombPull.GetYaxis().SetTitleOffset(1.4)
    toysCombPull.SetTitle("")
    toysCombPull.GetXaxis().SetTitle(f"{thepdf} Yield Pull")
    toysCombPull.SetTitleSize(0.06, 'XY')
    toysCombPull.SetLabelSize(0.05, 'XY')
    toysCombPull.SetStats(0)
    toysCombPull.Draw()

    if "Bc2BsRho" in list(dict_yield_vars.keys()):
        # BcBsPi
        pi_yield = dict_yield_vars["Bc2BsPi_misID"]
        toysPi = toystudy.plotParam(pi_yield, R.RooFit.Bins( 40 ))
        toysPiError = toystudy.plotError(pi_yield, R.RooFit.Bins( 40 ))
        toysPiPull = toystudy.plotPull(pi_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ))

        thepdf = dict_of_leg_entries["Bc2BsPi_misID"]
        c.cd(7)
        R.gPad.SetLeftMargin(0.15)
        toysPi.GetYaxis().SetTitleOffset(1.4)
        toysPi.SetTitle("")
        toysPi.Draw()
        c.cd(8)
        R.gPad.SetLeftMargin(0.15)
        toysPiError.GetYaxis().SetTitleOffset(1.4)
        toysPiError.SetTitle("")
        toysPiError.Draw()
        c.cd(9)
        R.gPad.SetLeftMargin(0.15)
        toysPiPull.GetYaxis().SetTitleOffset(1.4)
        toysPiPull.SetTitle("")
        toysPiPull.Draw()

        # BcBsPi
        rho_yield = dict_yield_vars["Bc2BsRho"]
        toysRho = toystudy.plotParam(rho_yield, R.RooFit.Bins( 40 ))
        toysRhoError = toystudy.plotError(rho_yield, R.RooFit.Bins( 40 ))
        toysRhoPull = toystudy.plotPull(rho_yield, R.RooFit.Bins( 40 ), R.RooFit.FitGauss( True ))

        thepdf = dict_of_leg_entries["Bc2BsRho"]
        c.cd(10)
        R.gPad.SetLeftMargin(0.15)
        toysRho.GetYaxis().SetTitleOffset(1.4)
        toysRho.SetTitle("")
        toysRho.Draw()
        c.cd(11)
        R.gPad.SetLeftMargin(0.15)
        toysRhoError.GetYaxis().SetTitleOffset(1.4)
        toysRhoError.SetTitle("")
        toysRhoError.Draw()
        c.cd(12)
        R.gPad.SetLeftMargin(0.15)
        toysRhoPull.GetYaxis().SetTitleOffset(1.4)
        toysRhoPull.SetTitle("")
        toysRhoPull.Draw()

    c.Draw()
    c.SaveAs(f"{outdir}/toys_sig_summary_plots_2D_{toys}_toys_{Bs_chan}_{with_or_without}_BB.png")
    c.SaveAs(f"{outdir}/toys_sig_summary_plots_2D_{toys}_toys_{Bs_chan}_{with_or_without}_BB.pdf")
    c.SaveAs(f"{outdir}/toys_sig_summary_plots_2D_{toys}_toys_{Bs_chan}_{with_or_without}_BB.C")
    sys.exit()
    ## Draw one toy
    n = 987 # for DsPi
    #n = 999 # for JpsiPhi
    toyfit_data = toystudy.genData( n )
    sigModel2, dict_yield_vars2 = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    result = sigModel2.fitTo( toyfit_data, R.RooFit.Extended( True ), R.RooFit.Save( True ) )
    params = result.floatParsFinal().getSize()
    outNameToy = f"{outdir}/toy_fit_example_2D_toy{n}_{Bs_chan}_{with_or_without}_BB"
    #drawStacked2D(new_x, new_y, toyfit_data, sigModel2, params, dict_of_fit_ranges, outNameToy, stacked=False)
    drawStacked2D(new_x, new_y, toyfit_data, sigModel2, params, dict_of_fit_ranges, outNameToy)
    #drawPulls2D(new_x, new_y, toyfit_data, sigModel2, params, outNameToy, beeston_barlow=False)


# Test Feldman Cousins with toys
def testFeldmanCousins(x, pdf_file, Bs_chan, dict_of_fit_ranges, ratio):

    outdir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit/FitResults/SigToys"

    print("Running Feldman-Cousins")

    # Get dict of HistPdfs
    dict_HistPdfs,  _, binning = samplePDFs(x, pdf_file)
    # Get fit model using those pdfs
    sigModel, dict_yield_vars = signalFitModel(dict_HistPdfs, dict_of_fit_ranges, ratio)
    theSigYield = dict_yield_vars["signal"]
    theCombYield = dict_yield_vars["combinatoric"]

    toy_data = sigModel.generateBinned(x)
    print("Checking generated dataset")
    print(f"Number of entries in generated dataset: {toy_data.sumEntries()}")

    # Get fit first
    result = sigModel.fitTo( toy_data, R.RooFit.Range(5000, 13000), R.RooFit.Extended(True), R.RooFit.Save(True) )
    params = result.floatParsFinal().getSize()

    # save plot?
    drawStacked(x, toy_data, sigModel, params, binning, dict_of_fit_ranges, f"{outdir}/toy_fit_for_FC_example_{Bs_chan}", stacked = True)

    confLevel = 0.95

    FCresult = runFeldmanCousins(x, theSigYield, theCombYield, sigModel, toy_data, confLevel)

    print(f"interval is [{FCresult[0]}, {FCresult[1]}] for CL {confLevel*100}%")



#################################
# SetUp                         #
#################################

if __name__ == "__main__":

    print("*"*30)
    print("Wooooo Real Signal Fit")
    print("*"*30)

    parser=argparse.ArgumentParser()
    parser.add_argument('--pdf_file', '-pf', required=True, type=str, help="File containing the pdfs")
    parser.add_argument('--data_file', '-df', type=str, default="toy", help="File containing data. If none specified, will fit one toy")
    parser.add_argument('--fit_option', '-o', type=str, default="2D", choices=["mcorr", "mvis", "2D", "sim", "toys"], help='''Option for what fit to do out of Bc mcorr ('mcorr'), Bc mvis ('mvis'), a simultaneous fit of both ('sim'), or the default: a 2D fit ('2D').
                        If you input 'toys' then a RooMCStudy will be done for both variables.''')
    parser.add_argument('--unblind', '-u', action="store_true", help="CAUTION THIS FLAG UNBLINDS")
    args = parser.parse_args()

    # set up
    pdf_file = args.pdf_file
    data_file = args.data_file
    fit_opt = args.fit_option
    
    if "DsPi" in pdf_file:
        chan = "DsPi"
        dict_of_fit_ranges = dict_of_fit_ranges_for_data_dspi
        ratio = ratio_dspi
    elif "JpsiPhi" in pdf_file:
        chan = "JpsiPhi"
        dict_of_fit_ranges = dict_of_fit_ranges_for_data_jpsiphi
        ratio = ratio_jpsiphi
    else:
        print("Channel unspecified in files, please check")
        sys.exit()

    # Category for simultaneous fits
    sample = R.RooCategory("sample", "sample")
    sample.defineType("Bc_Mvis")
    sample.defineType("Bc_Mcorr")

    # The fitting section!!!

    if fit_opt == "toys":

        print(f"\nFitting multiple toys using pdf file: {pdf_file} \n")
        mcorr_toyfit = R.RooRealVar("Bc_CORR_M", "M_{corr}(B_{c}^{+})", plotting_vars_mcorr["xrange"][0], plotting_vars_mcorr["xrange"][1])        
        mvis_toyfit  = R.RooRealVar("Bc_MDTF_M", "M(B_{s}^{0}#mu^{+})", plotting_vars_mvis["xrange"][0], plotting_vars_mvis["xrange"][1])

        # Toy studies for both variables
        print(f"\nToy studies with {mcorr_toyfit.GetName()}\n")
        fitToys(mcorr_toyfit, pdf_file, chan, dict_of_fit_ranges, ratio)
        print(f"\nToy studies with {mvis_toyfit.GetName()}\n")
        fitToys(mvis_toyfit, pdf_file, chan, dict_of_fit_ranges, ratio)

    else:
        if ".root" in data_file: # FIT DATA
            
            # TODO: Blinding check
            blind = True # DONT CHANGE

            # check to make sure bs channel is the same for the pdf file and data file
            if chan not in data_file:
                print("ERROR! Bs channel in pdf file and data file does not seem to match, please check")
                sys.exit
            
            if "Bc2BsMu" not in data_file:
                print(f"ERROR! Data file {data_file} does not seem to be for Bc2BsMuNu - please check")
                sys.exit()

            year = (data_file.split(f"Bs2{chan}_")[1]).split("_")[0]
            year_bdt = year

            print(f"\nFitting data with file {data_file} for year: {year}")
            print(f"PDF file: {pdf_file} \n")

            R.RooMsgService.instance().setSilentMode(True)

            # Get data and MC and apply cuts
            theChain = R.TChain("DecayTree")
            theChain.Add(data_file)

            # Get HistDataSet
            mcorr = R.RooRealVar("Bc_CORR_M", "M_{corr}(B_{c}^{+})", plotting_vars_mcorr["xrange"][0], plotting_vars_mcorr["xrange"][1])
            mvis  = R.RooRealVar("Bc_MDTF_M", "M(B_{s}^{0}#mu^{+})", plotting_vars_mvis["xrange"][0], plotting_vars_mvis["xrange"][1])
            BcBDT = R.RooRealVar("BcBDT_score", "BcBDT_score", -10, 10)
            DataArgList = R.RooArgList(mcorr)
            BsCut = "5300 < Bs_MM && Bs_MM < 5420"
            # Make mcorr histogram
            rdf = RDF(theChain)
            print("Applying cuts to data:")
            print(f"{BDT_cuts[chan][year_bdt]}")
            print(BsCut)
            rdf_filtered = rdf.Filter(BsCut).Filter(BDT_cuts[chan][year_bdt])
            mcorr_hist = rdf_filtered.Histo1D(("Mcorr", "", len(mcorr_bins)-1, mcorr_bins), "Bc_CORR_M")
            mvis_hist = rdf_filtered.Histo1D(("Mvis", "", len(mvis_bins)-1, mvis_bins), "Bc_MDTF_M")

            if fit_opt == "sim":
                theData_mcorr = R.RooDataHist("DataSamples_Mcorr", f"Data Bc->BsMuNu Bs->{chan} All MU+MD", mcorr, R.RooFit.Import( mcorr_hist.GetValue() ))
                theData_mvis  = R.RooDataHist("DataSamples_Mcorr", f"Data Bc->BsMuNu Bs->{chan} All MU+MD", mvis, R.RooFit.Import( mvis_hist.GetValue() ))
                theData = R.RooDataHist("combined_toy", "combined toy data", 
                                R.RooArgList(mcorr, mvis),
                                R.RooFit.Index(sample),
                                R.RooFit.Import("Bc_Mcorr", theData_mcorr), 
                                R.RooFit.Import("Bc_Mvis", theData_mvis))
                
                fitDataSimultaneous(mcorr, mvis, theData, sample, pdf_file, chan, dict_of_fit_ranges, ratio, blinding = blind)
            
            elif fit_opt == "2D":
                twoD_hist = rdf_filtered.Histo2D(("Mvis_vs_Mcorr", "",  len(mcorr_bins_for_2D)-1, mcorr_bins_for_2D, len(mvis_bins_for_2D)-1, mvis_bins_for_2D), "Bc_CORR_M", "Bc_MDTF_M")
                fitData2D(mcorr, mvis, twoD_hist, pdf_file, chan, dict_of_fit_ranges, ratio, blinding=True) # mcorr and mvis won't be used here

            else:
                if fit_opt == "mcorr":
                    var = mcorr
                    data_hist = mcorr_hist
                else:
                    var = mvis
                    data_hist = mvis_hist
                theData = R.RooDataHist("DataSamples_All", f"Data Bc->BsMuNu Bs->{chan} All MU+MD", mcorr, R.RooFit.Import( data_hist.GetValue() ))

                fitData(var, theData, pdf_file, chan, dict_of_fit_ranges, ratio, blinding = blind)
    
        else: # FIT TOYS
            print(f"\nFitting one toy using pdf file: {pdf_file} \n")
            if fit_opt != "mvis":
                mcorr_toyfit = R.RooRealVar("Bc_CORR_M", "M_{corr}(B_{c}^{+})", plotting_vars_mcorr["xrange"][0], plotting_vars_mcorr["xrange"][1])
            if fit_opt != "mcorr":
                mvis_toyfit  = R.RooRealVar("Bc_MDTF_M", "M(B_{s}^{0}#mu^{+})", plotting_vars_mvis["xrange"][0], plotting_vars_mvis["xrange"][1])

            # Fit
            if fit_opt == "mcorr":
                fitToyBlind(mcorr_toyfit, pdf_file, chan, dict_of_fit_ranges, ratio, blinding = False)
            elif fit_opt == "mvis":
                fitToyBlind(mvis_toyfit, pdf_file, chan, dict_of_fit_ranges, ratio, blinding = False)
            elif fit_opt == "sim":
                fitToySimultaneous(mcorr_toyfit, mvis_toyfit, pdf_file, sample, chan, dict_of_fit_ranges, ratio, blinding = False)
            elif fit_opt == "2D":
                fitToys2D(mcorr_toyfit, mvis_toyfit, pdf_file, chan, dict_of_fit_ranges, ratio, BB=False, blinding=False)
        


