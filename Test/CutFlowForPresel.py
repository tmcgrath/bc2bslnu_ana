#!/usr/bin/env python
# coding: utf-8

# In[1]:

import sys
from files_DsPi_2015_MU import input_files as infiles15
from files_DsPi_2016_MU import input_files as infiles16
import ROOT as R
R.ROOT.EnableImplicitMT()

#bc_reco = False
#if len(sys.argv)>=2:
#    bc_reco = bool(sys.argv[1])

print("Applying preselection cuts")
#print(f"Additional Bc cuts set to {bc_reco}")

names = R.std.vector('string')()
for n in infiles15:
    names.push_back(n)
for m in infiles16:
    names.push_back(m)
# In[2]:


#f2use = infiles#[len(infiles)-10: len(infiles)]
#print(len(f2use))
#print( f2use)


# In[3]:


#rdf = R.RDataFrame("Bc2BsMu_Bs2DsPi/DecayTree",names)\
rdf = R.RDataFrame("Bc2BsPi_Bs2DsPi/DecayTree",names)\
.Filter("Pi_TRACK_CHI2NDOF<3","pi_track_chi2")\
.Filter("K1_TRACK_CHI2NDOF<3","k1_track_chi2")\
.Filter("K2_TRACK_CHI2NDOF<3","k2_track_chi2")\
.Filter("K2_MC15TuneV1_ProbNNghost<0.3",'k2_ghostprob')\
.Filter("K1_MC15TuneV1_ProbNNghost<0.3",'k1_ghostprob')\
.Filter("Pi_MC15TuneV1_ProbNNghost<0.3",'pi_ghostprob')\
.Filter("Pi_from_Bs_MC15TuneV1_ProbNNghost<0.3",'pi_from_bs_ghostprob')\
.Filter("Mu_bach_MC15TuneV1_ProbNNghost<0.3",'pi_from_bc_ghostprob')\
.Filter("K1_PIDK>-10",'k1_pidk')\
.Filter("K2_PIDK>-10",'k2_pidk')\
.Filter("Pi_PIDK<10",'pi_pidk')\
.Filter("Pi_from_Bs_PIDK<10",'pi_from_bs_pidk')\
.Filter("Mu_bach_PIDK<10",'pi_from_bc_pidk')\
.Filter("Ds_ENDVERTEX_CHI2<10",'ds_vertex_chi2')\
.Filter("K1_PT>500",'k1_PT')\
.Filter("K2_PT>500",'k2_PT')\
.Filter("Pi_PT>500",'pi_PT')\
.Filter("K1_P>1000",'k1_P')\
.Filter("K2_P>1000",'k2_P')\
.Filter("Pi_P>1000",'pi_P')\
.Filter("Pi_P>5e3 || K1_P>5e3 || K2_P>5e3",'one_ds_child_p')\
.Filter("Pi_PT>1e3 || K1_PT>1e3 || K2_PT>1e3",'one_ds_child_pt')\
.Filter("K1_MC15TuneV1_ProbNNk>0.6",'k1_probnnk')\
.Filter("K2_MC15TuneV1_ProbNNk>0.6",'k2_probnnk')\
.Filter("Pi_MC15TuneV1_ProbNNpi>0.6",'pi_probnnpi')\
.Filter("Ds_M>1850 && Ds_M<2068.49",'ds_mass')\
.Filter("Bs_M>4000 && Bs_M<7000",'bs_mass')\
.Filter("Bs_ENDVERTEX_CHI2<10",'bs_endvertexchi2')\
.Filter("Pi_from_Bs_TRACK_CHI2NDOF<2.5",'pi_from_bs_track_chi2')\
.Filter("Pi_from_Bs_PT>1.7e3",'pi_from_bs_pt')\
.Filter("Pi_from_Bs_P>10.e3",'pi_from_bs_p')\
.Filter("Pi_from_Bs_MC15TuneV1_ProbNNpi>0.6",'pi_from_bs_probnnpi')\
.Filter("K1_PT+K2_PT+Pi_PT+Pi_from_Bs_PT>5.e3",'sumpt_bs_daug')\
.Filter("Mu_bach_PT>500","pi_from_bc_PT")\
.Filter("Mu_bach_TRACK_CHI2NDOF<3","pi_from_bc_track_chi2")\
.Filter("Bc_M>5e3","bc_mass")\
.Filter("Bc_ENDVERTEX_CHI2<10","bc_vertex_chi2")
#.Filter("Bc_FD_OWNPV>5","bc_flight_dist")\
#.Filter("Mu_bach_IP_OWNPV>0.2","bc_IP")

# In[ ]:


#c = R.TCanvas()
# hDs = rdf.Histo1D("Ds_M")
# hBs = rdf.Histo1D("Bs_M")


#rdf_bc.Snapshot("DecayTree","ReducedFile_signal_extracuts.root")
rdf.Snapshot("DecayTree","ReducedFile_control_15+16MU.root")
# hDs.Draw()
# c.Update()
# c.Draw()
# c.SaveAs("presel_Ds_mass.pdf")
# c.Clear()
# hBs.Draw()
# c.Update()
# c.Draw()
# c.SaveAs("presel_Bs_mass.pdf")

allCutsReport = rdf.Report()
allCutsReport.Print()


# In[ ]:




# In[ ]:




