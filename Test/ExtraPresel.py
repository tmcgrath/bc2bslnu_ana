#
# ExtraPresel.py
#
# Some more cuts quickly
#
# 02.10.20
#

import sys
import ROOT as R
RDF = R.RDataFrame
R.ROOT.EnableImplicitMT()

less_reduced_file = "ReducedFile_control_15+16MU.root"

print(f"Reading file: {less_reduced_file}")
print("Starting selection")

files = R.std.vector("string")()
files.push_back(less_reduced_file)

# RDataFrame + cuts
rdf = R.RDataFrame("DecayTree", files)\
.Filter("Ds_DIRA_OWNPV>0.99", "ds_dira")\
.Filter("K1_ProbNNk>0.95", "k1_probnnk")\
.Filter("K2_ProbNNk>0.95", "k2_probnnk")\
.Filter("Pi_ProbNNpi>0.95", "pi_probnnpi")\
.Filter("Pi_from_Bs_IP_OWNPV > 0.2", "pi_from_bs_IP")\
.Filter("Pi_from_Bs_ProbNNpi > 0.95", "pi_from_bs_probbnnpi")\
.Filter("Bs_ENDVERTEX_CHI2 < 4", "bs_vertex_chi2")
#.Filter("")\
#.Filter("")\

rdf.Snapshot("DecayTree", "PreselTestFile_15+16MU.root")

report = rdf.Report()
report.Print()


