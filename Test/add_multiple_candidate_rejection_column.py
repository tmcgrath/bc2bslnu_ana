#############################################
# Stolen from D_sl_ana/RD/make_data_to_fit/ #
# Repurposed for Bc->Bsmunu analysis        #
#                                           #
# 01.10.20                                  #
#############################################

import sys,os
import pandas as pd
import root_pandas as rp
import root_numpy as rn
import numpy as np

np.random.seed(51020)#implemented 05/10/2020
def get_multiple_candidate_selection(data_frame):
	"""
	Add a boolean column of whether to reject a multiple candidate based on run/event number
	Note we randomly shuffle the columns, then the "duplicates" function returns false for the
	first occurence, then true for subsequent. So we return this as the "should we reject the event"
	decision
	"""
	internal_clone = data_frame
	#get the old index so we can resort after sampling
	#make the shuffled dataset
	df_shuffle = internal_clone.sample(frac=1).reset_index()
	#print(df_shuffle.head())
	#choose the first multiple candidate from the list
	#this is then the randomized
	df_shuffle['is_mult_cand']= df_shuffle.duplicated(['eventNumber','runNumber'])
	#resort
	df_shuffle=df_shuffle.sort_values(by=['index']).reset_index()
	#should now put this in the original dataframe
	return df_shuffle

# def get_mult_cand_dec(rdfentry,df):
#     return int(df.loc[lambda df : df['rdfentry_']==rdfentry]['is_mult_cand'].values[0])


if len(sys.argv)<2:
	print('please pass a file to run multiple candidates on')
	print('will write a friend tree with the multiple candidate selection to it')
	sys.exit()

possible_trees = ['DecayTree']

outfname=sys.argv[-1].split("/")[-1].split('.root')[0]+'_multiple_candidate_rejection_friend_tree.root'
print('will write to',outfname)

for tr in possible_trees:
	print('processing',tr)
	#continue
	try:
		df = rp.read_root(sys.argv[-1],tr,columns=['runNumber','eventNumber'])
		print('got dataframe\n',df.head())
		df = get_multiple_candidate_selection(df)
		print('modified dataframe\n',df.head())
		df.to_root(outfname,key=tr+'_with_multiple_cand_sel',mode='a')
	except:
		print('problem with tree',tr,'... moving on')
		continue
