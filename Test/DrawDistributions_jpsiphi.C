void DrawDistributions_jpsiphi() {

  TFile* _file0 = TFile::Open("ReducedFile_JpsiPhi_control_15MU.root");
  gROOT->ProcessLine(".L ../lhcbStyle.C");
  _file0->ls();

  auto tr= (TTree*)_file0->Get("DecayTree");

  // Cuts
  TString PhiCut("abs(Phi_M-1019.46)<200 && K1_ProbNNk > 0.95 && K2_ProbNNk > 0.95");
  TString JpsiCut("abs(Jpsi_M-3096.90)<80 && Mu1_ProbNNmu > 0.95 && Mu2_ProbNNmu > 0.95");

  // Phi mass
  tr->Draw("Phi_M>>h1(70, 980, 1050)", PhiCut); // mass window cut
  auto h1 = (TH1D*)gPad->GetPrimitive("h1");
  h1->SetTitle(";m(K^{+}K^{-}) [MeV/c^{2}];Entries / 1 MeV/c^{2}");
  TF1* g1 = new TF1("g1", "gaus", 990, 1050);  
  h1->Fit(g1);
  g1->GetParameters();

  // Jpsi mass 
  tr->Draw("Jpsi_M>>h2(100,3000,3200)", JpsiCut);
  auto h2 = (TH1D*)gPad->GetPrimitive("h2");
  h2->SetTitle(";m(#mu^{+}#mu^{-}) [MeV/c^{2}];Entries / 2 MeV/c^{2}");
  TF1* g2 = new TF1("g2", "gaus", 3010, 3180);
  h2->Fit(g2);
  g2->GetParameters();
  
  // Bs mass
  auto threesig_phi = 3*g1->GetParameter(2);
  auto threesig_phi_string = std::to_string(threesig_phi);
  TString value_phi(threesig_phi_string);
  auto threesig_jpsi = 3*g2->GetParameter(2);
  auto threesig_jpsi_string = std::to_string(threesig_jpsi);
  TString value_jpsi(threesig_jpsi_string);
  
  std::cout << value_phi << std::endl;
  std::cout << value_jpsi << std::endl;

  TString BsCut(PhiCut+ " && " + JpsiCut+" && fabs(Phi_M-1019.46)<"+value_phi+ " && abs(Jpsi_M-3096.90)<"+value_jpsi);
  //TString BsCut(PhiCut+ " && " + JpsiCut);
  tr->Draw("Bs_M>>h3(190,5000,5950)", BsCut);
  auto h3 = (TH1D*)gPad->GetPrimitive("h3");
  h3->SetTitle(";m(K^{+}K^{-}#mu^{+}#mu^{-}) [MeV/c^{2}];Entries / 5 MeV/c^{2}");

  // Bc mass
  TString BcCut("&& Bc_FD_OWNPV>1 && Bc_ENDVERTEX_CHI2<4");
  tr->Draw("Bc_M>>h4(100, 5700, 6700)", BsCut + " && fabs(Bs_M-5366.77)<44.5" + BcCut);
  auto h4 = (TH1D*)gPad->GetPrimitive("h4");
  h4->SetTitle(";m(K^{+}K^{-}#mu^{+}#mu^{-}#pi^{#pm}) [MeV/c^{2}];Entries / 10 MeV/c^{2}");

  // DRAW
  TCanvas* c1 = new TCanvas();
  h1->Draw();

  TCanvas* c2 = new TCanvas();
  h2->Draw();

  TCanvas* c3 = new TCanvas();
  h3->Draw();  

  TCanvas* c4 = new TCanvas();
  h4->Draw();

}
