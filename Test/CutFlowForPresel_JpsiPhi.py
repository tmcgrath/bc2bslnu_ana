#!/usr/bin/env python
# coding: utf-8

# In[1]:

import sys
from files_JpsiPhi_2015_MU import input_files as infiles15
import ROOT as R
R.ROOT.EnableImplicitMT()

print("Applying preselection cuts")

names = R.std.vector('string')()
for n in infiles15:
    names.push_back(n)

rdf = R.RDataFrame("Bc2BsPi_Bs2JpsiPhi/DecayTree",names)\
.Filter("Mu1_PT>500", "mu1_pt")\
.Filter("Mu2_PT>500", "mu2_pt")\
.Filter("Mu1_P>5000", "mu1_p")\
.Filter("Mu2_P>5000", "mu2_p")\
.Filter("Mu1_TRACK_CHI2NDOF<3", "mu1_track_chi2")\
.Filter("Mu2_TRACK_CHI2NDOF<3", "mu2_track_chi2")\
.Filter("Mu1_PIDmu>0", "mu1_pidmu")\
.Filter("Mu2_PIDmu>0", "mu2_pidmu")\
.Filter("Jpsi_ENDVERTEX_CHI2<9", "jpsi_vertex_chi2")\
.Filter("K1_PT>300", "k1_pt")\
.Filter("K2_PT>300", "k2_pt")\
.Filter("K1_P>4000", "k1_p")\
.Filter("K2_P>4000", "k2_p")\
.Filter("K1_TRACK_CHI2NDOF<3", "k1_track_chi2")\
.Filter("K2_TRACK_CHI2NDOF<3", "k2_track_chi2")\
.Filter("K1_PIDK>-2", "k1_pidk")\
.Filter("K2_PIDK>-2", "k2_pidk")\
.Filter("K1_MC15TuneV1_ProbNNghost<0.4", "k1_ghostprob")\
.Filter("K2_MC15TuneV1_ProbNNghost<0.4", "k2_ghostprob")\
.Filter("Phi_ENDVERTEX_CHI2<10", "phi_vertex_chi2")\
.Filter("Phi_PT>1000", "phi_pt")\
.Filter("Bs_ENDVERTEX_CHI2<10","bs_vertex_chi2")\
.Filter("Mu_bach_MC15TuneV1_ProbNNghost<0.3",'pi_from_bc_ghostprob')\
.Filter("Mu_bach_PT>500","pi_from_bc_pt")\
.Filter("Mu_bach_P>5000","pi_from_bc_p")\
.Filter("Mu_bach_TRACK_CHI2NDOF<3","pi_from_bc_track_chi2")\
.Filter("Bc_M>5e3","bc_mass")\
.Filter("Bc_ENDVERTEX_CHI2<10","bc_vertex_chi2")


#rdf.Snapshot("DecayTree","ReducedFile_JpsiPhi_control_15MU.root")

allCutsReport = rdf.Report()
allCutsReport.Print()

