#
# draw_variables.py
#
# Script to draw some of the branches included in Bc->Bsmunu and Bc->Bspi data sample
#
# Tamaki Holly McGrath
# 15.09.20
#

import ROOT as R
RDF = R.ROOT.RDataFrame

#from files_from_holly import input_files

############################ Functions ####################################

def drawHist(rdf, variable, nBins, xmin, xmax, filename):
    histo = rdf.Histo1D((variable, variable, nBins, xmin, xmax), variable)
    histo.SetTitle(variable)
    canv = R.TCanvas()
    canv.cd()
    histo.Draw()
    canv.Update()
    canv.SaveAs(f"plots/{filename}")

###########################################################################

#names = R.std.vector('string')()
#for n in input_files:
#    names.push_back(n)

#name = 'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/user/t/tmcgrath/2020_07/385621/385621533/Bc_Tuples_DsPi.root'
#name = 'ReducedFile_control_15+16MU.root'
name = 'root://hake5.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/user/t/tmcgrath/2020_07/385630/385630687/Bc_Tuples_JpsiPhi.root'


# Set up RDFs
#rdf_bsmunu = RDF("Bc2BsMu_Bs2DsPi/DecayTree", name)
#rdf_bspi = RDF("Bc2BsPi_Bs2DsPi/DecayTree", name)
#rdf_bspi = RDF("DecayTree", name)
rdf_bspi = RDF("Bc2BsPi_Bs2JpsiPhi/DecayTree", name)

region = "jpsiphi_15MU"

if region == "peak":
    ds_cut = "fabs(Ds_M-1968.34)<(3*8.85)"
    bs_cut = "fabs(Bs_M-5366.77)<(2*20.83)"
    combcut = f"{ds_cut} && {bs_cut}"
elif region == "sideband":
    ds_cut = "fabs(Ds_M-1968.34)>(3*8.85)"
    bs_cut = "fabs(Bs_M-5366.77)>(2*20.83)"
    combcut = f"{ds_cut} || {bs_cut}"
else:
    combcut = ""


bsmunu = f"Bsmunu_{region}"
bspi = f"BsPi_{region}"

#bspi = "BsPi"

# Cuts
#rdf_bspi.Filter("fabs(Ds_M-1968.34)<(3*8.85) && fabs(Bs_M-5366.77)<(2*20.83)")
#rdf_bspi = rdf_bspi.Filter(combcut)
# Prompt
#region = "prompt"
#rdf_bspi = rdf_bspi.Filter("Bs_IP_OWNPV < 0.1")

# Histograms
drawHist(rdf_bspi, "K1_PT", 100, 0, 5000, f"{region}/{bspi}_K1_PT.png")
drawHist(rdf_bspi, "K2_PT", 100, 0, 5000, f"{region}/{bspi}_K2_PT.png")
#drawHist(rdf_bspi, "Pi_PT", 100, 0, 10000, f"{region}/{bspi}_Pi_PT.png")
drawHist(rdf_bspi, "K1_P", 100, 0, 80000, f"{region}/{bspi}_K1_P.png")
drawHist(rdf_bspi, "K2_P", 100, 0, 80000, f"{region}/{bspi}_K2_P.png")
#drawHist(rdf_bspi, "Pi_P", 100, 0, 80000, f"{region}/{bspi}_Pi_P.png")
drawHist(rdf_bspi, "K1_PIDK", 100, -20, 30, f"{region}/{bspi}_K1_PIDK.png")
#drawHist(rdf_bspi, "Pi_PIDK", 100, -20, 20, f"{bspi}_Pi_PIDK.png")
#drawHist(rdf_bspi, "K1_ProbNNk", 100, 0, 1, f"{region}/{bspi}_K1_ProbNNk.png")
#drawHist(rdf_bspi, "Pi_ProbNNpi", 100, 0, 1, f"{region}/{bspi}_Pi_ProbNNpi.png")
#drawHist(rdf_bspi, "K2_ProbNNk", 100, 0, 1, f"{region}/{bspi}_K2_ProbNNk.png")

#drawHist(rdf_bspi, "Ds_DIRA_OWNPV", 100, 0, 1, f"{region}/{bspi}_Ds_DIRA_OWNPV.png")
#drawHist(rdf_bspi, "Ds_ENDVERTEX_CHI2", 100, 0, 20, f"{region}/{bspi}_Ds_ENDVERTEX_CHI2.png")
#drawHist(rdf_bspi, "Ds_PT", 100, 0, 20000, f"{region}/{bspi}_Ds_PT.png")
#drawHist(rdf_bspi, "Ds_P", 100, 0, 100000, f"{region}/{bspi}_Ds_P.png")

drawHist(rdf_bspi, "Mu1_PT", 100, 0, 10000, f"{region}/{bspi}_Mu1_PT.png")
drawHist(rdf_bspi, "Mu1_P", 100, 0, 100000, f"{region}/{bspi}_Mu1_P.png")
drawHist(rdf_bspi, "Mu2_PT", 100, 0, 10000, f"{region}/{bspi}_Mu2_PT.png")
drawHist(rdf_bspi, "Mu2_P", 100, 0, 100000, f"{region}/{bspi}_Mu2_P.png")
drawHist(rdf_bspi, "Mu1_PIDmu", 100, -20, 20, f"{region}/{bspi}_Mu1_PIDmu.png")
drawHist(rdf_bspi, "Jpsi_ENDVERTEX_CHI2", 100, 0, 20, f"{region}/{bspi}_Jpsi_ENDVERTEX_CHI2.png")
drawHist(rdf_bspi, "Phi_ENDVERTEX_CHI2", 100, 0, 20, f"{region}/{bspi}_Phi_ENDVERTEX_CHI2.png")

#drawHist(rdf_bspi, "Bs_ENDVERTEX_CHI2", 100, 0, 20, f"{region}/{bspi}_Bs_ENDVERTEX_CHI2.png")
#drawHist(rdf_bspi, "Pi_from_Bs_IP_OWNPV", 100, 0, 10, f"{region}/{bspi}_Pi_from_Bs_IP_OWNPV.png")
#drawHist(rdf_bspi, "Pi_from_Bs_PT", 100, 0, 10000, f"{region}/{bspi}_Pi_from_Bs_PT.png")
#drawHist(rdf_bspi, "Pi_from_Bs_P", 100, 0, 100000, f"{region}/{bspi}_Pi_from_Bs_P.png")
#drawHist(rdf_bspi, "Pi_from_Bs_MC15TuneV1_ProbNNpi", 100, 0, 1, f"{region}/{bspi}_Pi_from_Bs_MC15TuneV1_ProbNNpi.png")
#drawHist(rdf_bspi, "Bs_PT", 100, 0, 20000, f"{region}/{bspi}_Bs_PT.png")
#drawHist(rdf_bspi, "Bs_P", 100, 0, 100000, f"{region}/{bspi}_Bs_P.png")

#drawHist(rdf_bsmunu, "Mu_bach_PIDmu", 100, -20, 20, f"{bsmunu}_Mu_bach_PIDmu.png")
#drawHist(rdf_bsmunu, "Mu_bach_ProbNNmu", 100, 0, 1, f"{bsmunu}_Mu_bach_ProbNNmu.png")
#drawHist(rdf_bsmunu, "Mu_bach_PT", 100, 0, 10000, f"{bsmunu}_Mu_bach_PT.png")
#drawHist(rdf_bspi, "Mu_bach_PT", 100, 0, 10000, f"{region}/{bspi}_Mu_bach_PT.png")
#drawHist(rdf_bsmunu, "Mu_bach_P", 100, 0, 100000, f"{bsmunu}_Mu_bach_P.png")
#drawHist(rdf_bspi, "Mu_bach_P", 100, 0, 100000, f"{region}/{bspi}_Mu_bach_P.png")
#drawHist(rdf_bspi, "Bc_ENDVERTEX_CHI2", 100, 0, 15, f"{region}/{bspi}_Bc_ENDVERTEX_CHI2.png")
#drawHist(rdf_bspi, "Bc_FD_OWNPV", 100, 0, 20, f"{region}/{bspi}_Bc_FD_OWNPV.png")
#drawHist(rdf_bspi, "Bc_FDCHI2_OWNPV", 100, 0, 20, f"{region}/{bspi}_Bc_FDCHI2_OWNPV.png")
#drawHist(rdf_bspi, "Mu_bach_IP_OWNPV", 100, 0, 5, f"{region}/{bspi}_Mu_bach_IP_OWNPV.png")
#drawHist(rdf_bsmunu, "Mu_bach_IP_OWNPV", 100, 0, 10, f"{bsmunu}_Mu_bach_IP_OWNPV.png")
#drawHist(rdf_bspi, "Mu_bach_IPCHI2_OWNPV", 100, 0, 20, f"{bspi}_Mu_bach_IPCHI2_OWNPV.png")
#drawHist(rdf_bsmunu, "Mu_bach_IPCHI2_OWNPV", 100, 0, 20, f"{bsmunu}_Mu_bach_IPCHI2_OWNPV.png")
#drawHist(rdf_bspi, "Bc_DIRA_OWNPV", 100, 0, 1, f"{region}/{bspi}_Bc_DIRA_OWNPV.png")
#drawHist(rdf_bspi, "Bc_PT", 100, 0, 1e5, f"{region}/{bspi}_Bc_PT.png")
#drawHist(rdf_bspi, "Bc_P", 100, 0, 1e6, f"{region}/{bspi}_Bc_P.png")



