# 
# Rmue_plot.py
#
# Script to plot the recent Rmue results
#

import math
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
small_size = 12
medium_size = 14
rc('font',**{'family':'serif','serif':['Roman'], 'size': medium_size})
rc('lines',**{'markeredgewidth': 1})
#rc('text', usetex=True)


#####################################
# Functions
#####################################

def calcRootMeanSq(list_of_errors):
    '''
    Inputs: list/tuple = (error1, error2, ...)
    Output: float = sqrt(error1^2 + error2^2 + ...) 
    '''
    sqsum = 0
    for entry in list_of_errors:
        sqsum += math.pow(entry, 2)
    rootMeanSq = math.sqrt( sqsum )
    return rootMeanSq

def calcRatio(tuple_num, tuple_den):
    '''
    Inputs: tuple = (branching fraction, error)
    Output: tuple = (Ratio, error)
    '''
    ratio = tuple_num[0] / tuple_den[0]
    ratio_error = ratio * math.sqrt( math.pow( (tuple_num[1] / tuple_num[0]), 2 ) + math.pow( (tuple_den[1] / tuple_den[0]), 2 ) )

    ratio_tuple = (ratio, ratio_error)
    return ratio_tuple

#####################################
# Gather info
#####################################

'''
Need branching fraction measurements and Rmue values
'''

# Rmue theory from:
Rmue_theory    = (0.9779, 0.0002, 0.0050) # https://arxiv.org/abs/2104.09883
Rmue_pi_theory = (0.985, 0.002)          # https://link.springer.com/article/10.1140/epjc/s10052-018-5943-5

# Rmue thoery to use
Rmue_theory = (Rmue_theory[0], calcRootMeanSq([err for i, err in enumerate(Rmue_theory) if i != 0]))

# Rmue exp from:
Rmue_exp    = (0.974, 0.007, 0.012) # https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.122.011804
Rmue_pi_exp = (0.922, 0.030, 0.022) # https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.121.171803

# Branching fractions from: https://pdg.lbl.gov/2023/listings/rpp2023-list-D-zero.pdf
D2Kenu    = (3.549, 0.026)
D2Kmunu   = (3.41, 0.04)
D2pienu   = (2.91, 0.04)
D2pimunu  = (2.67, 0.12)
D2Kstenu  = (2.15, 0.16)
D2Kstmunu = (1.89, 0.24)

# Calculated Rmues
Rmue_BR     = calcRatio(D2Kmunu, D2Kenu)
Rmue_pi_BR  = calcRatio(D2pimunu, D2pienu)
Rmue_Kst_BR = calcRatio(D2Kstmunu, D2Kstenu)

print(Rmue_BR)
print(Rmue_pi_BR)
print(Rmue_Kst_BR)

#####################################
# Main plotting
#####################################

## Put all info into plottable format
# Plot info
fig, ax = plt.subplots(layout="constrained")
ratio_names = (r'$\frac{B(D^0 \to \pi^- \mu^+ \nu_\mu)}{B(D^0 \to \pi^- e^+ \nu_e)}$', r'$\frac{B(D^0 \to K^{*-}(892) \mu^+ \nu_\mu)}{B(D^0 \to K^{*-}(892) e^+ \nu_e)}$', r'$\frac{B(D^0 \to K^- \mu^+ \nu_\mu)}{B(D^0 \to K^- e^+ \nu_e)}$')
y_for_ticks = np.arange(len(ratio_names))
ax.set_xlabel(r"$R_{\mu/e}$")
ax.set_xlim([0.7, 1.3])

# Plot Rmue error
RmueK_th = ax.axvline(x = Rmue_theory[0], color = 'royalblue', label = r'$\frac{B(D^0 \to K^- \mu^+ \nu_\mu)}{B(D^0 \to K^- e^+ \nu_e)}$ SM')
RmueK_err_th = ax.axvspan(Rmue_theory[0]-Rmue_theory[1], Rmue_theory[0]+Rmue_theory[1], facecolor='royalblue', alpha=0.4)

# Plot BR Rmue
y_BR = np.array([-0.1, 0.9, 1.9])
x_BR = np.array([Rmue_pi_BR[0], Rmue_Kst_BR[0], Rmue_BR[0]])
#x = x_array.copy(); x[x_array == 0] = np.nan
x_err_BR = np.array([Rmue_pi_BR[1], Rmue_Kst_BR[1], Rmue_BR[1]])
#x_err = x_err_array.copy(); x_err[x_err_array == 0] = np.nan
BR_plot = ax.errorbar(x_BR, y_BR, xerr=x_err_BR, capsize=3, color='black', marker='o', ms='3', linestyle='', label="PDG 2023")

# Plot D2Klnu Rmue exp
y_exp = np.array([0.1, 1.1, 2.1])
x_exp = np.array([np.nan, np.nan, Rmue_exp[0]])
x_err_exp = np.array([np.nan, np.nan, Rmue_exp[1]])
RmueK = ax.errorbar(x_exp, y_exp, xerr=x_err_exp, capsize=3, color='tab:orange', marker='o', ms='3', linestyle='', label="PRL 122(2019) 011804")

# Plot D2pilnu Rmue exp 
x_pi_exp = np.array([Rmue_pi_exp[0], np.nan, np.nan])
x_err_pi_exp = np.array([Rmue_pi_exp[1], np.nan, np.nan])
Rmuepi = ax.errorbar(x_pi_exp, y_exp, xerr=x_err_pi_exp, color='tab:green', capsize=3, marker='o', ms='3', linestyle='', label="PRL 121(2018) 171803")

ax.set_yticks(y_for_ticks, labels=ratio_names)
#charts = RmueK + Rmuepi + BR_plot
#labels = [l.get_label() for l in charts]
ax.legend(loc="upper right", fontsize='x-small')
plt.savefig("Rmue_plot_Jan_24_original.pdf")
plt.savefig("Rmue_plot_Jan_24_original.png")
plt.show()
