#
# calcEffs.py
#
# Script to calculate all efficiencies (except for generator level) for all channels and Bs modes
# Separate text file output for Bs2DsPi and Bs2JpsiPhi
# 
# e_tot = e_gen * e_recosplit * e_trigger * e_selection * e_
#

import sys, os, glob
import pandas as pd
import numpy as np
import math
import uproot as ur

############################
# Global Variables         #
############################

# Channels to loop over
Bs_channels = ["DsPi", "JpsiPhi"]
Bc_channels = ["Bc2BsMuNu", "Bc2BsPi", "Bc2BsstMuNu", "Bc2BsstPi", "Bc2BsRho", "Bc2BsPi_misID"]
Bc_channels_jpsiphi = ["Bc2BsMuNu", "Bc2BsPi", "Bc2BsstMuNu", "Bc2BsstPi", "Bc2BsRho"]
redecay_channels = ["Bc2BsstMuNu", "Bc2BsRho"]
#Bc_channels = ["Bc2BsMuNu"] # for testing

sel_category = ["RecoStrip", "Trigger", "Sel", "BDT"]
years = [2016, 2017, 2018]

## Truth matching strings

TM_strings_DsPi = {
    "Bc2BsMuNu" : "Bc_is_from_Bc_BsMuNu == 1",
    "Bc2BsPi" : "Bc_is_from_Bc_BsPi == 1",
    "Bc2BsstMuNu" : "Bc_is_from_Bc_BsstarMuNu == 1",
    "Bc2BsstPi" : "Bc_is_from_Bc_BsstarPi == 1",
    "Bc2BsRho" : "Bc_is_from_Bc_BsRho == 1",
    "Bc2BsPi_misID": "Bc_is_from_Bc_BsPi == 1"
}
TM_strings_JpsiPhi = {
    "Bc2BsMuNu" : "Bc_is_from_Bc_BsMuNu == 1",
    "Bc2BsPi" : "Bc_is_from_Bc_BsPi == 1",
    "Bc2BsstMuNu" : "Bc_is_from_Bc_BsstMuNu == 1",
    "Bc2BsstPi" : "Bc_is_from_Bc_BsstPi == 1",
    "Bc2BsRho" : "Bc_is_from_Bc_BsRho == 1",
    "Bc2BsPi_misID": "Bc_is_from_Bc_BsPi == 1"
}


## Filenames
base_sel_file = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Preselection/condor/logs/condor_{4}_presel_{0}_{1}.{2}.{3}*.out"
base_bdt_file = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCBDTApplied/{0}_Bs2{1}_All_*_BDT.root"

## Generated numers (requires input from Dirac so done by hand)
gen_nums_dspi = {
    "Bc2BsMuNu"     : {"2016_MU": 250163, "2016_MD": 255263, "2017_MU": 304721, "2017_MD": 302914, "2018_MU": 409387,"2018_MD": 405056},
    "Bc2BsPi"       : {"2016_MU": 158910, "2016_MD": 152387, "2017_MU": 157889, "2017_MD": 159185, "2018_MU": 185468,"2018_MD": 176088},
    "Bc2BsPi_misID" : {"2016_MU": 1001424, "2016_MD": 1007202, "2017_MU": 1096389, "2017_MD": 1001734, "2018_MU": 1001799,"2018_MD": 1006117},
    "Bc2BsstMuNu"   : {"2016_MU": 39532, "2016_MD": 38273, "2017_MU": 41789, "2017_MD": 41067, "2018_MU": 46916,"2018_MD": 52089},
    "Bc2BsRho"      : {"2016_MU": 34499, "2016_MD": 29123, "2017_MU": 35474, "2017_MD": 31239, "2018_MU": 37993,"2018_MD": 44412},
    "Bc2BsstPi"     : {"2017_MU": 1003653, "2017_MD": 1002683}
}
recostrip_nums_dspi = {
    "Bc2BsstMuNu": {"2016_MU": 1939989, "2016_MD": 1956639, "2017_MU": 2191819, "2017_MD": 2266429, "2018_MU": 2331075,"2018_MD": 2351317},
    "Bc2BsRho"   : {"2016_MU": 2526715, "2016_MD": 2592309, "2017_MU": 3888825, "2017_MD": 4016306, "2018_MU": 4543054,"2018_MD": 4646941},
}

gen_nums_jpsiphi = {
    "Bc2BsMuNu"  : {"2016_MU": 259096, "2016_MD": 300331, "2017_MU": 300804, "2017_MD": 301998, "2018_MU": 402584,"2018_MD": 403958},
    "Bc2BsPi"    : {"2016_MU": 151517, "2016_MD": 151690, "2017_MU": 156365, "2017_MD": 152711, "2018_MU": 179812,"2018_MD": 175542},
    "Bc2BsstMuNu": {"2016_MU": 40111, "2016_MD": 39272, "2017_MU": 42327, "2017_MD": 47181, "2018_MU": 50936,"2018_MD": 55450},
    "Bc2BsRho"   : {"2016_MU": 34282, "2016_MD": 36980, "2017_MU": 39583, "2017_MD": 39788, "2018_MU": 39602,"2018_MD": 44571},
    "Bc2BsstPi"  : {"2017_MU": 1125069, "2017_MD": 1000266}
}
recostrip_nums_jpsiphi_redecay = {
    "Bc2BsstMuNu": {"2016_MU": 1270035, "2016_MD": 1254213, "2017_MU": 1521445, "2017_MD": 1525509, "2018_MU": 2267544,"2018_MD": 2273295},
    "Bc2BsRho"   : {"2016_MU": 2572248, "2016_MD": 2542834, "2017_MU": 3405840, "2017_MD": 3439655, "2018_MU": 6820344,"2018_MD": 6722753},
}

############################
# Little functions for aid #
############################

sys.path.append("/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/Fit")
from BDT_cuts import BDT_cuts_norm, BDT_cuts_sig

## Functions for getting numbers before and after each selection stage

## Get file (for catching combined files)
def getFile(file_string):
    print(file_string)
    files = glob.glob(file_string)
    if len(files) != 1:
        print("Filestring returns multiple files:")
        print(files)
    return files[0]

# Get pandas dataframe from root file
def get_df(filename, tree="DecayTree"):
    filenameplus = f"{filename}:{tree}"
    tree = ur.open(filenameplus)
    branches = tree.keys()
    theDF = tree.arrays(branches, library="pd")

    return theDF

## Apply cuts
def applyTruthMatch(df, Bcchan, Bschan):
    if Bschan == "DsPi":
        TM_Bs = "Bs_is_from_Bs_DsPi == 1"
        TM_strings = TM_strings_DsPi
    else:
        TM_Bs = "Bs_is_from_Bs_JpsiPhi == 1"
        TM_strings = TM_strings_JpsiPhi
    TM = TM_Bs + " & " + TM_strings[Bcchan]

    filtered_df = df.query(TM)
    return filtered_df

def applyBDT_DsPi(df, bdt_dict,  year="All"):
    year = str(year)
    bdt_cuts = bdt_dict["DsPi"][year]
    cuts = f"Bs_MM > 5300 & Bs_MM < 5420 & {bdt_cuts}"
    filtered_df = df.query(cuts)
    print(f"Applying BDT cuts: {cuts}")
    return filtered_df

def applyBDT_JpsiPhi(df, bdt_dict, year="All"):
    year = str(year)
    cuts = bdt_dict["JpsiPhi"][year]
    filtered_df = df.query(cuts)
    print(f"Applying BDT cuts: {cuts}")
    return filtered_df

# Calculate a new column of efficiencies where the number in array1 (i.e. the sel stage after) is smaller than the one in array2 
def calcEff(array1, array2):

    #print(array1, array2)

    eff = np.divide(array1, array2)

    # error on efficiency BINOMIAL ERRORS!!
    numerator = np.multiply(eff, 1-eff)
    err = 100.0 * np.sqrt(np.divide(numerator, array2))

    #frac1 = np.divide( np.sqrt(array1), array1 )
    #frac2 = np.divide( np.sqrt(array2), array2 )
    #err = np.multiply( eff, np.sqrt( np.power(frac1, 2) + np.power(frac2, 2) ) ) 

    # return array of efficiency and error after calculationg
    eff = eff * 100
    return eff, err

# Caluclate efficiencies for whole numbers rather than arrays
def calcEffVal(after, before):
    before = float(before); after = float(after)

    eff = after / before
    err = math.sqrt(eff * (1-eff) / before) * 100.0
    
    eff = eff * 100
    return eff, err


## Get numbers from text file - need to do for each magpol and year
def getNumsFromSelFile(file, chan):
    numbs_each_stage = []
    print(file)
    with open(file) as f:
        for line in f.readlines():
            
            # Get number after reconstruction and after trigger
            if "L0        :" in line:
                if chan == "JpsiPhi":
                    print("Got to L0 bit")
                    after_reco = float( (line.split("all=")[1]).split("-- eff=")[0] )
                    numbs_each_stage.append(after_reco)
                    
            if "HLT2      :" in line:
                if chan == "DsPi": 
                    after_reco = float( (line.split("all=")[1]).split("-- eff=")[0] )
                    numbs_each_stage.append(after_reco)
                numbs_each_stage.append(0) # ReDecay column
                after_trigger = float( (line.split("pass=")[1]).split("all=")[0] )
                numbs_each_stage.append(after_trigger)

            # Get trigger
            # Get selection numbers
            if "Bach_isMuon ==" in line:
                after_sel = float((line.split("pass=")[1]).split("all=")[0])
                numbs_each_stage.append(after_sel)
    
    return numbs_each_stage

# For redecay channels
def getNumsFromSelFileForReDecay(MCfile, ReDecayfile, chan):
    numbs_each_stage = []

    with open(MCfile) as f:
        for line in f.readlines():
            if "L0        :" in line:
                if chan == "JpsiPhi":
                    print("Got to L0 bit")
                    after_reco = float( (line.split("all=")[1]).split("-- eff=")[0] )
                    numbs_each_stage.append(after_reco)
                    
            if "HLT2      :" in line:
                if chan == "DsPi": 
                    after_reco = float( (line.split("all=")[1]).split("-- eff=")[0] )
                    numbs_each_stage.append(after_reco)

    with open(ReDecayfile) as f:
        for line in f.readlines():
            
            # Get number after reconstruction and after trigger
            if "L0        :" in line:
                if chan == "JpsiPhi":
                    print("Got to L0 bit")
                    after_reco = float( (line.split("all=")[1]).split("-- eff=")[0] )
                    numbs_each_stage.append(after_reco)
                    
            if "HLT2      :" in line:
                if chan == "DsPi": 
                    after_reco = float( (line.split("all=")[1]).split("-- eff=")[0] )
                    numbs_each_stage.append(after_reco)

            # Get trigger
            if "HLT2      :" in line: 
                after_trigger = float( (line.split("pass=")[1]).split("all=")[0] )
                numbs_each_stage.append(after_trigger)

            # Get selection numbers
            if "Bach_isMuon ==" in line:
                after_sel = float((line.split("pass=")[1]).split("all=")[0])
                numbs_each_stage.append(after_sel)

    return numbs_each_stage

def getBDTCutNumbers(df_of_root, Bcchan, Bschan):

    df_to_study = df_of_root.copy()

    check_nums = len(df_to_study.index)
    df_to_study = applyTruthMatch(df_to_study, Bcchan, Bschan)
    before_BDT = len(df_to_study.index)
    if check_nums <= before_BDT:
        print("Check Truth Matching!")
        sys.exit()

    if "Mu" in Bcchan:
        BDT_cuts = BDT_cuts_sig
    else:
        BDT_cuts = BDT_cuts_norm

    if Bschan == "DsPi":
        df_to_study = applyBDT_DsPi(df_to_study, BDT_cuts)
    elif Bschan == "JpsiPhi":
        df_to_study = applyBDT_JpsiPhi(df_to_study, BDT_cuts)

    after_BDT = len(df_to_study.index)

    return after_BDT

## Get numbers from text file and add as column to df
def addNumbsToDF(df, columns, bc, bs, gen_nums):

    all_years = []

    for year in years:

        if bc == "Bc2BsstPi" and year != 2017:
            all_years.append([0, 0, 0, 0, 0])
            continue
        elif bc == "Bc2BsstMuNu" or bc == "Bc2BsRho":
            if year == 2016:
                all_years.append([0, 0, 0, 0, 0])
                continue

        MU_file = getFile(base_sel_file.format(bc, bs, year, "MU", "MC"))
        MD_file = getFile(base_sel_file.format(bc, bs, year, "MD", "MC"))
        #print(MU_file)
        #print(MD_file)

        MU_numbs = [float(gen_nums[bc][f"{year}_MU"])]
        MD_numbs = [float(gen_nums[bc][f"{year}_MD"])]

        if bc == "Bc2BsstMuNu" or bc == "Bc2BsRho":
            MU_file_redecay = getFile(base_sel_file.format(bc, bs, year, "MU", "ReDecay"))
            MD_file_redecay = getFile(base_sel_file.format(bc, bs, year, "MD", "ReDecay"))
            MU_numbs += getNumsFromSelFileForReDecay(MU_file, MU_file_redecay, bs)
            MD_numbs += getNumsFromSelFileForReDecay(MD_file, MD_file_redecay, bs)
        else:
            MU_numbs += getNumsFromSelFile(MU_file, bs)
            MD_numbs += getNumsFromSelFile(MD_file, bs)

        MU = np.array(MU_numbs); MD = np.array(MD_numbs)
        total = MU + MD # add arrays
        #print(total)

        all_years.append(total)

    # Get BDT numbers
    modified_bc = "Bc2BsMu" if bc == "Bc2BsMuNu" else bc
    BDT_file = getFile(base_bdt_file.format(modified_bc, bs))
    BDT_df = get_df(BDT_file)
    after_BDT = getBDTCutNumbers(BDT_df, bc, bs)
    after_BDT_array = np.array([float(after_BDT)])
    all_years.append(after_BDT_array)
    #print(all_years)

    total_row = np.concatenate(all_years, axis=None)

    print(columns)
    print(total_row)
    # Check that number of columns match with number of elements
    if len(total_row) != len(columns):
        print(f"Something wrong! len(columns) = {len(columns)}, len(total_rows) + 1 = {len(total_row)+1}")
        sys.exit()
    # Make dictionary of row
    new_row = {}
    for index, column in enumerate(columns):
        new_row[column] = total_row[index]
    # Add new row to df
    print(f"Adding row for {bc}")
    df.loc[bc] = new_row


def calculateEffs(df):
    copy_df = df.copy()

    # First combine nums for all years
    copy_df.loc[:, "N_Gen"] = copy_df.loc[:, 'N_Gen_2016'] + copy_df.loc[:, 'N_Gen_2017'] + copy_df.loc[:, 'N_Gen_2018']
    copy_df.loc[:, "N_RecoStrip"] = copy_df.loc[:, 'N_RecoStrip_2016'] + copy_df.loc[:, 'N_RecoStrip_2017'] + copy_df.loc[:, 'N_RecoStrip_2018']
    copy_df.loc[:, "N_RecoStrip_ReDecay"] = copy_df.loc[:, 'N_RecoStrip_ReDecay_2016'] + copy_df.loc[:, 'N_RecoStrip_ReDecay_2017'] + copy_df.loc[:, 'N_RecoStrip_ReDecay_2018']
    copy_df.loc[:, "N_Trigger"] = copy_df.loc[:, 'N_Trigger_2016'] + copy_df.loc[:, 'N_Trigger_2017'] + copy_df.loc[:, 'N_Trigger_2018']
    copy_df.loc[:, "N_Sel"] = copy_df.loc[:, 'N_Sel_2016'] + copy_df.loc[:, 'N_Sel_2017'] + copy_df.loc[:, 'N_Sel_2018']

    for i, cat in enumerate(sel_category):
        if i == 0:
            denom = "Gen"
        else:
            denom = sel_category[i-1]
        print(f"Calculating eff_{cat} = N_{cat} / N_{denom}")
        copy_df.loc[:, f"eff_{cat}"], copy_df.loc[:, f"eff_err_{cat}"]  = calcEff( copy_df.loc[:, f"N_{cat}"], copy_df.loc[:, f"N_{denom}"] )

    # Change trigger eff for Bc2BsstMuNu to use the ReDecay number as denominator
    if "Bc2BsstMuNu" in copy_df.index:
        copy_df.loc["Bc2BsstMuNu", f"eff_Trigger"], copy_df.loc["Bc2BsstMuNu", f"eff_err_Trigger"] = calcEffVal( copy_df.loc["Bc2BsstMuNu", f'N_Trigger'], copy_df.loc["Bc2BsstMuNu", f'N_RecoStrip_ReDecay'] )
        copy_df.loc["Bc2BsRho", f"eff_Trigger"], copy_df.loc["Bc2BsRho", f"eff_err_Trigger"] = calcEffVal( copy_df.loc["Bc2BsRho", f'N_Trigger'], copy_df.loc["Bc2BsRho", f'N_RecoStrip_ReDecay'] )

    

    return copy_df

############################
# Main                     #
############################

if __name__ == "__main__":
    
    for bs in Bs_channels:
        print(f"Calculating efficiencies for Bs2{bs}\n!")

        # make dataframe with bc channels as column names
        column_names = []
        for year in years:
            column_names.append(f"N_Gen_{year}")
            column_names.append(f"N_RecoStrip_{year}")
            column_names.append(f"N_RecoStrip_ReDecay_{year}")
            column_names.append(f"N_Trigger_{year}")
            column_names.append(f"N_Sel_{year}")
        column_names.append(f"N_BDT")
        df = pd.DataFrame(columns = column_names)
        print(df)
        if bs == "DsPi":
            gen_nums_dict = gen_nums_dspi
            Bc_channels_to_iterate = Bc_channels
            
        else:
            gen_nums_dict = gen_nums_jpsiphi
            Bc_channels_to_iterate = Bc_channels_jpsiphi

        # Add a row of number for each Bc channel
        for bc in Bc_channels_to_iterate:
            
            addNumbsToDF(df, column_names, bc, bs, gen_nums_dict)

        # Calculate efficiencies once all rows added
        final_df = calculateEffs(df)
        print(final_df)
        #print(final_df[["N_RecoStrip_2018", "N_Gen_2017", "eff_RecoStrip_2017", "eff_err_RecoStrip_2017"]])
        #print(final_df[["N_Trigger_2017", "N_RecoStrip_2017", "eff_Trigger_2017", "eff_err_Trigger_2017"]])
        #print(final_df[["N_Sel_2016", "N_Trigger_2016", "eff_Sel_2016", "eff_err_Sel_2016"]])

        # drop the columns for specific years
        output_df = final_df.copy()
        columns_to_drop = []
        print("Dropping columns:")
        for column in list(output_df.columns):
            if column.split("_")[-1] == "2016" or column.split("_")[-1] == "2017" or column.split("_")[-1] == "2018":
                print(column)
                columns_to_drop.append(column)
        output_df = output_df.drop(columns=columns_to_drop)
        print("Saving:")
        print(output_df)

        # Then output efficiencies into a text file
        outfile = f"./Effs/Efficiencies_{bs}.txt"
        with open(outfile, "w") as outf:
            df_printout = output_df.to_string()
            outf.write(df_printout)

        

