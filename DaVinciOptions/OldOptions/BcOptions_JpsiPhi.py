##################################################
# Script to produce Bc-> Bs mu nu and Bc-> Bs pi
# with Bs -> Jpsi phi
# Different stripping, all other particle cuts
# are same as in Bc->Bspi stripping & presel
# Authors: Adam Davis, Tamaki Holly McGrath
##################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import FilterInTrees, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection, FilterSelection
from StandardParticles import StdLooseMuons, StdLooseKaons, StdLoosePions, StdLooseDetachedPhi2KK, StdAllNoPIDsMuons, StdAllNoPIDsPions
#tuple tools
from Configurables import DecayTreeTuple, MCDecayTreeTuple, PrintDecayTreeTool, CheckPV
from DecayTreeTuple.Configuration import *
from Configurables import (TupleToolDecay, TupleToolTISTOS,  TupleToolNeutrinoReco,
                           TupleToolDecayTreeFitter, TupleToolTrigger, MCTupleToolKinematic,
                           TriggerTisTos, TupleToolDecayTreeFitter, TupleToolConeIsolation,
                           LoKi__Hybrid__TupleTool)

from GaudiKernel.SystemOfUnits import MeV

##############################################################################
#make a dictionary of cuts so we don't have to go searching
##############################################################################

selection_dictionary = {
    'K'   : {'trackchi2' : '3'},
    'mu'  : {'mom_T': '500*MeV',
             'trackchi2' : '3'},
    'phi(1020)': {'mom_T' : '1000*MeV',
                  'vchi2ndf_cut': '16'},
    'J/psi(1S)': {'mass_window' : '80*MeV', 
                  'vchi2ndf_cut':'16'},
    'B_s0': {'vchi2ndf_cut': '10',
             'ltime': '0.000', 
             'm_low': '5000*MeV',
             'm_high': '6000*MeV'}, # ltime in ns so 0.2 ps
    'mu+' : {'mom_T': '350*MeV',
             'trackchi2': '3',
             'min_ipchi2': '1',
            },
    'B_c+': {'vchi2ndf_cut': '100'}
    }
selection_sequences = []
if DaVinci().Simulation == False:
    (selection_dictionary['K'])['pidk'] = '-2'
    (selection_dictionary['mu'])['pidmu'] = '0'

##############################################################################
# Selection sequences
##############################################################################

# K and muon code (from phi and Jpsi)
kaon_code = "(MAXTREE( 'K+'==ABSID, TRCHI2DOF ) < %(trackchi2)s) "%selection_dictionary['K']
muon_code = "(MAXTREE( 'mu+'==ABSID, TRCHI2DOF ) < %(trackchi2)s)"%selection_dictionary['mu']


#if DaVinci().Simulation == False:
    #kaon_code = kaon_code + " & (MINTREE( 'K+'==ABSID, PIDK ) > %(pidk)s)"%selection_dictionary['K']
    #muon_code = muon_code + " & (MINTREE( 'mu+'==ABSID, PIDmu ) > %(pidmu)s)"%selection_dictionary['mu']

phi_sel = FilterSelection("phi_sel",
                          #ConfigurableGenerators.FilterInTrees,
                          [StdLooseDetachedPhi2KK],
                          Code = "('phi(1020)'==ABSID) & %s "%kaon_code
)
phi_sel_seq = SelectionSequence("phi_sel_seq",TopSelection  = phi_sel)
selection_sequences.append(phi_sel_seq)

# Jpsi
if DaVinci().Simulation == False:
    jpsi_location = '/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles'
else:
    jpsi_location = '/Event/AllStreams/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles'

jpsi = AutomaticData(jpsi_location)    
jpsi_sel = FilterSelection("jpsi_sel",
                           #ConfigurableGenerators.FilterInTrees,
                           [jpsi],
                           Code = "('J/psi(1S)'==ABSID) & %s"%muon_code
)
jspi_sel_seq = SelectionSequence("jspi_sel_seq",TopSelection  = jpsi_sel)
selection_sequences.append(jspi_sel_seq)


#Make Bs candidates
bs2jpsiphi_sel = SimpleSelection("bs2jpsiphi_sel",
                                 ConfigurableGenerators.CombineParticles,
                                 [jpsi_sel, phi_sel],
                                 DecayDescriptor = '[B_s0 -> J/psi(1S) phi(1020)]cc',
                                 DaughtersCuts = {
                                     'J/psi(1S)' : "(ADMASS('J/psi(1S)') < %(mass_window)s ) & (VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)"%(selection_dictionary['J/psi(1S)']),
                                     'phi(1020)' : "(PT > %(mom_T)s) & (VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)"%(selection_dictionary['phi(1020)'])
                                 },
                                 CombinationCut = "(AM > %(m_low)s) & (%(m_high)s > AM)"%selection_dictionary['B_s0'],
                                 MotherCut = "(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s) & (BPVLTIME() > %(ltime)s)"%selection_dictionary['B_s0']
)
bs2jpsiphi_sel_seq = SelectionSequence("bs2jpsiphi_sel_seq",TopSelection  =bs2jpsiphi_sel)
selection_sequences.append(bs2jpsiphi_sel_seq)

# Make Bc->Bsmunu
bc2bsmunu_jpsiphi_sel = SimpleSelection('bc2bsmunu_jpsiphi_sel',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2jpsiphi_sel, StdAllNoPIDsMuons],
                                     DecayDescriptor = '[B_c+ -> B_s0 mu+]cc',
                                     DaughtersCuts = {"mu+": "('mu+'==ABSID) & (PT > %(mom_T)s) & (TRCHI2DOF < %(trackchi2)s) & (TRGHP<0.4) & (MIPCHI2DV(PRIMARY) > %(min_ipchi2)s)"%selection_dictionary['mu+']},
                                     MotherCut = '(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bsmunu_jpsiphi_sel_seq = SelectionSequence("bc2bsmunu_jpsiphi_sel_seq",TopSelection = bc2bsmunu_jpsiphi_sel)
selection_sequences.append(bc2bsmunu_jpsiphi_sel_seq)

#Make Bc->Bspi
bc2bspi_jpsiphi_sel = SimpleSelection('bc2bspi_jpsiphi_sel',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2jpsiphi_sel, StdAllNoPIDsPions],
                                     DecayDescriptor = '[B_c+ -> B_s0 pi+]cc',
                                     DaughtersCuts = {"pi+": "('pi+'==ABSID) & (PT > %(mom_T)s) & (TRCHI2DOF < %(trackchi2)s) & (TRGHP<0.4) & (MIPCHI2DV(PRIMARY) > %(min_ipchi2)s)"%selection_dictionary['mu+']},
                                     MotherCut = '(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bspi_jpsiphi_sel_seq = SelectionSequence("bc2bspi_jpsiphi_sel_seq",TopSelection = bc2bspi_jpsiphi_sel)
selection_sequences.append(bc2bspi_jpsiphi_sel_seq)
# All sequences appended

##############################################################################
# Decay Tree Tuples for data and MC
##############################################################################

# Signal
Bc2BsMu_Bs2JpsiPhi = DecayTreeTuple("Bc2BsMu_Bs2JpsiPhi",
                                     Inputs = [bc2bsmunu_jpsiphi_sel_seq.outputLocation()],
                                     Decay = "[B_c+ -> ^(B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K- ) ) ^mu+]CC")
Bc2BsMu_Bs2JpsiPhi.addBranches({
    'Bc':"^([B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC)",
    'Bs':'[B_c+ -> ^(B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Jpsi':'[B_c+ -> (B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Phi':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K- ) ) mu+]CC',
    'K1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K- ) ) mu+]CC',
    'K2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K- ) ) mu+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) ^mu+]CC'})    

# Norm
Bc2BsPi_Bs2JpsiPhi = DecayTreeTuple("Bc2BsPi_Bs2JpsiPhi",
                                    Inputs = [bc2bspi_jpsiphi_sel_seq.outputLocation()],
                                    Decay = "[B_c+ -> ^(B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K- ) ) ^pi+]CC")
    
Bc2BsPi_Bs2JpsiPhi.addBranches({
    'Bc':"^([B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC)",
    'Bs':'[B_c+ -> ^(B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Jpsi':'[B_c+ -> (B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Mu1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Mu2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Phi':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K- ) ) pi+]CC',
    'K1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K- ) ) pi+]CC',
    'K2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K- ) ) pi+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) ^pi+]CC'})

# MC decay tree tuples - added neutrino

if DaVinci().Simulation == True:
    #from Configurables import MCDecayTreeTuple
    # Signal
    MC_Bc2BsMu_Bs2JpsiPhi = MCDecayTreeTuple("MC_Bc2BsMu_Bs2JpsiPhi",
                                          Inputs = [bc2bsmunu_jpsiphi_sel_seq.outputLocation()],
                                          Decay = "[B_c+ => ^(B_s0 => ^(J/psi(1S) => ^mu+ ^mu-) ^(phi(1020) => ^K+ ^K- ) ) ^mu+ ^nu_mu]CC")
    MC_Bc2BsMu_Bs2JpsiPhi.addBranches({
        'Bc':"^([B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC)",
        'Bs':'[B_c+ => ^(B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Jpsi':'[B_c+ => (B_s0 => ^(J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Mu1':'[B_c+ => (B_s0 => (J/psi(1S) => ^mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Mu2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ ^mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Phi':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) ^(phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'K1':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => ^K+ K- ) ) mu+ nu_mu]CC',
        'K2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ ^K- ) ) mu+ nu_mu]CC',
        'Mu_bach':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^mu+ nu_mu]CC',
        'Nu_mu': '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ ^nu_mu]CC'})    

    # Norm
    MC_Bc2BsPi_Bs2JpsiPhi = MCDecayTreeTuple("MC_Bc2BsPi_Bs2JpsiPhi",
                                          Inputs = [bc2bspi_jpsiphi_sel_seq.outputLocation()],
                                          Decay = "[B_c+ => ^(B_s0 => ^(J/psi(1S) => ^mu+ ^mu-) ^(phi(1020) => ^K+ ^K- ) ) ^pi+]CC")

    MC_Bc2BsPi_Bs2JpsiPhi.addBranches({
        'Bc':"^([B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC)",
        'Bs':'[B_c+ => ^(B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Jpsi':'[B_c+ => (B_s0 => ^(J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Mu1':'[B_c+ => (B_s0 => (J/psi(1S) => ^mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Mu2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ ^mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Phi':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) ^(phi(1020) => K+ K- ) ) pi+]CC',
        'K1':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => ^K+ K- ) ) pi+]CC',
        'K2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ ^K- ) ) pi+]CC',
        'Mu_bach':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^pi+]CC'})

##############################################################################
# Configure the tools for the DTTs
##############################################################################

tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",
                 "TupleToolPropertime",
                 "TupleToolANNPID",
                 "TupleToolTrigger",
                 ]

mctupletoollist = ["MCTupleToolHierarchy",
                   "MCTupleToolReconstructed",
                   #"MCTupleToolKinematic",
                   #"TupleToolMCTruth",
                   #"TupleToolMCBackgroundInfo",
                   #"MCTupleToolAngles",
                   #"LoKi::Hybrid::MCTupleTool/LoKi_Photos",
                  ]


triglist = ['L0MuonDecision',
            'L0HadronDecision',
            'Hlt1SingleMuonNoIPDecision',
            'Hlt1TrackMVADecision',
            'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackAllL0Decision',
            'Hlt2SingleMuonDecision',
            'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision',
            'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision'
        ]

# cone isolation tuple tool configuration
from Configurables import TupleToolConeIsolation
neutral = TupleToolConeIsolation()
neutral.FillIsolation = True
neutral.FillComponents = True
neutral.FillAsymmetry = True
neutral.FillDeltas = True
neutral.FillCharged = False
neutral.FillNeutral = True
neutral.MaxPtParticlesLocation = "Phys/StdAllNoPIDsMuons/Particles"


from Configurables import MCMatchObjP2MCRelator
# Just MC
if DaVinci().Simulation == True:
    for mctuple in [MC_Bc2BsMu_Bs2JpsiPhi, MC_Bc2BsPi_Bs2JpsiPhi]:
        mctuple.ToolList+=mctupletoollist
        #mctuple.TupleToolMCTruth.ToolList = ["MCTupleToolKinematic","MCTupleToolReconstructed","MCTupleToolHierarchy","MCTupleToolDecayType","MCTupleToolPID", "MCTupleToolPrompt", "TupleToolMCTruth", ]
        mctuple.addTool(MCTupleToolKinematic())
        mctuple.MCTupleToolKinematic.Verbose = True
        nPhotons = mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_Photons")
        nPhotons.Preambulo+=["from LoKiPhysMC.decorators import *" ,"from LoKiPhysMC.functions import *"]
        nPhotons.Variables = {"nPhotons": "MCNINTREE(('gamma' == MCABSID)) "}
        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

for dttuple in [Bc2BsMu_Bs2JpsiPhi, Bc2BsPi_Bs2JpsiPhi]:
    dttuple.ToolList+=tupletoollist
    LoKi_Bc = dttuple.Bc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bc")
    LoKi_Bc.Preambulo+=["from LoKiPhysMC.decorators import *",
                        "from LoKiPhysMC.functions import mcMatch"]
    LoKi_Bc.Variables = {"BPVETA" : "BPVETA",
                         "PHI" : "PHI",
                         "KEY": "KEY",
                         "BPVCORRM" : "BPVCORRM",
                        }

    # fill cone isolation variables
    dttuple.Bc.ToolList += ["TupleToolConeIsolation"]
    dttuple.Bc.addTool(neutral)    

    if DaVinci().Simulation == True:
        MCTruth = dttuple.addTupleTool('TupleToolMCTruth')
        MCTruth.ToolList += ['MCTupleToolKinematic']
        MCTruth.ToolList += ['MCTupleToolHierarchy']
        MCTruth.addTool(MCMatchObjP2MCRelator)
        MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        dttuple.addTupleTool('TupleToolMCBackgroundInfo')

        # MC Match variable to Bc and Bs
        LoKi_Bc.Variables["is_from_Bc_BsMuNu"] = "switch( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsPi"] = "switch( mcMatch( '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC'  , 1  ) , 1 , 0 )"

        LoKi_Bs = dttuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bs")
        LoKi_Bs.Preambulo+=["from LoKiPhysMC.decorators import *",
                            "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bs.Variables = {'is_from_Bs_JpsiPhi' : "switch( mcMatch( '[B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- )]CC' , 1 ), 1, 0)"}

    LoKi_Mu1 = dttuple.Mu1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu1")
    LoKi_Mu1.Variables = {"NSHAREDMU":"NSHAREDMU"}
    LoKi_Mu2 = dttuple.Mu2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu2")
    LoKi_Mu2.Variables = {"NSHAREDMU":"NSHAREDMU"}

    if Bc2BsMu_Bs2JpsiPhi==dttuple:
        LoKi_MuBach = dttuple.Mu_bach.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuBach")
        LoKi_MuBach.Variables = {"NSHAREDMU":"NSHAREDMU"}
        ttnr = dttuple.Bc.addTupleTool(TupleToolNeutrinoReco,name='ttnr')
        ttnr.Verbose=True
        ttnr.MotherMass = 6274.47*MeV#LHCb-PAPER-2020-003
        #http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/LHCb-PAPER-2020-003.html

    ttdtf=dttuple.Bc.addTupleTool(TupleToolDecayTreeFitter("TTDTF"))
    ttdtf.Verbose = True
    ttdtf.constrainToOriginVertex =False
    ttdtf.daughtersToConstrain = ["J/psi(1S)",'phi(1020)','B_s0']

    ttttBc = dttuple.Bc.addTupleTool(TupleToolTISTOS,name="ttttBc")
    ttttBs = dttuple.Bs.addTupleTool(TupleToolTISTOS,name="ttttBs")
    ttttMu = dttuple.Mu_bach.addTupleTool(TupleToolTISTOS,name="ttttMu")
    for T in [ttttBc,ttttBs, ttttMu]:
                T.VerboseL0   = True
                T.VerboseHlt1 = True
                T.VerboseHlt2 = True
                T.TriggerList = triglist[:]

##############################################################################
# Add selection to GaudiSequencer
##############################################################################

from Configurables import GaudiSequencer
gaudiSelSeq = GaudiSequencer('gaudiSelSeq')
checkingPV = CheckPV("CheckForOnePV")
checkingPV.MinPVs = 1 # check there is at least 1pv
gaudiSelSeq.Members += [checkingPV]
gaudiSelSeq.Members += [s.sequence() for s in selection_sequences]

##############################################################################
# Configure DaVinci
##############################################################################

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

# DaVinci config not needed in AP
DaVinci().PrintFreq = 1000 # change this to 1000 for all evts
if DaVinci().Simulation == True:
    DaVinci().TupleFile = "BcTuples_MC_JpsiPhi.root"
else:
    DaVinci().TupleFile = "BcTuples_data_JpsiPhi.root"
DaVinci().EvtMax = -1
# Set Luminosity information when not using MC
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().InputType = 'DST'

#DaVinci().UserAlgorithms+=[s.sequence() for s in selection_sequences]
DaVinci().UserAlgorithms+=[gaudiSelSeq]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2JpsiPhi]
DaVinci().UserAlgorithms+=[Bc2BsPi_Bs2JpsiPhi]
if DaVinci().Simulation == True:
    DaVinci().UserAlgorithms+=[MC_Bc2BsMu_Bs2JpsiPhi]
    DaVinci().UserAlgorithms+=[MC_Bc2BsPi_Bs2JpsiPhi]



