# Event type 14545200, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles([#"root://x509up_u129802@se.cis.gov.pl//dpm/cis.gov.pl/home/lhcb/MC/2017/ALLSTREAMS.DST/00150532/0000/00150532_00000023_7.AllStreams.dst",
                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150532/0000/00150532_00000006_7.AllStreams.dst", 
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150532/0000/00150532_00000007_7.AllStreams.dst",
#                       "root://x509up_u129802@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150532/0000/00150532_00000001_7.AllStreams.dst",
#                       "root://x509up_u129802@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150532/0000/00150532_00000011_7.AllStreams.dst"
],clear=True)
