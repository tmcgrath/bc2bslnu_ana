#!/bin/bash

lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsK_DsPi.py BcOptions_BsK_DsPi.py 2>&1 | tee Tuples/BsK_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsK_JpsiPhi.py BcOptions_BsK_JpsiPhi.py 2>&1 | tee Tuples/BsK_JpsiPhi.log
