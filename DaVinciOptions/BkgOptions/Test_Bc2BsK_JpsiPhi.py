# Event Type: 14135001, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150524/0000/00150524_00000008_7.AllStreams.dst",
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150524/0000/00150524_00000007_7.AllStreams.dst",
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150524/0000/00150524_00000012_7.AllStreams.dst",
#                       "root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150524/0000/00150524_00000006_7.AllStreams.dst",
#                       "root://x509up_u129802@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150524/0000/00150524_00000002_7.AllStreams.dst",
],clear=True)
