#!/bin/bash

lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsRho_DsPi.py BcOptions_BsRho_DsPi.py 2>&1 | tee Tuples/BsRho_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsRho_JpsiPhi.py BcOptions_BsRho_JpsiPhi.py 2>&1 | tee Tuples/BsRho_JpsiPhi.log
