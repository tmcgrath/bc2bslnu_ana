# Event Type: 13264021, 2018 MU

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2018" )
DaVinci().DataType = '2018'
IOHelper().inputFiles(["root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00092435/0000/00092435_00000125_7.AllStreams.dst",
                       "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/2018/ALLSTREAMS.DST/00092435/0000/00092435_00000276_7.AllStreams.dst",
],clear=True)
