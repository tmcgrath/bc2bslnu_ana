# Event type 14165003, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00139854/0000/00139854_00000138_7.AllStreams.dst",
                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00139854/0000/00139854_00000251_7.AllStreams.dst",
                       "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/2017/ALLSTREAMS.DST/00139854/0000/00139854_00000026_7.AllStreams.dst"
],clear=True)

