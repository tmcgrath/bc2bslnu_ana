#!/bin/bash

lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsK_DsPi.py BcOptions_BsK_DsPi.py 2>&1 | tee Tuples/BsK_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsK_JpsiPhi.py BcOptions_BsK_JpsiPhi.py 2>&1 | tee Tuples/BsK_JpsiPhi.log

lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsKst_DsPi.py BcOptions_BsKst_DsPi.py 2>&1 | tee Tuples/BsKst_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsKst_JpsiPhi.py BcOptions_BsKst_JpsiPhi.py 2>&1 | tee Tuples/BsKst_JpsiPhi.log

lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsRho_DsPi.py BcOptions_BsRho_DsPi.py 2>&1 | tee Tuples/BsRho_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsRho_JpsiPhi.py BcOptions_BsRho_JpsiPhi.py 2>&1 | tee Tuples/BsRho_JpsiPhi.log

lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsstMuNu_DsPi.py BcOptions_BsstMuNu_DsPi.py 2>&1 | tee Tuples/BsstMuNu_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsstMuNu_JpsiPhi.py BcOptions_BsstMuNu_JpsiPhi.py 2>&1 | tee Tuples/BsstMuNu_JpsiPhi.log
