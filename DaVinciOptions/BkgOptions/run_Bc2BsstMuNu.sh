#!/bin/bash

lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsstMuNu_DsPi.py BcOptions_BsstMuNu_DsPi.py 2>&1 | tee Tuples/BsstMuNu_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsstMuNu_JpsiPhi.py BcOptions_BsstMuNu_JpsiPhi.py 2>&1 | tee Tuples/BsstMuNu_JpsiPhi.log
