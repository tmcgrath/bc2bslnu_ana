# Event type 14575200, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150526/0000/00150526_00000005_7.AllStreams.dst"],clear=True)
