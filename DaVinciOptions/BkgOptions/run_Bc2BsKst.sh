#!/bin/bash

#lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsKst_DsPi.py BcOptions_BsKst_DsPi.py 2>&1 | tee Tuples/BsKst_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../IsoVariables.py Test_Bc2BsKst_JpsiPhi.py BcOptions_BsKst_JpsiPhi.py 2>&1 | tee Tuples/BsKst_JpsiPhi.log
