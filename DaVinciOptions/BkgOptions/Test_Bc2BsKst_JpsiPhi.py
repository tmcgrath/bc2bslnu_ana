# Event type 14347100, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150536/0000/00150536_00000014_7.AllStreams.dst",
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150536/0000/00150536_00000003_7.AllStreams.dst",
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150536/0000/00150536_00000005_7.AllStreams.dst",
#                       "root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150536/0000/00150536_00000001_7.AllStreams.dst",
#                       "root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150536/0000/00150536_00000010_7.AllStreams.dst"
],clear=True)
