# Event type 14145400, MagDown 

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150522/0000/00150522_00000003_7.AllStreams.dst",
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150522/0000/00150522_00000002_7.AllStreams.dst",
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150522/0000/00150522_00000004_7.AllStreams.dst",
#                       "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150522/0000/00150522_00000007_7.AllStreams.dst",
#                       "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/2017/ALLSTREAMS.DST/00150522/0000/00150522_00000011_7.AllStreams.dst",
],clear=True)

