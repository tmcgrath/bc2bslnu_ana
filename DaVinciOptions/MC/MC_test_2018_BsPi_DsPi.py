######################################################
# MC test sample for Bc->Bspi Bs->DsPi channel       #
# Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8 #
######################################################
# evt+std://MC/2018/14165002/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/ALLSTREAMS.DST
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
CondDB( LatestGlobalTagByDataType = "2018" )
DaVinci().DataType = '2018'
# Sim09f
#IOHelper().inputFiles([''],clear=True)
#sim09k
#input = [
#    'root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00151102/0000/00151102_00000033_7.AllStreams.dst',]
#    'root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00151080/0000/00151080_00000013_7.AllStreams.dst',
#    'root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00151080/0000/00151080_00000039_7.AllStreams.dst',
#    'root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2018/ALLSTREAMS.DST/00151080/0000/00151080_00000017_7.AllStreams.dst',
#    'root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2018/ALLSTREAMS.DST/00151080/0000/00151080_00000045_7.AllStreams.dst'
#]
# sim09j
input = [
    "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00122268/0000/00122268_00000171_7.AllStreams.dst"
]
IOHelper().inputFiles(input,clear=True)