######################################################
# MC test sample for Bc->Bspi Bs->DsPi channel       #
# Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8 #
######################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
# Sim09f
#IOHelper().inputFiles(['root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/2017/ALLSTREAMS.DST/00085767/0000/00085767_00000813_7.AllStreams.dst'],clear=True)

#Sim09k
IOHelper().inputFiles(['root://x509up_u129802@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150540/0000/00150540_00000005_7.AllStreams.dst'],clear=True)