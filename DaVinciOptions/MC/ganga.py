import os

def make_options(name,#savename of file
                 opts,#list of options
                 #dv2use,#path to DaVinci install
                 data):#data to process
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v45r4')
    myApp = GaudiExec()
    #myApp = DaVinci()
    #myApp.version = 'v45r4'
    myApp.directory = '/afs/cern.ch/work/t/tmcgrath/private/lbdevs/DaVinciDev_v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc9-opt'    

    return j

# DsPi MC
MC_2017MU_bspi_dspi_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14165002/ALLSTREAMS.DST"
dataset2017MU_dspi = BKQuery(path=MC_2017MU_bspi_dspi_path).getDataset()

# JpsiPhi MC
MC_2017MU_bspi_jpsiphi_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
dataset2017MU_jpsiphi = BKQuery(path=MC_2017MU_bspi_jpsiphi_path).getDataset()

# Run jobs
make_options("Bc_BsPi_DsPi_2017_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/2017MU.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_DsPi.py'],dataset2017MU_dspi)
make_options("Bc_BsPi_JpsiPhi_2017_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/2017MU.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_JpsiPhi.py'],dataset2017MU_jpsiphi)



