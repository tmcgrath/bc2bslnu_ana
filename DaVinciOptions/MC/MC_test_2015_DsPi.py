######################################################
# MC test sample for Bs->DsPi channel                #
# Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8 #
######################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper 
from Configurables import DaVinci
DaVinci().DataType = '2015'
# Bc->Bsmunu prob also magdown
IOHelper().inputFiles(['root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00110601/0000/00110601_00000017_6.AllStreams.dst'],clear=True)
DaVinci().DDDBtag = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20161124-vc-md100"
