from Configurables import DaVinci

year = "2016"
DaVinci().DDDBtag    = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20170721-2-vc-md100"
DaVinci().DataType   = year
DaVinci().Simulation = True