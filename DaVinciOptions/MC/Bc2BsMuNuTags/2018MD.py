from Configurables import DaVinci

year = "2018"
DaVinci().DDDBtag    = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20190430-vc-md100"
DaVinci().DataType   = year
DaVinci().Simulation = True
