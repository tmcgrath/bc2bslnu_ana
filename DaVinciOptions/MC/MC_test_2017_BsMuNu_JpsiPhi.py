######################################################
# MC test sample for Bc->BsMuNu Bs->JpsiPhi channel  #
# Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8 #
######################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(['root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00110254/0000/00110254_00000053_7.AllStreams.dst',
                       'root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00110254/0000/00110254_00000057_7.AllStreams.dst',
                       'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/2017/ALLSTREAMS.DST/00110254/0000/00110254_00000004_7.AllStreams.dst',
                       'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/2017/ALLSTREAMS.DST/00110254/0000/00110254_00000027_7.AllStreams.dst',
                       'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/2017/ALLSTREAMS.DST/00110254/0000/00110254_00000022_7.AllStreams.dst'
],clear=True)

