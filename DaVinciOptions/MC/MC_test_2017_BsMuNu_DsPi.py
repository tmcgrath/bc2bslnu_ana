######################################################
# MC test sample for Bc->BsMuNu Bs->DsPi channel     #
# Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8 #
######################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(['root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/2017/ALLSTREAMS.DST/00110256/0000/00110256_00000052_7.AllStreams.dst'],clear=True)
