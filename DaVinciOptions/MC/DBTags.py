# All the database tags for signal and background MC samples
# 14.10.22

Bc2BsMuNu_Tags    = { 
                           "2016" : {
                                      "Sim"       : "Sim09h",
                                      "DDDB"      : "dddb-20170721-3",
                                      "CondDB_MU" : "sim-20170721-2-vc-mu100",
                                      "CondDB_MD" : "sim-20170721-2-vc-md100"
                           },
                           "2017" : {
                                      "Sim"       : "Sim09h",
                                      "DDDB"      : "dddb-20170721-3",
                                      "CondDB_MU" : "sim-20190430-1-vc-mu100",
                                      "CondDB_MD" : "sim-20190430-1-vc-md100"
                           },
                           "2018" : {
                                      "Sim"       : "Sim09h",
                                      "DDDB"      : "dddb-20170721-3",
                                      "CondDB_MU" : "sim-20190430-vc-mu100",
                                      "CondDB_MD" : "sim-20190430-vc-md100"
                           }
                         }

# using Sim09k cos it's the most recent (but Sim09j there is more but only Bs->Dspi)
Bkg_Tags      = { 
                           "2016" : {
                                      "Sim"       : "Sim09k",
                                      "DDDB"      : "dddb-20170721-3",
                                      "CondDB_MU" : "sim-20170721-2-vc-mu100",
                                      "CondDB_MD" : "sim-20170721-2-vc-md100"
                           },
                           "2017" : {
                                      "Sim"       : "Sim09k",
                                      "DDDB"      : "dddb-20170721-3",
                                      "CondDB_MU" : "sim-20190430-1-vc-mu100",
                                      "CondDB_MD" : "sim-20190430-1-vc-md100"
                           },
                           "2018" : {
                                      "Sim"       : "Sim09k",
                                      "DDDB"      : "dddb-20170721-3",
                                      "CondDB_MU" : "sim-20190430-vc-mu100",
                                      "CondDB_MD" : "sim-20190430-vc-md100"
                           }
                         }
