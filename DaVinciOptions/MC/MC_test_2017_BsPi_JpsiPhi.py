######################################################
# MC test sample for Bc->BsPi Bs->JpsiPhi channel    #
# Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8 #
######################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
# sim09f
#IOHelper().inputFiles(['root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/2017/ALLSTREAMS.DST/00090646/0000/00090646_00000008_7.AllStreams.dst'],clear=True)

#sim09k
IOHelper().inputFiles(['root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150538/0000/00150538_00000009_7.AllStreams.dst',
                       'root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150538/0000/00150538_00000010_7.AllStreams.dst',
                       'root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150538/0000/00150538_00000028_7.AllStreams.dst',
                       'root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00150538/0000/00150538_00000029_7.AllStreams.dst',
                       'root://x509up_u129802@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00150538/0000/00150538_00000007_7.AllStreams.dst' 
],clear=True)
