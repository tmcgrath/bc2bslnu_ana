from Configurables import DaVinci

year = "2017"
DaVinci().DDDBtag    = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20190430-1-vc-md100"
DaVinci().DataType   = year
DaVinci().Simulation = True
