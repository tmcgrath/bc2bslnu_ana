##################################################
# Script to produce Bc-> Bs mu nu and Bc-> Bs pi
# with Bs -> Jpsi phi
# Authors: Adam Davis, Tamaki Holly McGrath
##################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import FilterInTrees
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection, FilterSelection
from StandardParticles import StdLooseKaons, StdLooseMuons, StdLoosePions, StdLooseDetachedPhi2KK
#tuple tools
from Configurables import DecayTreeTuple, PrintDecayTreeTool
from DecayTreeTuple.Configuration import *
from Configurables import (TupleToolDecay, TupleToolTISTOS,  TupleToolNeutrinoReco,
                           TupleToolDecayTreeFitter, TupleToolTrigger,
                           TriggerTisTos, TupleToolDecayTreeFitter,
                           LoKi__Hybrid__TupleTool)

from GaudiKernel.SystemOfUnits import MeV
#make a dictionary of cuts so we don't have to go searching
selection_dictionary = {
    'phi(1020)': {'mass_window': '200*MeV',
                  'vchi2ndf_cut':'10'},
    'J/psi(1S)': {'mass_window' : '100*MeV', 'vchi2ndf_cut':'9'},
    'B_s0': {'vchi2ndf_cut': '100'},
    'B_c+': {'vchi2ndf_cut': '100'}
    }
selection_sequences = []

# Selection sequences

# Jpsi
jpsi_location = '/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles'
jpsi_sel = SimpleSelection("jpsi_sel",
                           ConfigurableGenerators.FilterInTrees,
                           AutomaticData(jpsi_location),
                           Code = "('J/psi(1S)'==ABSID)",
)
jspi_sel_seq = SelectionSequence("jspi_sel_seq",TopSelection  = jpsi_sel)
selection_sequences.append(jspi_sel_seq)

#Make Bs candidates
bs2jpsiphi_sel = SimpleSelection("bs2jpsiphi_sel",
                                 ConfigurableGenerators.CombineParticles,
                                 [jpsi_sel, StdLooseDetachedPhi2KK],
                                 DecayDescriptor = 'B_s0 -> J/psi(1S) phi(1020)',
                                 DaughtersCuts = {
                                     'J/psi(1S)' : "(ADMASS('J/psi(1S)') < %(mass_window)s ) & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%(selection_dictionary['J/psi(1S)']),
                                     'phi(1020)' : "(ADMASS('phi(1020)') < %(mass_window)s )  & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%(selection_dictionary['phi(1020)'])
                                 },
                                 MotherCut = "(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['B_s0']
)
bs2jpsiphi_sel_seq = SelectionSequence("bs2jpsiphi_sel_seq",TopSelection  =bs2jpsiphi_sel)
selection_sequences.append(bs2jpsiphi_sel_seq)

# Make Bc->Bsmunu
bc2bsmunu_jpsiphi_sel = SimpleSelection('bc2bsmunu_jpsiphi_sel',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2jpsiphi_sel, StdLooseMuons],
                                     DecayDescriptors = ['[B_c+ -> B_s0 mu+]cc','[B_c- -> B_s0 mu-]cc'],
                                     MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bsmunu_jpsiphi_sel_seq = SelectionSequence("bc2bsmunu_jpsiphi_sel_seq",TopSelection = bc2bsmunu_jpsiphi_sel)
selection_sequences.append(bc2bsmunu_jpsiphi_sel_seq)

#Make Bc->Bspi
bc2bspi_jpsiphi_sel = SimpleSelection('bc2bspi_jpsiphi_sel',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2jpsiphi_sel, StdLoosePions],
                                     DecayDescriptors = ['[B_c+ -> B_s0 pi+]cc','[B_c- -> B_s0 pi-]cc'],
                                     MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bspi_jpsiphi_sel_seq = SelectionSequence("bc2bspi_jpsiphi_sel_seq",TopSelection = bc2bspi_jpsiphi_sel)
selection_sequences.append(bc2bspi_jpsiphi_sel_seq)
# All sequences appended


# Make the decay tree tuples

Bc2BsMu_Bs2JpsiPhi = DecayTreeTuple("Bc2BsMu_Bs2JpsiPhi",
                                    Inputs = [bc2bsmunu_jpsiphi_sel_seq.outputLocation()],
                                    Decay = "[B_c+ -> ^((B_s0|B_s~0) -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K- ) ) ^mu+]CC")
Bc2BsMu_Bs2JpsiPhi.addBranches({
    'Bc':"^([B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC)",
    'Bs':'[B_c+ -> ^((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Jpsi':'[B_c+ -> ((B_s0|B_s~0) -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu1':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu2':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Phi':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K- ) ) mu+]CC',
    'K1':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K- ) ) mu+]CC',
    'K2':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K- ) ) mu+]CC',
    'Mu_bach':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) ^mu+]CC'})

# Norm
Bc2BsPi_Bs2JpsiPhi = DecayTreeTuple("Bc2BsPi_Bs2JpsiPhi",
                                    Inputs = [bc2bspi_jpsiphi_sel_seq.outputLocation()],
                                    Decay = "[B_c+ -> ^((B_s0|B_s~0) -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K- ) ) ^pi+]CC")
Bc2BsPi_Bs2JpsiPhi.addBranches({
    'Bc':"^([B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC)",
    'Bs':'[B_c+ -> ^((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Jpsi':'[B_c+ -> ((B_s0|B_s~0) -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Mu1':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Mu2':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Phi':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K- ) ) pi+]CC',
    'K1':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K- ) ) pi+]CC',
    'K2':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K- ) ) pi+]CC',
    'Mu_bach':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) ^pi+]CC'})

#configure the tools for the DTTs
tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",
                 "TupleToolANNPID",
                 "TupleToolTrigger"
                 ]
triglist = ['L0MuonDecision',
            'L0HadronDecision',
            'Hlt1SingleMuonNoIPDecision',
            'Hlt1TrackMVADecision',
            'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackAllL0Decision',
            'Hlt2SingleMuonDecision',
            'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision',
            'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision'
        ]

#preamble = [
#    'BPVETA = BPVETA(VZ)',
#]

for tuple in [Bc2BsMu_Bs2JpsiPhi, Bc2BsPi_Bs2JpsiPhi]:
    tuple.ToolList+=tupletoollist
    LoKi_Bc = tuple.Bc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bc")
    LoKi_Bc.Variables = {"BPVETA" : "BPVETA",
                         "PHI" : "PHI",
                         "KEY": "KEY",
                         "BPVCORRM" : "BPVCORRM",
                        }
    LoKi_Mu1 = tuple.Mu1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu1")
    LoKi_Mu1.Variables = {"NSHAREDMU":"NSHAREDMU"}
    LoKi_Mu2 = tuple.Mu2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu2")
    LoKi_Mu2.Variables = {"NSHAREDMU":"NSHAREDMU"}

    if Bc2BsMu_Bs2JpsiPhi==tuple:
        LoKi_MuBach = tuple.Mu_bach.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuBach")
        LoKi_MuBach.Variables = {"NSHAREDMU":"NSHAREDMU"}
        ttnr = tuple.Bc.addTupleTool(TupleToolNeutrinoReco,name='ttnr')
        ttnr.Verbose=True
        ttnr.MotherMass = 6274.47*MeV#LHCb-PAPER-2020-003
        #http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/LHCb-PAPER-2020-003.html

    ttdtf=tuple.Bc.addTupleTool(TupleToolDecayTreeFitter("TTDTF"))
    ttdtf.Verbose = True
    ttdtf.constrainToOriginVertex =False
    ttdtf.daughtersToConstrain = ["J/psi(1S)",'phi(1020)','B_s0']

    ttttBc = tuple.Bc.addTupleTool(TupleToolTISTOS,name="ttttBc")
    ttttBs = tuple.Bs.addTupleTool(TupleToolTISTOS,name="ttttBs")
    if Bc2BsMu_Bs2JpsiPhi==tuple:
        ttttMu = tuple.Mu_bach.addTupleTool(TupleToolTISTOS,name="ttttMu")
        for T in [ttttBc,ttttBs, ttttMu]:
            T.VerboseL0   = True
            T.VerboseHlt1 = True
            T.VerboseHlt2 = True
            T.TriggerList = triglist[:]
    else:
        for T in [ttttBc,ttttBs]:
            T.VerboseL0   = True
            T.VerboseHlt1 = True
            T.VerboseHlt2 = True
            T.TriggerList = triglist[:]


# Configure DaVinci

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

DaVinci().PrintFreq = 1000 # change this to 1000 for all evts
DaVinci().TupleFile = "Bc_Tuples_JpsiPhi.root"
DaVinci().EvtMax = -1
DaVinci().Simulation = False
# Set Luminosity information when not using MC
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().UserAlgorithms+=[s.sequence() for s in selection_sequences]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2JpsiPhi]
DaVinci().UserAlgorithms+=[Bc2BsPi_Bs2JpsiPhi]
DaVinci().InputType = 'DST'



