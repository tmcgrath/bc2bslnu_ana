##################################################
# Script to produce Bc-> Bs mu nu and Bc-> Bs pi
# with Bs -> Ds pi
# Authors: Adam Davis, Tamaki Holly McGrath
##################################################
###################
isSimulation = True
###################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import FilterInTrees
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection, FilterSelection
from StandardParticles import StdLooseKaons, StdLooseMuons, StdLoosePions
#tuple tools
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import (TupleToolDecay, TupleToolTISTOS,  TupleToolNeutrinoReco,
                           TupleToolDecayTreeFitter, TupleToolTrigger,
                           TriggerTisTos, TupleToolDecayTreeFitter,
                           LoKi__Hybrid__TupleTool)

from GaudiKernel.SystemOfUnits import MeV
#make a dictionary of cuts so we don't have to go searching
selection_dictionary = {
    'D_s-': {'mass_window': '200*MeV',
             'vchi2ndf_cut': '10'},
    'B_s0': {'vchi2ndf_cut': '100'},
    'B_c+': {'vchi2ndf_cut': '100'}
    }
selection_sequences = []


# Selection sequences

# Ds
# Select Ds first, select kaons and pions from there, then recombine to Ds
bs_event = "/Event/AllStreams/Phys/B02DPiNoIPD2HHHCFPIDBeauty2CharmLine/Particles"
ds_filter = SimpleSelection("ds_filter",
                            ConfigurableGenerators.FilterInTrees,
                            AutomaticData(bs_event),
                            Code = "('D+'==ABSID)"
)
kaon_sel = SimpleSelection("kaon_sel",
                            ConfigurableGenerators.FilterInTrees,
                            [ds_filter],
                            Code = "('K+'==ABSID)"
)
pion_sel = SimpleSelection("pion_sel",
                         ConfigurableGenerators.FilterInTrees,
                         [ds_filter],
                         Code = "('pi+'==ABSID)"
)
ds_sel = SimpleSelection("ds_sel",
                         ConfigurableGenerators.CombineParticles,
                         [kaon_sel, pion_sel],
                         DecayDescriptor = '[D_s- -> K+ K- pi-]cc',
                         CombinationCut = '(AM < 2100) & (AM > 1750 )',
                         MotherCut = "(ADMASS('D_s-') < %(mass_window)s) & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['D_s-']
)
ds_sel_seq = SelectionSequence("ds_sel_seq",TopSelection = ds_sel)
selection_sequences.append(ds_sel_seq)


# Make Bs candidate
bs2dspi_sel = SimpleSelection('bs2dspi_sel',
                              ConfigurableGenerators.CombineParticles,
                              [ds_sel, StdLoosePions],
                              DecayDescriptor = '[B_s0 -> D_s- pi+]cc',
                              #CombinationCut = '',
                              MotherCut = "('B_s0'==ABSID) & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['B_s0']
)
bs2dspi_sel_seq = SelectionSequence('bs2dspi_sel_seq',TopSelection = bs2dspi_sel)
selection_sequences.append(bs2dspi_sel_seq)

# Make Bc->Bsmunu          
bc2bsmunu_dspi_sel = SimpleSelection('bc2bsmunu_dspi_sel',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2dspi_sel, StdLooseMuons],
                                     DecayDescriptors = ['[B_c+ -> B_s0 mu+]cc', '[B_c- -> B_s0 mu-]cc'],
                                     MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bsmunu_dspi_sel_seq = SelectionSequence("bc2bsmunu_dspi_sel_seq",TopSelection = bc2bsmunu_dspi_sel)
selection_sequences.append(bc2bsmunu_dspi_sel_seq)

# Make Bc->Bspi for cross checks
bc2bspi_dspi_sel = SimpleSelection('bc2bspi_dspi_sel',
                                   ConfigurableGenerators.CombineParticles,
                                   [bs2dspi_sel, StdLoosePions],
                                   DecayDescriptors = ['[B_c+ -> B_s0 pi+]cc', '[B_c- -> B_s0 pi-]cc'],
                                   MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bspi_dspi_sel_seq = SelectionSequence("bc2bspi_dspi_sel_seq",TopSelection = bc2bspi_dspi_sel)
selection_sequences.append(bc2bspi_dspi_sel_seq)
# All sequences appended


# Make the decay tree tuples

# Signal
Bc2BsMu_Bs2DsPi = DecayTreeTuple("Bc2BsMu_Bs2DsPi",
                                 Inputs = [bc2bsmunu_dspi_sel_seq.outputLocation()],
                                 Decay = '[B_c+ -> ^((B_s0|B_s~0) -> ^((D_s- | D_s+) -> ^K- ^K+ ^(pi- | pi+ ) ) ^pi+ ) ^mu+]CC')
Bc2BsMu_Bs2DsPi.addBranches({
    'Bc':'^([B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC)',
    'Bs':'[B_c+ -> ^((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'Ds':'[B_c+ -> ((B_s0|B_s~0) -> ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'K1':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'K2':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'Pi':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) pi+ ) mu+]CC',
    'Pi_from_Bs':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) ^pi+ ) mu+]CC',
    'Mu_bach':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) ^mu+]CC'
    })

# Norm
Bc2BsPi_Bs2DsPi = DecayTreeTuple("Bc2BsPi_Bs2DsPi",
                                 Inputs = [bc2bspi_dspi_sel_seq.outputLocation()],
                                 Decay = '[B_c+ -> ^((B_s0|B_s~0) -> ^((D_s- | D_s+) -> ^K- ^K+ ^(pi- | pi+ ) ) ^pi+ ) ^pi+]CC')
Bc2BsPi_Bs2DsPi.addBranches({
    'Bc':'^([B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) pi+]CC)',
    'Bs':'[B_c+ -> ^((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) pi+]CC',
    'Ds':'[B_c+ -> ((B_s0|B_s~0) -> ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) pi+]CC',
    'K1':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) pi+ ) pi+]CC',
    'K2':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) pi+ ) pi+]CC',
    'Pi':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) pi+ ) pi+]CC',
    'Pi_from_Bs':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) ^pi+ ) pi+]CC',
    'Mu_bach':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) ^pi+]CC'
    })

MC_Bc2BsMu_Bs2DsPi = MCDecayTreeTuple("MC_Bc2BsMu_Bs2DsPi",
                                      Inputs = [bc2bsmunu_dspi_sel_seq.outputLocation()],
                                      Decay = '[B_c+ => ^((B_s0|B_s~0) => ^((D_s- | D_s+) ==> ^K- ^K+ ^(pi- | pi+ ) ) ^pi+ ) ^mu+ ^nu_mu]CC',
                                      Branches = {'Bc':'^([B_c+ => ((B_s0|B_s~0) => ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+ nu_mu]CC)',
                                                  'Bs':'[B_c+ => ^((B_s0|B_s~0) => ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+ nu_mu]CC',
                                                  'Ds':'[B_c+ => ((B_s0|B_s~0) => ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+ nu_mu]CC',
                                                  'K1':'[B_c+ => ((B_s0|B_s~0) => ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) pi+ ) mu+ nu_mu]CC',
                                                  'K2':'[B_c+ => ((B_s0|B_s~0) => ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) pi+ ) mu+ nu_mu]CC',
                                                  'Pi':'[B_c+ => ((B_s0|B_s~0) => ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) pi+ ) mu+ nu_mu]CC',
                                                  'Pi_from_Bs':'[B_c+ => ((B_s0|B_s~0) => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) ^pi+ ) mu+ nu_mu]CC',
                                                  'Mu_bach':'[B_c+ => ((B_s0|B_s~0) => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) pi+ ) ^mu+ nu_mu]CC',
                                                  'nu': '[B_c+ => ((B_s0|B_s~0) => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) pi+ ) mu+ ^nu_mu]CC'})
#configure the tools for the DTTs
tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",
                 "TupleToolANNPID",
                 "TupleToolTrigger"
                 ]
mctupletoollist = ["MCTupleToolHierarchy",
                   "MCTupleToolReconstructed",
                   "MCTupleToolKinematic",
                   "TupleToolMCTruth",
                   "TupleToolMCBackgroundInfo",
                   #"MCTupleToolAngles"
                   "LoKi::Hybrid::MCTupleTool/LoKi_Photos",                   
                   ]

triglist = ['L0MuonDecision',
            'L0HadronDecision',
            'Hlt1SingleMuonNoIPDecision',
            'Hlt1TrackMVADecision',
            'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackAllL0Decision',
            'Hlt2SingleMuonDecision',
            'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision',
            'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision'
        ]
MC_Bc2BsMu_Bs2DsPi.ToolList+=mctupletoollist
nPhotons = MC_Bc2BsMu_Bs2DsPi.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_Photons")
nPhotons.Preambulo+=["from LoKiPhysMC.decorators import *" ,"from LoKiPhysMC.functions import *"]
nPhotons.Variables = {"nPhotons": "MCNINTREE(('gamma' == MCABSID)) "}
from Configurables import MCMatchObjP2MCRelator
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

for tuple in [Bc2BsMu_Bs2DsPi, Bc2BsPi_Bs2DsPi]:
    tuple.ToolList+=tupletoollist
    LoKi_Bc = tuple.Bc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bc")
    LoKi_Bc.Variables = {"PV_ETA" : "BPVETA",
                         "PHI" : "PHI",
                         "KEY": "KEY",
                         "CORR_M" : "BPVCORRM",
                        }
    MCTruth = tuple.addTupleTool('TupleToolMCTruth')
    MCTruth.ToolList += ['MCTupleToolKinematic']
    MCTruth.ToolList += ['MCTupleToolHierarchy']
    MCTruth.addTool(MCMatchObjP2MCRelator)
    MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
    
    if Bc2BsMu_Bs2DsPi==tuple:
        LoKi_MuBach = tuple.Mu_bach.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuBach")
        LoKi_MuBach.Variables = {"NSHAREDMU":"NSHAREDMU"}
        ttnr = tuple.Bc.addTupleTool(TupleToolNeutrinoReco,name='ttnr')
        ttnr.Verbose=True
        ttnr.MotherMass = 6274.47*MeV#LHCb-PAPER-2020-003
        #http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/LHCb-PAPER-2020-003.html

    ttdtf=tuple.Bc.addTupleTool(TupleToolDecayTreeFitter("TTDTF"))
    ttdtf.Verbose = True
    ttdtf.constrainToOriginVertex =False
    ttdtf.daughtersToConstrain = ["D_s-",'B_s0']

    ttttBc = tuple.Bc.addTupleTool(TupleToolTISTOS,name="ttttBc")
    ttttBs = tuple.Bs.addTupleTool(TupleToolTISTOS,name="ttttBs") 
    if Bc2BsMu_Bs2DsPi==tuple:
        ttttMu = tuple.Mu_bach.addTupleTool(TupleToolTISTOS,name="ttttMu")
        for T in [ttttBc,ttttBs, ttttMu]:
            T.VerboseL0   = True
            T.VerboseHlt1 = True
            T.VerboseHlt2 = True
            T.TriggerList = triglist[:]
    else:
        for T in [ttttBc,ttttBs]:
            T.VerboseL0   = True
            T.VerboseHlt1 = True
            T.VerboseHlt2 = True
            T.TriggerList = triglist[:] 

# Configure DaVinci

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

DaVinci().PrintFreq = 1000 # Change to 1000 when all events
DaVinci().TupleFile = "Bc_Tuples_DsPi.root"
DaVinci().EvtMax = -1
DaVinci().Simulation = isSimulation
# Set Luminosity information when not using MC
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().UserAlgorithms+=[s.sequence() for s in selection_sequences]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2DsPi]
DaVinci().UserAlgorithms+=[Bc2BsPi_Bs2DsPi]
DaVinci().UserAlgorithms+=[MC_Bc2BsMu_Bs2DsPi]
DaVinci().InputType = 'DST'


from GaudiConf import IOHelper
IOHelper("ROOT").inputFiles(['root://x509up_u38522@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00110595/0000/00110595_00000050_7.AllStreams.dst',
                             #'root://heplnx234.pp.rl.ac.uk:1094/pnfs/pp.rl.ac.uk/data/lhcb/lhcb/MC/2016/ALLSTREAMS.DST/00110595/0000/00110595_00000005_7.AllStreams.dst',
                             #'root://x509up_u38522@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00110595/0000/00110595_00000068_7.AllStreams.dst',
                             #'root://x509up_u38522@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00110595/0000/00110595_00000025_7.AllStreams.dst',
                             ],clear=True)
