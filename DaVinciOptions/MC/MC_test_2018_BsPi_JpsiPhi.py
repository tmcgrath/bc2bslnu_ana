######################################################
# MC test sample for Bc->Bspi Bs->DsPi channel       #
# Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8 #
######################################################
# evt+std://MC/2018/14135000/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/ALLSTREAMS.DST
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
CondDB( LatestGlobalTagByDataType = "2018" )
DaVinci().DataType = '2018'
# Sim09f
#IOHelper().inputFiles([''],clear=True)
# sim09k
input = [
    "root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2018/ALLSTREAMS.DST/00151100/0000/00151100_00000013_7.AllStreams.dst"
    #"root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00151074/0000/00151074_00000047_7.AllStreams.dst",
    #"root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00151100/0000/00151100_00000026_7.AllStreams.dst",
    #"root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/2018/ALLSTREAMS.DST/00151100/0000/00151100_00000007_7.AllStreams.dst",
    #"root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2018/ALLSTREAMS.DST/00151100/0000/00151100_00000006_7.AllStreams.dst",
    #"root://x509up_u129802@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/2018/ALLSTREAMS.DST/00151100/0000/00151100_00000004_7.AllStreams.dst"
]
IOHelper().inputFiles(input,clear=True)