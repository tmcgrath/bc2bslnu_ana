##################################################
# Script to produce Bc-> Bs mu nu
# with Bs -> Jpsi/phi or Ds pi
# Authors: Adam Davis, Tamaki Holly McGrath
##################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import FilterInTrees
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection, FilterSelection
from StandardParticles import StdLooseKaons, StdLooseMuons, StdLoosePions
#tuple tools
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import (TupleToolDecay, TupleToolTISTOS,  TupleToolNeutrinoReco,
                           TupleToolDecayTreeFitter, TupleToolTrigger,
                           TriggerTisTos, TupleToolDecayTreeFitter,
                           LoKi__Hybrid__TupleTool)

from GaudiKernel.SystemOfUnits import MeV
#make a dictionary of cuts so we don't have to go searching
selection_dictionary = {
    'phi(1020)': {'mass_window': '200*MeV',
                  'vchi2ndf_cut':'10'},
    'J/psi(1S)': {'mass_window' : '100*MeV', 'vchi2ndf_cut':'9'}, 
    'D_s-': {'mass_window': '200*MeV',
             'vchi2ndf_cut': '10'},
    'B_s0': {'vchi2ndf_cut': '100'},
    'B_c+': {'vchi2ndf_cut': '100'}
    }
selection_sequences = []

#start as far down the chain as we can
#first Jpsi

jpsi_location = '/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles'
jpsi_sel = SimpleSelection("jpsi_sel",
                           ConfigurableGenerators.FilterInTrees,
                           AutomaticData(jpsi_location),
                           Code = "('J/psi(1S)'==ABSID) & (ADMASS('J/psi(1S)') < %(mass_window)s ) & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%(selection_dictionary['J/psi(1S)']), 
                           )
jspi_sel_seq = SelectionSequence("jspi_sel_seq",TopSelection  = jpsi_sel)
selection_sequences.append(jspi_sel_seq)

#kaons from phi
kplus_from_phi_sel = FilterSelection("kplus_from_phi_sel",
                                     [StdLooseKaons],
                                     Code="('K+' == ABSID)"
)
kminus_from_phi_sel = FilterSelection("kminus_from_phi_sel",
                                     [StdLooseKaons],
                                     Code="('K-' == ABSID)"
)
#phi 
# Don't need a dedicated selection
og_phi_sel = SimpleSelection("og_phi_sel",
                          ConfigurableGenerators.CombineParticles,
                          [kplus_from_phi_sel, kminus_from_phi_sel],
                          DecayDescriptor = '[phi(1020) -> K+ K-]cc',
                          CombinationCut = '(AM < 1400)',
                          MotherCut = " (ADMASS('phi(1020)') < %(mass_window)s )  & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%(selection_dictionary['phi(1020)'])
                          )
phi_location = "Phys/StdLooseDetachedPhi2KK/Particles"
phi_sel = SimpleSelection("phi_sel",
                          ConfigurableGenerators.FilterInTrees,
                          AutomaticData(phi_location),
                          #Code = "('phi(1020)'==ABSID) & (ADMASS('phi(1020)') < %(mass_window)s ) & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%(selection_dictionary['phi(1020)']),
                          #Code = "('phi(1020)'==ABSID)"
)
phi_sel_seq = SelectionSequence("phi_sel_seq",TopSelection = phi_sel)
selection_sequences.append(phi_sel_seq)


# Kaons from D_s
kplus_from_Ds_sel = FilterSelection("kplus_from_Ds_sel",
                                     [StdLooseKaons],
                                     Code="('K+' == ABSID)"
)
kminus_from_Ds_sel = FilterSelection("kminus_from_Ds_phi_sel",
                                      [StdLooseKaons],
                                      Code="('K-' == ABSID)"
)

# Pion from D_s -> K+ K- pi-
pion_from_Ds_sel = FilterSelection("pion_from_Ds_sel",
                                   [StdLoosePions],
                                   Code="('pi-' == ABSID)",
)
ds_sel = SimpleSelection("ds_sel",
                         ConfigurableGenerators.CombineParticles,
                         [kplus_from_Ds_sel, kminus_from_Ds_sel, pion_from_Ds_sel],
                         DecayDescriptor = '[D_s- -> K+ K- pi-]cc',
                         CombinationCut = '(AM < 2100) & (AM > 1750 )',
                         MotherCut = "(ADMASS('D_s-') < %(mass_window)s) & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['D_s-']
                         )
ds_sel_seq = SelectionSequence("ds_sel_seq",TopSelection = ds_sel)
#selection_sequences.append(ds_sel_seq)
# Pion from Bs -> Ds- pi+
pion_from_Bs_sel = FilterSelection("pion_from_Bs_sel",
                                   [StdLoosePions],
                                   Code="('pi+' == ABSID)"
)

#Make Bs candidates
bs2jpsiphi_sel = SimpleSelection("bs2jpsiphi_sel",
                                 ConfigurableGenerators.CombineParticles,
                                 [jpsi_sel, phi_sel],
                                 DecayDescriptor = '[B_s0 -> J/psi(1S) phi(1020)]cc',
                                 #CombinationCut = '',
                                 MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_s0']
                                 )
bs2jpsiphi_sel_seq = SelectionSequence("bs2jpsiphi_sel_seq",TopSelection  =bs2jpsiphi_sel)
selection_sequences.append(bs2jpsiphi_sel_seq)

og_bs2dspi_sel = SimpleSelection('og_bs2dspi_sel',
                              ConfigurableGenerators.CombineParticles,
                              [ds_sel, pion_from_Bs_sel],
                              DecayDescriptors = ['B_s0 -> D_s- pi+', 'B_s~0 -> D_s+ pi-'],
                              #CombinationCut = '',
                              MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_s0']
                              )
# Bs from stripping line
#from Bc mass measurment and Bc->Bspi observation: 
#       specifically for B0->Dpi but could be the cuts needed tho
bs2dspi_location = 'Phys/B02DPiNoIPD2HHHCFPIDBeauty2CharmLine/Particles'
bs2dspi_sel = SimpleSelection('bs2dspi_sel',
                              ConfigurableGenerators.FilterInTrees,
                              AutomaticData(bs2dspi_location),
                              #Code = "(('B_s0'==ABSID)|('B_s~0'==ABSID)) & (VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['B_s0']
                              #Code = "(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['B_s0']
)
bs2dspi_sel_seq = SelectionSequence('bs2dspi_sel_seq',TopSelection = bs2dspi_sel)
selection_sequences.append(bs2dspi_sel_seq)

#Make the Bc
bach_mu_jpsiphi_sel = FilterSelection("bach_mu_jpsiphi_sel",
                                     [StdLooseMuons],
                                     Code="('mu+' == ABSID) | ('mu-' == ABSID)"
)
bach_mu_jpsiphi_sel_seq = SelectionSequence("bach_mu_jpsiphi_sel_seq",TopSelection = bach_mu_jpsiphi_sel)
selection_sequences.append(bach_mu_jpsiphi_sel_seq)
bc2bsmunu_jpsiphi_sel = SimpleSelection('bc2bsmunu_jpsiphi_sel',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2jpsiphi_sel, bach_mu_jpsiphi_sel],
                                     DecayDescriptors = ['B_c+ -> B_s0 mu+', 'B_c- -> B_s~0 mu-'],
                                     MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s )'%selection_dictionary['B_c+']
                                     )
bc2bsmunu_jpsiphi_sel_seq = SelectionSequence("bc2bsmunu_jpsiphi_sel_seq",TopSelection = bc2bsmunu_jpsiphi_sel)
selection_sequences.append(bc2bsmunu_jpsiphi_sel_seq)

bach_mu_dspi_sel = FilterSelection("bach_mu_dspi_sel",
                                     [StdLooseMuons],
                                     Code="('mu+' == ABSID) | ('mu-' == ABSID)"
)
bach_mu_dspi_sel_seq = SelectionSequence("bach_mu_dspi_sel_seq",TopSelection = bach_mu_dspi_sel)
selection_sequences.append(bach_mu_dspi_sel_seq)
bc2bsmunu_dspi_sel = SimpleSelection('bc2bsmunu_dspi_sel',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2dspi_sel, bach_mu_dspi_sel],
                                     DecayDescriptors = ['B_c+ -> B_s0 mu+', 'B_c- -> B_s~0 mu-'],
                                     MotherCut = '(VFASPF(VCHI2/VDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
                                     )
bc2bsmunu_dspi_sel_seq = SelectionSequence("bc2bsmunu_dspi_sel_seq",TopSelection = bc2bsmunu_dspi_sel)
selection_sequences.append(bc2bsmunu_dspi_sel_seq)
# add everything to the list of sequences to append

#done! Make the decay tree tuples.
Bc2BsMu_Bs2JpsiPhi = DecayTreeTuple("Bc2BsMu_Bs2JpsiPhi",
                                    Inputs = [bc2bsmunu_jpsiphi_sel_seq.outputLocation()],
                                    Decay = "[B_c+ -> ^((B_s0|B_s~0) -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K- ) ) ^mu+]CC")
Bc2BsMu_Bs2JpsiPhi.addBranches({
    'Bc':"^([B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC)",
    'Bs':'[B_c+ -> ^((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Jpsi':'[B_c+ -> ((B_s0|B_s~0) -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu1':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu2':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Phi':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K- ) ) mu+]CC',
    'K1':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K- ) ) mu+]CC',
    'K2':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K- ) ) mu+]CC',
    'Mu_bach':'[B_c+ -> ((B_s0|B_s~0) -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) ^mu+]CC'})

Bc2BsMu_Bs2DsPi = DecayTreeTuple("Bc2BsMu_Bs2DsPi",
                                 Inputs = [bc2bsmunu_dspi_sel_seq.outputLocation()],
                                 Decay = '[B_c+ -> ^((B_s0|B_s~0) -> ^((D_s- | D_s+) -> ^K- ^K+ ^(pi- | pi+ ) ) ^pi+ ) ^mu+]CC')
Bc2BsMu_Bs2DsPi.addBranches({
    'Bc':'^([B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC)',
    'Bs':'[B_c+ -> ^((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'Ds':'[B_c+ -> ((B_s0|B_s~0) -> ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'K1':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'K2':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) pi+ ) mu+]CC',
    'Pi':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) pi+ ) mu+]CC',
    'Pi_from_Bs':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) ^pi+ ) mu+]CC',
    'Mu_bach':'[B_c+ -> ((B_s0|B_s~0) -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) pi+ ) ^mu+]CC'
    })
#configure the tools for the DTTs
tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",                 
                 "TupleToolANNPID",
                 "TupleToolTrigger"
                 ]
triglist = ['L0MuonDecision',
            'L0HadronDecision',
            'Hlt1SingleMuonNoIPDecision',
	    'Hlt1TrackMVADecision',
	    'Hlt1TwoTrackMVADecision',
	    'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackAllL0Decision',
            'Hlt2SingleMuonDecision',
            'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision',
            'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision'            
	]


for tuple in [Bc2BsMu_Bs2JpsiPhi,Bc2BsMu_Bs2DsPi]:
    tuple.ToolList+=tupletoollist
    LoKi_Bc = tuple.Bc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bc")
    LoKi_Bc.Variables = {"PV_ETA" : "BPVETA",
                         "PHI" : "PHI",
                         "KEY": "KEY",
                         "CORR_M" : "BPVCORRM",
                        }
    LoKi_MuBach = tuple.Mu_bach.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuBach")
    LoKi_MuBach.Variables = {"NSHAREDMU":"NSHAREDMU"}
    if Bc2BsMu_Bs2JpsiPhi==tuple:
        LoKi_Mu1 = tuple.Mu1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu1")
        LoKi_Mu1.Variables = {"NSHAREDMU":"NSHAREDMU"}
        LoKi_Mu2 = tuple.Mu2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu2")
        LoKi_Mu2.Variables = {"NSHAREDMU":"NSHAREDMU"}
    ttnr = tuple.Bc.addTupleTool(TupleToolNeutrinoReco,name='ttnr')
    ttnr.Verbose=True
    ttnr.MotherMass = 6274.47*MeV#LHCb-PAPER-2020-003
    #http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/LHCb-PAPER-2020-003.html
    
    ttdtf=tuple.Bc.addTupleTool(TupleToolDecayTreeFitter("TTDTF"))
    ttdtf.Verbose = True
    ttdtf.constrainToOriginVertex =False
    if Bc2BsMu_Bs2JpsiPhi==tuple:
        ttdtf.daughtersToConstrain = ["J/psi(1S)",'phi(1020)','B_s0']
    else:
        ttdtf.daughtersToConstrain = ["D_s-",'B_s0']
        
    ttttBc = tuple.Bc.addTupleTool(TupleToolTISTOS,name="ttttBc")
    ttttBs = tuple.Bs.addTupleTool(TupleToolTISTOS,name="ttttBs")
    ttttMu = tuple.Mu_bach.addTupleTool(TupleToolTISTOS,name="ttttMu")
    for T in [ttttBc,ttttBs, ttttMu]:
        T.VerboseL0   = True
        T.VerboseHlt1 = True
        T.VerboseHlt2 = True
        T.TriggerList = triglist[:]

#done, configure DaVinci
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )
    
DaVinci().PrintFreq = 1000
DaVinci().TupleFile = "Bc_Tuples_060720_500evt.root"
DaVinci().EvtMax = 500
DaVinci().Simulation = False
# Set Luminosity information when not using MC
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().UserAlgorithms+=[s.sequence() for s in selection_sequences]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2JpsiPhi]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2DsPi]
DaVinci().InputType = 'DST'
#DaVinci().DataType = '2015'

