import os

def make_options(name,#savename of file
                 opts,#list of options
                 #dv2use,#path to DaVinci install
                 data):#data to process
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v45r4')
    myApp = GaudiExec()
    #myApp = DaVinci()
    #myApp.version = 'v45r4'
    myApp.directory = '/afs/cern.ch/work/t/tmcgrath/private/lbdevs/DaVinciDev_v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc9-opt'    

    return j

# DsPi data -> Bhadroncompleteevent
data_2015MD_dspi_path = "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/BHADRONCOMPLETEEVENT.DST"
dataset2015MD_dspi = BKQuery(path=data_2015MD_dspi_path).getDataset()
data_2015MU_dspi_path = "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/BHADRONCOMPLETEEVENT.DST"
dataset2015MU_dspi = BKQuery(path=data_2015MU_dspi_path).getDataset()
data_2016MD_dspi_path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/BHADRONCOMPLETEEVENT.DST"
dataset2016MD_dspi = BKQuery(path=data_2016MD_dspi_path).getDataset()
data_2016MU_dspi_path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/BHADRONCOMPLETEEVENT.DST"
dataset2016MU_dspi = BKQuery(path=data_2016MU_dspi_path).getDataset()

# JpsiPhi data -> Dimuon
data_2015MD_jpsiphi_path = "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/DIMUON.DST"
dataset2015MD_jpsiphi = BKQuery(path=data_2015MD_jpsiphi_path).getDataset()
data_2015MU_jpsiphi_path = "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/DIMUON.DST"
dataset2015MU_jpsiphi = BKQuery(path=data_2015MU_jpsiphi_path).getDataset()
data_2016MD_jpsiphi_path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST"
dataset2016MD_jpsiphi = BKQuery(path=data_2016MD_jpsiphi_path).getDataset()
data_2016MU_jpsiphi_path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST"
dataset2016MU_jpsiphi = BKQuery(path=data_2016MU_jpsiphi_path).getDataset()

# Run jobs
make_options("Bc_DsPi_2015_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/BcOptions_DsPi.py','/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2015.py'],dataset2015MU_dspi)
make_options("Bc_JpsiPhi_2015_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/BcOptions_JpsiPhi.py','/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2015.py'],dataset2015MU_jpsiphi)
make_options("Bc_DsPi_2016_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/BcOptions_DsPi.py','/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2016.py'],dataset2016MU_dspi)
make_options("Bc_JpsiPhi_2016_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/BcOptions_JpsiPhi.py','/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2016.py'],dataset2016MU_jpsiphi)


