from Gaudi.Configuration import *
from GaudiConf import IOHelper
files=[
    "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00146430/0000/00146430_00008318_1.leptonic.mdst",
    #"root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00146430/0000/00146430_00002405_1.leptonic.mdst",
    #"root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00146430/0000/00146430_00008217_1.leptonic.mdst",
    #"root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00146430/0000/00146430_00008225_1.leptonic.mdst",
    #"root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00146430/0000/00146430_00002731_1.leptonic.mdst",
    #"root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00146430/0000/00146430_00008150_1.leptonic.mdst",
    #"root://x509up_u129802@se.cis.gov.pl//dpm/cis.gov.pl/home/lhcb/LHCb/Collision18/LEPTONIC.MDST/00146430/0000/00146430_00005235_1.leptonic.mdst"
]
IOHelper().inputFiles(files,clear=True)
