from Gaudi.Configuration import *
from GaudiConf import IOHelper
# Stripping 34
files=[
    "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00077434/0000/00077434_00006401_1.bhadroncompleteevent.dst",
    "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00077434/0000/00077434_00006273_1.bhadroncompleteevent.dst"
]
IOHelper().inputFiles(files,clear=True)
