options for running DaVinci to produce Bc->Bs mu nu

Note: the test file right now is only on for Bs->JpsiPhi. Need to think about best way to separate for Ds pi an Jpsi phi modes.

To run

```
lb-run DaVinci/latest gaudirun.py BcOptions.py {year}.py
```

to run over the test dataset, add the dataset at the end

```
lb-run DaVinci/latest gaudirun.py BcOptions.py {year}.py test_dataset.py
```
