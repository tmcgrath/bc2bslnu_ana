from Gaudi.Configuration import *
from GaudiConf import IOHelper
# Stripping 34
files=[
    "root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/DIMUON.DST/00076476/0001/00076476_00010362_1.dimuon.dst",
    "root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/LHCb/Collision18/DIMUON.DST/00076476/0000/00076476_00007750_1.dimuon.dst"
]
IOHelper().inputFiles(files,clear=True)
