
## Functions to return Dictionary with isolation variables

def ConeVariablesDict(bachParticle, BsDecay):
    theDict = {
        "Cone_ANGLE"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEANGLE', -1.)".format(bachParticle, BsDecay),
        "Cone_MULT"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEMULT', -1.)".format(bachParticle, BsDecay),
        "Cone_PT"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPT', -1.)".format(bachParticle, BsDecay),
        "Cone_P"          : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEP', -1.)".format(bachParticle, BsDecay),
        "Cone_PX"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPX', -1.)".format(bachParticle, BsDecay),   
        "Cone_PY"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPY', -1.)".format(bachParticle, BsDecay),
        "Cone_PZ"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZ', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_P"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PT"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPTASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PX"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPXASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PY"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPYASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PZ"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZSYM', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaEta"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAETA', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaPhi"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_MULT_0.4"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MULT', -1.)".format(bachParticle, BsDecay),
        "NC_sPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_SPT', -1.)".format(bachParticle, BsDecay),
        "NC_vPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_VPT', -1.)".format(bachParticle, BsDecay),
        "NC_PX_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PX', -1.)".format(bachParticle, BsDecay),
        "NC_PY_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PY', -1.)".format(bachParticle, BsDecay),
        "NC_PZ_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZ', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PT_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PTASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_P_0.4"    : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PX_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PXASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PY_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PYASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PZ_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZASYM', -1.)".format(bachParticle, BsDecay),
        "NC_deltaEta_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAETA', -1.)".format(bachParticle, BsDecay),
        "NC_deltaPhi_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_IT_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_IT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PT_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PX_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PY_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PZ_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay)}
    return theDict

