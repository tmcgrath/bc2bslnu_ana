# Event type 14145400, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/BC2BSX_JPSIPHI.STRIP.DST/00177239/0000/00177239_00000006_1.bc2bsx_jpsiphi.strip.dst"],
                      clear=True)
