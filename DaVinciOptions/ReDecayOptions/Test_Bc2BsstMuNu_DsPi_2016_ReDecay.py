# Event type 14575200, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2016" )
DaVinci().DataType = '2016'
IOHelper().inputFiles(["root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2016/BC2BSX_DSPI.STRIP.DST/00176813/0000/00176813_00000014_1.bc2bsx_dspi.strip.dst"],clear=True)
