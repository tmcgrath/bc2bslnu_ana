## DaVinci Options for the Stripping Filtered ReDecay files

Only difference between the DaVinci options in this directory and the one before are:
* the re-stripping for MC is removed here, since the ReDecay samples are already stripping filtered
* it pulls events from a different location in the TES

To test the DaVinci tupling for ReDecay samples:
```bash
lb-run DaVinci/v46r8 gaudirun.py Test_Bc2<Bc_channel>_<Bs_channel>_<year>_ReDecay.py BcOptions_<Bc_channel>_<Bs_channel>.py
```
