# Event type 14575200, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/2017/BC2BSX_DSPI.STRIP.DST/00176807/0000/00176807_00000052_1.bc2bsx_dspi.strip.dst"],clear=True)
