# Event type 14545200, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2016" )
DaVinci().DataType = '2016'
IOHelper().inputFiles(["root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/2016/BC2BSX_JPSIPHI.STRIP.DST/00176792/0000/00176792_00000002_1.bc2bsx_jpsiphi.strip.dst"
],clear=True)
