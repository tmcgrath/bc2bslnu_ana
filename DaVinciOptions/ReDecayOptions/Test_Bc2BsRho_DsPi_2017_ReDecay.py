# Event type 14165409, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@storage01.lcg.cscs.ch:1094//pnfs/lcg.cscs.ch/lhcb/lhcb/MC/2017/BC2BSX_DSPI.STRIP.DST/00176949/0000/00176949_00000001_1.bc2bsx_dspi.strip.dst"],
                      clear=True)
