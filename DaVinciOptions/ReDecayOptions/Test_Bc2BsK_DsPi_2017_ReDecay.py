# Event type 14165003, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/2017/BC2BSX_DSPI.STRIP.DST/00176955/0000/00176955_00000044_1.bc2bsx_dspi.strip.dst",
                       #"root://x509up_u129802@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/BC2BSX_DSPI.STRIP.DST/00176955/0000/00176955_00000030_1.bc2bsx_dspi.strip.dst"
                       ],clear=True)
