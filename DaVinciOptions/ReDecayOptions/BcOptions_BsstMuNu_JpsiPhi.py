###################################################
## Functions                                      #
###################################################

ReDecay = True

def fillTuple( tuple, myBranches, myTriggerList, ifMC, bachelor ):
    
    # DecayTreeTuple
    from Configurables import DecayTreeTuple, TupleToolTrigger, TupleToolTISTOS, TupleToolRecoStats, TupleToolEventInfo, TupleToolDecay
    
    tuple.Branches = myBranches 
    
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",     
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    if ifMC == True:
        from Configurables import MCMatchObjP2MCRelator, TupleToolMCTruth, TupleToolMCBackgroundInfo
        tuple.ToolList += ["TupleToolMCTruth"]
        MCTruth = tuple.addTool(TupleToolMCTruth, name = "TupleToolMCTruth")
        MCTruth.ToolList += ["MCTupleToolHierarchy", "MCTupleToolReconstructed"]

        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
        MCTruth.addTool(MCMatchObjP2MCRelator, name = "MCMatchObjP2MCRelator")
        MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        
        tuple.ToolList += ["TupleToolMCBackgroundInfo"]
        tuple.addTool(TupleToolMCBackgroundInfo, name = "TupleToolMCBackgroundInfo")


    tuple.addTool(TupleToolDecay, name = 'MuM')
    tuple.addTool(TupleToolDecay, name = 'MuP')
    tuple.addTool(TupleToolDecay, name = 'Jpsi')
    tuple.addTool(TupleToolDecay, name = 'KaonP')
    tuple.addTool(TupleToolDecay, name = 'KaonM')
    tuple.addTool(TupleToolDecay, name = 'Phi')
    tuple.addTool(TupleToolDecay, name = 'Bach')
    tuple.addTool(TupleToolDecay, name = 'Bs')
    tuple.addTool(TupleToolDecay, name = 'Bc')

    from Configurables import TupleToolKinematic
    tuple.addTool(TupleToolKinematic)
    tuple.TupleToolKinematic.Verbose=True

    
    # TISTOS
    # Jpsi
    tuple.MuP.ToolList += [ "TupleToolTISTOS" ] 
    tuple.MuP.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.MuP.TupleToolTISTOS.VerboseL0   = True
    tuple.MuP.TupleToolTISTOS.VerboseHlt1 = True
    tuple.MuP.TupleToolTISTOS.VerboseHlt2 = True
    tuple.MuP.TupleToolTISTOS.TriggerList = myTriggerList
    tuple.MuM.ToolList += [ "TupleToolTISTOS" ] 
    tuple.MuM.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.MuM.TupleToolTISTOS.VerboseL0   = True
    tuple.MuM.TupleToolTISTOS.VerboseHlt1 = True
    tuple.MuM.TupleToolTISTOS.VerboseHlt2 = True
    tuple.MuM.TupleToolTISTOS.TriggerList = myTriggerList

    tuple.Jpsi.ToolList += [ "TupleToolTISTOS" ] 
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Jpsi.TupleToolTISTOS.VerboseL0   = True
    tuple.Jpsi.TupleToolTISTOS.VerboseHlt1 = True
    tuple.Jpsi.TupleToolTISTOS.VerboseHlt2 = True
    tuple.Jpsi.TupleToolTISTOS.TriggerList = myTriggerList

    # Phi
    tuple.KaonP.ToolList += [ "TupleToolTISTOS" ] 
    tuple.KaonP.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.KaonP.TupleToolTISTOS.VerboseL0   = True
    tuple.KaonP.TupleToolTISTOS.VerboseHlt1 = True
    tuple.KaonP.TupleToolTISTOS.VerboseHlt2 = True
    tuple.KaonP.TupleToolTISTOS.TriggerList = myTriggerList
    tuple.KaonM.ToolList += [ "TupleToolTISTOS" ] 
    tuple.KaonM.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.KaonM.TupleToolTISTOS.VerboseL0   = True
    tuple.KaonM.TupleToolTISTOS.VerboseHlt1 = True
    tuple.KaonM.TupleToolTISTOS.VerboseHlt2 = True
    tuple.KaonM.TupleToolTISTOS.TriggerList = myTriggerList
    
    tuple.Phi.ToolList += [ "TupleToolTISTOS" ] 
    tuple.Phi.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Phi.TupleToolTISTOS.VerboseL0   = True
    tuple.Phi.TupleToolTISTOS.VerboseHlt1 = True
    tuple.Phi.TupleToolTISTOS.VerboseHlt2 = True
    tuple.Phi.TupleToolTISTOS.TriggerList = myTriggerList

    # Bach mu/pi
    tuple.Bach.ToolList += [ "TupleToolTISTOS" ] 
    tuple.Bach.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Bach.TupleToolTISTOS.VerboseL0   = True
    tuple.Bach.TupleToolTISTOS.VerboseHlt1 = True
    tuple.Bach.TupleToolTISTOS.VerboseHlt2 = True
    tuple.Bach.TupleToolTISTOS.TriggerList = myTriggerList

    # Bs
    tuple.Bs.ToolList += [ "TupleToolTISTOS/BsTupleToolTISTOS" ] 
    tuple.Bs.addTool(TupleToolTISTOS("BsTupleToolTISTOS"))
    tuple.Bs.BsTupleToolTISTOS.VerboseL0   = True
    tuple.Bs.BsTupleToolTISTOS.VerboseHlt1 = True
    tuple.Bs.BsTupleToolTISTOS.VerboseHlt2 = True
    tuple.Bs.BsTupleToolTISTOS.TriggerList = myTriggerList

    # B
    tuple.Bc.ToolList += [ "TupleToolTISTOS" ] 
    tuple.Bc.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Bc.TupleToolTISTOS.VerboseL0   = True
    tuple.Bc.TupleToolTISTOS.VerboseHlt1 = True
    tuple.Bc.TupleToolTISTOS.VerboseHlt2 = True
    tuple.Bc.TupleToolTISTOS.TriggerList = myTriggerList
    
    # RecoStats to filling SpdMult, etc
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    # Mass constraint 
    from Configurables import TupleToolParticleReFit, OfflineVertexFitter
    tuple.addTool(TupleToolParticleReFit, name = "refittool_JpsiMassConstr")
    tuple.refittool_JpsiMassConstr.addTool(OfflineVertexFitter())
    tuple.refittool_JpsiMassConstr.OfflineVertexFitter.useResonanceVertex = True
    tuple.refittool_JpsiMassConstr.OfflineVertexFitter.applyDauMassConstraint = True
    
    tuple.ToolList += [ "TupleToolParticleReFit/refittool_JpsiMassConstr" ]

    # Decay Tree Fitter
    from Configurables import TupleToolDecayTreeFitter

    """
    Bs
    """
    tuple.Bs.ToolList +=  [ "TupleToolDecayTreeFitter/BsPVFit",          # fit with PV constraint
                            "TupleToolDecayTreeFitter/BsMassFit" ]       # fit with J/psi mass constraint
    
    tuple.Bs.addTool(TupleToolDecayTreeFitter("BsPVFit"))
    tuple.Bs.BsPVFit.Verbose = True
    tuple.Bs.BsPVFit.constrainToOriginVertex = True
    tuple.Bs.BsPVFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)" ]
    
    tuple.Bs.addTool(TupleToolDecayTreeFitter("BsMassFit"))
    tuple.Bs.BsMassFit.constrainToOriginVertex = False
    tuple.Bs.BsMassFit.Verbose = True
    tuple.Bs.BsMassFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)" ]

    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Bs=LoKi__Hybrid__TupleTool("LoKi_Bs")
    if ifMC == True:
        LoKi_Bs.Preambulo += ["from LoKiPhysMC.decorators import *",
                              "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bs.Variables = {"is_from_Bs_JpsiPhi" : "switch(  mcMatch (' [B_s0 => ( J/psi(1S) => mu+ mu- ) ( phi(1020) => K+ K-)]CC ', 1), 1, 0)",
                             "LOKI_FDCHI2"          : "BPVVDCHI2",
                             "LOKI_FDS"             : "BPVDLS",
                             "LOKI_DIRA"            : "BPVDIRA",
                             "LV01"                 : "LV01",
                             "LV02"                 : "LV02"
        }

    else:
        LoKi_Bs.Variables = {"LOKI_FDCHI2"          : "BPVVDCHI2",
                             "LOKI_FDS"             : "BPVDLS",
                             "LOKI_DIRA"            : "BPVDIRA",
                             "LV01"                 : "LV01",
                             "LV02"                 : "LV02"
        }
    tuple.Bs.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Bs"]
    tuple.Bs.addTool(LoKi_Bs)

    """
    Bc
    """
    tuple.Bc.ToolList +=  [ "TupleToolDecayTreeFitter/PVFit",          # fit with PV constraint
                           "TupleToolDecayTreeFitter/MassFit" ]       # fit with intermediate particle mass constraint
    
    tuple.Bc.addTool(TupleToolDecayTreeFitter("PVFit"))
    tuple.Bc.PVFit.Verbose = True
    tuple.Bc.PVFit.constrainToOriginVertex = True
    tuple.Bc.PVFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)", "B_s0" ]
    
    tuple.Bc.addTool(TupleToolDecayTreeFitter("MassFit"))
    tuple.Bc.MassFit.Verbose = True
    tuple.Bc.MassFit.constrainToOriginVertex = False
    tuple.Bc.MassFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)", "B_s0" ]
    

    #LoKi one
    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    
    SLVars = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LV01"                 : "LV01",
        "LV02"                 : "LV02",
        "CORR_M"               : "BPVCORRM"
    }

    LoKi_Bc.Variables = SLVars

    # Add MC truth matching
    if ifMC == True:
        LoKi_Bc.Preambulo += ["from LoKiPhysMC.decorators import *",
                             "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bc.Variables["is_from_Bc_BsstMuNu"] = "switch( mcMatch(  '[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsPi"] = "switch( mcMatch( '[ B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+ ]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsMuNu"] = "switch( mcMatch( '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC'  , 1  ) , 1 , 0 )"

    tuple.Bc.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Bc"]
    tuple.Bc.addTool(LoKi_Bc)

    #tuple.Bs.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Bc"]
    #tuple.Bs.addTool(LoKi_Bc)
    
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y",
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All) 

    #LoKi one
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple")
    LoKi_EvtTuple.VOID_Variables = {
        "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
        "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
        "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
        "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
        "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
        "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
        "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
        }

    tuple.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
    tuple.addTool(LoKi_EvtTuple)

def fillMCTuple( mctuple, myBranches):
    from Configurables import MCMatchObjP2MCRelator, MCTupleToolKinematic

    mctuple.Branches = myBranches

    mctupletoollist = ["MCTupleToolHierarchy",
                       "MCTupleToolReconstructed"]

    mctuple.ToolList+=mctupletoollist
    mctuple.addTool(MCTupleToolKinematic())
    mctuple.MCTupleToolKinematic.Verbose = True
    from Configurables import LoKi__Hybrid__TupleTool
    nPhotons = LoKi__Hybrid__TupleTool("LoKi_Photons")
    nPhotons.Preambulo +=["from LoKiPhysMC.decorators import *" ,"from LoKiPhysMC.functions import *"]
    nPhotons.Variables = {"nPhotons": "NINTREE(('gamma' == ABSID)) "}
    mctuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Photons"]
    mctuple.addTool(nPhotons) 

    default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
    rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

def ConeVariablesDict(bachParticle, BsDecay):
    theDict = {
        "Cone_ANGLE"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEANGLE', -1.)".format(bachParticle, BsDecay),
        "Cone_MULT"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEMULT', -1.)".format(bachParticle, BsDecay),
        "Cone_PT"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPT', -1.)".format(bachParticle, BsDecay),
        "Cone_P"          : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEP', -1.)".format(bachParticle, BsDecay),
        "Cone_PX"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPX', -1.)".format(bachParticle, BsDecay),   
        "Cone_PY"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPY', -1.)".format(bachParticle, BsDecay),
        "Cone_PZ"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZ', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_P"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PT"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPTASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PX"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPXASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PY"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPYASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PZ"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZSYM', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaEta"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAETA', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaPhi"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_MULT_0.4"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MULT', -1.)".format(bachParticle, BsDecay),
        "NC_sPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_SPT', -1.)".format(bachParticle, BsDecay),
        "NC_vPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_VPT', -1.)".format(bachParticle, BsDecay),
        "NC_PX_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PX', -1.)".format(bachParticle, BsDecay),
        "NC_PY_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PY', -1.)".format(bachParticle, BsDecay),
        "NC_PZ_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZ', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PT_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PTASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_P_0.4"    : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PX_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PXASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PY_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PYASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PZ_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZASYM', -1.)".format(bachParticle, BsDecay),
        "NC_deltaEta_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAETA', -1.)".format(bachParticle, BsDecay),
        "NC_deltaPhi_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_IT_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_IT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PT_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PX_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PY_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PZ_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay)}
    return theDict


###################################################
## Functions                                      #
###################################################

from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, DecayTreeTuple, MCDecayTreeTuple, OfflineVertexFitter
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
from Configurables import DaVinci

myTriggerList = [
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0MuonHighDecision",
    "L0HadronDecision",
    "L0PhotonDecision",
    
    "Hlt1DiMuonHighMassDecision",
    "Hlt1DiMuonLowMassDecision",
    "Hlt1SingleMuonNoIPDecision",
    "Hlt1SingleMuonHighPTDecision",
    
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",
    
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    "Hlt2TopoMu2BodyDecision",
    "Hlt2TopoMu3BodyDecision",
    "Hlt2TopoMu4BodyDecision",     

    "Hlt2DiMuonDecision",
    "Hlt2DiMuonLowMassDecision",
    "Hlt2DiMuonJPsiDecision",
    "Hlt2DiMuonJPsiHighPTDecision",
    "Hlt2DiMuonDetachedDecision",
    "Hlt2DiMuonDetachedHeavyDecision",
    "Hlt2DiMuonDetachedJPsiDecision",
    "Hlt2DiMuonNoPVDecision"
    ]


from Configurables import DaVinci

line_bsmu = "Bc2BsBc2BsMu_Jpsiphi"
line_bspi = "Bc2BsBc2BsPi_Jpsiphi"

bc_bsmu = "/Event/Bc2BsX_JpsiPhi.Strip/Phys/{0}/Particles".format(line_bsmu)
bc_bspi = "/Event/Bc2BsX_JpsiPhi.Strip/Phys/{0}/Particles".format(line_bspi)

"""
Bc2BsHTuple
"""
Bc2BsPiTuple = DecayTreeTuple("Bc2BsPi_Tuple")
Bc2BsPiTuple.Inputs = [bc_bspi]
Bc2BsPiTuple.Decay = "[ (B_c+ -> ^( B_s0 -> ^( J/psi(1S) -> ^mu+ ^mu-) ^( phi(1020) -> ^K+ ^K-) ) ^pi+), (B_c- -> ^( B_s0 -> ^( J/psi(1S) -> ^mu+ ^mu-) ^( phi(1020) -> ^K+ ^K-) ) ^pi-) ]"

Bc2BsPiBranches = {
     "MuP"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    ,"MuM"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"  
    ,"Jpsi" : "[ (B_c+ -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    ,"KaonP": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) pi-) ]"
    ,"KaonM": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) pi-) ]"
    ,"Phi"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) pi-) ]"
    ,"Bs"   : "[ (B_c+ -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    ,"Bach" : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^pi-) ]" 
    ,"Bc" : "[ ^(B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), ^(B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    }

fillTuple( Bc2BsPiTuple, Bc2BsPiBranches, myTriggerList, DaVinci().Simulation, "Pi" )

"""
Bc2BsMuTuple
"""
Bc2BsMuTuple = DecayTreeTuple("Bc2BsMu_Tuple")
Bc2BsMuTuple.Inputs = [bc_bsmu]
Bc2BsMuTuple.Decay = "[ (B_c+ -> ^( B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-) ) ^mu+), (B_c- -> ^( B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-) ) ^mu-) ]"

Bc2BsMuBranches = {
     "MuP"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    ,"MuM"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"  
    ,"Jpsi" : "[ (B_c+ -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    ,"KaonP": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) mu-) ]"
    ,"KaonM": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) mu-) ]"
    ,"Phi"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) mu-) ]"
    ,"Bs"   : "[ (B_c+ -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    ,"Bach" : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^mu-) ]" 
    ,"Bc" : "[ ^( B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), ^(B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    }

fillTuple( Bc2BsMuTuple, Bc2BsMuBranches, myTriggerList, DaVinci().Simulation, "Mu" )

"""
Bc2BsstMu MCTuple
"""

MCBc2BsstMuNuTuple = MCDecayTreeTuple("Bc2BsstMuNu_MCTuple",
                                     Inputs = [bc_bsmu],
                                     Decay = '[B_c+ => ^(B*_s0 => ^(B_s0 => ^(J/psi(1S) => ^mu+ ^mu-) ^(phi(1020) => ^K+ ^K- ) ) ^gamma ) ^mu+ ^nu_mu]CC')
MCBc2BsstMuNuBranches = {
    'Bc':"^([B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC)",
    'Bsstar':'[B_c+ => ^(B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC',
    'Bs':'[B_c+ => (B*_s0 => ^(B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC',
    'Jpsi':'[B_c+ => (B*_s0 => (B_s0 => ^(J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC',
    'MuP':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => ^mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC',
    'MuM':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ ^mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC',
    'Phi':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) ^(phi(1020) => K+ K- ) ) gamma ) mu+ nu_mu]CC',
    'KaonP':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => ^K+ K- ) ) gamma ) mu+ nu_mu]CC',
    'KaonM':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ ^K- ) ) gamma ) mu+ nu_mu]CC',
    'Gamma':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^gamma ) mu+ nu_mu]CC',
    'Bach':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) ^mu+ nu_mu]CC',
    'Nu_mu':'[B_c+ => (B*_s0 => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) gamma ) mu+ ^nu_mu]CC'
}

fillMCTuple(MCBc2BsstMuNuTuple, MCBc2BsstMuNuBranches)


SeqPreselBc2BsX = GaudiSequencer("SeqPreselBc2BsX")
SeqPreselBc2BsX.Members += [ Bc2BsPiTuple ]
SeqPreselBc2BsX.Members += [ Bc2BsMuTuple ] 

from Configurables import DecodeVeloRawBuffer
createVeloClusters = DecodeVeloRawBuffer("createVeloClusters")
createVeloClusters.DecodeToVeloLiteClusters = True
createVeloClusters.DecodeToVeloClusters     = True

from Configurables import RawBankToSTClusterAlg
createITClusters = RawBankToSTClusterAlg("createITClusters")
createITClusters.DetType     = "IT"


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Jpsi2MuMuLineFilters = LoKi_Filters (
    STRIP_Code = "HLT_PASS('StrippingBetaSBs2JpsiPhiDetachedLineDecision')",
    VOID_Code  = "CONTAINS('Rec/Vertex/Primary')>0.5"
    )

from Configurables import DaVinci
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000

if DaVinci().Simulation == False: # data
    DaVinci().TupleFile = "Bc2BsstMuNu_Bs2JpsiPhi_{0}.root".format(DaVinci().DataType)   
else: # MC
    DaVinci().TupleFile = "Bc2BsstMuNu_Bs2JpsiPhi_{0}_MC.root".format(DaVinci().DataType)

#DaVinci().EventPreFilters = Jpsi2MuMuLineFilters.filters ('Jpsi2MuMuLineFilters')
DaVinci().appendToMainSequence( [createVeloClusters] )
DaVinci().appendToMainSequence( [createITClusters] )

DaVinci().appendToMainSequence( [Bc2BsMuTuple] )            
if DaVinci().Simulation == True:
    DaVinci().appendToMainSequence( [MCBc2BsstMuNuTuple] ) 

# Get Luminosity
DaVinci().Lumi = not DaVinci().Simulation

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]
