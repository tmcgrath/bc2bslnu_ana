###################################################
## Functions                                      #
###################################################

def fillTuple( tuple, myBranches, myTriggerList, ifMC, bachelor ):
    # bachelor has to be either "Mu", or "Pi"
    
    # DecayTreeTuple
    from Configurables import DecayTreeTuple, TupleToolTrigger, TupleToolTISTOS, TupleToolRecoStats, TupleToolEventInfo, TupleToolDecay
    
    tuple.Branches = myBranches 
    
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",     
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo",
        #"TupleToolSLTools"
        ]


    if ifMC == True:
        from Configurables import MCMatchObjP2MCRelator, TupleToolMCTruth, TupleToolMCBackgroundInfo
        tuple.ToolList += ["TupleToolMCTruth"]
        MCTruth = tuple.addTool(TupleToolMCTruth, name = "TupleToolMCTruth")
        MCTruth.ToolList += ["MCTupleToolHierarchy", "MCTupleToolReconstructed"]

        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
        MCTruth.addTool(MCMatchObjP2MCRelator, name = "MCMatchObjP2MCRelator")
        MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        
        tuple.ToolList += ["TupleToolMCBackgroundInfo"]
        tuple.addTool(TupleToolMCBackgroundInfo, name = "TupleToolMCBackgroundInfo")

    tuple.addTool(TupleToolDecay, name = 'H1Ds')
    tuple.addTool(TupleToolDecay, name = 'H2Ds')
    tuple.addTool(TupleToolDecay, name = 'H3Ds')
    tuple.addTool(TupleToolDecay, name = 'Ds')
    tuple.addTool(TupleToolDecay, name = 'PionBs')
    tuple.addTool(TupleToolDecay, name = 'Bach')
    tuple.addTool(TupleToolDecay, name = 'Bs')
    tuple.addTool(TupleToolDecay, name = 'Bc')

    from Configurables import TupleToolKinematic
    tuple.addTool(TupleToolKinematic)
    tuple.TupleToolKinematic.Verbose=True
    
    # TISTOS
    # Ds final state particles
    tuple.H1Ds.ToolList += [ "TupleToolTISTOS" ] 
    tuple.H1Ds.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.H1Ds.TupleToolTISTOS.VerboseL0   = True
    tuple.H1Ds.TupleToolTISTOS.VerboseHlt1 = True
    tuple.H1Ds.TupleToolTISTOS.VerboseHlt2 = True
    tuple.H1Ds.TupleToolTISTOS.TriggerList = myTriggerList
    tuple.H2Ds.ToolList += [ "TupleToolTISTOS" ] 
    tuple.H2Ds.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.H2Ds.TupleToolTISTOS.VerboseL0   = True
    tuple.H2Ds.TupleToolTISTOS.VerboseHlt1 = True
    tuple.H2Ds.TupleToolTISTOS.VerboseHlt2 = True
    tuple.H2Ds.TupleToolTISTOS.TriggerList = myTriggerList
    tuple.H3Ds.ToolList += [ "TupleToolTISTOS" ] 
    tuple.H3Ds.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.H3Ds.TupleToolTISTOS.VerboseL0   = True
    tuple.H3Ds.TupleToolTISTOS.VerboseHlt1 = True
    tuple.H3Ds.TupleToolTISTOS.VerboseHlt2 = True
    tuple.H3Ds.TupleToolTISTOS.TriggerList = myTriggerList

    # Ds
    tuple.Ds.ToolList += [ "TupleToolTISTOS" ] 
    tuple.Ds.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Ds.TupleToolTISTOS.VerboseL0   = True
    tuple.Ds.TupleToolTISTOS.VerboseHlt1 = True
    tuple.Ds.TupleToolTISTOS.VerboseHlt2 = True
    tuple.Ds.TupleToolTISTOS.TriggerList = myTriggerList

    # Pi from Bs
    tuple.PionBs.ToolList += [ "TupleToolTISTOS" ] 
    tuple.PionBs.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.PionBs.TupleToolTISTOS.VerboseL0   = True
    tuple.PionBs.TupleToolTISTOS.VerboseHlt1 = True
    tuple.PionBs.TupleToolTISTOS.VerboseHlt2 = True
    tuple.PionBs.TupleToolTISTOS.TriggerList = myTriggerList

    # Bachelor muon / pion
    tuple.Bach.ToolList += [ "TupleToolTISTOS" ] 
    tuple.Bach.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Bach.TupleToolTISTOS.VerboseL0   = True
    tuple.Bach.TupleToolTISTOS.VerboseHlt1 = True
    tuple.Bach.TupleToolTISTOS.VerboseHlt2 = True
    tuple.Bach.TupleToolTISTOS.TriggerList = myTriggerList

    # Bs
    tuple.Bs.ToolList += [ "TupleToolTISTOS/BsTupleToolTISTOS" ] 
    tuple.Bs.addTool(TupleToolTISTOS("BsTupleToolTISTOS"))
    tuple.Bs.BsTupleToolTISTOS.VerboseL0   = True
    tuple.Bs.BsTupleToolTISTOS.VerboseHlt1 = True
    tuple.Bs.BsTupleToolTISTOS.VerboseHlt2 = True
    #tuple.Bs.BsTupleToolTISTOS.Verbose=True
    tuple.Bs.BsTupleToolTISTOS.TriggerList = myTriggerList

    # B
    tuple.Bc.ToolList += [ "TupleToolTISTOS" ] 
    tuple.Bc.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Bc.TupleToolTISTOS.VerboseL0   = True
    tuple.Bc.TupleToolTISTOS.VerboseHlt1 = True
    tuple.Bc.TupleToolTISTOS.VerboseHlt2 = True
    #tuple.Bc.TupleToolTISTOS.Verbose=True
    tuple.Bc.TupleToolTISTOS.TriggerList = myTriggerList
    
    # RecoStats to filling SpdMult, etc
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    # Mass constraint 
    from Configurables import TupleToolParticleReFit, OfflineVertexFitter
    tuple.addTool(TupleToolParticleReFit, name = "refittool_JpsiMassConstr")
    tuple.refittool_JpsiMassConstr.addTool(OfflineVertexFitter())
    tuple.refittool_JpsiMassConstr.OfflineVertexFitter.useResonanceVertex = True
    tuple.refittool_JpsiMassConstr.OfflineVertexFitter.applyDauMassConstraint = True
    
    tuple.ToolList += [ "TupleToolParticleReFit/refittool_JpsiMassConstr" ]

    # Decay Tree Fitter
    from Configurables import TupleToolDecayTreeFitter

    """
    Bs
    """
    tuple.Bs.ToolList +=  [ "TupleToolDecayTreeFitter/BsPVFit",          # fit with PV constraint
                            "TupleToolDecayTreeFitter/BsMassFit" ]       # fit with J/psi mass constraint
    
    tuple.Bs.addTool(TupleToolDecayTreeFitter("BsPVFit"))
    tuple.Bs.BsPVFit.Verbose = True
    tuple.Bs.BsPVFit.constrainToOriginVertex = True
    tuple.Bs.BsPVFit.daughtersToConstrain = [ "D_s+", "D_s-" ]
    
    tuple.Bs.addTool(TupleToolDecayTreeFitter("BsMassFit"))
    tuple.Bs.BsMassFit.Verbose = True
    tuple.Bs.BsMassFit.constrainToOriginVertex = False
    tuple.Bs.BsMassFit.daughtersToConstrain = [ "D_s+", "D_s-" ]

    # Add MC truth matching
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Bs=LoKi__Hybrid__TupleTool("LoKi_Bs")
    if ifMC == True:
        LoKi_Bs.Preambulo += ["from LoKiPhysMC.decorators import *",
                              "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bs.Variables = {"is_from_Bs_DsPi" : "switch(  mcMatch (' [ (B_s0|B_s~0) => (D_s- ==> K- K+ pi-) pi+ ]CC ', 1), 1, 0)",
                             "LOKI_FDCHI2"          : "BPVVDCHI2",
                             "LOKI_FDS"             : "BPVDLS",
                             "LOKI_DIRA"            : "BPVDIRA",
                             "LV01"                 : "LV01",
                             "LV02"                 : "LV02",
                             "PV_ETA"               : "BPVETA",
                             "PHI"                  : "PHI",
                             "KEY"                  : "KEY"
        }
    else:
        LoKi_Bs.Variables = {"LOKI_FDCHI2"          : "BPVVDCHI2",
                             "LOKI_FDS"             : "BPVDLS",
                             "LOKI_DIRA"            : "BPVDIRA",
                             "LV01"                 : "LV01",
                             "LV02"                 : "LV02",
                             "PV_ETA"               : "BPVETA",
                             "PHI"                  : "PHI",
                             "KEY"                  : "KEY"
        }
    tuple.Bs.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Bs"]
    tuple.Bs.addTool(LoKi_Bs)


    """
    B
    """
    tuple.Bc.ToolList +=  [ "TupleToolDecayTreeFitter/PVFit",          # fit with PV constraint
                           "TupleToolDecayTreeFitter/MassFit" ]       # fit with J/psi mass constraint
    
    tuple.Bc.addTool(TupleToolDecayTreeFitter("PVFit"))
    tuple.Bc.PVFit.Verbose = True
    tuple.Bc.PVFit.constrainToOriginVertex = True
    tuple.Bc.PVFit.daughtersToConstrain = [ "D_s+", "D_s-", "B_s0", "B_s~0" ]
    tuple.Bc.addTool(TupleToolDecayTreeFitter("MassFit"))
    tuple.Bc.MassFit.Verbose = True
    tuple.Bc.MassFit.constrainToOriginVertex = False
    tuple.Bc.MassFit.daughtersToConstrain = [ "D_s+", "D_s-", "B_s0", "B_s~0" ]
    
    # SL tools
    #from Configurables import TupleToolSLTools
    #tuple.Bc.ToolList += ["TupleToolSLTools"]
    #tuple.Bc.addTool(TupleToolSLTools, name='SLTools')
    #tuple.Bc.SLTools.Bmass = 6274.47

    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    
    SLVars = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LV01"                 : "LV01",
        "LV02"                 : "LV02",
        "PV_ETA"               : "BPVETA",
        "PHI"                  : "PHI",
        "KEY"                  : "KEY",
        "CORR_M"               : "BPVCORRM", 
        }

    LoKi_Bc.Variables = SLVars

    # Add MC truth matching
    if ifMC == True:
        LoKi_Bc.Preambulo += ["from LoKiPhysMC.decorators import *",
                             "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bc.Variables["is_from_Bc_BsRho"] = "switch( mcMatch(  '[ B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ pi0)]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsPi"] = "switch( mcMatch( '[ B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+ ]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsMuNu"] = "switch( mcMatch( '[ B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu ]CC'  , 1  ) , 1 , 0 )"

    tuple.Bc.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Bc"]
    tuple.Bc.addTool(LoKi_Bc)

    #tuple.Bs.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Bc"]
    #tuple.Bs.addTool(LoKi_Bc)
    
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    
    LoKi_All.Variables = {
        "ETA"           : "ETA",
        "Y"             : "Y",
        "LOKI_IPCHI2"   : "BPVIPCHI2()"
        }
    
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All) 

    #LoKi one
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple")
    LoKi_EvtTuple.VOID_Variables = {
        "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
        "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
        "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
        "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
        "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
        "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
        "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
        }

    tuple.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
    tuple.addTool(LoKi_EvtTuple)


def fillMCTuple( mctuple, myBranches):
    from Configurables import MCMatchObjP2MCRelator, MCTupleToolKinematic

    mctuple.Branches = myBranches

    mctupletoollist = ["MCTupleToolHierarchy",
                       "MCTupleToolReconstructed"]

    mctuple.ToolList+=mctupletoollist
    mctuple.addTool(MCTupleToolKinematic())
    mctuple.MCTupleToolKinematic.Verbose = True
    from Configurables import LoKi__Hybrid__TupleTool
    nPhotons = LoKi__Hybrid__TupleTool("LoKi_Photons")
    nPhotons.Preambulo +=["from LoKiPhysMC.decorators import *" ,"from LoKiPhysMC.functions import *"]
    nPhotons.Variables = {"nPhotons": "NINTREE(('gamma' == ABSID)) "}
    mctuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Photons"]
    mctuple.addTool(nPhotons) 

    default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
    rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
    
def ConeVariablesDict(bachParticle, BsDecay):
    theDict = {
        "Cone_ANGLE"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEANGLE', -1.)".format(bachParticle, BsDecay),
        "Cone_MULT"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEMULT', -1.)".format(bachParticle, BsDecay),
        "Cone_PT"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPT', -1.)".format(bachParticle, BsDecay),
        "Cone_P"          : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEP', -1.)".format(bachParticle, BsDecay),
        "Cone_PX"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPX', -1.)".format(bachParticle, BsDecay),   
        "Cone_PY"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPY', -1.)".format(bachParticle, BsDecay),
        "Cone_PZ"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZ', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_P"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PT"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPTASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PX"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPXASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PY"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPYASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PZ"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZSYM', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaEta"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAETA', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaPhi"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_MULT_0.4"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MULT', -1.)".format(bachParticle, BsDecay),
        "NC_sPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_SPT', -1.)".format(bachParticle, BsDecay),
        "NC_vPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_VPT', -1.)".format(bachParticle, BsDecay),
        "NC_PX_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PX', -1.)".format(bachParticle, BsDecay),
        "NC_PY_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PY', -1.)".format(bachParticle, BsDecay),
        "NC_PZ_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZ', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PT_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PTASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_P_0.4"    : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PX_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PXASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PY_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PYASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PZ_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZASYM', -1.)".format(bachParticle, BsDecay),
        "NC_deltaEta_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAETA', -1.)".format(bachParticle, BsDecay),
        "NC_deltaPhi_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_IT_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_IT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PT_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PX_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PY_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PZ_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay)}
    return theDict


###################################################
## Execution                                      #
###################################################

from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, DecayTreeTuple, MCDecayTreeTuple, OfflineVertexFitter, FilterDesktop, SubstitutePID
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
from PhysConf.Selections import SimpleSelection, FilterSelection, TupleSelection

myTriggerList = ['L0MuonDecision',
                'L0DiMuonDecision',
                'L0HadronDecision',
                'Hlt1SingleMuonNoIPDecision',
                'Hlt1TrackMVADecision',
                'Hlt1TwoTrackMVADecision',
                'Hlt1TrackMuonDecision',
                'Hlt1TrackAllL0Decision',
                'Hlt2SingleMuonDecision',
                'Hlt2TopoMu2BodyDecision',
                'Hlt2TopoMu3BodyDecision',
                'Hlt2TopoMu4BodyDecision',
                'Hlt2Topo2BodyDecision',
                'Hlt2Topo3BodyDecision',
                'Hlt2Topo4BodyDecision',
                "Hlt2PhiIncPhiDecision"
                ]


from Configurables import DaVinci

line_bsmu = "Bc2BsBc2BsMu_Dspi"
line_bspi = "Bc2BsBc2BsPi_Dspi"

bc_bsmu = AutomaticData("/Event/Bc2BsX_DsPi.Strip/Phys/{0}/Particles".format(line_bsmu))
bc_bspi = AutomaticData("/Event/Bc2BsX_DsPi.Strip/Phys/{0}/Particles".format(line_bspi))

selection_sequences = []
selection_dictionary = {
    'D_s-': {'mass_window': '200*MeV',
             'mom_T' : '1500*MeV'},
    'pi_from_B_s0' : {'mom_T' : '1000*MeV',
                     },
    'B_s0': {'vchi2ndf_cut': '10',
            },
   }

# Filter Bc->Bspi
bc2bspi_cuts = {
    '[ [ B_c+ -> ( B_s0 -> (D_s- -> K- K+ pi-) ^pi+ ) pi+ ]CC, [ B_c+ -> ( B_s~0 -> (D_s+ -> K+ K- pi+) ^pi- ) pi+ ]CC ]' : "(PT > %(mom_T)s)"%selection_dictionary['pi_from_B_s0'],
    '[ [ B_c+ -> ( B_s0 -> ^(D_s- -> K- K+ pi-) pi+ ) pi+ ]CC, [ B_c+ -> ( B_s~0 -> ^(D_s+ -> K+ K- pi+) pi- ) pi+ ]CC ]' : "(PT > %(mom_T)s) & (ADMASS('D_s+') < %(mass_window)s)"%selection_dictionary['D_s-'],
    '[ [ B_c+ -> ^( B_s0 -> (D_s- -> K- K+ pi-) pi+ ) pi+ ]CC, [ B_c+ -> ^( B_s~0 -> (D_s+ -> K+ K- pi+) pi- ) pi+ ]CC ]' : "(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['B_s0'],
}
all_bspi_cuts = ["CHILDCUT({0}, '{1}')".format("("+bc2bspi_cuts[selector]+")", selector) for selector in bc2bspi_cuts]
bc2bspi_dspi_sel = FilterSelection("bc2bspi_dspi_sel",
                                    [bc_bspi],
                                    Code = " & ".join(all_bspi_cuts)
                                  )
bc2bspi_dspi_sel_seq = SelectionSequence("bc2bspi_dspi_sel_seq",TopSelection = bc2bspi_dspi_sel)
selection_sequences.append(bc2bspi_dspi_sel_seq)

# Filter Bc->Bsmu
bc2bsmu_cuts = {
    '[ [ B_c+ -> ( B_s0 -> (D_s- -> K- K+ pi-) ^pi+ ) mu+ ]CC, [ B_c+ -> ( B_s~0 -> (D_s+ -> K+ K- pi+) ^pi- ) mu+ ]CC ]' : "(PT > %(mom_T)s)"%selection_dictionary['pi_from_B_s0'],
    '[ [ B_c+ -> ( B_s0 -> ^(D_s- -> K- K+ pi-) pi+ ) mu+ ]CC, [ B_c+ -> ( B_s~0 -> ^(D_s+ -> K+ K- pi+) pi- ) mu+ ]CC ]' : "(PT > %(mom_T)s) & (ADMASS('D_s+') < %(mass_window)s)"%selection_dictionary['D_s-'],
    '[ [ B_c+ -> ^( B_s0 -> (D_s- -> K- K+ pi-) pi+ ) mu+ ]CC, [ B_c+ -> ^( B_s~0 -> (D_s+ -> K+ K- pi+) pi- ) mu+ ]CC ]' : "(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)"%selection_dictionary['B_s0'],
    '[ [ B_c+ -> ( B_s0 -> (D_s- -> K- K+ pi-) pi+ ) ^mu+ ]CC, [ B_c+ -> ( B_s~0 -> (D_s+ -> K+ K- pi+) pi- ) ^mu+ ]CC ]' : "ISMUON",
}
all_bsmu_cuts = ["CHILDCUT({0}, '{1}')".format("("+bc2bsmu_cuts[selector]+")", selector) for selector in bc2bsmu_cuts]
bc2bsmu_dspi_sel = FilterSelection("bc2bsmu_dspi_sel",
                                    [bc_bsmu],
                                    Code = " & ".join(all_bsmu_cuts)
                                  )
bc2bsmu_dspi_sel_seq = SelectionSequence("bc2bsmu_dspi_sel_seq",TopSelection = bc2bsmu_dspi_sel)
selection_sequences.append(bc2bsmu_dspi_sel_seq)

"""
Bc2BsPiTuple
"""
Bc2BsPiTuple = DecayTreeTuple("Bc2BsPi_Tuple")
Bc2BsPiTuple.Inputs = [ bc2bspi_dspi_sel_seq.outputLocation() ]
Bc2BsPiTuple.Decay = "[ [ B_c+ -> ^(B_s0 -> ^(D_s- -> ^K- ^K+ ^pi-) ^pi+ ) ^pi+ ]CC, [ B_c+ -> ^( B_s~0 -> ^(D_s+ -> ^K+ ^K- ^pi+) ^pi- ) ^pi+ ]CC ]"

Bc2BsPiBranches = {
     "H1Ds"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- -> ^K-  K+  pi- ) pi+ )  pi+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ -> ^K+  K-  pi+ ) pi- ) pi+ ]CC]"
    ,"H2Ds"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K- ^K+  pi- ) pi+ )  pi+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+ ^K-  pi+ ) pi- ) pi+ ]CC]"
    ,"H3Ds"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+ ^pi- ) pi+ )  pi+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K- ^pi+ ) pi- ) pi+ ]CC]"
    ,"Ds"     : "[ [ B_c+ -> ( B_s0 -> ^( D_s- ->  K-  K+  pi- ) pi+ )  pi+ ]CC, [ B_c+ -> ( B_s~0 -> ^( D_s+ ->  K+  K-  pi+ ) pi- ) pi+ ]CC]"
    ,"PionBs" : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+  pi- )^pi+ )  pi+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K-  pi+ )^pi- ) pi+ ]CC]"
    ,"Bs"     : "[ [ B_c+ -> ^(B_s0 -> ( D_s- ->  K-  K+  pi- ) pi+ )  pi+ ]CC, [ B_c+ -> ^(B_s~0 -> ( D_s+ ->  K+  K-  pi+ ) pi- ) pi+ ]CC]"
    ,"Bach"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+  pi- ) pi+ ) ^pi+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K-  pi+ ) pi- )^pi+ ]CC]"  
    ,"Bc"      : "[ ^([ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+  pi- ) pi+ ) pi+ ]CC), ^([ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K-  pi+ ) pi- ) pi+ ]CC)]"
     }

fillTuple( Bc2BsPiTuple, Bc2BsPiBranches, myTriggerList, DaVinci().Simulation, "Pi" )


"""
Bc2BsMuTuple
"""
Bc2BsMuTuple = DecayTreeTuple("Bc2BsMu_Tuple")
Bc2BsMuTuple.Inputs = [ bc2bsmu_dspi_sel_seq.outputLocation() ]
Bc2BsMuTuple.Decay = "[ [ B_c+ -> ^(B_s0 -> ^(D_s- -> ^K- ^K+ ^pi-) ^pi+ ) ^mu+ ]CC, [ B_c+ -> ^( B_s~0 -> ^(D_s+ -> ^K+ ^K- ^pi+) ^pi- ) ^mu+ ]CC ]"

Bc2BsMuBranches = {
     "H1Ds"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- -> ^K-  K+  pi- ) pi+ )  mu+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ -> ^K+  K-  pi+ ) pi- ) mu+ ]CC]"
    ,"H2Ds"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K- ^K+  pi- ) pi+ )  mu+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+ ^K-  pi+ ) pi- ) mu+ ]CC]"
    ,"H3Ds"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+ ^pi- ) pi+ )  mu+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K- ^pi+ ) pi- ) mu+ ]CC]"
    ,"Ds"     : "[ [ B_c+ -> ( B_s0 -> ^( D_s- ->  K-  K+  pi- ) pi+ )  mu+ ]CC, [ B_c+ -> ( B_s~0 -> ^( D_s+ ->  K+  K-  pi+ ) pi- ) mu+ ]CC]"
    ,"PionBs" : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+  pi- )^pi+ )  mu+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K-  pi+ ) ^pi- ) mu+ ]CC]"
    ,"Bs"     : "[ [ B_c+ -> ^(B_s0 -> ( D_s- ->  K-  K+  pi- ) pi+ )  mu+ ]CC, [ B_c+ -> ^(B_s~0 -> ( D_s+ ->  K+  K-  pi+ ) pi- ) mu+ ]CC]"
    ,"Bach"   : "[ [ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+  pi- ) pi+ ) ^mu+ ]CC, [ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K-  pi+ ) pi- ) ^mu+ ]CC]"  
    ,"Bc"      : "[ ^([ B_c+ -> ( B_s0 -> ( D_s- ->  K-  K+  pi- ) pi+ ) mu+ ]CC), ^([ B_c+ -> ( B_s~0 -> ( D_s+ ->  K+  K-  pi+ ) pi- ) mu+ ]CC)]"
     }

fillTuple( Bc2BsMuTuple, Bc2BsMuBranches, myTriggerList, DaVinci().Simulation, "Mu" )

"""
Bc2BsRho MCTuple
"""

MCBc2BsRhoTuple = MCDecayTreeTuple("Bc2BsRho_MCTuple",
                                     Inputs = [bc2bsmu_dspi_sel_seq.outputLocation()],
                                     Decay = '[B_c+ => ^(B_s0 => ^((D_s- | D_s+) ==> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^(rho(770)+ => ^pi+ ^pi0)]CC')
MCBc2BsRhoBranches = {
    'Bc':'^([B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ pi0) ]CC)',
    'Bs':'[B_c+ => ^(B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ pi0) ]CC',
    'Ds':'[B_c+ => (B_s0 => ^((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ pi0) ]CC',
    'H1Ds':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ pi0) ]CC',
    'H2Ds':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ pi0) ]CC',
    'H3Ds':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ pi0) ]CC',
    'PionBs':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) (rho(770)+ => pi+ pi0) ]CC',
    'Rho':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^(rho(770)+ => pi+ pi0) ]CC',
    'Pi_from_Rho': '[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => ^pi+ pi0) ]CC',
    'Pi0_from_Rho': '[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) (rho(770)+ => pi+ ^pi0) ]CC' 
}

fillMCTuple(MCBc2BsRhoTuple, MCBc2BsRhoBranches)


from Configurables import DecodeVeloRawBuffer
createVeloClusters = DecodeVeloRawBuffer("createVeloClusters")
createVeloClusters.DecodeToVeloLiteClusters = True
createVeloClusters.DecodeToVeloClusters     = True

from Configurables import RawBankToSTClusterAlg
createITClusters = RawBankToSTClusterAlg("createITClusters")
createITClusters.DetType     = "IT"


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Bc2BsPiLineFilters = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('StrippingBc2BsBc2BsPi_DspiDecision')")
#    VOID_Code  = "CONTAINS('Rec/Vertex/Primary')>0.5"
#    )
#DaVinci().EventPreFilters = Bc2BsPiLineFilters.filters('Bc2BsPiLineFilters')

DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
if DaVinci().Simulation == False:
    DaVinci().TupleFile = "Bc2BsRho_Bs2DsPi_{0}.root".format(DaVinci().DataType)
else:
    DaVinci().TupleFile = "Bc2BsRho_Bs2DsPi_{0}_MC.root".format(DaVinci().DataType)
    #DaVinci().appendToMainSequence( [event_node_killer] )
    #DaVinci().appendToMainSequence( [sc.sequence()] )

DaVinci().appendToMainSequence( [createVeloClusters] )
DaVinci().appendToMainSequence( [createITClusters] )

DaVinci().appendToMainSequence( [ bc2bsmu_dspi_sel_seq.sequence()] )
DaVinci().appendToMainSequence( [Bc2BsMuTuple] )
if DaVinci().Simulation == True:
    DaVinci().appendToMainSequence( [MCBc2BsRhoTuple] )    

# Get Luminosity
DaVinci().Lumi = not DaVinci().Simulation

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]