# Event type 14545200, MagDown

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, CondDB
DaVinci().Simulation = True
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = '2017'
IOHelper().inputFiles(["root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/BC2BSX_JPSIPHI.STRIP.DST/00176786/0000/00176786_00000003_1.bc2bsx_jpsiphi.strip.dst",
                       #"root://x509up_u129802@se0003.m45.ihep.su//pnfs/m45.ihep.su/data/lhcb/MC/2017/BC2BSX_JPSIPHI.STRIP.DST/00176786/0000/00176786_00000007_1.bc2bsx_jpsiphi.strip.dst"
],clear=True)
