# Ntupling with DaVinci

The ntupling of data and MC (converting Stripped data and MC to ROOT files) is performed in `ganga`. The instructions for this are under [How to run on `ganga`](#how-to-run-on-ganga) but to run in terminal for testing and such:

### Data

```bash
lb-run DaVinci/v45r4 gaudirun.py Data/data.py BcOptions_{decay}.py Data/{year}.py (Optional)Data/{test_file}.py
```

### MC

```bash
lb-run DaVinci/v45r4 gaudirun.py MC/MC.py BcOptions_{decay}.py MC/{year+magnet_polarity}.py (Optional)MC/{test_file}.py
```

## How to run on `ganga`

### Basic `ganga` info

The starterkit chapters on ganga are a good start to understand all the commands to use:
https://lhcb.github.io/starterkit-lessons/first-analysis-steps/davinci-grid.html

Extra ganga documentation:
https://ganga.readthedocs.io/en/stable/UserGuide/JobManipulation.html

## Setup

Please `git clone` the analysis repository:
https://gitlab.cern.ch/tmcgrath/bc2bslnu_ana/-/tree/master

The relevant scripts are:
* [`BcOptions_DsPi.py`](./BcOptions_DsPi.py) and [`BcOptions_JpsiPhi.py`](./BcOptions_JpsiPhi.py) that tells DaVinci what to do in a python script
* Various ganga configuration files in [`Ganga`](./Ganga/)


### Local DaVinci Build

I found that the most pain free way to run ganga was to build a local version of DaVinci and compiling by:
```
lb-dev DaVinci/v45r8
cd ./DaVinciDev_v45r8
make
```
in your chosen directory. Please make a note of the path for later. The Davinci version can be changed (and this is probably quite outdated).

## Ganga Submission

The script to configure the DaVinci job is [`Ganga/ganga_DsPi.py`](./Ganga/ganga_DsPi.py) for $B_s^0 \to D_s^- \pi^+$ mode data and corresponding files for $B_s^0 \to J/\psi \phi$ and MC. There are othe ganga files for processing various other background channel MC files. Before submitting, please edit `myApp.directory` in `make_options` to the directory where you have `DaVinciDev_v45r8`.

To submit jobs on ganga enter a ganga session in the directory with the script and configure jobs by:
```python=
%ganga ganga_DsPi.py <year>
```
and specify the year to run as an argument. This should configure jobs for MagUp and MagDown.
You can check the job number of the jobs by entering `jobs`. Submit the relevant one by
```python=
jobs(<job_number>).submit()
```
For other ganga scripts may have different arguments so best to check before submitting.

### How to Get the Files

Once the job has completed, we can save all the PFNs from a job into a text file `<file_number>` with these commands in ganga:
```python=
j = jobs(<job_number>).backend.getOutputDataAccessURLs()
with open(<file_name>, "w") as f:
    for file in j:
        f.write(file+"\n")
```
### How to Delete Files

To delete all files in a job on the grid, do:
```python=
j = jobs(<job_number>)
for sj in j.subjobs: 
  for f in sj.outputfiles.get(DiracFile):
    if f.lfn:
      f.remove()
```
More tips and tricks in the [Ganga FAQ](https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/GangaLHCbFAQ).

