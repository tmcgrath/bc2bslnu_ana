# 
# ganga submission for restripping of background samples s29r2p2 (DV v42r11p2)
#

import os, argparse

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('year', type=int, choices=[2016, 2017, 2018],
                    help='Year of data-taking to run over')
args = parser.parse_args()
year = args.year

def make_options(name,#savename of file
                 opts,#list of options
                 data, #data to process
                 davinci_version # version of davinci
    ):
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v44r11p1', '.')
    myApp = GaudiExec()
    myApp.directory = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Ganga/DaVinciDev_{}".format(davinci_version)
    #myApp = DaVinci()
    #myApp.version = 'v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc62-opt'    

    return j

# Paths for each year

# 2018 Background MC
bc2bsk_dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14165003/ALLSTREAMS.DST"
bc2bsk_dspi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14165003/ALLSTREAMS.DST"
bc2bsk_jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14135001/ALLSTREAMS.DST"
bc2bsk_jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14135001/ALLSTREAMS.DST"

bc2bskst_dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14367100/ALLSTREAMS.DST"
bc2bskst_dspi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14367100/ALLSTREAMS.DST"
bc2bskst_jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14347100/ALLSTREAMS.DST"
bc2bskst_jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14347100/ALLSTREAMS.DST"

bc2bsrho_dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14165409/ALLSTREAMS.DST"
bc2bsrho_dspi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14165409/ALLSTREAMS.DST"
bc2bsrho_jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14145400/ALLSTREAMS.DST"
bc2bsrho_jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14145400/ALLSTREAMS.DST"

bc2bsstmunu_dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14575200/ALLSTREAMS.DST"
bc2bsstmunu_dspi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14575200/ALLSTREAMS.DST"
bc2bsstmunu_jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14545200/ALLSTREAMS.DST"
bc2bsstmunu_jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14545200/ALLSTREAMS.DST"

# 2017 Background MC
bc2bsk_dspi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14165003/ALLSTREAMS.DST"
bc2bsk_dspi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14165003/ALLSTREAMS.DST"
bc2bsk_jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135001/ALLSTREAMS.DST"
bc2bsk_jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135001/ALLSTREAMS.DST"

bc2bskst_dspi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14367100/ALLSTREAMS.DST"
bc2bskst_dspi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14367100/ALLSTREAMS.DST"
bc2bskst_jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14347100/ALLSTREAMS.DST"
bc2bskst_jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14347100/ALLSTREAMS.DST"

bc2bsrho_dspi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14165409/ALLSTREAMS.DST"
bc2bsrho_dspi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14165409/ALLSTREAMS.DST"
bc2bsrho_jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14145400/ALLSTREAMS.DST"
bc2bsrho_jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14145400/ALLSTREAMS.DST"

bc2bsstmunu_dspi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14575200/ALLSTREAMS.DST"
bc2bsstmunu_dspi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14575200/ALLSTREAMS.DST"
bc2bsstmunu_jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14545200/ALLSTREAMS.DST"
bc2bsstmunu_jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14545200/ALLSTREAMS.DST"

# 2016 Background MC
bc2bsk_dspi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14165003/ALLSTREAMS.DST"
bc2bsk_dspi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14165003/ALLSTREAMS.DST"
bc2bsk_jpsiphi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14135001/ALLSTREAMS.DST"
bc2bsk_jpsiphi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14135001/ALLSTREAMS.DST"

bc2bskst_dspi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14367100/ALLSTREAMS.DST"
bc2bskst_dspi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14367100/ALLSTREAMS.DST"
bc2bskst_jpsiphi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14347100/ALLSTREAMS.DST"
bc2bskst_jpsiphi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14347100/ALLSTREAMS.DST"

bc2bsrho_dspi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14165409/ALLSTREAMS.DST"
bc2bsrho_dspi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14165409/ALLSTREAMS.DST"
bc2bsrho_jpsiphi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14145400/ALLSTREAMS.DST"
bc2bsrho_jpsiphi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14145400/ALLSTREAMS.DST"

bc2bsstmunu_dspi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14575200/ALLSTREAMS.DST"
bc2bsstmunu_dspi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14575200/ALLSTREAMS.DST"
bc2bsstmunu_jpsiphi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14545200/ALLSTREAMS.DST"
bc2bsstmunu_jpsiphi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14545200/ALLSTREAMS.DST"


if year == 2018:
    dv_version = "v44r11p1"

    dataset_bc2bsk_dspi_MU = BKQuery(path=bc2bsk_dspi_2018MU_path).getDataset()
    dataset_bc2bsk_dspi_MD = BKQuery(path=bc2bsk_dspi_2018MD_path).getDataset()
    dataset_bc2bsk_jpsiphi_MU = BKQuery(path=bc2bsk_jpsiphi_2018MU_path).getDataset()
    dataset_bc2bsk_jpsiphi_MD = BKQuery(path=bc2bsk_jpsiphi_2018MD_path).getDataset()

    dataset_bc2bskst_dspi_MU = BKQuery(path=bc2bskst_dspi_2018MU_path).getDataset()
    dataset_bc2bskst_dspi_MD = BKQuery(path=bc2bskst_dspi_2018MD_path).getDataset()
    dataset_bc2bskst_jpsiphi_MU = BKQuery(path=bc2bskst_jpsiphi_2018MU_path).getDataset()
    dataset_bc2bskst_jpsiphi_MD = BKQuery(path=bc2bskst_jpsiphi_2018MD_path).getDataset()

    dataset_bc2bsrho_dspi_MU = BKQuery(path=bc2bsrho_dspi_2018MU_path).getDataset()
    dataset_bc2bsrho_dspi_MD = BKQuery(path=bc2bsrho_dspi_2018MD_path).getDataset()
    dataset_bc2bsrho_jpsiphi_MU = BKQuery(path=bc2bsrho_jpsiphi_2018MU_path).getDataset()
    dataset_bc2bsrho_jpsiphi_MD = BKQuery(path=bc2bsrho_jpsiphi_2018MD_path).getDataset()

    dataset_bc2bsstmunu_dspi_MU = BKQuery(path=bc2bsstmunu_dspi_2018MU_path).getDataset()
    dataset_bc2bsstmunu_dspi_MD = BKQuery(path=bc2bsstmunu_dspi_2018MD_path).getDataset()
    dataset_bc2bsstmunu_jpsiphi_MU = BKQuery(path=bc2bsstmunu_jpsiphi_2018MU_path).getDataset()
    dataset_bc2bsstmunu_jpsiphi_MD = BKQuery(path=bc2bsstmunu_jpsiphi_2018MD_path).getDataset()

elif year == 2017:
    dv_version = "v42r11p2"

    dataset_bc2bsk_dspi_MU = BKQuery(path=bc2bsk_dspi_2017MU_path).getDataset()
    dataset_bc2bsk_dspi_MD = BKQuery(path=bc2bsk_dspi_2017MD_path).getDataset()
    dataset_bc2bsk_jpsiphi_MU = BKQuery(path=bc2bsk_jpsiphi_2017MU_path).getDataset()
    dataset_bc2bsk_jpsiphi_MD = BKQuery(path=bc2bsk_jpsiphi_2017MD_path).getDataset()

    dataset_bc2bskst_dspi_MU = BKQuery(path=bc2bskst_dspi_2017MU_path).getDataset()
    dataset_bc2bskst_dspi_MD = BKQuery(path=bc2bskst_dspi_2017MD_path).getDataset()
    dataset_bc2bskst_jpsiphi_MU = BKQuery(path=bc2bskst_jpsiphi_2017MU_path).getDataset()
    dataset_bc2bskst_jpsiphi_MD = BKQuery(path=bc2bskst_jpsiphi_2017MD_path).getDataset()

    dataset_bc2bsrho_dspi_MU = BKQuery(path=bc2bsrho_dspi_2017MU_path).getDataset()
    dataset_bc2bsrho_dspi_MD = BKQuery(path=bc2bsrho_dspi_2017MD_path).getDataset()
    dataset_bc2bsrho_jpsiphi_MU = BKQuery(path=bc2bsrho_jpsiphi_2017MU_path).getDataset()
    dataset_bc2bsrho_jpsiphi_MD = BKQuery(path=bc2bsrho_jpsiphi_2017MD_path).getDataset()

    dataset_bc2bsstmunu_dspi_MU = BKQuery(path=bc2bsstmunu_dspi_2017MU_path).getDataset()
    dataset_bc2bsstmunu_dspi_MD = BKQuery(path=bc2bsstmunu_dspi_2017MD_path).getDataset()
    dataset_bc2bsstmunu_jpsiphi_MU = BKQuery(path=bc2bsstmunu_jpsiphi_2017MU_path).getDataset()
    dataset_bc2bsstmunu_jpsiphi_MD = BKQuery(path=bc2bsstmunu_jpsiphi_2017MD_path).getDataset()
    
else: # 2016
    dv_version = "v44r11p4"

    dataset_bc2bsk_dspi_MU = BKQuery(path=bc2bsk_dspi_2016MU_path).getDataset()
    dataset_bc2bsk_dspi_MD = BKQuery(path=bc2bsk_dspi_2016MD_path).getDataset()
    dataset_bc2bsk_jpsiphi_MU = BKQuery(path=bc2bsk_jpsiphi_2016MU_path).getDataset()
    dataset_bc2bsk_jpsiphi_MD = BKQuery(path=bc2bsk_jpsiphi_2016MD_path).getDataset()

    dataset_bc2bskst_dspi_MU = BKQuery(path=bc2bskst_dspi_2016MU_path).getDataset()
    dataset_bc2bskst_dspi_MD = BKQuery(path=bc2bskst_dspi_2016MD_path).getDataset()
    dataset_bc2bskst_jpsiphi_MU = BKQuery(path=bc2bskst_jpsiphi_2016MU_path).getDataset()
    dataset_bc2bskst_jpsiphi_MD = BKQuery(path=bc2bskst_jpsiphi_2016MD_path).getDataset()

    dataset_bc2bsrho_dspi_MU = BKQuery(path=bc2bsrho_dspi_2016MU_path).getDataset()
    dataset_bc2bsrho_dspi_MD = BKQuery(path=bc2bsrho_dspi_2016MD_path).getDataset()
    dataset_bc2bsrho_jpsiphi_MU = BKQuery(path=bc2bsrho_jpsiphi_2016MU_path).getDataset()
    dataset_bc2bsrho_jpsiphi_MD = BKQuery(path=bc2bsrho_jpsiphi_2016MD_path).getDataset()

    dataset_bc2bsstmunu_dspi_MU = BKQuery(path=bc2bsstmunu_dspi_2016MU_path).getDataset()
    dataset_bc2bsstmunu_dspi_MD = BKQuery(path=bc2bsstmunu_dspi_2016MD_path).getDataset()
    dataset_bc2bsstmunu_jpsiphi_MU = BKQuery(path=bc2bsstmunu_jpsiphi_2016MU_path).getDataset()
    dataset_bc2bsstmunu_jpsiphi_MD = BKQuery(path=bc2bsstmunu_jpsiphi_2016MD_path).getDataset()


config  = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/{0}Tags/{1}.py"

# Run jobs for Bc->BsK
make_options("Bc2BsK_DsPi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsK_DsPi.py"], dataset_bc2bsk_dspi_MU, dv_version)
make_options("Bc2BsK_DsPi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsK_DsPi.py"], dataset_bc2bsk_dspi_MD, dv_version)
make_options("Bc2BsK_JpsiPhi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsK_JpsiPhi.py"], dataset_bc2bsk_jpsiphi_MU, dv_version)
make_options("Bc2BsK_JpsiPhi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsK_JpsiPhi.py"], dataset_bc2bsk_jpsiphi_MD, dv_version)

# Run jobs for Bc->BsKst
make_options("Bc2BsKst_DsPi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsKst_DsPi.py"], dataset_bc2bskst_dspi_MU, dv_version)
make_options("Bc2BsKst_DsPi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsKst_DsPi.py"], dataset_bc2bskst_dspi_MD, dv_version)
make_options("Bc2BsKst_JpsiPhi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsKst_JpsiPhi.py"], dataset_bc2bskst_jpsiphi_MU, dv_version)
make_options("Bc2BsKst_JpsiPhi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsKst_JpsiPhi.py"], dataset_bc2bskst_jpsiphi_MD, dv_version)

# Run jobs for Bc->BsRho
make_options("Bc2BsRho_DsPi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsRho_DsPi.py"], dataset_bc2bsrho_dspi_MU, dv_version)
make_options("Bc2BsRho_DsPi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsRho_DsPi.py"], dataset_bc2bsrho_dspi_MD, dv_version)
make_options("Bc2BsRho_JpsiPhi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsRho_JpsiPhi.py"], dataset_bc2bsrho_jpsiphi_MU, dv_version)
make_options("Bc2BsRho_JpsiPhi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsRho_JpsiPhi.py"], dataset_bc2bsrho_jpsiphi_MD, dv_version)

# Run jobs for Bc->BsstMuNu
make_options("Bc2BsstMuNu_DsPi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsstMuNu_DsPi.py"], dataset_bc2bsstmunu_dspi_MU, dv_version)
make_options("Bc2BsstMuNu_DsPi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsstMuNu_DsPi.py"], dataset_bc2bsstmunu_dspi_MD, dv_version)
make_options("Bc2BsstMuNu_JpsiPhi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsstMuNu_JpsiPhi.py"], dataset_bc2bsstmunu_jpsiphi_MU, dv_version)
make_options("Bc2BsstMuNu_JpsiPhi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsstMuNu_JpsiPhi.py"], dataset_bc2bsstmunu_jpsiphi_MD, dv_version)
