# 
# ganga submission for restripping of background samples s29r2p2 (DV v42r11p2)
#

import os, argparse

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('--year', '-y', required=False, type=int, default=2017, choices=[2016, 2017, 2018],
                    help='Year of data-taking to run over') # only 2017 available for Bc->Bsstpi
args = parser.parse_args()
year = args.year

def make_options(name,#savename of file
                 opts,#list of options
                 data, #data to process
                 davinci_version # version of davinci
    ):
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v44r11p1', '.')
    myApp = GaudiExec()
    myApp.directory = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Ganga/DaVinciDev_{}".format(davinci_version)
    #myApp = DaVinci()
    #myApp.version = 'v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc62-opt'    

    return j

# Paths for each year

# 2017 Background MC

bc2bsstpi_dspi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09j/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14265200/ALLSTREAMS.DST"
bc2bsstpi_dspi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09j/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14265200/ALLSTREAMS.DST"
bc2bsstpi_jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09j/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14145200/ALLSTREAMS.DST"
bc2bsstpi_jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09j/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14145200/ALLSTREAMS.DST"


if year == 2018:
    dv_version = "v44r11p1"

elif year == 2017:
    dv_version = "v42r11p2"

    dataset_bc2bsstpi_dspi_MU = BKQuery(path=bc2bsstpi_dspi_2017MU_path).getDataset()
    dataset_bc2bsstpi_dspi_MD = BKQuery(path=bc2bsstpi_dspi_2017MD_path).getDataset()
    dataset_bc2bsstpi_jpsiphi_MU = BKQuery(path=bc2bsstpi_jpsiphi_2017MU_path).getDataset()
    dataset_bc2bsstpi_jpsiphi_MD = BKQuery(path=bc2bsstpi_jpsiphi_2017MD_path).getDataset()
    
else: # 2016
    dv_version = "v44r11p4"


config  = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/{0}Tags/{1}.py"

# Run jobs for Bc->BsstPi
make_options("Bc2BsstPi_DsPi_MC_Sim09j_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsstPi_DsPi.py"], dataset_bc2bsstpi_dspi_MU, dv_version)
make_options("Bc2BsstPi_DsPi_MC_Sim09j_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsstPi_DsPi.py"], dataset_bc2bsstpi_dspi_MD, dv_version)
make_options("Bc2BsstPi_JpsiPhi_MC_Sim09j_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), "../BkgOptions/BcOptions_BsstPi_JpsiPhi.py"], dataset_bc2bsstpi_jpsiphi_MU, dv_version)
make_options("Bc2BsstPi_JpsiPhi_MC_Sim09j_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), "../BkgOptions/BcOptions_BsstPi_JpsiPhi.py"], dataset_bc2bsstpi_jpsiphi_MD, dv_version)
