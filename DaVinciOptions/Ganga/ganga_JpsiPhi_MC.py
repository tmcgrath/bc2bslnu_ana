#
# ganga submission for testing Bc->BsX and Bs->JpsiPhi davinci options
#

import os, argparse

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('year', type=int, choices=[2016, 2017, 2018],
                    help='Year of data-taking to run over')
args = parser.parse_args()
year = args.year

def make_options(name,#savename of file
                 opts,#list of options
                 data, #data to process
                 davinci_version # version of davinci
    ):
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v44r11p1', '.')
    myApp = GaudiExec()
    myApp.directory = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Ganga/DaVinciDev_{}".format(davinci_version)
    #myApp = DaVinci()
    #myApp.version = 'v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc62-opt'    

    return j

# Data paths for each year

# Bc2BsMuNu Sim09h
#2018
bc2bsmunu_bs2jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14545006/ALLSTREAMS.DST"
bc2bsmunu_bs2jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14545006/ALLSTREAMS.DST"
#2017
bc2bsmunu_bs2jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14545006/ALLSTREAMS.DST"
bc2bsmunu_bs2jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14545006/ALLSTREAMS.DST"
#2016
bc2bsmunu_bs2jpsiphi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14545006/ALLSTREAMS.DST"
bc2bsmunu_bs2jpsiphi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14545006/ALLSTREAMS.DST"

# Bc2BsPi Sim09k
#2018
bc2bspi_bs2jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
bc2bspi_bs2jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
#2017
bc2bspi_bs2jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
bc2bspi_bs2jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
#2016
bc2bspi_bs2jpsiphi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
bc2bspi_bs2jpsiphi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"

if year == 2018:
    dv_version = "v44r11p1"
    dataset_bc2bsmunu_MU = BKQuery(path=bc2bsmunu_bs2jpsiphi_2018MU_path).getDataset()
    dataset_bc2bsmunu_MD = BKQuery(path=bc2bsmunu_bs2jpsiphi_2018MD_path).getDataset()
    dataset_bc2bspi_MU   = BKQuery(path=bc2bspi_bs2jpsiphi_2018MU_path).getDataset()
    dataset_bc2bspi_MD   = BKQuery(path=bc2bspi_bs2jpsiphi_2018MD_path).getDataset()
elif year == 2017:
    dv_version = "v42r11p2"
    dataset_bc2bsmunu_MU = BKQuery(path=bc2bsmunu_bs2jpsiphi_2017MU_path).getDataset()
    dataset_bc2bsmunu_MD = BKQuery(path=bc2bsmunu_bs2jpsiphi_2017MD_path).getDataset()
    dataset_bc2bspi_MU   = BKQuery(path=bc2bspi_bs2jpsiphi_2017MU_path).getDataset()
    dataset_bc2bspi_MD   = BKQuery(path=bc2bspi_bs2jpsiphi_2017MD_path).getDataset()
else: # 2016
    dv_version = "v44r11p4"
    dataset_bc2bsmunu_MU = BKQuery(path=bc2bsmunu_bs2jpsiphi_2016MU_path).getDataset()
    dataset_bc2bsmunu_MD = BKQuery(path=bc2bsmunu_bs2jpsiphi_2016MD_path).getDataset()
    dataset_bc2bspi_MU   = BKQuery(path=bc2bspi_bs2jpsiphi_2016MU_path).getDataset()
    dataset_bc2bspi_MD   = BKQuery(path=bc2bspi_bs2jpsiphi_2016MD_path).getDataset()


# Make options
options = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_JpsiPhi.py"
config  = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/{0}Tags/{1}.py"

# Bc2BsMuNu
#make_options("Bc2BsMuNu_JpsiPhi_MC_Sim09h_{}MU".format(str(year)), [config.format("Bc2BsMuNu", str(year)+"MU"), options], dataset_bc2bsmunu_MU, dv_version)
#make_options("Bc2BsMuNu_JpsiPhi_MC_Sim09h_{}MD".format(str(year)), [config.format("Bc2BsMuNu", str(year)+"MD"), options], dataset_bc2bsmunu_MD, dv_version)

# Bc2BsPi
make_options("Bc2BsPi_JpsiPhi_MC_Sim09k_{}MU".format(str(year)), [config.format("BKG", str(year)+"MU"), options], dataset_bc2bspi_MU, dv_version)
make_options("Bc2BsPi_JpsiPhi_MC_Sim09k_{}MD".format(str(year)), [config.format("BKG", str(year)+"MD"), options], dataset_bc2bspi_MD, dv_version)
