#
# ganga submission for testing Bc->BsX and Bs->JpsiPhi davinci options
#

import os, argparse

# Arguments
parser = argparse.ArgumentParser()
#parser.add_argument('--chan', '-c', type=str, choices=["DsPi", "JpsiPhi"],
#                    help='Bs deacy channel (DsPi, JpsiPhi)')
parser.add_argument('--year', '-y', type=int, choices=[2016, 2017, 2018],
                    help='Data taking year')     
args = parser.parse_args()
#chan = args.chan
year = args.year
#loose_cuts = args.loose_cuts

def make_options(name,#savename of file
                 opts,#list of options
                 data, #data to process
                 davinci_version # version of davinci
    ):
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v44r11p1', '.')
    myApp = GaudiExec()
    myApp.directory = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Ganga/DaVinciDev_{}".format(davinci_version)
    #myApp = DaVinci()
    #myApp.version = 'v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc62-opt'    

    return j

# Data paths for each year

# Bs2DsPi 13264021 Sim09g
# Bs_Dspi,KKpi=DecProdCut.dec
#bs2dspi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/13264021/ALLSTREAMS.MDST"
#bs2dspi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/13264021/ALLSTREAMS.MDST"
#bs2dspi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/13264021/ALLSTREAMS.DST"
#bs2dspi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/13264021/ALLSTREAMS.DST"
#bs2dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09g/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/13264021/ALLSTREAMS.DST"
#bs2dspi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/13264021/ALLSTREAMS.DST"#

# Bs2JpsiPhi 13144011 Sim09h 
# Bs_Jpsiphi,mm=CPV,update2016,DecProdCut
bs2jpsiphi_2016MU_path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/13144011/ALLSTREAMS.DST"
bs2jpsiphi_2016MD_path = "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/13144011/ALLSTREAMS.DST"
bs2jpsiphi_2017MU_path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/13144011/ALLSTREAMS.DST"
bs2jpsiphi_2017MD_path = "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/13144011/ALLSTREAMS.DST"
bs2jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09g/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/13144011/ALLSTREAMS.DST"
bs2jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/13144011/ALLSTREAMS.DST"

if year == 2018:
    dv_version = "v44r11p1"
    #dataset_bs2dspi_MU = BKQuery(path=bs2dspi_2018MU_path).getDataset()
    #dataset_bs2dspi_MD = BKQuery(path=bs2dspi_2018MD_path).getDataset()
    dataset_bs2jpsiphi_MU = BKQuery(path=bs2jpsiphi_2018MU_path).getDataset()
    dataset_bs2jpsiphi_MD = BKQuery(path=bs2jpsiphi_2018MD_path).getDataset()
    sim_jpsiphi = "g"
elif year == 2017:
    dv_version = "v42r11p2"
    #dataset_bs2dspi_MU = BKQuery(path=bs2dspi_2017MU_path).getDataset()
    #dataset_bs2dspi_MD = BKQuery(path=bs2dspi_2017MD_path).getDataset()
    dataset_bs2jpsiphi_MU = BKQuery(path=bs2jpsiphi_2017MU_path).getDataset()
    dataset_bs2jpsiphi_MD = BKQuery(path=bs2jpsiphi_2017MD_path).getDataset()
    sim_jpsiphi = "g"
else: # 2016
    dv_version = "v44r11p4"
    #dataset_bs2dspi_MU = BKQuery(path=bs2dspi_2016MU_path).getDataset()
    #dataset_bs2dspi_MD = BKQuery(path=bs2dspi_2016MD_path).getDataset()
    dataset_bs2jpsiphi_MU = BKQuery(path=bs2jpsiphi_2016MU_path).getDataset()
    dataset_bs2jpsiphi_MD = BKQuery(path=bs2jpsiphi_2016MD_path).getDataset()
    sim_jpsiphi = "c"

# Make options
loose_string=""
config  = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/{0}Tags/{1}.py"
#if chan == "DsPi":
#    options = f"/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BkgOptions/BcOptions_Prompt_Bs2DsPi{loose_string}.py"
#    make_options("Bs2DsPi_MC_Sim09{0}{1}_{2}_MU".format(sim_dspi, loose_string, str(year)), [config.format("Bc2BsMuNu", str(year)+"MU"), options], dataset_bs2dspi_MU, dv_version)
#    make_options("Bs2DsPi_MC_Sim09{0}{1}_{2}_MD".format(sim_dspi, loose_string, str(year)), [config.format("Bc2BsMuNu", str(year)+"MD"), options], dataset_bs2dspi_MD, dv_version)
#else:
options = f"/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BkgOptions/BcOptions_Prompt_Bs2JpsiPhi{loose_string}.py"
make_options("Bs2JpsiPhi_MC_Sim09{0}{1}_{2}_MU".format(sim_jpsiphi, loose_string, str(year)), [config.format("Bc2BsMuNu", str(year)+"MU"), options], dataset_bs2jpsiphi_MU, dv_version)
make_options("Bs2JpsiPhi_MC_Sim09{0}{1}_{2}_MD".format(sim_jpsiphi, loose_string, str(year)), [config.format("Bc2BsMuNu", str(year)+"MD"), options], dataset_bs2jpsiphi_MD, dv_version)

