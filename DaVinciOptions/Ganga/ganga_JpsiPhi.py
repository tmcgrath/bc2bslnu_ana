#
# ganga submission for testing Bc davinci options
#

import os, argparse

def make_options(name,#savename of file
                 opts,#list of options
                 #dv2use,#path to DaVinci install
                 data):#data to process
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v45r4')
    myApp = GaudiExec()
    #myApp = DaVinci()
    #myApp.version = 'v45r4'
    #TODO: chnage this
    myApp.directory = '/afs/cern.ch/work/t/tmcgrath/private/lbdevs/DaVinciDev_v45r8'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64_v2-centos7-gcc10-opt'    

    return j

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('year', type=int, choices=[2016, 2017, 2018],
                    help='Year of data-taking to run over')
args = parser.parse_args()
year = args.year


# data for each year
leptonic_2016MU_path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2p1/90000000/LEPTONIC.MDST"
leptonic_2016MD_path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2p1/90000000/LEPTONIC.MDST"

leptonic_2017MU_path = "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2p2/90000000/LEPTONIC.MDST"
leptonic_2017MD_path = "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2p2/90000000/LEPTONIC.MDST"

leptonic_2018MU_path = "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34r0p2/90000000/LEPTONIC.MDST"
leptonic_2018MD_path = "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34r0p2/90000000/LEPTONIC.MDST"

## Bs -> DsPi
# Options from Bc->BsPi discovery
if year == 2018:
    dataset_leptonic_MU = BKQuery(path=leptonic_2018MU_path).getDataset()
    dataset_leptonic_MD = BKQuery(path=leptonic_2018MD_path).getDataset()
    make_options("Bc2BsX_JpsiPhi_data_2018MU", ['../Data/2018.py', '../BcOptions_JpsiPhi.py'], dataset_leptonic_MU)
    make_options("Bc2BsX_JpsiPhi_data_2018MD", ['../Data/2018.py', '../BcOptions_JpsiPhi.py'], dataset_leptonic_MD)

elif year == 2017:
    dataset_leptonic_MU = BKQuery(path=leptonic_2017MU_path).getDataset()
    dataset_leptonic_MD = BKQuery(path=leptonic_2017MD_path).getDataset()
    make_options("Bc2BsX_JpsiPhi_data_2017MU", ['../Data/2017.py', '../BcOptions_JpsiPhi.py'], dataset_leptonic_MU)
    make_options("Bc2BsX_JpsiPhi_data_2017MD", ['../Data/2017.py', '../BcOptions_JpsiPhi.py'], dataset_leptonic_MD)

elif year == 2016:
    dataset_leptonic_MU = BKQuery(path=leptonic_2016MU_path).getDataset()
    dataset_leptonic_MD = BKQuery(path=leptonic_2016MD_path).getDataset()
    make_options("Bc2BsX_JpsiPhi_data_2016MU", ['../Data/2016.py', '../BcOptions_JpsiPhi.py'], dataset_leptonic_MU)
    make_options("Bc2BsX_JpsiPhi_data_2016MD", ['../Data/2016.py', '../BcOptions_JpsiPhi.py'], dataset_leptonic_MD)

