#
# ganga submission for 2018 stripping validation data
#

import os

def make_options(name,#savename of file
                 opts,#list of options
                 #dv2use,#path to DaVinci install
                 data):#data to process
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v45r4')
    myApp = GaudiExec()
    #myApp = DaVinci()
    #myApp.version = 'v45r4'
    myApp.directory = '/afs/cern.ch/work/t/tmcgrath/private/lbdevs/DaVinciDev_v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc9-opt'    

    return j

# 2018 stripping vlidation data
val_2018MD_bc2bsx_path = "/validation/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34r0p2/90000000/LEPTONIC.MDST"
dataset2018MD_validation = BKQuery(path=val_2018MD_bc2bsx_path).getDataset()

# Run jobs separately for DsPi and JpsiPhi
make_options("Bc2BsX_DsPi_2018_MD",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_DsPi_TheNewStrip.py'],dataset2018MD_validation)
make_options("Bc2BsX_JpsiPhi_2018_MD",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_JpsiPhi_TheNewStrip.py'],dataset2018MD_validation)

