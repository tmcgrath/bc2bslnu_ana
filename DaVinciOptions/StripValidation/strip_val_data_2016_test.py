# Bc->BsX

from Gaudi.Configuration import *
from GaudiConf import IOHelper 
from Configurables import CondDB, DaVinci
CondDB( LatestGlobalTagByDataType = "2016" )
DaVinci().DataType = "2016"
DaVinci().Simulation = False

# Stripping validation 2016 data
IOHelper().inputFiles(['root://x509up_u129802@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/LEPTONIC.MDST/00150912/0000/00150912_00000038_1.leptonic.mdst'],clear=True)

