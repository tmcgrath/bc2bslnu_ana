# Bc->BsX

from Gaudi.Configuration import *
from GaudiConf import IOHelper 
from Configurables import CondDB, DaVinci
CondDB( LatestGlobalTagByDataType = "2017" )
DaVinci().DataType = "2017"
DaVinci().Simulation = False

# Stripping validation 2017 data
IOHelper().inputFiles(['root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/validation/Collision17/LEPTONIC.MDST/00135279/0000/00135279_00000091_1.leptonic.mdst'],clear=True)

