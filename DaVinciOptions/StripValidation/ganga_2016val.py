#
# ganga submission for 2018 stripping validation data
#

import os

def make_options(name,#savename of file
                 opts,#list of options
                 #dv2use,#path to DaVinci install
                 data):#data to process
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v45r4')
    myApp = GaudiExec()
    #myApp = DaVinci()
    #myApp.version = 'v45r4'
    myApp.directory = '/afs/cern.ch/work/t/tmcgrath/private/lbdevs/DaVinciDev_v45r8'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64_v2-centos7-gcc10-opt'    

    return j

# 2018 stripping vlidation data
val_2016MU_bc2bsx_path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2p1/90000000/LEPTONIC.MDST"
dataset2016MU_validation = BKQuery(path=val_2016MU_bc2bsx_path).getDataset()

# Run jobs separately for DsPi and JpsiPhi
make_options("Bc2BsX_DsPi_2016_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2016.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_DsPi_NewStrip16.py'],dataset2016MU_validation)
make_options("Bc2BsX_JpsiPhi_2016_MU",['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2016.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_JpsiPhi_NewStrip16.py'],dataset2016MU_validation)

