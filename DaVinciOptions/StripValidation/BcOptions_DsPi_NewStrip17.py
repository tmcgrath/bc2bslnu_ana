##################################################
# Script to produce Bc-> Bs mu nu and Bc-> Bs pi
# with Bs -> Ds pi
# Using stripping from Bc->BsPi discovery 
# and some similar cuts
# Authors: Adam Davis, Tamaki Holly McGrath
##################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import FilterInTrees
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection, FilterSelection
from StandardParticles import StdLooseKaons, StdLooseMuons, StdLoosePions, StdAllNoPIDsMuons, StdAllNoPIDsPions
#stripping conf
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
#tuple tools
from Configurables import DecayTreeTuple, MCDecayTreeTuple, PrintDecayTreeTool
from DecayTreeTuple.Configuration import *
from Configurables import (TupleToolDecay, TupleToolTISTOS,  TupleToolNeutrinoReco,
                           TupleToolDecayTreeFitter, TupleToolTrigger, MCTupleToolKinematic,
                           TriggerTisTos, TupleToolDecayTreeFitter, TupleToolConeIsolation,
                           LoKi__Hybrid__TupleTool)

from GaudiKernel.SystemOfUnits import MeV

##############################################################################
#make a dictionary of cuts so we don't have to go searching
##############################################################################

selection_dictionary = {
    'K'   : {'mom_T': '100*MeV',
             'mom': '1000*MeV',
             'trackchi2': '4'},
    'pi'  : {'mom_T': '100*MeV',
             'mom': '1000*MeV',
             'trackchi2': '4'},
    'D_s-': {'mass_window': '200*MeV',
             'vchi2ndf_cut': '10',
             'mom_T': '1500*MeV',
             },
    'pi_from_B_s0' : {'mom_T' : '1700*MeV',
                      'mom' : '10000*MeV',
                      'trackchi2': '2.5',
                      'min_ipchi2': '1',
                     },
    'B_s0': {'vchi2ndf_cut': '10',
             'm_low': '5000*MeV',
             'm_high': '6000*MeV',
             'ltime': '0.000',
            },
    'mu+' : {'mom_T': '350*MeV',
             'trackchi2': '3',
             'min_ipchi2': '1',
            },
    'B_c+': {'vchi2ndf_cut': '100'}
   }
selection_sequences = []
if DaVinci().Simulation == False:
    (selection_dictionary['K'])['pidk'] = '-10'
    (selection_dictionary['pi'])['pidk'] = '20'
    (selection_dictionary['pi_from_B_s0'])['pidk'] = '10'

##############################################################################
# Selection sequences
##############################################################################

### NEED TO CHECK FORMAT ###

line_bsmu = "Bc2BsLine"
line_bspi = "Bc2BsLine"

# Ds
# Select Ds first, select kaons and pions from there, then recombine to Ds
if DaVinci().Simulation == False: # if data
    #bc_bsmu = "/Event/Leptonic/Phys/{0}/Particles".format(line_bsmu)
    #bc_bspi = "/Event/Leptonic/Phys/{0}/Particles".format(line_bspi)
    bc_bsmu = "Phys/{0}/Particles".format(line_bsmu)
    bc_bspi = "Phys/{0}/Particles".format(line_bspi)
else:
    bc_bsmu = "/Event/AllStreams/Phys/{0}/Particles".format(line_bsmu)
    bc_bspi = "/Event/AllStreams/Phys/{0}/Particles".format(line_bspi)


# Make Bc->Bsmunu          
bc2bsmunu_dspi_sel = FilterSelection('bc2bsmunu_dspi_sel',
                                     [AutomaticData(bc_bsmu)],
                                     DecayDescriptor = '[B_c+ -> B_s0 mu+]cc',
                                     Code = "('B_c+'==ABSID)"
)
bc2bsmunu_dspi_sel_seq = SelectionSequence("bc2bsmunu_dspi_sel_seq",TopSelection = bc2bsmunu_dspi_sel)
selection_sequences.append(bc2bsmunu_dspi_sel_seq)

# Make Bc->Bspi
bc2bspi_dspi_sel = FilterSelection('bc2bspi_dspi_sel',
                                   [AutomaticData(bc_bspi)],
                                   DecayDescriptor = '[B_c+ -> B_s0 pi+]cc',
                                   Code = "('B_c+'==ABSID)"
)
bc2bspi_dspi_sel_seq = SelectionSequence("bc2bspi_dspi_sel_seq",TopSelection = bc2bspi_dspi_sel)
selection_sequences.append(bc2bspi_dspi_sel_seq)


##############################################################################
# Make the decay tree tuples
##############################################################################

# Signal
Bc2BsMu_Bs2DsPi = DecayTreeTuple("Bc2BsMu_Bs2DsPi",
                                 Inputs = [bc2bsmunu_dspi_sel_seq.outputLocation()],
                                 Decay = '[B_c+ -> ^(B_s0 -> ^((D_s- | D_s+) -> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^mu+]CC')
Bc2BsMu_Bs2DsPi.addBranches({
    'Bc':'^([B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC)',
    'Bs':'[B_c+ -> ^(B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'Ds':'[B_c+ -> (B_s0 -> ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'K1':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'K2':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'Pi':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'Pi_from_Bs':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) mu+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^mu+]CC'
    })

# Norm
Bc2BsPi_Bs2DsPi = DecayTreeTuple("Bc2BsPi_Bs2DsPi",
                                 Inputs = [bc2bspi_dspi_sel_seq.outputLocation()],
                                 Decay = '[B_c+ -> ^(B_s0 -> ^((D_s- | D_s+) -> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^pi+]CC')
Bc2BsPi_Bs2DsPi.addBranches({
    'Bc':'^([B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC)',
    'Bs':'[B_c+ -> ^(B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'Ds':'[B_c+ -> (B_s0 -> ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'K1':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'K2':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'Pi':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'Pi_from_Bs':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) pi+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^pi+]CC'
    })

# MC decay tree tuple - added neutrino
if DaVinci().Simulation == True:
    # Signal
    MC_Bc2BsMu_Bs2DsPi = MCDecayTreeTuple("MC_Bc2BsMu_Bs2DsPi",
                                     Inputs = [bc2bsmunu_dspi_sel_seq.outputLocation()],
                                     Decay = '[B_c+ => ^(B_s0 => ^((D_s- | D_s+) ==> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^mu+ ^nu_mu]CC')
    MC_Bc2BsMu_Bs2DsPi.addBranches({
        'Bc':'^([B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC)',
        'Bs':'[B_c+ => ^(B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'Ds':'[B_c+ => (B_s0 => ^((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'K1':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'K2':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'Pi':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'Pi_from_Bs':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) mu+ nu_mu]CC',
        'Mu_bach':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^mu+ nu_mu]CC',
        'Nu_mu':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ ^nu_mu]CC'
        })


    # Norm
    MC_Bc2BsPi_Bs2DsPi = MCDecayTreeTuple("MC_Bc2BsPi_Bs2DsPi",
                                     Inputs = [bc2bspi_dspi_sel_seq.outputLocation()],
                                     Decay = '[B_c+ => ^(B_s0 => ^((D_s- | D_s+) ==> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^pi+]CC')
    MC_Bc2BsPi_Bs2DsPi.addBranches({
        'Bc':'^([B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC)',
        'Bs':'[B_c+ => ^(B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'Ds':'[B_c+ => (B_s0 => ^((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'K1':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'K2':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'Pi':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'Pi_from_Bs':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) pi+]CC',
        'Mu_bach':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^pi+]CC'
        })

##############################################################################
# Configure the tools for the DTTs
##############################################################################

tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",
                 "TupleToolPropertime",
                 "TupleToolANNPID",
                 "TupleToolTrigger"
                 ]

mctupletoollist = ["MCTupleToolHierarchy",
                   "MCTupleToolReconstructed",
                   #"MCTupleToolKinematic",
                   #"TupleToolMCTruth",
                   #"TupleToolMCBackgroundInfo",
                   #"MCTupleToolAngles",
		   #"LoKi::Hybrid::MCTupleTool/LoKi_Photons",
                  ]

triglist = ['L0MuonDecision',
            'L0HadronDecision',
            'Hlt1SingleMuonNoIPDecision',
            'Hlt1TrackMVADecision',
            'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackAllL0Decision',
            'Hlt2SingleMuonDecision',
            'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision',
            'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision'
        ]

# cone isolation tuple tool configuration
from Configurables import TupleToolConeIsolation
neutral = TupleToolConeIsolation()
neutral.FillIsolation = True
neutral.FillComponents = True
neutral.FillAsymmetry = True
neutral.FillDeltas = True
neutral.FillCharged = False
neutral.FillNeutral = True
neutral.MaxPtParticlesLocation = "Phys/StdAllNoPIDsMuons/Particles"

from Configurables import MCMatchObjP2MCRelator
# Just for MC
if DaVinci().Simulation == True:
    for mctuple in [MC_Bc2BsMu_Bs2DsPi, MC_Bc2BsPi_Bs2DsPi]:
        mctuple.ToolList+=mctupletoollist
        mctuple.addTool(MCTupleToolKinematic())
        mctuple.MCTupleToolKinematic.Verbose = True
        nPhotons = mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_Photons")
        nPhotons.Preambulo+=["from LoKiPhysMC.decorators import *" ,"from LoKiPhysMC.functions import *"]
        nPhotons.Variables = {"nPhotons": "MCNINTREE(('gamma' == MCABSID)) "}
        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]


for dttuple in [Bc2BsMu_Bs2DsPi, Bc2BsPi_Bs2DsPi]:

    dttuple.ToolList+=tupletoollist
    LoKi_Bc = dttuple.Bc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bc")
    LoKi_Bc.Preambulo += ["from LoKiPhysMC.decorators import *",
                          "from LoKiPhysMC.functions import mcMatch"]
    LoKi_Bc.Variables = {"PV_ETA" : "BPVETA",
                         "PHI" : "PHI",
                         "KEY": "KEY",
                         "CORR_M" : "BPVCORRM",
                        }

    # fill cone isolation variables
    dttuple.Bc.ToolList += ["TupleToolConeIsolation"] 
    dttuple.Bc.addTool(neutral)

    if DaVinci().Simulation == True:
        MCTruth = dttuple.addTupleTool('TupleToolMCTruth')
        MCTruth.ToolList += ['MCTupleToolKinematic']
        MCTruth.ToolList += ['MCTupleToolHierarchy']
        MCTruth.addTool(MCMatchObjP2MCRelator)
        MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        dttuple.addTupleTool('TupleToolMCBackgroundInfo')

        # MC Match variable
        LoKi_Bc.Variables["is_from_Bc_BsMuNu"] = "switch( mcMatch(  '[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsPi"] = "switch( mcMatch( '[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC'  , 1  ) , 1 , 0 )"
        
        LoKi_Bs = dttuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bs")
        LoKi_Bs.Preambulo+=["from LoKiPhysMC.decorators import *",
                            "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bs.Variables = {'is_from_Bs_DsPi' : "switch(  mcMatch (' [(B_s0|B_s~0) => (D_s- ==> K- K+ pi-) pi+]CC ', 1), 1, 0)"}

    if Bc2BsMu_Bs2DsPi==dttuple:
        LoKi_MuBach = dttuple.Mu_bach.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuBach")
        LoKi_MuBach.Variables = {"NSHAREDMU":"NSHAREDMU"}
        ttnr = dttuple.Bc.addTupleTool(TupleToolNeutrinoReco,name='ttnr')
        ttnr.Verbose=True
        ttnr.MotherMass = 6274.47*MeV#LHCb-PAPER-2020-003
        #http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/LHCb-PAPER-2020-003.html

    ttdtf=dttuple.Bc.addTupleTool(TupleToolDecayTreeFitter("TTDTF"))
    ttdtf.Verbose = True
    ttdtf.constrainToOriginVertex =False
    ttdtf.daughtersToConstrain = ["D_s-",'B_s0']

    ttttBc = dttuple.Bc.addTupleTool(TupleToolTISTOS,name="ttttBc")
    ttttBs = dttuple.Bs.addTupleTool(TupleToolTISTOS,name="ttttBs") 
    ttttMu = dttuple.Mu_bach.addTupleTool(TupleToolTISTOS,name="ttttMu")
    for T in [ttttBc,ttttBs, ttttMu]:
        T.VerboseL0   = True
        T.VerboseHlt1 = True
        T.VerboseHlt2 = True
        T.TriggerList = triglist[:]

##############################################################################
# Configure DaVinci
##############################################################################

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

# DaVinci config not needed in AP
DaVinci().PrintFreq = 1000 # Change to 1000 when all events
if DaVinci().Simulation == True:
    DaVinci().TupleFile = "BcTuples_MC_DsPi_StripVal17.root"
else:
    DaVinci().TupleFile = "BcTuples_data_DsPi_StripVal17.root"
DaVinci().EvtMax = 10000
# Set Luminosity information when not using MC
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().InputType = 'DST'

DaVinci().UserAlgorithms+=[s.sequence() for s in selection_sequences]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2DsPi]
DaVinci().UserAlgorithms+=[Bc2BsPi_Bs2DsPi]
if DaVinci().Simulation == True:
    DaVinci().UserAlgorithms+=[MC_Bc2BsMu_Bs2DsPi]
    DaVinci().UserAlgorithms+=[MC_Bc2BsPi_Bs2DsPi]


