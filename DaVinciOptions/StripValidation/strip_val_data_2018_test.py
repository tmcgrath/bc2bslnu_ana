# Bc->BsX

from Gaudi.Configuration import *
from GaudiConf import IOHelper 
from Configurables import CondDB, DaVinci
CondDB( LatestGlobalTagByDataType = "2018" )
DaVinci().DataType = "2018"
DaVinci().Simulation = False

# Stripping validation 2018 MD data
# lb-dirac dirac-dms-lfn-accessURL /lhcb/validation/Collision18/LEPTONIC.MDST/00145862/0000/00145862_00000024_1.leptonic.mdst
IOHelper().inputFiles(['root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/validation/Collision18/LEPTONIC.MDST/00145862/0000/00145862_00000024_1.leptonic.mdst'],clear=True)

