##################################################
# Script to produce Bc-> Bs mu nu and Bc-> Bs pi
# with Bs -> Jpsi phi
# Different stripping, all other particle cuts
# are same as in Bc->Bspi stripping & presel
# Authors: Adam Davis, Tamaki Holly McGrath
##################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import FilterInTrees, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection, FilterSelection
from StandardParticles import StdLooseMuons, StdLooseKaons, StdLoosePions, StdLooseDetachedPhi2KK, StdAllNoPIDsMuons, StdAllNoPIDsPions
#tuple tools
from Configurables import DecayTreeTuple, MCDecayTreeTuple, PrintDecayTreeTool, CheckPV
from DecayTreeTuple.Configuration import *
from Configurables import (TupleToolDecay, TupleToolTISTOS,  TupleToolNeutrinoReco,
                           TupleToolDecayTreeFitter, TupleToolTrigger, MCTupleToolKinematic,
                           TriggerTisTos, TupleToolDecayTreeFitter, TupleToolConeIsolation,
                           LoKi__Hybrid__TupleTool)

from GaudiKernel.SystemOfUnits import MeV

##############################################################################
#make a dictionary of cuts so we don't have to go searching
##############################################################################

selection_dictionary = {
    'K'   : {'trackchi2' : '3'},
    'mu'  : {'mom_T': '500*MeV',
             'trackchi2' : '3'},
    'phi(1020)': {'mom_T' : '1000*MeV',
                  'vchi2ndf_cut': '16'},
    'J/psi(1S)': {'mass_window' : '80*MeV', 
                  'vchi2ndf_cut':'16'},
    'B_s0': {'vchi2ndf_cut': '10',
             'ltime': '0.000', 
             'm_low': '5000*MeV',
             'm_high': '6000*MeV'}, # ltime in ns so 0.2 ps
    'mu+' : {'mom_T': '350*MeV',
             'trackchi2': '3',
             'min_ipchi2': '1',
            },
    'B_c+': {'vchi2ndf_cut': '100'}
    }
selection_sequences = []
if DaVinci().Simulation == False:
    (selection_dictionary['K'])['pidk'] = '-2'
    (selection_dictionary['mu'])['pidmu'] = '0'

def ConeVariablesDict(bachParticle, BsDecay):
    theDict = {
        "Cone_ANGLE"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEANGLE', -1.)".format(bachParticle, BsDecay),
        "Cone_MULT"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEMULT', -1.)".format(bachParticle, BsDecay),
        "Cone_PT"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPT', -1.)".format(bachParticle, BsDecay),
        "Cone_P"          : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEP', -1.)".format(bachParticle, BsDecay),
        "Cone_PX"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPX', -1.)".format(bachParticle, BsDecay),   
        "Cone_PY"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPY', -1.)".format(bachParticle, BsDecay),
        "Cone_PZ"         : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZ', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_P"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PT"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPTASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PX"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPXASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PY"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPYASYM', -1.)".format(bachParticle, BsDecay),
        "Cone_asy_PZ"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEPZSYM', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaEta"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAETA', -1.)".format(bachParticle, BsDecay),
        "Cone_deltaPhi"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeVarInfo', 'CONEDELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_MULT_0.4"     : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MULT', -1.)".format(bachParticle, BsDecay),
        "NC_sPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_SPT', -1.)".format(bachParticle, BsDecay),
        "NC_vPT_0.4"      : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_VPT', -1.)".format(bachParticle, BsDecay),
        "NC_PX_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PX', -1.)".format(bachParticle, BsDecay),
        "NC_PY_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PY', -1.)".format(bachParticle, BsDecay),
        "NC_PZ_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZ', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PT_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PTASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_P_0.4"    : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PX_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PXASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PY_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PYASYM', -1.)".format(bachParticle, BsDecay),
        "NC_asy_PZ_0.4"   : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_PZASYM', -1.)".format(bachParticle, BsDecay),
        "NC_deltaEta_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAETA', -1.)".format(bachParticle, BsDecay),
        "NC_deltaPhi_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_DELTAPHI', -1.)".format(bachParticle, BsDecay),
        "NC_IT_0.4"       : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_IT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PT_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PT', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PX_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PY_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay),
        "NC_maxPt_PZ_0.4" : "RELINFO('/Event/Leptonic/Phys/Bc2BsBc2Bs{0}_{1}/ConeIsoInfo0p4', 'NC_MAXPT_PX', -1.)".format(bachParticle, BsDecay)}
    return theDict


##############################################################################
# Selection sequences
##############################################################################

### NEED TO CHECK FORMAT ###

DaVinci().RootInTES = "/Event/Leptonic"

line_1 = "Bc2BsBc2BsMu_Jpsiphi"
line_2 = "Bc2BsBc2BsPi_Jpsiphi"

# Jpsi
if DaVinci().Simulation == False:
    bc_bsmu = 'Phys/{0}/Particles'.format(line_1)
    bc_bspi = 'Phys/{0}/Particles'.format(line_2)
else:
    bc_bsmu = '/Event/AllStreams/Phys/{0}/Particles'.format(line_1)
    bc_bspi = '/Event/AllStreams/Phys/{0}/Particles'.format(line_2)

##############################################################################
# Decay Tree Tuples for data and MC
##############################################################################

# Signal
Bc2BsMu_Bs2JpsiPhi = DecayTreeTuple("Bc2BsMu_Bs2JpsiPhi",
                                     Inputs = [bc_bsmu],
                                     Decay = "[B_c+ -> ^(B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K- ) ) ^mu+]CC")
Bc2BsMu_Bs2JpsiPhi.addBranches({
    'Bc':"^([B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC)",
    'Bs':'[B_c+ -> ^(B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Jpsi':'[B_c+ -> (B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Mu2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K- ) ) mu+]CC',
    'Phi':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K- ) ) mu+]CC',
    'K1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K- ) ) mu+]CC',
    'K2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K- ) ) mu+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) ^mu+]CC'})    

# Norm
Bc2BsPi_Bs2JpsiPhi = DecayTreeTuple("Bc2BsPi_Bs2JpsiPhi",
                                    Inputs = [bc_bspi],
                                    Decay = "[B_c+ -> ^(B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K- ) ) ^pi+]CC")
    
Bc2BsPi_Bs2JpsiPhi.addBranches({
    'Bc':"^([B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC)",
    'Bs':'[B_c+ -> ^(B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Jpsi':'[B_c+ -> (B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Mu1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Mu2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K- ) ) pi+]CC',
    'Phi':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K- ) ) pi+]CC',
    'K1':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K- ) ) pi+]CC',
    'K2':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K- ) ) pi+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K- ) ) ^pi+]CC'})

# MC decay tree tuples - added neutrino

if DaVinci().Simulation == True:
    #from Configurables import MCDecayTreeTuple
    # Signal
    MC_Bc2BsMu_Bs2JpsiPhi = MCDecayTreeTuple("MC_Bc2BsMu_Bs2JpsiPhi",
                                          Inputs = [bc_bsmu],
                                          Decay = "[B_c+ => ^(B_s0 => ^(J/psi(1S) => ^mu+ ^mu-) ^(phi(1020) => ^K+ ^K- ) ) ^mu+ ^nu_mu]CC")
    MC_Bc2BsMu_Bs2JpsiPhi.addBranches({
        'Bc':"^([B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC)",
        'Bs':'[B_c+ => ^(B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Jpsi':'[B_c+ => (B_s0 => ^(J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Mu1':'[B_c+ => (B_s0 => (J/psi(1S) => ^mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Mu2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ ^mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'Phi':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) ^(phi(1020) => K+ K- ) ) mu+ nu_mu]CC',
        'K1':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => ^K+ K- ) ) mu+ nu_mu]CC',
        'K2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ ^K- ) ) mu+ nu_mu]CC',
        'Mu_bach':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^mu+ nu_mu]CC',
        'Nu_mu': '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ ^nu_mu]CC'})    

    # Norm
    MC_Bc2BsPi_Bs2JpsiPhi = MCDecayTreeTuple("MC_Bc2BsPi_Bs2JpsiPhi",
                                          Inputs = [bc_bspi],
                                          Decay = "[B_c+ => ^(B_s0 => ^(J/psi(1S) => ^mu+ ^mu-) ^(phi(1020) => ^K+ ^K- ) ) ^pi+]CC")

    MC_Bc2BsPi_Bs2JpsiPhi.addBranches({
        'Bc':"^([B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC)",
        'Bs':'[B_c+ => ^(B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Jpsi':'[B_c+ => (B_s0 => ^(J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Mu1':'[B_c+ => (B_s0 => (J/psi(1S) => ^mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Mu2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ ^mu-) (phi(1020) => K+ K- ) ) pi+]CC',
        'Phi':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) ^(phi(1020) => K+ K- ) ) pi+]CC',
        'K1':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => ^K+ K- ) ) pi+]CC',
        'K2':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ ^K- ) ) pi+]CC',
        'Mu_bach':'[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^pi+]CC'})

##############################################################################
# Configure the tools for the DTTs
##############################################################################

tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",
                 "TupleToolPropertime",
                 "TupleToolANNPID",
                 "TupleToolTrigger",
                 ]

mctupletoollist = ["MCTupleToolHierarchy",
                   "MCTupleToolReconstructed",
                   #"MCTupleToolKinematic",
                   #"TupleToolMCTruth",
                   #"TupleToolMCBackgroundInfo",
                   #"MCTupleToolAngles",
                   #"LoKi::Hybrid::MCTupleTool/LoKi_Photos",
                  ]


triglist = ['L0MuonDecision',
            'L0HadronDecision',
            'Hlt1SingleMuonNoIPDecision',
            'Hlt1TrackMVADecision',
            'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackAllL0Decision',
            'Hlt2SingleMuonDecision',
            'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision',
            'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision'
        ]


from Configurables import MCMatchObjP2MCRelator
# Just MC
if DaVinci().Simulation == True:
    for mctuple in [MC_Bc2BsMu_Bs2JpsiPhi, MC_Bc2BsPi_Bs2JpsiPhi]:
        mctuple.ToolList+=mctupletoollist
        #mctuple.TupleToolMCTruth.ToolList = ["MCTupleToolKinematic","MCTupleToolReconstructed","MCTupleToolHierarchy","MCTupleToolDecayType","MCTupleToolPID", "MCTupleToolPrompt", "TupleToolMCTruth", ]
        mctuple.addTool(MCTupleToolKinematic())
        mctuple.MCTupleToolKinematic.Verbose = True
        nPhotons = mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_Photons")
        nPhotons.Preambulo+=["from LoKiPhysMC.decorators import *" ,"from LoKiPhysMC.functions import *"]
        nPhotons.Variables = {"nPhotons": "MCNINTREE(('gamma' == MCABSID)) "}
        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

for dttuple in [Bc2BsMu_Bs2JpsiPhi, Bc2BsPi_Bs2JpsiPhi]:
    dttuple.ToolList+=tupletoollist
    LoKi_Bc = dttuple.Bc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bc")
    LoKi_Bc.Preambulo+=["from LoKiPhysMC.decorators import *",
                        "from LoKiPhysMC.functions import mcMatch"]
    SLVars = {"BPVETA" : "BPVETA",
                         "PHI" : "PHI",
                         "KEY": "KEY",
                         "BPVCORRM" : "BPVCORRM",
                        }
    if dttuple == Bc2BsMu_Bs2JpsiPhi:
        ConeVars = ConeVariablesDict("Mu", "Jpsiphi")
    else:
        ConeVars = ConeVariablesDict("Pi", "Jpsiphi")
    BcVars = ConeVars.copy()
    BcVars.update(SLVars)
    LoKi_Bc.Variables = BcVars

    if DaVinci().Simulation == True:
        MCTruth = dttuple.addTupleTool('TupleToolMCTruth')
        MCTruth.ToolList += ['MCTupleToolKinematic']
        MCTruth.ToolList += ['MCTupleToolHierarchy']
        MCTruth.addTool(MCMatchObjP2MCRelator)
        MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        dttuple.addTupleTool('TupleToolMCBackgroundInfo')

        # MC Match variable to Bc and Bs
        LoKi_Bc.Variables["is_from_Bc_BsMuNu"] = "switch( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsPi"] = "switch( mcMatch( '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) pi+]CC'  , 1  ) , 1 , 0 )"

        LoKi_Bs = dttuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bs")
        LoKi_Bs.Preambulo+=["from LoKiPhysMC.decorators import *",
                            "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bs.Variables = {'is_from_Bs_JpsiPhi' : "switch( mcMatch( '[B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- )]CC' , 1 ), 1, 0)"}

    LoKi_Mu1 = dttuple.Mu1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu1")
    LoKi_Mu1.Variables = {"NSHAREDMU":"NSHAREDMU"}
    LoKi_Mu2 = dttuple.Mu2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu2")
    LoKi_Mu2.Variables = {"NSHAREDMU":"NSHAREDMU"}

    if Bc2BsMu_Bs2JpsiPhi==dttuple:
        LoKi_MuBach = dttuple.Mu_bach.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuBach")
        LoKi_MuBach.Variables = {"NSHAREDMU":"NSHAREDMU"}
        ttnr = dttuple.Bc.addTupleTool(TupleToolNeutrinoReco,name='ttnr')
        ttnr.Verbose=True
        ttnr.MotherMass = 6274.47*MeV#LHCb-PAPER-2020-003
        #http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/LHCb-PAPER-2020-003.html

    ttdtf=dttuple.Bc.addTupleTool(TupleToolDecayTreeFitter("TTDTF"))
    ttdtf.Verbose = True
    ttdtf.constrainToOriginVertex =False
    ttdtf.UpdateDaughters = True
    ttdtf.daughtersToConstrain = ["J/psi(1S)",'phi(1020)','B_s0']

    ttttBc = dttuple.Bc.addTupleTool(TupleToolTISTOS,name="ttttBc")
    ttttBs = dttuple.Bs.addTupleTool(TupleToolTISTOS,name="ttttBs")
    ttttMu = dttuple.Mu_bach.addTupleTool(TupleToolTISTOS,name="ttttMu")
    for T in [ttttBc,ttttBs, ttttMu]:
                T.VerboseL0   = True
                T.VerboseHlt1 = True
                T.VerboseHlt2 = True
                T.TriggerList = triglist[:]

##############################################################################
# Add selection to GaudiSequencer
##############################################################################

from Configurables import GaudiSequencer
gaudiSelSeq = GaudiSequencer('gaudiSelSeq')
checkingPV = CheckPV("CheckForOnePV")
checkingPV.MinPVs = 1 # check there is at least 1pv
gaudiSelSeq.Members += [checkingPV]
#gaudiSelSeq.Members += [s.sequence() for s in selection_sequences]

##############################################################################
# Configure DaVinci
##############################################################################

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

# DaVinci config not needed in AP
DaVinci().PrintFreq = 1000 # change this to 1000 for all evts
if DaVinci().Simulation == True:
    DaVinci().TupleFile = "BcTuples_MC_JpsiPhi_ReStrip16.root"
else:
    DaVinci().TupleFile = "BcTuples_data_JpsiPhi_ReStrip16.root"
DaVinci().EvtMax = -1
# Set Luminosity information when not using MC
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().InputType = 'MDST'

#DaVinci().UserAlgorithms+=[s.sequence() for s in selection_sequences]
DaVinci().UserAlgorithms+=[gaudiSelSeq]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2JpsiPhi]
DaVinci().UserAlgorithms+=[Bc2BsPi_Bs2JpsiPhi]
if DaVinci().Simulation == True:
    DaVinci().UserAlgorithms+=[MC_Bc2BsMu_Bs2JpsiPhi]
    DaVinci().UserAlgorithms+=[MC_Bc2BsPi_Bs2JpsiPhi]



