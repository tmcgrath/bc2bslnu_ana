#
# ganga submission for testing Bc davinci options
#

import os

def make_options(name,#savename of file
                 opts,#list of options
                 #dv2use,#path to DaVinci install
                 data):#data to process
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v45r4')
    myApp = GaudiExec()
    #myApp = DaVinci()
    #myApp.version = 'v45r4'
    #TODO: chnage this
    myApp.directory = '/afs/cern.ch/work/t/tmcgrath/private/lbdevs/DaVinciDev_v45r8'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64_v2-centos7-gcc10-opt'    

    return j

# 2018 data
# Stripping 34 bhadroncomplete
# Take first 100 files
bhadron_2018MU_path = "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/BHADRONCOMPLETEEVENT.DST"
dataset_bhadron_2018MU = BKQuery(path=bhadron_2018MU_path).getDataset()
bhadron_2018MD_path = "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/BHADRONCOMPLETEEVENT.DST"
dataset_bhadron_2018MD = BKQuery(path=bhadron_2018MD_path).getDataset()
# Stripping 34 dimuon
dimuon_2018MU_path = "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/DIMUON.DST"
dataset_dimuon_2018MU = BKQuery(path=dimuon_2018MU_path).getDataset()
dimuon_2018MD_path = "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/DIMUON.DST"
dataset_dimuon_2018MD = BKQuery(path=dimuon_2018MD_path).getDataset()
# Stripping 34r0p2 leptonic
# has 4Xstats of bhadron dsts per file
# Take first 25 files
leptonic_2018MU_path = "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34r0p2/90000000/LEPTONIC.MDST"
dataset_leptonic_2018MU = BKQuery(path=leptonic_2018MU_path).getDataset()

last = len(dataset_bhadron_2018MU) - 1

## Bs -> DsPi
# Options from Bc->BsPi discovery
#make_options("Bc2BsPi_DsPi_oldOpt_oldStrip_18MU", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/DsPi/DaVinci_Bc2BsPi_DsPiNoIP_R12S17.py'], dataset_bhadron_2018MU)
#make_options("Bc2BsPi_DsPi_oldOpt_oldStrip_18MD", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/DsPi/DaVinci_Bc2BsPi_DsPiNoIP_R12S17.py'], dataset_bhadron_2018MD)
#make_options("Bc2BsPi_DsPi_oldOpt_newStrip", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/DsPi/DaVinci_Bc2BsPi_DsPiNoIP_R12S17_newStrip.py'], dataset_leptonic_2018MU)

# My options
#make_options("Bc2BsPi_DsPi_newOpt_oldStrip", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/BcOptions_DsPi_OldStrip_newDecDesc.py'], dataset_bhadron_2018MU)
#make_options("Bc2BsPi_DsPi_newOpt_newStrip", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/BcOptions_DsPi_NewStrip_newDecDesc.py'], dataset_leptonic_2018MU)

## Bs->JpsiPhi
# Options from Bc->BsPi discovery
#make_options("Bc2BsPi_JpsiPhi_oldOpt_oldStrip_18MU", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/JpsiPhi/DaVinci_Bc2BsPi_JpsiPhi_R12S17.py'], dataset_dimuon_2018MU)
#make_options("Bc2BsPi_JpsiPhi_oldOpt_oldStrip_18MD", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/JpsiPhi/DaVinci_Bc2BsPi_JpsiPhi_R12S17.py'], dataset_dimuon_2018MD)
make_options("Bc2BsPi_JpsiPhi_oldOpt_newStrip_18MU_oldopt", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Data/2018.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/JpsiPhi/DaVinci_Bc2BsPi_JpsiPhi_R12S17_newStrip.py'], dataset_leptonic_2018MU[768:788])