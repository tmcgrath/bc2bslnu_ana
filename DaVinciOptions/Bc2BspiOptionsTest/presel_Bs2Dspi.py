#
# presel_Bs2Dspi.py
#

import sys
import ROOT as R
import argparse
R.ROOT.EnableImplicitMT()

###### Parse arguments ######

parser=argparse.ArgumentParser()
parser.add_argument('--file','-f',dest='file',required=True,type=str,help='Text file containing all root file names')
parser.add_argument('--mc', '-m', dest='mc', action="store_true", help='Flag to specify whether file is MC or not')
parser.add_argument('--bachelor','-b',dest='bachelor',required=True,type=str,choices=['Mu','Pi'],help='name of bachelor particle (Mu or Pi)')

parsed = vars(parser.parse_args())
bach = parsed['bachelor']

###### RDF function ######

def apply_cuts(files):

    filesVec = R.std.vector('string')()
    for file in files:
        filename = file.strip()
        filesVec.push_back(filename)
        print(f"Added {filename}")

    #df = R.RDataFrame("Bc2BsPi_KKPi_Tuple/DecayTree", filesVec)
    df = R.RDataFrame(f"Bc2Bs{bach}_KKPi_Tuple/DecayTree", filesVec)

    dfFiltered = df.Filter("Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 9", "Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 9")\
    .Filter("Bc_IPCHI2_OWNPV < 25", "B_IPCHI2_OWNPV < 25")\
    .Filter("Bc_DIRA_OWNPV > 0.99", "B_DIRA_OWNPV > 0.99")\
    .Filter("Bs_TAU > 0.000", "Bs_TAU > 0.000")\
    .Filter("1943.49 < Ds_MM && Ds_MM < 1993.49", "1943.49 < Ds_MM && Ds_MM < 1993.49")\
    .Filter("5200 < Bs_MM && Bs_MM < 5900", "5200 < Bs_MM && Bs_MM < 5900")\
    .Filter("PionBs_PT > 1700", "PionBs_PT > 1700")\
    .Filter("Ds_PT > 2000", "Ds_PT > 2000")
    #.Filter("5300 < Bs_MM && Bs_MM < 5420", "5300 < Bs_MM && Bs_MM < 5420")

    return dfFiltered

###### Files ######

text_file = parsed["file"]
if parsed["mc"]:
    index = (text_file.split("/")[-1]).split("_MCFiles")[0]
else:
    index = (text_file.split("/")[-1]).split("_Files")[0]
print(bach)
print(index)

with open(text_file, "r") as f:
    files = f.readlines()
#from files.Bc2BsPi_DsPi_oldOpt_oldStrip_MCFiles_sim09j import files

###### RDF ######

# Put file name in vector
length = len(files)
N = int(length / 9)
left = int(length % 9)

files1 = files[0:N*1]
files2 = files[N*1:N*2]
files3 = files[N*2:N*3]
files4 = files[N*3:N*4]
files5 = files[N*4:N*5]
files6 = files[N*5:N*6]
files7 = files[N*6:N*7]
files8 = files[N*7:N*8]
files9 = files[N*8:N*9]
files10 = files[N*9:N*9+left]


print("Applying cuts to files section 1......")
if parsed["mc"]:
    df1 = apply_cuts(files)
    df1.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}.root")
    sys.exit()
else:
    df1 = apply_cuts(files1)
    df1.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_1.root")

print("Applying cuts to files section 2......")
df2 = apply_cuts(files2)
df2.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_2.root")

print("Applying cuts to files section 3......")
df3 = apply_cuts(files3)
df3.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_3.root")

print("Applying cuts to files section 4......")
df4 = apply_cuts(files4)
df4.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_4.root")

print("Applying cuts to files section 5......")
df5 = apply_cuts(files5)
df5.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_5.root")

print("Applying cuts to files section 6......")
df6 = apply_cuts(files6)
df6.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_6.root")

print("Applying cuts to files section 7......")
df7 = apply_cuts(files7)
df7.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_7.root")

print("Applying cuts to files section 8......")
df8 = apply_cuts(files8)
df8.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_8.root")

print("Applying cuts to files section 9......")
df9 = apply_cuts(files9)
df9.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_9.root")

print("Applying cuts to files section 10......")
df10 = apply_cuts(files10)
df10.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}_10.root")

sys.exit()

print("Applying cuts to files section 11......")
#df11 = apply_cuts(files11)
#df11.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_11.root")

print("Applying cuts to files section 12......")
#df12 = apply_cuts(files12)
#df12.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_12.root")

print("Applying cuts to files section 13......")
#df13 = apply_cuts(files13)
#df13.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_13.root")

print("Applying cuts to files section 14......")
#df14 = apply_cuts(files14)
#df14.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_14.root")

print("Applying cuts to files section 15......")
#df15 = apply_cuts(files15)
#df15.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_15.root")

print("Applying cuts to files section 16......")
#df16 = apply_cuts(files16)
#df16.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_16.root")

print("Applying cuts to files section 17......")
#df17 = apply_cuts(files17)
#df17.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_17.root")

print("Applying cuts to files section 18......")
#df18 = apply_cuts(files18)
#df18.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_18.root")

print("Applying cuts to files section 19......")
#df19 = apply_cuts(files19)
#df19.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_19.root")

print("Applying cuts to files section 20......")
#df20 = apply_cuts(files20)
#df20.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/Bc2BsPi_DsPi_{index}_20.root")


print("Done")
