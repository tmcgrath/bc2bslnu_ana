def fillTuple( tuple, myBranches, myTriggerList, ifMC):
    
    # DecayTreeTuple
    from Configurables import DecayTreeTuple, TupleToolTrigger, TupleToolTISTOS, TupleToolRecoStats, TupleToolEventInfo, TupleToolDecay
    
    tuple.Branches = myBranches 
    
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",     
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    if ifMC == True:
        from Configurables import MCMatchObjP2MCRelator, TupleToolMCTruth, TupleToolMCBackgroundInfo
        MCTruth = tuple.addTool(TupleToolMCTruth)
        MCTruth.ToolList = ["MCTupleToolKinematic",
                             "MCTupleToolHierarchy",
                             "MCTupleToolReconstructed"]
        #MCTruth.ToolList += ['MCTupleToolKinematic']
        #MCTruth.ToolList += ['MCTupleToolHierarchy']
        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
        #MCTruth.addTool(MCMatchObjP2MCRelator)
        MCTruth.addTool(MCMatchObjP2MCRelator)
        MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        tuple.addTool(TupleToolMCBackgroundInfo)

    tuple.addTool(TupleToolDecay, name = 'Bs')
    tuple.addTool(TupleToolDecay, name = 'B')

    from Configurables import TupleToolKinematic
    tuple.addTool(TupleToolKinematic)
    tuple.TupleToolKinematic.Verbose=True

    
    # TISTOS
    # Bs
    tuple.Bs.ToolList += [ "TupleToolTISTOS/BsTupleToolTISTOS" ] 
    tuple.Bs.addTool(TupleToolTISTOS("BsTupleToolTISTOS"))
    tuple.Bs.BsTupleToolTISTOS.Verbose=True
    tuple.Bs.BsTupleToolTISTOS.TriggerList = myTriggerList

    # B
    tuple.B.ToolList += [ "TupleToolTISTOS" ] 
    tuple.B.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.B.TupleToolTISTOS.Verbose=True
    tuple.B.TupleToolTISTOS.TriggerList = myTriggerList
    
    # RecoStats to filling SpdMult, etc
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    # Mass constraint 
    from Configurables import TupleToolParticleReFit, OfflineVertexFitter
    tuple.addTool(TupleToolParticleReFit, name = "refittool_JpsiMassConstr")
    tuple.refittool_JpsiMassConstr.addTool(OfflineVertexFitter())
    tuple.refittool_JpsiMassConstr.OfflineVertexFitter.useResonanceVertex = True
    tuple.refittool_JpsiMassConstr.OfflineVertexFitter.applyDauMassConstraint = True
    
    tuple.ToolList += [ "TupleToolParticleReFit/refittool_JpsiMassConstr" ]

    # Decay Tree Fitter
    from Configurables import TupleToolDecayTreeFitter

    """
    Bs
    """
    tuple.Bs.ToolList +=  [ "TupleToolDecayTreeFitter/BsPVFit",          # fit with PV constraint
                            "TupleToolDecayTreeFitter/BsMassFit" ]       # fit with J/psi mass constraint
    
    tuple.Bs.addTool(TupleToolDecayTreeFitter("BsPVFit"))
    tuple.Bs.BsPVFit.Verbose = True
    tuple.Bs.BsPVFit.constrainToOriginVertex = True
    tuple.Bs.BsPVFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)" ]
    
    tuple.Bs.addTool(TupleToolDecayTreeFitter("BsMassFit"))
    tuple.Bs.BsMassFit.constrainToOriginVertex = False
    tuple.Bs.BsMassFit.Verbose = True
    tuple.Bs.BsMassFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)" ]

    from Configurables import LoKi__Hybrid__TupleTool
    if ifMC == True:
        LoKi_Bs=LoKi__Hybrid__TupleTool("LoKi_Bs")
        LoKi_Bs.Preambulo += ["from LoKiPhysMC.decorators import *",
                              "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bs.Variables = {'is_from_Bs_JpsiPhi' : "switch(  mcMatch (' [B_s0 => ( J/psi(1S) => mu+ mu- ) ( phi(1020) => K+ K-)]CC ', 1), 1, 0)"}

        tuple.Bs.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Bs"]
        tuple.Bs.addTool(LoKi_Bs)

    """
    B
    """
    tuple.B.ToolList +=  [ "TupleToolDecayTreeFitter/PVFit",          # fit with PV constraint
                           "TupleToolDecayTreeFitter/MassFit" ]       # fit with intermediate particle mass constraint
    
    tuple.B.addTool(TupleToolDecayTreeFitter("PVFit"))
    tuple.B.PVFit.Verbose = True
    tuple.B.PVFit.constrainToOriginVertex = True
    tuple.B.PVFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)", "B_s0" ]
    
    tuple.B.addTool(TupleToolDecayTreeFitter("MassFit"))
    tuple.B.MassFit.Verbose = True
    tuple.B.MassFit.constrainToOriginVertex = False
    tuple.B.MassFit.daughtersToConstrain = [ "J/psi(1S)", "phi(1020)", "B_s0" ]
    

    #LoKi one
    LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
    
    LoKi_B.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LV01"                 : "LV01",
        "LV02"                 : "LV02"
        }
    
    # Add MC truth matching
    if ifMC == True:
        LoKi_B.Preambulo += ["from LoKiPhysMC.decorators import *",
                             "from LoKiPhysMC.functions import mcMatch"]
        LoKi_B.Variables["is_from_Bc_BsPi"] = "switch( mcMatch( '[ (B_c+ => ( B_s0 => ( J/psi(1S) => mu+ mu-) ( phi(1020) => K+ K-) ) pi+), (B_c- => ( B_s0 => ( J/psi(1S) => mu+ mu-) ( phi(1020) => K+ K-) ) pi-) ]'  , 1  ) , 1 , 0 )"
        LoKi_B.Variables["is_from_Bc_BsMuNu"] = "switch( mcMatch(  '[ (B_c+ => ( B_s0 => ( J/psi(1S) => mu+ mu-) ( phi(1020) => K+ K-) ) mu+ nu_mu), (B_c- => ( B_s0 => ( J/psi(1S) => mu+ mu-) ( phi(1020) => K+ K-) ) mu- nu_mu~) ]'  , 1  ) , 1 , 0 )"

    tuple.B.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]
    tuple.B.addTool(LoKi_B)

    tuple.Bs.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]
    tuple.Bs.addTool(LoKi_B)

    
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y",
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All) 

    #LoKi one
    from Configurables import LoKi__Hybrid__EvtTupleTool
    LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple")
    LoKi_EvtTuple.VOID_Variables = {
        "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
        "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
        "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
        "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
        "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
        "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
        "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
        }

    tuple.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
    tuple.addTool(LoKi_EvtTuple)

    
    
from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, DecayTreeTuple, OfflineVertexFitter
from Configurables import DaVinci

myTriggerList = [
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0MuonHighDecision",
    "L0HadronDecision",
    "L0PhotonDecision",
    "L0ElectronDecision",
    
    "Hlt1DiMuonHighMassDecision",
    "Hlt1DiMuonLowMassDecision",
    "Hlt1SingleMuonNoIPDecision",
    "Hlt1SingleMuonHighPTDecision",
    
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",
    
    "Hlt2Topo2BodySimpleDecision",
    "Hlt2Topo3BodySimpleDecision",
    "Hlt2Topo4BodySimpleDecision",
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo4BodyBBDTDecision",
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2TopoMu3BodyBBDTDecision",
    "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2TopoE2BodyBBDTDecision",
    "Hlt2TopoE3BodyBBDTDecision",
    "Hlt2TopoE4BodyBBDTDecision",        

    "Hlt2DiMuonDecision",
    "Hlt2DiMuonLowMassDecision",
    "Hlt2DiMuonJPsiDecision",
    "Hlt2DiMuonJPsiHighPTDecision",
    "Hlt2DiMuonDetachedDecision",
    "Hlt2DiMuonDetachedHeavyDecision",
    "Hlt2DiMuonDetachedJPsiDecision",
    "Hlt2DiMuonNoPVDecision"
    ]



BsStripLocation = "/Event/Dimuon/Phys/BetaSBs2JpsiPhiDetachedLine/Particles"  
if DaVinci().Simulation == True:
    BsStripLocation = "/Event/AllStreams/Phys/BetaSBs2JpsiPhiDetachedLine/Particles"


#"Bs2JpsiPhi Tuple"
Bs2JpsiPhiTuple = DecayTreeTuple("Bs2JpsiPhiTuple")
Bs2JpsiPhiTuple.Inputs = [ BsStripLocation ]
Bs2JpsiPhiTuple.Decay = "B_s0 -> ^( J/psi(1S) -> ^mu+ ^mu-) ^( phi(1020) -> ^K+ ^K-)"

Bs2JpsiPhiBranches = {
     "MuP"  : "B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-)"
    ,"MuM"  : "B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-)"  
    ,"Jpsi" : "B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-)"
    ,"KaonP": "B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-)"
    ,"KaonM": "B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-)"
    ,"Phi"  : "B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-)"
    ,"Bs" : "B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K- )"
    }

fillTuple( Bs2JpsiPhiTuple, Bs2JpsiPhiBranches, myTriggerList, DaVinci().Simulation )




"""
FilterBs
"""
from Configurables import FilterDesktop
FilterBs = FilterDesktop('FilterBs')
FilterBs.Inputs = [ BsStripLocation ]
FilterBs.Code = "in_range(5000,M,6000)"


"""
Bc->B_s0 Pi
"""
from StandardParticles import StdAllNoPIDsPions
PreselBc2BsH = CombineParticles("PreselBc2BsH")
PreselBc2BsH.Inputs = ["Phys/FilterBs/Particles",
                       StdAllNoPIDsPions.outputLocation()
                       ]
PreselBc2BsH.DecayDescriptors = ["B_c+ -> B_s0 pi+", "B_c- -> B_s0 pi-"]
PreselBc2BsH.DaughtersCuts = { "B_s0": "ALL",
                               "pi+" : "(TRCHI2DOF<5.0) & (PT>0.25*GeV) & (BPVIPCHI2()>1.)",
                               }
#PreselBc2BsH.CombinationCut = "(ADAMASS('B_c+')<500*MeV)" 
PreselBc2BsH.MotherCut = "(VFASPF(VCHI2/VDOF)<25)" 
PreselBc2BsH.ReFitPVs = True

"""
Bc->B_s0 Mu
"""
from StandardParticles import StdAllNoPIDsMuons
PreselBc2BsMu = CombineParticles("PreselBc2BsMu")
PreselBc2BsMu.Inputs = ["Phys/FilterBs/Particles",
                       StdAllNoPIDsMuons.outputLocation()
                       ]
PreselBc2BsMu.DecayDescriptors = ["B_c+ -> B_s0 mu+", "B_c- -> B_s0 mu-"]
PreselBc2BsMu.DaughtersCuts = { "B_s0": "ALL",
                               "mu+" : "(TRCHI2DOF<5.0) & (PT>0.1*GeV) & (BPVIPCHI2()>1.)",
                               }
#PreselBc2BsMu.CombinationCut = "(ADAMASS('B_c+')<500*MeV)" 
PreselBc2BsMu.MotherCut = "(VFASPF(VCHI2/VDOF)<25)" 
PreselBc2BsMu.ReFitPVs = True


"""
Bc2BsHTuple
"""
Bc2BsHTuple = DecayTreeTuple("Bc2BsPi_Tuple")
Bc2BsHTuple.Inputs = ["Phys/PreselBc2BsH/Particles"]
Bc2BsHTuple.Decay = "[ (B_c+ -> ^( B_s0 -> ^( J/psi(1S) -> ^mu+ ^mu-) ^( phi(1020) -> ^K+ ^K-) ) ^pi+), (B_c- -> ^( B_s0 -> ^( J/psi(1S) -> ^mu+ ^mu-) ^( phi(1020) -> ^K+ ^K-) ) ^pi-) ]"

Bc2BsHBranches = {
     "MuP"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    ,"MuM"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"  
    ,"Jpsi" : "[ (B_c+ -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    ,"KaonP": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) pi-) ]"
    ,"KaonM": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) pi-) ]"
    ,"Phi"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) pi-) ]"
    ,"Bs"   : "[ (B_c+ -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), (B_c- -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    ,"Bach" : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^pi+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^pi-) ]" 
    ,"B" : "[ ^(B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi+), ^(B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) pi-) ]"
    }

fillTuple( Bc2BsHTuple, Bc2BsHBranches, myTriggerList, DaVinci().Simulation )


"""
Bc2BsMuTuple
"""
Bc2BsMuTuple = DecayTreeTuple("Bc2BsMu_Tuple")
Bc2BsMuTuple.Inputs = ["Phys/PreselBc2BsMu/Particles"]
Bc2BsMuTuple.Decay = "[ (B_c+ -> ^( B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-) ) ^mu+), (B_c- -> ^( B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-) ) ^mu-) ]"

Bc2BsMuBranches = {
     "MuP"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> ^mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    ,"MuM"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ ^mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"  
    ,"Jpsi" : "[ (B_c+ -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ^( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    ,"KaonP": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> ^K+ K-) ) mu-) ]"
    ,"KaonM": "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ ^K-) ) mu-) ]"
    ,"Phi"  : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-) ) mu-) ]"
    ,"Bs"   : "[ (B_c+ -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), (B_c- -> ^( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    ,"Bach" : "[ (B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^mu+), (B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) ^mu-) ]" 
    ,"B" : "[ ^( B_c+ -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu+), ^(B_c- -> ( B_s0 -> ( J/psi(1S) -> mu+ mu- ) ( phi(1020) -> K+ K-) ) mu-) ]"
    }

fillTuple( Bc2BsMuTuple, Bc2BsMuBranches, myTriggerList, DaVinci().Simulation )

SeqPreselBc2BsH = GaudiSequencer("SeqPreselBc2BsH")
#SeqPreselBc2BsH.Members += [ Bs2JpsiPhiTuple ]
SeqPreselBc2BsH.Members += [ FilterBs ]
SeqPreselBc2BsH.Members += [ PreselBc2BsH ]
SeqPreselBc2BsH.Members += [ PreselBc2BsMu ]
SeqPreselBc2BsH.Members += [ Bc2BsHTuple ]
SeqPreselBc2BsH.Members += [ Bc2BsMuTuple ] 

from Configurables import DecodeVeloRawBuffer
createVeloClusters = DecodeVeloRawBuffer("createVeloClusters")
createVeloClusters.DecodeToVeloLiteClusters = True
createVeloClusters.DecodeToVeloClusters     = True

from Configurables import RawBankToSTClusterAlg
createITClusters = RawBankToSTClusterAlg("createITClusters")
createITClusters.DetType     = "IT"


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Jpsi2MuMuLineFilters = LoKi_Filters (
    STRIP_Code = "HLT_PASS('StrippingBetaSBs2JpsiPhiDetachedLineDecision')",
    VOID_Code  = "CONTAINS('Rec/Vertex/Primary')>0.5"
    )

from Configurables import DaVinci
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
#DaVinci().DataType = "2011"
#DaVinci().Simulation    = False
#DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
#DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().TupleFile = "Bc2BsX_JpsiPhi_oldOpt_oldStrip.root"            # Ntuple
if DaVinci().Simulation == True:
    DaVinci().TupleFile = "Bc2BsX_JpsiPhi_oldOpt_oldStrip_MC.root"
DaVinci().EventPreFilters = Jpsi2MuMuLineFilters.filters ('Jpsi2MuMuLineFilters')
DaVinci().UserAlgorithms = [ createVeloClusters,
                             createITClusters,
                             Bs2JpsiPhiTuple,
                             SeqPreselBc2BsH
                             ]  # The algorithms

# Get Luminosity
DaVinci().Lumi = not DaVinci().Simulation

# database
#from Configurables import CondDB
#CondDB(UseOracle = True)
#DaVinci().DDDBtag  = "head-20110914"
#DaVinci().CondDBtag = "head-20111111"

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]