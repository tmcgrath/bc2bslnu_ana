#
# ganga submission for testing Bc davinci options
#

import os

def make_options(name,#savename of file
                 opts,#list of options
                 #dv2use,#path to DaVinci install
                 data):#data to process
    #should you ever need to modify davinci code yourself, you can use these
    #APP = GaudiExec()
    #APP.directory = dv2use
    #myApp = prepareGaudiExec('DaVinci','v44r11p1', '.')
    myApp = GaudiExec()
    myApp.directory = "./DaVinciDev_v44r11p1"
    #myApp = DaVinci()
    #myApp.version = 'v45r4'

    j = Job(
        name            = name,
        application     = myApp,
        inputdata       = data,
        splitter        = SplitByFiles(filesPerJob = 10),
        outputfiles     = [DiracFile("*.root")],
        do_auto_resubmit= False,
        backend         = Dirac(),
        parallel_submit = True
        )
    j.application.options = opts
    j.application.platform = 'x86_64-centos7-gcc62-opt'    

    return j

# sim09j Bc2BsPi Bs2DsPi
#bc2bspi_bs2dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14165002/ALLSTREAMS.DST"
bc2bspi_bs2dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09j/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14165002/ALLSTREAMS.DST"
dataset_bc2bspi_bs2dspi_2018MU = BKQuery(path=bc2bspi_bs2dspi_2018MU_path).getDataset()

make_options("Bc2BsPi_DsPi_oldOpt_newStrip_MC_sim09j_MU", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/Bc2BsPiTags/2018MU.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/Bc2BspiOptionsTest/DsPi/BcOptions_DsPi.py'], dataset_bc2bspi_bs2dspi_2018MU)

# sim09j Bc2BsPi Bs2DsPi
bc2bsmunu_bs2dspi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14575001/ALLSTREAMS.DST"
dataset_bc2bsmunu_bs2dspi_2018MU = BKQuery(path=bc2bsmunu_bs2dspi_2018MU_path).getDataset()

make_options("Bc2BsMuNu_DsPi_oldOpt_newStrip_MC_sim09k_MU", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/Bc2BsMuNuTags/2018MU.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_DsPi.py'], dataset_bc2bsmunu_bs2dspi_2018MU)

# sim09j Bc2BsPi Bs2JpsiPhi
#bc2bspi_bs2jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
#bc2bspi_bs2jpsiphi_2018MD_path = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09k/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
#dataset_bc2bspi_bs2jpsiphi_2018MU = BKQuery(path=bc2bspi_bs2jpsiphi_2018MU_path).getDataset()
#dataset_bc2bspi_bs2jpsiphi_2018MD = BKQuery(path=bc2bspi_bs2jpsiphi_2018MD_path).getDataset()

#make_options("Bc2BsPi_JpsiPhi_oldOpt_newStrip_MC_sim09k_MU", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/Bc2BsPiTags/2018MU.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_JpsiPhi.py'], dataset_bc2bspi_bs2jpsiphi_2018MU)
#make_options("Bc2BsPi_JpsiPhi_oldOpt_newStrip_MC_sim09k_MD", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/Bc2BsPiTags/2018MD.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_JpsiPhi.py'], dataset_bc2bspi_bs2jpsiphi_2018MD)
 
# sim09h Bc2Bsmunu Bs2JpsiPhi
bc2bsmunu_bs2jpsiphi_2018MU_path = "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14545006/ALLSTREAMS.DST"
dataset_bc2bsmunu_bs2jpsiphi_2018MU = BKQuery(path=bc2bsmunu_bs2jpsiphi_2018MU_path).getDataset()

make_options("Bc2BsMuNu_JpsiPhi_oldOpt_newStrip_MC_sim09k_MU_TriggerStudies", ['/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/MC/Bc2BsMuNuTags/2018MU.py', '/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/DaVinciOptions/BcOptions_JpsiPhi.py'], dataset_bc2bsmunu_bs2jpsiphi_2018MU)
