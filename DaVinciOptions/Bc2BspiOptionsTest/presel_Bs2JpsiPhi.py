#
# presel_Bs2JpsiPhi.py
#

import sys
import ROOT as R
import argparse
R.ROOT.EnableImplicitMT()

###### Parse arguments ######

parser=argparse.ArgumentParser()
parser.add_argument('--file','-f',dest='file',required=True,type=str,help='Text file containing all root file names')
parser.add_argument('--mc', '-m', dest='mc', action="store_true", help='Flag to specify whether file is MC or not')
parser.add_argument('--bachelor','-b',dest='bachelor',required=True,type=str,choices=['Mu','Pi'],help='name of bachelor particle (Mu or Pi)')

parsed = vars(parser.parse_args())
bach = parsed['bachelor']

treename = f"Bc2Bs{bach}_Tuple/DecayTree"

###### RDF function ######

def apply_cuts(files):

    filesVec = R.std.vector('string')()
    for theFile in files:
        filename = theFile.strip()
        filesVec.push_back(filename)
        print(f"Added {filename}")

    df = R.RDataFrame(treename, filesVec)

    dfFiltered = df.Filter("5000 < Bs_MM && Bs_MM < 6000", "5000 < Bs_MM && Bs_MM < 6000")\
    .Filter("Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 10", "Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 10")\
    .Filter("Bc_IPCHI2_OWNPV < 25", "Bc_IPCHI2_OWNPV < 25")\
    .Filter("Bc_DIRA_OWNPV > 0.99", "Bc_DIRA_OWNPV > 0.99")\
    .Filter("Bs_TAU > 0.0002", "Bs_TAU > 0.0002")\
    .Filter("MuP_TRACK_GhostProb < 0.4", "KaonP_TRACK_GhostProb < 0.4")\
    .Filter("MuM_TRACK_GhostProb < 0.4", "KaonM_TRACK_GhostProb < 0.4")\
    .Filter("KaonP_TRACK_GhostProb < 0.4", "KaonP_TRACK_GhostProb < 0.4")\
    .Filter("KaonM_TRACK_GhostProb < 0.4", "KaonM_TRACK_GhostProb < 0.4")\
    .Filter("Phi_ENDVERTEX_CHI2/Phi_ENDVERTEX_NDOF < 16", "Phi_ENDVERTEX_CHI2/Phi_ENDVERTEX_NDOF < 16")\
    .Filter("Bach_IPCHI2_OWNPV > 2", "Bach_IPCHI2_OWNPV > 2")\
    .Filter("Bach_TRACK_CHI2NDOF < 3", "Bach_TRACK_CHI2NDOF < 3")\
    .Filter("Bach_TRACK_GhostProb < 0.4", "Bach_TRACK_GhostProb < 0.4")
    
    #.Filter("B_L0MuonDecision_TOS == 1 | B_L0DiMuonDecision_TOS == 1 | B_L0HadronDecision_TOS == 1", "L0 trigger")\
    #.Filter("B_Hlt1DiMuonHighMassDecision_TOS == 1 | B_Hlt1TrackMuonDecision_TOS == 1 | B_Hlt1TrackAllL0Decision_TOS == 1", "Hlt1")\
    #.Filter("B_Hlt2DiMuonDetachedJPsiDecision_TOS == 1 | B_Hlt2DiMuonJPsiHighPTDecision_TOS == 1 | B_Hlt2TopoMu2BodyBBDTDecision_TOS == 1 | B_Hlt2TopoMu3BodyBBDTDecision_TOS == 1 | B_Hlt2TopoMu4BodyBBDTDecision_TOS == 1", "Hlt2")
    #.Filter("B_DIRA_OWNPV > 0.99", "B_DIRA_OWNPV > 0.99")\

    return dfFiltered

###### Files ######

text_file = parsed["file"]
if parsed["mc"]:
    index = (text_file.split("/")[-1]).split("_MCFiles")[0]
else:
    index = (text_file.split("/")[-1]).split("_Files")[0]
print(bach)
print(index)

with open(text_file, "r") as f:
    files = f.readlines()
#from files.Bc2BsPi_DsPi_oldOpt_oldStrip_MCFiles_sim09j import files

###### RDF ######

# Put file name in vector
length = len(files)
N = int(length / 9)
left = int(length % 9)

files1 = files[0:N*1]
files2 = files[N*1:N*2]
files3 = files[N*2:N*3]
files4 = files[N*3:N*4]
files5 = files[N*4:N*5]
files6 = files[N*5:N*6]
files7 = files[N*6:N*7]
files8 = files[N*7:N*8]
files9 = files[N*8:N*9]
files10 = files[N*9:N*9+left]

print("Applying cuts to files......")
df1 = apply_cuts(files)
df1.Snapshot("DecayTree", f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/BcOptionsTest/{index}.root")

print("Done")
