__author__ = ['Adam Davis']
__date__ = '12/03/2021'
__version__ = '$Revision: 1.0 $'
from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder
import logging

"""
Selections for Bc -> Bs X
"""


default_config = {
  'NAME'        : 'Bc2Bs',
  'WGs'         : ['Semileptonic'],
  'BUILDERTYPE' : 'Bc2BsBuilder',
  'CONFIG'      :  {
      "GEC_nLongTrk"        : 250.  , #adimensional
      "TRGHOSTPROB"         : 0.5    ,#adimensional
      #Kaons
      "KaonTRCHI2"          : 4.     ,#adimensional
      "KaonP"               : 3000.  ,#MeV
      "KaonPT"              : 400.   ,#MeV
      "KaonPIDK"            : 5.     ,#adimensional 
      "KaonPIDmu"           : 5.     ,#adimensional
      "KaonPIDp"            : 5.     ,#adimensional
      "KaonMINIP"           : 0.05   ,#mm

      "KaonP_phi"           : 3000.  ,#MeV
      "KaonPT_phi"          : 200.   ,#MeV
      "KaonPIDK_phi"        : 0.     ,#adimensional 
      "KaonPIDmu_phi"       : -2.    ,#adimensional
      "KaonPIDp_phi"        : -2.    ,#adimensional
      "KaonMINIP_phi"       : 0.025  ,#mm

      #k from bc

      "K_from_bc_TRCHI2"      : 4.     ,#adminensional
      "K_from_bc_P"           : 1000.  ,#MeV
      "K_from_bc_PT"          : 150.   ,#MeV
      "K_from_bc_PIDK"        : 0.     ,#adimensional
      "K_from_bc_MINIP"       : 0.05   ,#mm      
      #Pions 
      "PionTRCHI2"          : 4.     ,#adimensional
      "PionP"               : 3000.  ,#MeV
      "PionPT"              : 500.   ,#MeV
      "PionPIDK"            : -2.    ,#adimensional 
      "PionMINIP"           : 0.05   ,#mm

      "BachPionTRCHI2"      : 4.     ,#adminensional
      "BachPionP"           : 1000.  ,#MeV
      "BachPionPT"          : 150.   ,#MeV
      "BachPionPIDK"        : 2      ,#adimensional
      "BachPionMINIP"       : 0.05   ,#mm

      "PionFromBsTRCHI2"    : 4.     ,#adminensional
      "PionFromBsP"         : 1000.  ,#MeV
      "PionFromBsPT"        : 300.   ,#MeV
      "PionFromBsPIDK"      : 2      ,#adimensional
      "PionFromBsMINIP"     : 0.05   ,#mm
      #muons
      "MuonGHOSTPROB"       : 0.35   ,#adimensional
      "MuonTRCHI2"          : 4.     ,#adimensional
      "MuonP"               : 3000.  ,#MeV
      "MuonPT"              : 750.  ,#MeV
      "MuonPIDK"            : -2.    ,#adimensional
      "MuonPIDmu"           : 0.     ,#adimensional      
      "MuonMINIP"           : 0.025  ,#mm
      "BachMuonTRCHI2"      : 4.     ,#adminensional
      "BachMuonP"           : 1000.  ,#MeV
      "BachMuonPT"          : 150.   ,#MeV
      "BachMuonPIDmu"       : 0      ,#adimensional
      "BachMuonMINIP"       : 0.02   ,#mm
      #electron
      "ElectronTRCHI2"      : 10     ,#adimensional
      "ElectronP"           : 3000.  ,#MeV
      "ElectronPT"          : 1000.  ,#MeV
      "ElectronGHOSTPROB"   : 0.6    ,#adimensional
      "ElectronPIDe"        : -0.2   ,#adimensional
      "ElectronMINIP"       : 0.05   ,#mm
      #phi 
      "PhiVCHI2DOF"         : 6     ,#adimensional
      "PhiPT"               : 600.  ,#MeV
      "PhiMINIP"            : 0.05  ,#adimensional
      "PhiDIRA"             : 0.9   ,#adimensional
      "PhiMassWindow"       : 200.  ,#MeV
      "Phi_CHI2DOF"         : 35.   ,#MeV

      #jpsi
      "JpsiMassWindow"      : 200.  ,#MeV
      "JpsiPT"              : 1000. ,#MeV
      "Jpsi_CHI2DOF"        : 15.   ,#adimensional
      "Jpsi_DIRA"           : 0.9   ,#adimensional  
      "Jpsi_BPVVDZcut"      : 1.5     ,#mm
      #ds
      "DsMassWindow"       : 400.  ,#MeV
      "Ds_CHI2DOF"         : 15.   ,#adimensional
      "Ds_FDCHI2HIGH"      : 100.  ,#adimensional
      "Ds_DIRA"            : 0.99 ,#adimensional
      "Ds_BPVVDZcut"       : 0.0   ,#mm
      #bs
      "BsMassWindowHigh"   : 6000. ,#MeV
      "BsMassWindowLow"    : 5000. ,#MeV,
      "BsFDCHI2HIGH"       : 35.   ,#adimensional
      "BsDIRA"             : 0.9   ,#adimensional
      "BsPVVDZcut"         : 2.    ,#mm
      "Bs_JpsiPhi_CHI2DOF" : 35.   ,#adimensional
      "Bs_DsPi_CHI2DOF"    : 35.   ,#adimensional
      #bc
      "BcPVVDZcut"         : 0.080  ,#mm
      "Bc_CHI2DOF"         : 35.   ,#adimensional
      "BcPT"               : 2000. ,#MeV
      "BcP"                : 10000.,#MeV
      "BcDIRA_Tight"       : 0.99  ,#adimensional
      "BcDIRA_Loose"       : 0.9   ,#adimensional
      },
  'STREAMS'     : ['Leptonic']    
}



class Bc2BsBuilder(LineBuilder):
    """
    definition of the Bc->BsX stripping module
    """
    __configuration_keys__ = default_config['CONFIG'].keys()
    def __init__(self,name,config):
        LineBuilder.__init__(self,name,config)
        self.config = config
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        ## GECs for # long tracks
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        #filtering for MC tests
        from Configurables import FilterDesktop
        self.matchBc2BsMu_Bs2JpsiPhi_muons = FilterDesktop("mcmatch_Bc2BsMu_jpsiphi_muons",
            Code = "( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => ^mu+ ^mu-) (phi(1020) => K+ K- ) ) mu+ nu_mu]CC') )",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        self.matchBc2BsMu_Bs2JpsiPhi_kaons = FilterDesktop("mcmatch_Bc2Bsmu_jpsiphi_kaons",
            Code = "( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => ^K+ ^K- ) ) mu+ nu_mu]CC') )",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        self.matchBc2BsMu_Bs2JpsiPhi_bach_mu = FilterDesktop("mcmatch_Bc2Bsmu_jpsiphi_bachmu",
            Code = "( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^mu+ nu_mu]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsMu_Bs2Dspi_kaons = FilterDesktop("mcmatch_Bc2Bsmu_dspi_kaons",
            Code = "( mcMatch('[B_c+ => (B_s0 => (D_s- ==> ^K- ^K+ pi- ) pi+ ) mu+ nu_mu ]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsMu_Bs2Dspi_pion = FilterDesktop("mcmatch_Bc2Bsmu_dspi_pion",
            Code = "( mcMatch('[B_c+ => (B_s0 => (D_s- ==> K- K+ ^pi- ) pi+ ) mu+ nu_mu]CC') )",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        self.matchBc2BsMu_Bs2Dspi_bach_pi = FilterDesktop("mcmatch_Bc2Bsmu_dspi_bachpi",
            Code = "( mcMatch('[B_c+ => (B_s0 => (D_s- ==> K- K+ pi- ) ^pi+ ) mu+ nu_mu]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsMu_Bs2Dspi_bach_mu = FilterDesktop("mcmatch_Bc2Bsmu_dspi_bachmu",
            Code = "( mcMatch('[B_c+ => (B_s0 => (D_s- ==> K- K+ pi- ) pi+ ) ^mu+ nu_mu]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        ### pion
        self.matchBc2BsPi_Bs2JpsiPhi_muons = FilterDesktop("mcmatch_Bc2Bspi_jpsiphi_muons",
            Code = "( mcMatch(  '[ B_c+ => (B_s0 => (J/psi(1S) => ^mu+ ^mu-) (phi(1020) => K+ K- ) ) pi+]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsPi_Bs2JpsiPhi_kaons = FilterDesktop("mcmatch_bc2bspi_jspiphi_kaons",
            Code = "( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => ^K+ ^K- ) ) pi+]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        
        self.matchBc2BsPi_Bs2JpsiPhi_pi_from_bc = FilterDesktop("mcmatch_bc2bspi_jpsiphi_pi_from_bc",
            Code = "( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^pi+]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsPi_Bs2Dspi_kaons = FilterDesktop("mcmatch_bc2bspi_dspi_kaons",
            Code = "( mcMatch('[B_c+ => (B_s0 => (D_s-  ==> ^K- ^K+ pi- ) pi+  ) pi+]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsPi_Bs2Dspi_pion = FilterDesktop("mcmatch_bc2bspi_dspi_pion",
            Code = "( mcMatch('[B_c+ => (B_s0 => ( D_s- ==> K- K+ ^pi- ) pi+ ) pi+]CC') )",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        self.matchBc2BsPi_Bs2Dspi_bach_pi = FilterDesktop("mcmatch_bc2bspi_dspi_bachpi",
            Code = "( mcMatch('[B_c+ => (B_s0 => ( D_s-  ==> K- K+ pi- ) ^pi+ ) pi+]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsPi_Bs2Dspi_bach_pi = FilterDesktop("mcmatch_bc2bspi_dspi_bachpi",
            Code = "( mcMatch('[B_c+ => (B_s0 => (D_s- ==> K- K+ pi- ) ^pi+ ) pi+]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])

        self.matchBc2BsPi_Bs2Dspi_pi_from_bc = FilterDesktop("mcmatch_bc2bspi_dspi_pi_from_bc",
            Code = "( mcMatch('[B_c+ => (B_s0 => (D_s- ==> K- K+ pi- ) pi+ ) ^pi+]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        self.matchBc2BsE_Bs2JpsiPhi_e = FilterDesktop("mcmatch_bc2bse_jpsiphi_e",
            Code = "( mcMatch(  '[B_c+ => (B_s0 => (J/psi(1S) => mu+ mu-) (phi(1020) => K+ K- ) ) ^e+ nu_e]CC'))",
            Preambulo = [ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ])
        ## Pions
        self._pionSel=None
        self._pionFilter()
        self._pionFromBsSel = None
        self._pionFromBsFilter()
        self._bachPionSel = None
        self._bachPionFilter()
        ## Kaons
        self._kaonSel=None
        self._kaonFilter()
        self._kaonSelForPhi = None
        self._kaonFilter_forPhi()

        self._kaonFromBcSel = None
        self._kaonFromBcFilter()
        #muons
        self._muonSel=None
        self._muonFilter()
        self._bachMuSel = None
        self._bachMuFilter()

        #electrons
        self._electronSel = None
        self._electronFilter()
        #ds
        self._Ds2KKpiSel = None
        self._Ds2KKpiMaker()
        #jpsi
        self._Jpsi2mumuSel = None
        self._Jpsi2mumuMaker()
        #phi
        self._Phi2KKSel = None
        self._Phi2KKMaker()
        
        #jpsi/phi
        self._Bs2JpsiPhiSel = None
        self._Bs2JpsiPhiMaker()
        #dspi
        self._Bs2DsPiSel = None
        self._Bs2DsPiMaker()
        #register lines
        for line in self.Bc2BsMu():
            self.registerLine(line)
        for line in self.Bc2BsE():
            self.registerLine(line)
        for line in self.Bc2BsPi():
            self.registerLine(line)
        for line in self.Bc2BsK():
            self.registerLine(line)


    #selections for the muons
    def _NominalMuSelection( self ):
        return "monitor( (TRCHI2DOF < %(MuonTRCHI2)s ) ,'nom_mu_trchi2',LoKi.Monitoring.StatSvc) &"\
               "monitor( (P> %(MuonP)s *MeV) ,'nom_mu_P',LoKi.Monitoring.StatSvc) &"\
               "monitor( (PT> %(MuonPT)s* MeV) , 'nom_mu_PT',LoKi.Monitoring.StatSvc) &"\
               "monitor( (TRGHOSTPROB < %(MuonGHOSTPROB)s) ,'nom_mu_ghostprob',LoKi.Monitoring.StatSvc)&"\
               "monitor( (PIDmu > %(MuonPIDmu)s ) ,'nom_mu_pidmu',LoKi.Monitoring.StatSvc) &"\
               "monitor( (MIPDV(PRIMARY)> %(MuonMINIP)s ), 'nom_mu_ip',LoKi.Monitoring.StatSvc)"

    def _BachMuSelection( self ):
      return "monitor( (TRCHI2DOF < %(BachMuonTRCHI2)s ) ,'bach_mu_trchi2',LoKi.Monitoring.StatSvc)&"\
             "monitor( (P> %(BachMuonP)s *MeV) ,'bach_mu_p',LoKi.Monitoring.StatSvc)&"\
             "monitor( (PT> %(BachMuonPT)s* MeV) ,'bach_mu_pt',LoKi.Monitoring.StatSvc)&"\
             "monitor( (TRGHOSTPROB < %(MuonGHOSTPROB)s) ,'bach_mu_ghostprob',LoKi.Monitoring.StatSvc)&"\
             "monitor( (PIDmu > %(BachMuonPIDmu)s ) ,'bach_mu_pidmu',LoKi.Monitoring.StatSvc)&"\
             "monitor( (MIPDV(PRIMARY)> %(BachMuonMINIP)s) ,'bach_mu_ip',LoKi.Monitoring.StatSvc)"

    def _NominalPiSelection( self ):
        return "monitor( (TRCHI2DOF < %(PionTRCHI2)s ),'nom_pi_trchi2',LoKi.Monitoring.StatSvc)&"\
               "monitor( (P> %(PionP)s *MeV),'nom_pi_p',LoKi.Monitoring.StatSvc )&"\
               "monitor( (PT> %(PionPT)s *MeV),'nom_pi_pt',LoKi.Monitoring.StatSvc )&"\
               "monitor( (TRGHOSTPROB < %(TRGHOSTPROB)s),'nom_pi_ghostprob',LoKi.Monitoring.StatSvc )&"\
               "monitor( (PIDK < %(PionPIDK)s ),'nom_pi_pidk',LoKi.Monitoring.StatSvc )&"\
               "monitor( (MIPDV(PRIMARY)> %(PionMINIP)s ) ,'nom_pi_ip',LoKi.Monitoring.StatSvc ) "

    def _NominalPiFromBsSelection( self ):
        return "monitor( (TRCHI2DOF < %(PionFromBsTRCHI2)s ) ,'pi_from_bs_trchi2',LoKi.Monitoring.StatSvc)&"\
               "monitor( (P> %(PionFromBsP)s *MeV) ,'pi_from_bs_p',LoKi.Monitoring.StatSvc)& "\
               "monitor( (PT> %(PionFromBsPT)s *MeV) ,'pi_from_bs_pt',LoKi.Monitoring.StatSvc)& "\
               "monitor( (TRGHOSTPROB < %(TRGHOSTPROB)s) ,'pi_from_bs_ghostprob',LoKi.Monitoring.StatSvc)& "\
               "monitor( (PIDK < %(PionFromBsPIDK)s ) ,'pi_from_bs_pidk',LoKi.Monitoring.StatSvc)& "\
               "monitor( (MIPDV(PRIMARY)> %(PionFromBsMINIP)s ) ,'pi_from_bs_ip',LoKi.Monitoring.StatSvc)"

    def _NominalKSelection( self ):
        return "monitor( (TRCHI2DOF < %(KaonTRCHI2)s ) ,'nom_k_trchi2',LoKi.Monitoring.StatSvc)& "\
               "monitor( (P> %(KaonP)s *MeV) ,'nom_k_p',LoKi.Monitoring.StatSvc)& "\
               "monitor( (PT> %(KaonPT)s *MeV) ,'nom_k_pt',LoKi.Monitoring.StatSvc)&"\
               "monitor( (TRGHOSTPROB < %(TRGHOSTPROB)s) ,'nom_k_ghostprob',LoKi.Monitoring.StatSvc)&"\
               "monitor( (PIDK > %(KaonPIDK)s ) ,'nom_k_pidk',LoKi.Monitoring.StatSvc)&"\
               "monitor( (MIPDV(PRIMARY)> %(KaonMINIP)s ),'nom_k_ip',LoKi.Monitoring.StatSvc)"

    def _NominalKSelectionForPhi( self ):
        return "monitor( (TRCHI2DOF < %(KaonTRCHI2)s ) ,'k_for_phi_trchi2',LoKi.Monitoring.StatSvc)& "\
               "monitor( (P> %(KaonP_phi)s *MeV)  ,'k_for_phi_p',LoKi.Monitoring.StatSvc)& "\
               "monitor( (PT> %(KaonPT_phi)s *MeV) ,'k_for_phi_pt',LoKi.Monitoring.StatSvc)&"\
               "monitor( (TRGHOSTPROB < %(TRGHOSTPROB)s) ,'k_for_phi_ghostprob',LoKi.Monitoring.StatSvc)&"\
               "monitor( (PIDK > %(KaonPIDK_phi)s )  ,'k_for_phi_pidk',LoKi.Monitoring.StatSvc)&"\
               "monitor( (MIPDV(PRIMARY)> %(KaonMINIP_phi)s ) ,'k_for_phi_ip',LoKi.Monitoring.StatSvc)"

    def _NominalElectronSelection( self ):
        return "monitor( (TRCHI2DOF < %(ElectronTRCHI2)s ) ,'e_trchi2',LoKi.Monitoring.StatSvc) &"\
               "monitor( ( P> %(ElectronP)s * MeV) ,'e_p',LoKi.Monitoring.StatSvc) &"\
               "monitor( ( PT > %(ElectronPT)s * MeV) ,'e_pt',LoKi.Monitoring.StatSvc) &"\
               "monitor( ( TRGHOSTPROB < %(ElectronGHOSTPROB)s ) ,'e_ghostprob',LoKi.Monitoring.StatSvc) &"\
               "monitor( ( PIDe > %(ElectronPIDe)s ) ,'e_pide',LoKi.Monitoring.StatSvc) &"\
               "monitor( (MIPDV(PRIMARY) > %(ElectronMINIP)s ),'e_ip',LoKi.Monitoring.StatSvc) "
    
    def _NominalBachPiSelection( self ):
        return "monitor( (TRCHI2DOF < %(BachPionTRCHI2)s ),'bachpi_trchi2',LoKi.Monitoring.StatSvc) & "\
               "monitor( (P> %(BachPionP)s *MeV) ,'bachpi_p',LoKi.Monitoring.StatSvc) & "\
               "monitor( (PT> %(BachPionPT)s *MeV) ,'bachpi_pt',LoKi.Monitoring.StatSvc) &"\
               "monitor( (TRGHOSTPROB < %(TRGHOSTPROB)s) ,'bachpi_ghostprob',LoKi.Monitoring.StatSvc) &"\
               "monitor( (PIDK < %(BachPionPIDK)s ) ,'bachpi_pidk',LoKi.Monitoring.StatSvc) &"\
               "monitor( (MIPDV(PRIMARY)> %(BachPionMINIP)s ),'bachpi_ip',LoKi.Monitoring.StatSvc) "
    
    def _NominalKfromBcSelection( self ):
      return "monitor( (TRCHI2DOF < %(K_from_bc_TRCHI2)s ),'bachpi_trchi2',LoKi.Monitoring.StatSvc) & "\
             "monitor( (P> %(K_from_bc_P)s *MeV) ,'bachpi_p',LoKi.Monitoring.StatSvc) & "\
             "monitor( (PT> %(K_from_bc_PT)s *MeV) ,'bachpi_pt',LoKi.Monitoring.StatSvc) &"\
             "monitor( (TRGHOSTPROB < %(TRGHOSTPROB)s) ,'bachpi_ghostprob',LoKi.Monitoring.StatSvc) &"\
             "monitor( (PIDK > %(K_from_bc_PIDK)s ) ,'bachpi_pidk',LoKi.Monitoring.StatSvc) &"\
             "monitor( (MIPDV(PRIMARY)> %(K_from_bc_MINIP)s ),'bachpi_ip',LoKi.Monitoring.StatSvc) "
    
    def _kaonFromBcFilter( self ):
      if self._kaonFromBcSel  is not None:
        return self._kaonFromBcSel

      from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
      from PhysSelPython.Wrappers import Selection
      from StandardParticles import StdAllLooseKaons
      _k_from_bc =  FilterDesktop( Code = self._NominalKfromBcSelection() % self._config )
      _k_from_bc_sel = Selection("K_from_bc_for"+self._name,
                                Algorithm = _k_from_bc,
                                RequiredSelections = [StdAllLooseKaons]
                                )
      self._kaonFromBcSel = _k_from_bc_sel
      return _k_from_bc_sel

    def _electronFilter( self ):
        if self._electronSel is not None:
            return self._electronSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseElectrons
        _tm_el = Selection("TM_e_from_jpsiphi"+self._name,Algorithm =  self.matchBc2BsE_Bs2JpsiPhi_e, RequiredSelections=[StdLooseElectrons])
        _el = FilterDesktop( Code = self._NominalElectronSelection() % self._config )
        _electronSel=Selection("Electron_for"+self._name,
                               Algorithm=_el,
                               #RequiredSelections = [StdLooseElectrons]
                               RequiredSelections = [_tm_el]
                               )
        
        self._electronSel=_electronSel
        
        return _electronSel
    
    def _muonFilter( self ):
        if self._muonSel is not None:
            return self._muonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseMuons
        _tm_mu = Selection("TM_jpsi_muons_jpsi_for"+self._name, Algorithm=self.matchBc2BsPi_Bs2JpsiPhi_muons,RequiredSelections=[StdAllLooseMuons])
        _mu = FilterDesktop( Code = self._NominalMuSelection()%self._config )
        _muSel=Selection("Mu_for"+self._name,
                         Algorithm=_mu,
                         #RequiredSelections = [StdLooseMuons]
                         RequiredSelections =[_tm_mu]
                         )
        
        self._muonSel=_muSel

        return _muSel

    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions
        _tm_pi = Selection("TM_pi_from_ds_for"+self._name,Algorithm = self.matchBc2BsPi_Bs2Dspi_pion, RequiredSelections = [StdAllLoosePions])
        _pia = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _piaSel=Selection("Pi_for"+self._name,
                          Algorithm=_pia,
                          #RequiredSelections = [StdLoosePions]
                          RequiredSelections = [_tm_pi]
                          )
        self._pionSel=_piaSel
        return _piaSel

    def _pionFromBsFilter( self ):
        if self._pionFromBsSel is not None:
            return self._pionFromBsSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions
        #_tm_pib = Selection("TM_pi_fromBs_for_ds_for"+self._name,Algorithm = self.matchBc2BsMu_Bs2Dspi_bach_pi, RequiredSelections = [StdAllLoosePions])
        _tm_pib = Selection("TM_pi_fromBs_for_ds_for"+self._name,Algorithm = self.matchBc2BsPi_Bs2Dspi_bach_pi, RequiredSelections = [StdAllLoosePions])
        _pib = FilterDesktop( Code = self._NominalPiFromBsSelection() % self._config )
        _pibSel=Selection("PiFromBs_for"+self._name,
                          Algorithm=_pib,
                          #RequiredSelections = [StdLoosePions]
                          RequiredSelections = [_tm_pib]
                          )
        self._pionFromBsSel=_pibSel
        return _pibSel

    def _kaonFilter( self ):
        if self._kaonSel is not None:
            return self._kaonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons, StdAllNoPIDsKaons
        _tm_ka = Selection("TM_k_from_Ds_for"+self._name,Algorithm = self.matchBc2BsPi_Bs2Dspi_kaons, RequiredSelections = [StdAllNoPIDsKaons])
        _ka = FilterDesktop( Code = self._NominalKSelection() % self._config )
        _kaSel=Selection("K_for"+self._name,
                         Algorithm=_ka,
                         #RequiredSelections = [StdLooseKaons]
                         RequiredSelections = [_tm_ka]
                         )
        self._kaonSel=_kaSel
        return _kaSel
    
    def _kaonFilter_forPhi( self ):
        if self._kaonSelForPhi is not None:
            return self._kaonSelForPhi
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseKaons

        _tm_kap = Selection("TM_k_from_phi_for"+self._name,Algorithm = self.matchBc2BsPi_Bs2JpsiPhi_kaons, RequiredSelections = [StdAllLooseKaons])

        _kap = FilterDesktop(Code = self._NominalKSelectionForPhi() % self._config )
        _kapSel=Selection("K_for_phi_for"+self._name,
                         Algorithm=_kap,
                         #RequiredSelections = [StdAllLooseKaons]
                         RequiredSelections = [_tm_kap]
                         )
        self._kaonSelForPhi=_kapSel
        return _kapSel

    def _bachPionFilter( self ):
        if self._bachPionSel is not None:
            return self._bachPionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions
        #_tm_bachpi = Selection('tm_bachpi_from_ps_jpsiphi'+self._name,Algorithm = self.matchBc2BsPi_Bs2JpsiPhi_pi_from_bc,RequiredSelections = [StdAllLoosePions])
        _tm_bachpi = Selection('tm_bachpi_from_ps_dspi'+self._name, Algorithm = self.matchBc2BsPi_Bs2Dspi_pi_from_bc,RequiredSelections = [StdAllLoosePions])
        _bachpi = FilterDesktop( Code = self._NominalBachPiSelection() % self._config )
        _bachpiSel=Selection("pis_for"+self._name,
                         Algorithm=_bachpi,
                         #RequiredSelections = [StdAllLoosePions]
                         RequiredSelections = [_tm_bachpi]
                         )
        self._bachPionSel=_bachpiSel
        return _bachpiSel

    def _bachMuFilter( self ):
        if self._bachMuSel is not None:
            return self._bachMuSel
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseMuons
        _tm_bachmu = Selection("TM_bach_mu_for"+self._name,Algorithm = self.matchBc2BsMu_Bs2Dspi_bach_mu, RequiredSelections = [StdAllLooseMuons])
        #_tm_bachmu = Selection("TM_bach_mu_for"+self._name,Algorithm = self.matchBc2BsMu_Bs2JpsiPhi_bach_mu, RequiredSelections = [StdAllLooseMuons])
        _bachmu = FilterDesktop( Code = self._BachMuSelection() % self.config )
        _bachmusel = Selection("bach_mu_for"+self._name,
                               Algorithm = _bachmu,
                               #RequiredSelections = [StdAllLooseMuons]
                               RequiredSelections = [_tm_bachmu]
                               )
        self._bachMuSel = _bachmusel
        return _bachmusel
    
    def _Bc2BsXMothCuts( self, points_to_PV=False ):
      the_string = "monitor( ( VFASPF(VCHI2/VDOF)< %(Bc_CHI2DOF)s ) ,'bc_2bsx_vchi2',LoKi.Monitoring.StatSvc)&"\
             "monitor( ( PT > %(BcPT)s * MeV ) ,'bc_2bsx_pt',LoKi.Monitoring.StatSvc)&"\
             "monitor( ( P  > %(BcP)s * MeV ) ,'bc_2bsx_p',LoKi.Monitoring.StatSvc)&"\
             "monitor( (BPVVDZ > %(BcPVVDZcut)s),'bc2bsx_pvdz',LoKi.Monitoring.StatSvc)"
      if True==points_to_PV:
          the_string += " & monitor( ( BPVDIRA > %(BcDIRA_Tight)s ) ,'bc_2bsx_dira_tight',LoKi.Monitoring.StatSvc) "
      else:
          the_string += " & monitor( ( BPVDIRA > %(BcDIRA_Loose)s ) ,'bc_2bsx_dira_loose',LoKi.Monitoring.StatSvc) "
      return the_string

    #### composites
    def _Ds2KKpiMaker( self ):
        if self._Ds2KKpiSel is not None:
            return self._Ds2KKpiSel
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseKaons

        _Ds2KKpi = CombineParticles(
            DecayDescriptors = ["[D_s+ -> K+ K- pi+]cc"],
            DaughtersCuts = {"K+":self._NominalKSelection() % self._config,
                             "pi+":self._NominalPiSelection() % self._config },
            CombinationCut =  "monitor( ( ADAMASS('D_s+') < %(DsMassWindow)s * MeV) , 'ds_combo_masswindow',LoKi.Monitoring.StatSvc)" % self._config,
            MotherCut = "monitor( (VFASPF(VCHI2/VDOF)< %(Ds_CHI2DOF)s ) , 'ds_mocut_vchi2ndf',LoKi.Monitoring.StatSvc)&"\
                        "monitor( (BPVVDCHI2 >%(Ds_FDCHI2HIGH)s) , 'ds_fd_chi2',LoKi.Monitoring.StatSvc)&"\
                        "monitor( (BPVDIRA > %(Ds_DIRA)s) , 'ds_dira',LoKi.Monitoring.StatSvc)&"\
                        "monitor( (BPVVDZ > %(Ds_BPVVDZcut)s), 'ds_bpvvdz',LoKi.Monitoring.StatSvc)" % self._config)
        _Ds2KKpiSel = Selection("Ds2KKpi_for"+self._name,
                                Algorithm = _Ds2KKpi,
                                RequiredSelections = [self._pionFilter(), self._kaonFilter()])
        self._Ds2KKpiSel = _Ds2KKpiSel
        return _Ds2KKpiSel

    def _Phi2KKMaker( self ):
        if self._Phi2KKSel is not None:
            return self._Phi2KKSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseKaons

        _Phi2KK = CombineParticles(
            DecayDescriptors = ["phi(1020) -> K+ K-"],
            DaughtersCuts = {"K+":self._NominalKSelection() % self._config,
                             "K-":self._NominalKSelection() % self._config},
            CombinationCut =  "monitor( ( ADAMASS('phi(1020)') < %(PhiMassWindow)s * MeV) , 'phi_mass_window',LoKi.Monitoring.StatSvc)" % self._config,
            MotherCut = "monitor( (VFASPF(VCHI2/VDOF)< %(Phi_CHI2DOF)s ) , 'phi_moth_vtx_chi2',LoKi.Monitoring.StatSvc) &"\
                        "monitor( (PT > %(PhiPT)s * MeV) , 'phi_pt',LoKi.Monitoring.StatSvc) &"\
                        "monitor( (BPVDIRA > %(PhiDIRA)s), 'phi_dira',LoKi.Monitoring.StatSvc) "%self._config)
        _Phi2KKSel = Selection("Phi2KK_for"+self._name,
                                Algorithm = _Phi2KK,
                                RequiredSelections = [ self._kaonFilter_forPhi()])
        self._Phi2KKSel = _Phi2KKSel
        return _Phi2KKSel

    def _Jpsi2mumuMaker( self ):
        if self._Jpsi2mumuSel is not None:
            return self._Jpsi2mumuSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseMuons

        _Jpsi2mumu = CombineParticles(
            DecayDescriptors = ["J/psi(1S) -> mu+ mu-"],
            DaughtersCuts = {"mu+":'ALL',#self._NominalMuSelection() % self._config,
                             "mu-":'ALL',#self._NominalMuSelection() % self._config
                             },
            CombinationCut =  "monitor( ( ADAMASS('J/psi(1S)') < %(JpsiMassWindow)s * MeV) , 'jpsi_mass_window',LoKi.Monitoring.StatSvc)" % self._config,
            MotherCut = "monitor( (VFASPF(VCHI2/VDOF)< %(Jpsi_CHI2DOF)s ) ,'jpsi_vchi2',LoKi.Monitoring.StatSvc)&"\
                        "monitor( (PT > %(JpsiPT)s ) ,'jpsi_pt',LoKi.Monitoring.StatSvc)&"\
                        "monitor( (BPVDIRA > %(Jpsi_DIRA)s) ,'jpsi_dira',LoKi.Monitoring.StatSvc)&"\
                        "monitor( (BPVVDZ > %(Jpsi_BPVVDZcut)s),'jpsi_bpvvdz',LoKi.Monitoring.StatSvc)" % self._config)
        _Jpsi2mumuSel = Selection("Jpsi2mumu_for"+self._name,
                                Algorithm = _Jpsi2mumu,
                                RequiredSelections = [self._muonFilter()])
        self._Jpsi2mumuSel = _Jpsi2mumuSel
        return _Jpsi2mumuSel

    def _Bs2JpsiPhiMaker( self ):
        if self._Bs2JpsiPhiSel is not None:
            return self._Bs2JpsiPhiSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection


        _Bs2JpsiPhi = CombineParticles(
            DecayDescriptors = ["[ B_s0 -> J/psi(1S)  phi(1020) ]cc"],
            DaughtersCuts = {'J/psi(1S)':"ALL",'phi(1020)':"ALL"},
            CombinationCut =  "monitor( ( AM < %(BsMassWindowHigh)s * MeV) & ( AM > %(BsMassWindowLow)s ) ,'bs2jpsiphi_mass_window',LoKi.Monitoring.StatSvc)" % self._config,
            MotherCut = "monitor( (VFASPF(VCHI2/VDOF)< %(Bs_JpsiPhi_CHI2DOF)s ) , 'bs_jpsiphi_vchi2',LoKi.Monitoring.StatSvc) &"\
                        "monitor( (BPVVDCHI2 >%(BsFDCHI2HIGH)s) , 'bs_jpsiphi_fdchi2',LoKi.Monitoring.StatSvc) &"\
                        "monitor( (BPVDIRA > %(BsDIRA)s) , 'bs_jpsiphi_dira',LoKi.Monitoring.StatSvc) &"\
                        "monitor( (BPVVDZ > %(BsPVVDZcut)s), 'bs_psiphi_pvvdchi2',LoKi.Monitoring.StatSvc) " % self._config)
        _Bs2JpsiPhiSel = Selection("Bs2JpsiPhi_for"+self._name,
                                Algorithm = _Bs2JpsiPhi,
                                RequiredSelections = [ self._Jpsi2mumuMaker(), self._Phi2KKMaker() ])
        self._Bs2JpsiPhiSel = _Bs2JpsiPhiSel
        return _Bs2JpsiPhiSel

    def _Bs2DsPiMaker( self ):
        if self._Bs2DsPiSel is not None:
            return self._Bs2DsPiSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _Bs2DsPi = CombineParticles(
            DecayDescriptors = ["[ B_s0 -> D_s- pi+ ]cc", "[B_s0 -> D_s+ pi+]cc"],
            DaughtersCuts = {'D_s-':"ALL",'pi+':"ALL"},
            CombinationCut =  "monitor( ( AM < %(BsMassWindowHigh)s * MeV) & ( AM > %(BsMassWindowLow)s ), 'bs_dspi_mass_window',LoKi.Monitoring.StatSvc)" % self._config,
            MotherCut = "monitor( (VFASPF(VCHI2/VDOF)< %(Bs_DsPi_CHI2DOF)s )  , 'bs_dspi_vchi2', LoKi.Monitoring.StatSvc) &"\
                        "monitor( (BPVVDCHI2 >%(BsFDCHI2HIGH)s)  , 'bs_dspi_fdchi2', LoKi.Monitoring.StatSvc) &"\
                        "monitor( (BPVDIRA > %(BsDIRA)s)  , 'bs_dspi_dira', LoKi.Monitoring.StatSvc) &"\
                        "monitor( (BPVVDZ > %(BsPVVDZcut)s) , 'bs_dspi_pvvdz', LoKi.Monitoring.StatSvc) " % self._config)
        _Bs2DsPiSel = Selection("Bs2DsPi_for"+self._name,
                                Algorithm = _Bs2DsPi,
                                RequiredSelections = [self._Ds2KKpiMaker(), self._pionFromBsFilter() ])
        self._Bs2DsPiSel = _Bs2DsPiSel
        return _Bs2DsPiSel



    # finally Bc
    def Bc2BsXmaker( self, _name, _descriptors,_mothCuts, _bachPart, _BsSel ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _cparts = CombineParticles(
            DecayDescriptors = _descriptors,
            MotherCut = _mothCuts
            )
        _sel = Selection(_name,
                         Algorithm = _cparts,
                         RequiredSelections = [_bachPart,_BsSel])
        return _sel
        

    def Bc2BsMu(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsMu_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsMuNuJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 mu+]cc', '[B_c+ -> B_s~0 mu+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                   self._bachMuFilter(),
                                                   self._Bs2JpsiPhiMaker())]),
                 StrippingLine(self._name+"Bc2BsMu_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsMuNuDsPiSel",
                                                        ['[B_c+ -> B_s0 mu+]cc','[B_c+ -> B_s~0 mu+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                        self._bachMuFilter(),
                                                        self._Bs2DsPiMaker())])
                 ]
    
                                                        
                                                        
    def Bc2BsE(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsE_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsENuJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 e+]cc', '[B_c+ -> B_s~0 e+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                   self._electronFilter(),
                                                   self._Bs2JpsiPhiMaker())]),
                 StrippingLine(self._name+"Bc2BsE_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsENuDsPiSel",
                                                        ['[B_c+ -> B_s0 e+]cc','[B_c+ -> B_s~0 e+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                        self._electronFilter(),
                                                        self._Bs2DsPiMaker())])
                 ]
    def Bc2BsPi(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsPi_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsPiJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 pi+]cc', '[B_c+ -> B_s~0 pi+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                   self._bachPionFilter(),
                                                   self._Bs2JpsiPhiMaker())]),
                 StrippingLine(self._name+"Bc2BsPi_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsPiDsPiSel",
                                                        ['[B_c+ -> B_s0 pi+]cc','[B_c+ -> B_s~0 pi+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                        self._bachPionFilter(),
                                                        self._Bs2DsPiMaker())])
                 ]
    def Bc2BsK(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsK_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsKJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 K+]cc', '[B_c+ -> B_s~0 K+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                   self._kaonFromBcFilter(),
                                                   self._Bs2JpsiPhiMaker())]),
                 StrippingLine(self._name+"Bc2BsK_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsKDsPiSel",
                                                        ['[B_c+ -> B_s0 K+]cc','[B_c+ -> B_s~0 K+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                        self._kaonFromBcFilter(),
                                                        self._Bs2DsPiMaker())])
                 ]
