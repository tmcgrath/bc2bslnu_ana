"""
Stripping filter options for Bc->BsMuNu analysis (Bs->DsPi 2016)
@author Tamaki Holly McGrath
@date 2023-02-07
"""

## Set up

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Stripping version
stripping='stripping28r2p1' # for 2016

myLines = ["StrippingBc2BsBc2BsPi_Dspi",
           "StrippingBc2BsBc2BsMu_Dspi"]
#myLines = ["Bc2BsBc2BsMu_Dspi",
#           "Bc2BsBc2BsPi_Dspi"]

# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

#streams = buildStreams(stripping = config, archive = archive)

# Remove prevous stripping
from Configurables import EventNodeKiller, ProcStatusCheck
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

# Build a stream that contains the desired line
from StrippingSelections.StrippingSL.StrippingBc2Bs import Bc2BsBuilder
from StrippingSelections.StrippingSL.StrippingBc2Bs import default_config as configs

# "Remove" PID from stripping line
configs['CONFIG']["KaonPIDK"] = -99999.
configs['CONFIG']["KaonPIDK_phi"] = -99999.
configs['CONFIG']["K_from_bc_PIDK"] = -99999.
configs['CONFIG']["K_from_bs_PIDK"] = -99999.
configs['CONFIG']["PionPIDK"] = 99999.
configs['CONFIG']["BachPionPIDK"] = 99999.
configs['CONFIG']["PionFromBsPIDK"] = 99999.
configs['CONFIG']["MuonPIDmu"] = -99999.
configs['CONFIG']["BachMuonPIDmu"] = -99999.
configs['CONFIG']["ElectronPIDe"] = -99999.

confBc2Bs = Bc2BsBuilder(name = "Bc2Bs", config = configs['CONFIG'])
MyStream = StrippingStream("Bc2BsX_DsPi.Strip")

linesToAdd = []
#for stream in streams:
#    if 'Leptonic' in stream.name():
print("ADDING LINES")
for line in confBc2Bs.lines():
    print(line.name())
    if line.name() in myLines:
        print("Appending line {0}".format(line.name()))
        line._prescale = 1.0
        linesToAdd.append(line)

MyStream.appendLines(linesToAdd)

# Merge into one stream and run in flag mode
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    TESPrefix = 'Strip'
                    )

MyStream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out


## Configuration of SelDSTWriter
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }
            
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '000000',
                          SelectionSequences = sc.activeStreams()
                          )

## Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports',TCK=0x44b42821)


## DaVinci config
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos_2016_DsPi.root"
#DaVinci().TupleFile = "DVTuple_2016_DsPi.root"
DaVinci().appendToMainSequence( [ event_node_killer ] )
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing Table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
