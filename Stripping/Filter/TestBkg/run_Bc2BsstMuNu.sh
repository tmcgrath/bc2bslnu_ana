#!/bin/bash

lb-run DaVinci/v42r11p2 gaudirun.py ../../../DaVinciOptions/BkgOptions/Test_Bc2BsstMuNu_DsPi.py ../Filter_Bc2BsX_DsPi_2017MC.py 2>&1 | tee BsstMuNu_DsPi.log
lb-run DaVinci/v42r11p2 gaudirun.py ../../../DaVinciOptions/BkgOptions/Test_Bc2BsstMuNu_JpsiPhi.py ../Filter_Bc2BsX_JpsiPhi_2017MC.py 2>&1 | tee BsstMuNu_JpsiPhi.log
