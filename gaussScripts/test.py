#
# test.py
#
# Python script to calculate basic distributions of the Bc->Bsmunu test ntuples produced by gauss
#
# Tamaki Holly McGrath
# 21.04.20
#

import sys, os, glob
import numpy as np
import ROOT as R

RDF = R.ROOT.RDataFrame

############################# Strings ########################################

## Common four vectors
f_fv_Kpl = "ROOT::Math::PxPyPzEVector P_Kpl(Kplus_TRUEP_X/1000, Kplus_TRUEP_Y/1000, Kplus_TRUEP_Z/1000, Kplus_TRUEP_E/1000); return P_Kpl;"
f_fv_Kmi = "ROOT::Math::PxPyPzEVector P_Kmi(Kminus_TRUEP_X/1000, Kminus_TRUEP_Y/1000, Kminus_TRUEP_Z/1000, Kminus_TRUEP_E/1000); return P_Kmi;"

## Bc->(Bs->(Ds-->K+ K- pi-) pi+) mu nu
# Four vectors
f_fv_pimi = "ROOT::Math::PxPyPzEVector P_pimi(piminus_TRUEP_X/1000, piminus_TRUEP_Y/1000, piminus_TRUEP_Z/1000, piminus_TRUEP_E/1000); return P_pimi;"
f_fv_pipl = "ROOT::Math::PxPyPzEVector P_pipl(piplus_TRUEP_X/1000, piplus_TRUEP_Y/1000, piplus_TRUEP_Z/1000, piplus_TRUEP_E/1000); return P_pipl;"
f_fv_Ds = "ROOT::Math::PxPyPzEVector P_Ds = P_Kpl + P_Kmi + P_pimi; return P_Ds;"
f_fv_Bs1 = "ROOT::Math::PxPyPzEVector P_Bs = P_Ds + P_pipl; return P_Bs;"
# Mass calculations
f_M_Ds = "return P_Ds.M();"

# dalitz
f_fv_kk = "ROOT::Math::PxPyPzEVector P_KK = P_Kpl + P_Kmi; return P_KK.M2();"
f_fv_kpi = "ROOT::Math::PxPyPzEVector P_Kpi = P_Kpl + P_pimi; return P_Kpi.M2();"

## Bc->(Bs->(Jpsi-> mu+ mu-) (phi-> K+ K-)) mu nu
# Four vectors
f_fv_mupl = "ROOT::Math::PxPyPzEVector P_mupl(muplus_TRUEP_X/1000, muplus_TRUEP_Y/1000, muplus_TRUEP_Z/1000, muplus_TRUEP_E/1000); return P_mupl;"
f_fv_mumi = "ROOT::Math::PxPyPzEVector P_mumi(muminus_TRUEP_X/1000, muminus_TRUEP_Y/1000, muminus_TRUEP_Z/1000, muminus_TRUEP_E/1000); return P_mumi;"
f_fv_jpsi = "ROOT::Math::PxPyPzEVector P_Jpsi = P_mupl + P_mumi; return P_Jpsi;"
f_fv_phi = "ROOT::Math::PxPyPzEVector P_phi = P_Kpl + P_Kmi; return P_phi;"
f_fv_Bs2 = "ROOT::Math::PxPyPzEVector P_Bs = P_Jpsi + P_phi; return P_Bs;" 
# Mass Calculations
f_M_jpsi = "return P_Jpsi.M();"
f_M_phi = "return P_phi.M();"

## Both
f_fv_l1 = "ROOT::Math::PxPyPzEVector P_l(muplus_TRUEP_X/1000, muplus_TRUEP_Y/1000, muplus_TRUEP_Z/1000, muplus_TRUEP_E/1000); return P_l;"
f_fv_l2 = "ROOT::Math::PxPyPzEVector P_l(muplus0_TRUEP_X/1000, muplus0_TRUEP_Y/1000, muplus0_TRUEP_Z/1000, muplus0_TRUEP_E/1000); return P_l;"
f_fv_num = "ROOT::Math::PxPyPzEVector P_nu(nu_mu_TRUEP_X/1000, nu_mu_TRUEP_Y/1000, nu_mu_TRUEP_Z/1000, nu_mu_TRUEP_E/1000); return P_nu;"
f_fv_Bc = "ROOT::Math::PxPyPzEVector P_Bc = P_Bs + P_l + P_nu; return P_Bc;"
f_q2 = "ROOT::Math::PxPyPzEVector q = P_l + P_nu; return q.mag2();"
f_M_Bs = "return P_Bs.M();"
f_M_Bc = "return P_Bc.M();"

############################ Functions #######################################

def defineHist(rdf, variable, nBins, xmin, xmax):
    return rdf.Histo1D((variable, variable, nBins, xmin, xmax), variable)

def defineHist2(rdf, title, Xvariable, Yvariable, nBinsX, xmin, xmax, nBinsY, ymin, ymax):
    return rdf.Histo2D((title, title, nBinsX, xmin, xmax, nBinsY, ymin, ymax), Xvariable, Yvariable) 

############################### Main #########################################

if __name__ == "__main__":
    
    # Input args
    if len(sys.argv)<2:
        print("ERROR! Please pass the event number as the first command line argument")
        sys.exit()
    else:
        evtNo = sys.argv[1]
        if "14575001" in evtNo:
            histTitle = "B_{c}^{+} #rightarrow (B_{s}^{0} #rightarrow D_{s}^{-} #pi^{+}) #mu^{+} #nu_{#mu}"
        elif "14545006" in evtNo:
            histTitle = "B_{c}^{+} #rightarrow (B_{s}^{0} #rightarrow J/#psi #phi) #mu^{+} #nu_{#mu}"
        else:
            print("ERROR! An ntuple file with the event number you provided not found")
            sys.exit()

    # Strings used
    indir = "/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/ntuple/"
    infile = f"{indir}DVntuple_{evtNo}.root"
    treename = "MCDecayTreeTuple/MCDecayTree"
    outdir = "/afs/cern.ch/work/t/tmcgrath/private/B_c/ntuple/test/"

    # RDF object
    bcRDF = RDF(treename, infile)
    if "14575001" in evtNo:
        fsRDF = bcRDF.Define("P_l", f_fv_l1).Define("P_nu", f_fv_num).Define("P_Kpl", f_fv_Kpl).Define("P_Kmi", f_fv_Kmi).Define("P_pimi", f_fv_pimi).Define("P_pipl", f_fv_pipl)
        fvRDF = fsRDF.Define("P_Ds", f_fv_Ds).Define("P_Bs", f_fv_Bs1).Define("P_Bc", f_fv_Bc).Define("M2_KK", f_fv_kk).Define("M2_Kpi", f_fv_kpi)
        q2RDF = fvRDF.Define("q2true", f_q2).Define("Ds_mass", f_M_Ds).Define("Bs_mass", f_M_Bs).Define("Bc_mass", f_M_Bc)
        muversion = "muplus"
    else:
        fsRDF = bcRDF.Define("P_l", f_fv_l2).Define("P_nu", f_fv_num).Define("P_Kpl", f_fv_Kpl).Define("P_Kmi", f_fv_Kmi).Define("P_mumi", f_fv_mumi).Define("P_mupl", f_fv_mupl)
        fvRDF = fsRDF.Define("P_Jpsi", f_fv_jpsi).Define("P_phi", f_fv_phi).Define("P_Bs", f_fv_Bs2).Define("P_Bc", f_fv_Bc)
        q2RDF = fvRDF.Define("q2true", f_q2).Define("Jpsi_mass", f_M_jpsi).Define("phi_mass", f_M_phi).Define("Bs_mass", f_M_Bs).Define("Bc_mass", f_M_Bc)
        muversion = "muplus0"    

    # q^2 histogram
    c1 = R.TCanvas()
    c1.cd()
    q2Hist = defineHist(q2RDF, "q2true", 20, 0, 1)
    q2Hist.SetTitle(f"{histTitle} q^2 Distribution")
    q2Hist.GetYaxis().SetTitle("Entries")
    q2Hist.GetXaxis().SetTitle("q^{2} (GeV^{2})")
    q2Hist.Draw()
    #c1.SaveAs(f"{outdir}q2dist_{evtNo}.png")
    #c1.SaveAs(f"{outdir}q2dist_{evtNo}.pdf")
    
    # Oscillation histogram
    c2 = R.TCanvas()
    c2.cd()
    oscilHist = defineHist(q2RDF, "B_s0_OSCIL", 2, 0, 2)
    oscilHist.SetTitle("Did B_{s}^{0} oscillate in " + histTitle + "???")
    oscilHist.GetYaxis().SetTitle("Entries")
    oscilHist.GetXaxis().SetTitle("No/Yes")
    oscilHist.GetYaxis().SetRangeUser(0,600)
    oscilHist.Draw() 
    #c2.SaveAs(f"{outdir}B_s0_oscil_{evtNo}.png")
    #c2.SaveAs(f"{outdir}B_s0_oscil_{evtNo}.pdf")

    # Mass histograms
    if "14575001" in evtNo:
        c3 = R.TCanvas("Mass measurements", "Mass measurements", 1700, 500)
        c3.Divide(3)
        m = 1
        DsHist = defineHist(q2RDF, "Ds_mass", 40, 1.9682, 1.9684)
        DsHist.SetTitle(histTitle + " - D_{s} Mass")
        DsHist.GetYaxis().SetTitle("Entries")
        DsHist.GetXaxis().SetTitle("M (GeV/c^{2})")
        c3.cd(1)
        DsHist.Draw()
    else:
        c3 = R.TCanvas("Mass measurements", "Mass measurements", 2700, 500)
        c3.Divide(4)
        m = 2
        JpsiHist = defineHist(q2RDF, "Jpsi_mass", 40, 3.096, 3.098)
        JpsiHist.SetTitle(histTitle + " - J/#psi Mass")
        JpsiHist.GetYaxis().SetTitle("Entries")
        JpsiHist.GetXaxis().SetTitle("M (GeV/c^{2})")
        c3.cd(1)
        JpsiHist.Draw()
        phiHist = defineHist(q2RDF, "phi_mass", 40, 1, 1.1)
        phiHist.SetTitle(histTitle + " - #phi(1020) Mass")
        phiHist.GetYaxis().SetTitle("Entries")
        phiHist.GetXaxis().SetTitle("M (GeV/c^{2})")
        c3.cd(2)
        phiHist.Draw()

    BsHist = defineHist(q2RDF, "Bs_mass", 40, 5.3667, 5.3668)
    BsHist.SetTitle(histTitle + " - B_{s}^{0} Mass")
    BsHist.GetYaxis().SetTitle("Entries")
    BsHist.GetXaxis().SetTitle("M (GeV/c^{2})")
    BcHist = defineHist(q2RDF, "Bc_mass", 40, 6.27595, 6.27605)
    BcHist.SetTitle(histTitle + " - B_{c}^{+} Mass")
    BcHist.GetYaxis().SetTitle("Entries")
    BcHist.GetXaxis().SetTitle("M (GeV/c^{2})")
    
    c3.cd(m+1)
    BsHist.Draw()
    c3.cd(m+2)
    BcHist.Draw()
    #c3.SaveAs(f"{outdir}massDists_zoomed_{evtNo}.png")
    #c3.SaveAs(f"{outdir}massDists_zoomed_{evtNo}.pdf")
    c3.Update()

    # pt Plots
    c4 = R.TCanvas("pT", "pT")

    muPtHist = defineHist(q2RDF, muversion+"_TRUEPT", 70, 0, 7000)
    muPtHist.SetTitle(histTitle + " - Muon p_{T}")
    muPtHist.GetYaxis().SetTitle("Entries")
    muPtHist.GetXaxis().SetTitle("p_{T} (MeV/c)")
    c4.cd()
    muPtHist.Draw()
    c4.Update()
    #c4.SaveAs(f"{outdir}mupt_{evtNo}.png")
    #c4.SaveAs(f"{outdir}mupt_{evtNo}.pdf")

    # dalitz
    c5 = R.TCanvas("dalitz", "dalitz")
    dal_hist = defineHist2(q2RDF, "D_{s} Dalitz", "M2_Kpi", "M2_KK", 50, 0, 3, 50, 0, 3)
    dal_hist.GetYaxis().SetTitle("m^{2}(K^{+}K^{-}) (GeV/c^{2})")
    dal_hist.GetXaxis().SetTitle("m^{2}(K^{+}#pi^{-}) (GeV/c^{2})")
    c5.cd()
    dal_hist.Draw()
    c5.Update()


