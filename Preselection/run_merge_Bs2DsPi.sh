#!/bin/bash

python3 merge_files.py -f MCFilesEOS/Bs2DsPi_MC_sim09g_2017_MU_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bs2DsPi_MC_sim09g_2017_MD_Files.txt -b Mu

#python3 merge_files.py -f MCFilesEOS/Bs2DsPi_MC_sim09g_2018_MU_Files.txt -b Mu
#python3 merge_files.py -f MCFilesEOS/Bs2DsPi_MC_sim09g_2018_MD_Files.txt -b Mu
