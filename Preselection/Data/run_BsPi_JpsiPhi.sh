#!/bin/bash

python3 presel_Bs2JpsiPhi.py -f ../FilesOnGrid/Data/Bc2BsX_JpsiPhi_2016_MD_Files.txt -b Pi -s 2>&1 | tee Data/logs/BsPi_JpsiPhi_2016_MD.log
python3 presel_Bs2JpsiPhi.py -f ../FilesOnGrid/Data/Bc2BsX_JpsiPhi_2016_MU_Files.txt -b Pi -s 2>&1 | tee Data/logs/BsPi_JpsiPhi_2016_MU.log
python3 presel_Bs2JpsiPhi.py -f ../FilesOnGrid/Data/Bc2BsX_JpsiPhi_2017_MD_Files.txt -b Pi -s 2>&1 | tee Data/logs/BsPi_JpsiPhi_2017_MD.log
python3 presel_Bs2JpsiPhi.py -f ../FilesOnGrid/Data/Bc2BsX_JpsiPhi_2017_MU_Files.txt -b Pi -s 2>&1 | tee Data/logs/BsPi_JpsiPhi_2017_MU.log
python3 presel_Bs2JpsiPhi.py -f ../FilesOnGrid/Data/Bc2BsX_JpsiPhi_2018_MD_Files.txt -b Pi -s 2>&1 | tee Data/logs/BsPi_JpsiPhi_2018_MD.log
python3 presel_Bs2JpsiPhi.py -f ../FilesOnGrid/Data/Bc2BsX_JpsiPhi_2018_MU_Files.txt -b Pi -s 2>&1 | tee Data/logs/BsPi_JpsiPhi_2018_MU.log

