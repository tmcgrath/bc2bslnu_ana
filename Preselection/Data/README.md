## Offline Selection for Data

Shell scripts to run the data offline processing on the Manchester machines (I do it on hepgpu5)
The shell scripts need to be run in one directory above this i.e. in `bc2bslnu_ana/Preselection`
