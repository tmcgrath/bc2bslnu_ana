#!/bin/bash

python3 presel_Bs2DsPi.py -f ../FilesOnGrid/Data/Bc2BsX_DsPi_2016_MD_Files.txt -b Mu -s 2>&1 | tee Data/logs/BsMuNu_DsPi_2016_MD.log
python3 presel_Bs2DsPi.py -f ../FilesOnGrid/Data/Bc2BsX_DsPi_2016_MU_Files.txt -b Mu -s 2>&1 | tee Data/logs/BsMuNu_DsPi_2016_MU.log
python3 presel_Bs2DsPi.py -f ../FilesOnGrid/Data/Bc2BsX_DsPi_2017_MD_Files.txt -b Mu -s 2>&1 | tee Data/logs/BsMuNu_DsPi_2017_MD.log
python3 presel_Bs2DsPi.py -f ../FilesOnGrid/Data/Bc2BsX_DsPi_2017_MU_Files.txt -b Mu -s 2>&1 | tee Data/logs/BsMuNu_DsPi_2017_MU.log
python3 presel_Bs2DsPi.py -f ../FilesOnGrid/Data/Bc2BsX_DsPi_2018_MD_Files.txt -b Mu -s 2>&1 | tee Data/logs/BsMuNu_DsPi_2018_MD.log
python3 presel_Bs2DsPi.py -f ../FilesOnGrid/Data/Bc2BsX_DsPi_2018_MU_Files.txt -b Mu -s 2>&1 | tee Data/logs/BsMuNu_DsPi_2018_MU.log

