#!/bin/bash

python3 merge_files.py -f MCFilesEOS/Bc2BsPi_DsPi_MC_sim09k_2016_MU_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_DsPi_MC_sim09k_2016_MD_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_DsPi_MC_sim09k_2017_MU_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_DsPi_MC_sim09k_2017_MD_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_DsPi_MC_sim09k_2018_MU_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_DsPi_MC_sim09k_2018_MD_Files.txt -b Pi

python3 merge_files.py -f MCFilesEOS/Bc2BsPi_JpsiPhi_MC_sim09k_2016_MU_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_JpsiPhi_MC_sim09k_2016_MD_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_JpsiPhi_MC_sim09k_2017_MU_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_JpsiPhi_MC_sim09k_2017_MD_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_JpsiPhi_MC_sim09k_2018_MU_Files.txt -b Pi
python3 merge_files.py -f MCFilesEOS/Bc2BsPi_JpsiPhi_MC_sim09k_2018_MD_Files.txt -b Pi