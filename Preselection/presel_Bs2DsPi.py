#
# presel_Bs2JpsiPhi.py
#

import sys, os, glob
import ROOT as R
import argparse
import numpy as np

# for debugging
#verbosity = R.Experimental.RLogScopedVerbosity(R.Detail.RDF.RDFLogChannel(), R.Experimental.ELogLevel.kInfo)

###### Parse arguments ######

parser=argparse.ArgumentParser()
parser.add_argument('--file','-f',dest='file',required=True,type=str,help='Text file containing all root file names')
parser.add_argument('--magpol','-mp', dest='mp', required=False, type=str, choices=['MU','MD'], help='Magnet polarity of files you want to reduce (only required when specifying directory where root files located)')
parser.add_argument('--mc', '-m', dest='mc', action="store_true", help='Flag to specify whether file is MC or not')
parser.add_argument('--bachelor','-b',dest='bachelor',required=True,type=str,choices=['Mu','Pi'],help='name of bachelor particle (Mu or Pi)')
parser.add_argument('--truth_match', '-tm', dest='TM', type=str, help="Bc decay of sample (for truth-matching)")
parser.add_argument('--split', '-sp', dest='split', type=int, default=0, choices=[0, 1, 2, 3, 4, 5], help='Split the files into 5 to only select on a portion, the number specifes which portion (default=0 to select on all files)')
parser.add_argument('--save_file', '-s', dest='save_file',action="store_true", help="Flag whether to save the output file")
parser.add_argument('--prompt', '-p', dest='prompt', action="store_true", help="Flag to indicate if its a prompt Bs sample (for correct truth matching application)")

parsed = vars(parser.parse_args())
bach = parsed['bachelor']

treename = f"Bc2Bs{bach}_Tuple/DecayTree"

MC = parsed['mc']
if MC:
    outFolder = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/MCReduced/"
    treename = "DecayTree"
    tm_string = f"Bc_is_from_{parsed['TM']}"
    if parsed['TM'] == "Bc_BsKstar":
        tm_string = f"Bc_is_from_{parsed['TM']}_Ks2pippim == 1 | Bc_is_from_{parsed['TM']}_Ks2pizpiz"
else:
    outFolder = "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/DataReduced/"
no_Bc_TM = parsed['prompt']

# if looking at Bc->BsPi misID check only that 
BsPi_misID = False
if bach == "Mu" and parsed['TM'] == "Bc_BsPi":
    print(f"Bachelor set for {bach} for Bc2BsPi -> misID sample")
    BsPi_misID = True
    #no_Bc_TM = True
magpol = parsed['mp']

if parsed['split'] == 0: split = False
else: split = True
portion = parsed['split']

###### Extra function ######

def splitFiles(file_list: list, section: int):
    cv_array = np.array(file_list)
    index = section-1
    cv_array_section = np.array_split(cv_array, 5)[index]
    output_list = cv_array_section.tolist()

    return output_list

###### RDF function ######

# Trigger cuts
hlt2 = "Bc_Hlt2Topo2BodyDecision_TOS == 1 || Bc_Hlt2Topo3BodyDecision_TOS == 1 || Bc_Hlt2Topo4BodyDecision_TOS == 1 || Bc_Hlt2PhiIncPhiDecision_TOS == 1 "

def apply_cuts(files, outputname):

    chain = R.TChain(treename)
    for file in files:
        file = file.strip()
        if MC == True:
            if ".txt" in parsed['file']: file = file.replace(".root", "_merged.root")
            chain.Add(file)
            print(f"Added file: {file}")
        else:
            chain.Add(file)
            print(f"Added file: {file}")
    

    df = R.RDataFrame(chain)

    extra_pi = ""
    if bach == "Pi":
        extra_pi = " && Bach_PT > 350"
    if MC:
        if no_Bc_TM == False:
            df = df.Filter(f"{tm_string} == 1", f"{tm_string} == 1")
        df_for_eff = df.Filter("Bs_is_from_Bs_DsPi == 1")\
        .Filter(hlt2, "HLT2")\
        .Filter("5000 < Bs_MM && Bs_MM < 6000", "5000 < Bs_MM && Bs_MM < 6000")\
        .Filter("Bs_ENDVERTEX_CHI2 / Bs_ENDVERTEX_NDOF < 10", "Bs_ENDVERTEX_CHI2 / Bs_ENDVERTEX_NDOF < 10")\
        .Filter("Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 9", "Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 9")\
        .Filter("Bc_IPCHI2_OWNPV < 25", "Bc_IPCHI2_OWNPV < 25")\
        .Filter("Bc_IPCHI2_OWNPV >= 0", "Bc_IPCHI2_OWNPV >= 0")\
        .Filter(f"Bc_PT > 3000 && Bc_P > 30000 {extra_pi}", "cuts in 2018 + 2016 but not 2017")\
        .Filter("Bs_TAU > 0.000", "Bs_TAU > 0.000")\
        .Filter("Ds_PT > 2000", "Ds_PT > 2000")\
        .Filter("1943.49 < Ds_MM && Ds_MM < 1993.49", "1943.49 < Ds_MM && Ds_MM < 1993.49")\
        .Filter("PionBs_PT > 1700", "PionBs_PT > 1700")
    dfFiltered = df.Filter(hlt2, "HLT2")\
    .Filter("5000 < Bs_MM && Bs_MM < 6000", "5000 < Bs_MM && Bs_MM < 6000")\
    .Filter("Bs_ENDVERTEX_CHI2 / Bs_ENDVERTEX_NDOF < 10", "Bs_ENDVERTEX_CHI2 / Bs_ENDVERTEX_NDOF < 10")\
    .Filter("Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 9", "Bc_ENDVERTEX_CHI2 / Bc_ENDVERTEX_NDOF < 9")\
    .Filter("Bc_IPCHI2_OWNPV < 25", "B_IPCHI2_OWNPV < 25")\
    .Filter(f"Bc_PT > 3000 && Bc_P > 30000 {extra_pi}", "cuts in 2018 + 2016 but not 2017")\
    .Filter("Bs_TAU > 0.000", "Bs_TAU > 0.000")\
    .Filter("Ds_PT > 2000", "Ds_PT > 2000")\
    .Filter("1943.49 < Ds_MM && Ds_MM < 1993.49", "1943.49 < Ds_MM && Ds_MM < 1993.49")\
    .Filter("PionBs_PT > 1700", "PionBs_PT > 1700")

    # Apply PID cuts if MC
    if MC: 
        PIDCuts = "H1Ds_PIDK_corr > 5 && H2Ds_PIDK_corr > 5 && H3Ds_PIDK_corr < -2 && PionBs_PIDK_corr < -2"
        if bach == "Mu":
            PIDCuts += " && Bach_PIDmu_corr > 0 && Bach_isMuon == 1"
            BachCut1 = "Bach_PIDmu_corr > 0 "
            BachCut2 = "Bach_isMuon == 1"
        else:
            PIDCuts += " && Bach_PIDK_corr < 2 && Bach_isMuon == 0"
            BachCut1 = "Bach_PIDK_corr < 2 "
            BachCut2 = "Bach_isMuon == 0"
        df_for_eff = df_for_eff.Filter("H1Ds_PIDK_corr > 5", "H1Ds_PIDK_corr > 5").Filter("H2Ds_PIDK_corr > 5", "H2Ds_PIDK_corr > 5").Filter("H3Ds_PIDK_corr < -2", "H3Ds_PIDK_corr < -2")\
        .Filter("PionBs_PIDK_corr < -2", "PionBs_PIDK_corr < -2")\
        .Filter(BachCut1, BachCut1)\
        .Filter(BachCut2, BachCut2)
        dfFiltered = dfFiltered.Filter(PIDCuts, "PID Cuts")

        # Extra step for Bc->Bspi misID
        if BsPi_misID:
            # make sure bachelor parent is not Bc 
            id_check = 0 # TODO

    # Put branches to save in output in vector
    columns = dfFiltered.GetColumnNames() 
    print(columns)
    branches2Save = R.std.vector("string")()
    for branch in columns:
        if "DecayTree_" not in str(branch):
            #print(branch)
            branches2Save.push_back(branch)

    if parsed["save_file"]:
        print("Saving output files")
        dfFiltered.Snapshot("DecayTree", outputname, branches2Save)

    # print efficiency
    if MC:
        cuts = df_for_eff.Report()
        cuts.Print()


###### Files ######

text_file = parsed["file"]

if split == True:
    suffix = f"_{portion}"
else:
    suffix = ""

# can get files from text file or file path
if ".txt" in text_file:
    print("Getting root files from text file....")
    outName = os.path.basename(text_file).replace("_Files.txt",f"_reduced{suffix}.root")
    with open(text_file, "r") as f:
        files = f.readlines()
else:
    print("Getting root files from file path....")
    if "/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/" not in text_file:
        print(f"File path {text_file} not valid, ABORT!")
        sys.exit()
    elif os.path.exists(text_file) == False:
        print(f"File path {text_file} does not exist, ABORT!")
        sys.exit()
    if magpol == None:
        print("Magnet polarity not specified, ABORT!")
    if text_file[-1] != "/":
        text_file += "/" 
    
    if parsed['prompt'] == True:
        file_beginning = "Bs2DsPi"
    else:
        file_beginning = f"*_Bs2DsPi"

    print(f"Getting files with pattern: {text_file}{file_beginning}_*_{magpol}_*_merged.root")
    files = glob.glob(f"{text_file}{file_beginning}_*_{magpol}_*_merged.root")
    example = os.path.basename(files[0])

    # Need to do something a bit more involved for the text file name
    year = (example.split(f"_{magpol}_")[0]).split("_")[-1]
    characteristic = example.split(f"_{year}_{magpol}_")[0]
    if parsed['prompt'] == False:
        characteristic = characteristic.replace("Bs2DsPi", "DsPi")
    if "/MC" in text_file: file_type = "MC"
    elif "/ReDecay" in text_file: file_type = "ReDecay"
    elif "/Data" in text_file: file_type = "Data"
    grid_file = glob.glob(f"/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/FilesOnGrid/{file_type}/{characteristic}_{file_type}_*_{year}_{magpol}_Files.txt")
    print(grid_file)
    print(f"/afs/cern.ch/work/t/tmcgrath/private/Bc2Bslnu_ana/FilesOnGrid/{file_type}/{characteristic}_{file_type}_*_{year}_{magpol}_Files.txt")
    outName = os.path.basename( grid_file[0] ).replace("_Files.txt",f"_reduced{suffix}.root")
    if BsPi_misID: # change name if Bc2BsPi misID sample
        outName = outName.replace(characteristic, f"{characteristic}_misID")

print(f"Bachelor particle: {bach}")

# If splitting files
if split == True:
    files = splitFiles(files, portion)
    print(f"\nSplit files into 5, taking section {portion}:")
    print(files)

if MC == False:
    outName = outName.replace("Bc2BsX", f"Bc2Bs{bach}")

###### RDF ######

print("Applying cuts to files......")
print(f"Output filename: {outFolder}{outName}")

apply_cuts(files, f"{outFolder}{outName}")

print("Done")

