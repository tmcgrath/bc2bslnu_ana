# Everything to do with Offline Selection
(Except BDT)

## Merging DaVinci and PIDGen Files

**FOR MC ONLY**

The original MC DaVinci outputted MC files are merged with the files containing the "corrected" PID variables with [`merge_files.py`](./merge_files.py). One of the input arguments is a text file containing the  MC file locations on eos. These are in [`MCFilesEOS`](./MCFilesEOS) (which may need to be changed if running yourself) per $B_c^+$ channel, $B_s^0$ channel, year, and magnet polarity. Usage is as follows:
```
python3 merge_files.py -f <text_file> -b <Mu_or_Pi>
```
The second argument should be `Pi` for $B_c^+ \to B_s^0 \pi^+$ files and `Mu` for all others (for accessing the correct decay tree).


## Offline Selection

The offline selection is recommended to be run on own insititutions batch submission system because they require beefy CPUs. However, there are `condor` batch job submission scripts here that should work for lxplus as well (although a bit naughty).

The scripts that actually apply the preselection are [`presel_Bs2DsPi.py`](./presel_Bs2DsPi.py) and [`presel_Bs2JpsiPhi.py`](./presel_Bs2JpsiPhi.py) for both data + MC. The condor submission scripts are all located in [`condor`](./condor/) for each channel, data, and MC types. All the shell scripts that are ran in each condor job are located in this current directory as well.

Can run the offline selection in a terminal for each $B_s^0$ decay channel separately on data with:
```
./run_presel_Bs2DsPi.sh <text_file_of_filenames> <Mu_or_Pi>
./run_presel_Bs2JpsiPhi.sh <text_file_of_filenames> <Mu_or_Pi>
```
and MC with:
```
./run_presel_MC_Bs2DsPi.sh <text_file_of_filenames> <Mu_or_Pi>
./run_presel_MC_Bs2JpsiPhi.sh <text_file_of_filenames> <Mu_or_Pi>
```

