#!/bin/bash

python3 merge_files.py -c Bs2DsPi_loose -ym "2017_MU" -b Mu
python3 merge_files.py -c Bs2DsPi_loose -ym "2017_MD" -b Mu

python3 merge_files.py -c Bs2DsPi_loose -ym "2018_MU" -b Mu
python3 merge_files.py -c Bs2DsPi_loose -ym "2018_MD" -b Mu
