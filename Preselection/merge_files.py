#
# merge_files.py
#
# Script to merge the PIDGen files with their original MC files
#

import sys, os, glob
import ROOT as R
import argparse

###### Parse arguments ######

parser=argparse.ArgumentParser()
parser.add_argument('--file','-f',dest='file',type=str,default="no_file", help='Text file containing all root file names')
parser.add_argument('--bachelor','-b',dest='bachelor',required=True,type=str,choices=['Mu','Pi'],help='name of bachelor particle (Mu or Pi)')
parser.add_argument('--channel','-c', dest='chan', type=str, help="Channel of files to merge")
parser.add_argument('--redecay', '-r', dest='redecay', action="store_true", help="Flag to specify ReDecay")
parser.add_argument('--year_magpol', '-ym', dest='ym', type=str, choices=["2016_MU", "2016_MD", "2017_MU", "2017_MD", "2018_MU", "2018_MD"])

parsed = vars(parser.parse_args())
bach = parsed['bachelor']
treename = f"Bc2Bs{bach}_Tuple/DecayTree"
text_file = parsed["file"]
if parsed["redecay"] ==  True:
    mc_type = "ReDecay"
else:
    mc_type = "MC"
if text_file == "no_file":
    chan = parsed["chan"]
    ym = parsed["ym"]
    year = ym.split("_")[0]
    files = glob.glob(f"/eos/lhcb/user/t/tmcgrath/Bc2Bslnu_ana/{mc_type}/{chan}/{year}/*_{ym}_*.root")
else:
    with open(text_file, "r") as f:
        files = f.readlines()

# remove merged files in list
cleaned_files = [item for item in files if "_merged" not in item]

print("Merging for files:")
print(cleaned_files)
print("\n")

for file in cleaned_files:

    file = file.strip()
    outName = file.replace(".root", "_merged.root")
    print(f"\nOn file {file}")

    #skip if file already has been merged
    if os.path.exists(outName) == True:
        print(f"File {file} already merged - ignored!")
        continue

    #file = file.replace("root://eoslhcb.cern.ch//eos/", "/eos/")
    # for situations where the tree doesn't exist
    try:
        theFile = R.TFile.Open(file)
        theTree = theFile.Get(treename)
    except:
        print(f"Could not find {treename} in {file}, skipping")
        continue
    friendFilename = os.path.basename(file).replace(".root", "_pidgen_applied.root")
    friendFile = os.path.dirname(file)+"/PIDGen/"+friendFilename
    if os.path.exists(friendFile.replace("root://eoslhcb.cern.ch/", "")) == True:
        thefriendFile = R.TFile.Open(friendFile)
    else:
        print(f"Could not find friend file {friendFile}, skipping")
        continue
    thefriendTree = thefriendFile.Get("DecayTree")
    print(f"Merging {file} with {friendFile}")
    theTree.AddFriend(thefriendTree)

    df = R.RDataFrame(theTree)
    columns = df.GetColumnNames() 
    #print(columns)

    branches2Save = R.std.vector("string")()
    for branch in columns:
        branches2Save.push_back(str(branch))

    print(f"Outputting: {outName}")

    df.Snapshot("DecayTree", outName, branches2Save)

    theFile.Close()

