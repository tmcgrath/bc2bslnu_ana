#!/bin/bash

python3 merge_files.py -f MCFilesEOS/Bc2BsKst_DsPi_MC_sim09k_2016_MU_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_DsPi_MC_sim09k_2016_MD_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_DsPi_MC_sim09k_2017_MU_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_DsPi_MC_sim09k_2017_MD_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_DsPi_MC_sim09k_2018_MU_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_DsPi_MC_sim09k_2018_MD_Files.txt -b Mu

python3 merge_files.py -f MCFilesEOS/Bc2BsKst_JpsiPhi_MC_sim09k_2016_MU_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_JpsiPhi_MC_sim09k_2016_MD_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_JpsiPhi_MC_sim09k_2017_MU_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_JpsiPhi_MC_sim09k_2017_MD_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_JpsiPhi_MC_sim09k_2018_MU_Files.txt -b Mu
python3 merge_files.py -f MCFilesEOS/Bc2BsKst_JpsiPhi_MC_sim09k_2018_MD_Files.txt -b Mu